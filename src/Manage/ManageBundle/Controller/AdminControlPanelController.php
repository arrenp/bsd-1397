<?php

namespace Manage\ManageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use classes\classBundle\Entity\members;
use classes\classBundle\Entity\manageUsersAccounts;
use Shared\PromoDocsBundle\Classes\promoDocs;
class AdminControlPanelController extends Controller
{


	public function manageUsersAction()
	{
		$repository = $this->getDoctrine()->getRepository('classesclassBundle:manageUsers');
		$users = $repository->findBy(array());

		return $this->render('ManageManageBundle:AdminControlPanel:manageusers.html.twig',array("users" => $users));
	}

	public function manageUsersAvailableAccountsAction()
	{

		$request = Request::createFromGlobals();
		$request->getPathInfo();
		$manageid = $request->query->get("id","");

		$repository = $this->getDoctrine()->getRepository('classesclassBundle:members');
		$accounts = $repository->findBy(array());


		foreach ($accounts as $account)
		{
			$account->checked = "";
			$repository = $this->getDoctrine()->getRepository('classesclassBundle:manageUsersAccounts');
			$accountfinder= $repository->findOneBy(array("userid" => $account->id,"manageid" => $manageid));

			if ($accountfinder != null)
			$account->checked = "checked";
		}
		return $this->render('ManageManageBundle:AdminControlPanel:manageusersavailableaccounts.html.twig',array("accounts" => $accounts,'manageid' => $manageid));
	}

	public function manageUsersAvailableAccountsSaveAction()
	{

		$request = Request::createFromGlobals();
		$request->getPathInfo();
		$manageid = $request->request->get("id","");
		$em = $this->getDoctrine()->getManager();
		$accountidlist = explode(",",$request->request->get("accountlist",""));

		//delete all old records
		$repository = $this->getDoctrine()->getRepository('classesclassBundle:manageUsersAccounts');
		$currentaccounts= $repository->findBy(array("manageid" => $manageid));

		foreach ($currentaccounts as $account)
		{
			$em->remove($account);
			$em->flush();
		}

		foreach ($accountidlist as $userid)
		{
			$account = new manageUsersAccounts();
			$account->userid = $userid;
			$account->manageid = $manageid;
			$em->persist($account);
			$em->flush();

		}

		return new Response("saved");
	}

	public function manageUsersTabsAction()
	{

		$request = Request::createFromGlobals();
		$request->getPathInfo();
		$manageid = $request->query->get("id","");

		$sectionString = "accounts,portal,reports,emailTemplates,sales";//string to be used to initialize array
		$fullaccessDenyString = "reports,sales";//fields that do not have full access option

		$repository = $this->getDoctrine()->getRepository('classesclassBundle:manageUsers');
		$currentUser = $repository->findOneBy(array("id" => $manageid));

		$sectionNames = explode(",",$sectionString);
		$fullaccessDeny =  explode(",",$fullaccessDenyString);

		$sections = array();
		$i = 0;
		foreach ($sectionNames as $name)
		{
			$sections[$i]->fullaccessEnabled = "";
			foreach ($fullaccessDeny as $denyName)
			if ($denyName == $name)
			$sections[$i]->fullaccessEnabled = "disabled";
			$sections[$i]->name = $name;
			$sections[$i]->value = $currentUser->$name;

			$sections[$i]->noaccessChecked = "";
			$sections[$i]->readaccessChecked = "";
			$sections[$i]->fullaccessChecked = "";
			$sections[$i]->defaultChecked = "";

			if ($sections[$i]->value == 0)
			$sections[$i]->noaccessChecked = "checked";

			if ($sections[$i]->value == 1)
			$sections[$i]->readaccessChecked = "checked";

			if ($sections[$i]->value == 2)
			$sections[$i]->fullaccessChecked = "checked";



			if ($currentUser->defaultTab == $name)
			$sections[$i]->defaultChecked = "checked";

			$i++;
		}


		return $this->render('ManageManageBundle:AdminControlPanel:manageuserstabs.html.twig',array("sections" => $sections,"manageid" => $manageid));

	}

	public function manageUsersTabsSaveAction()
	{
		$request = Request::createFromGlobals();
		$request->getPathInfo();
		$em = $this->getDoctrine()->getManager();
		$manageid = $request->request->get("id","");
		$repository = $this->getDoctrine()->getRepository('classesclassBundle:manageUsers');
		$currentuser = $repository->findOneBy(array("id" => $manageid));

		$elements = $request->request;

		foreach ($elements as $key => $value)
		{
			if ($key != "id")
			{
				$currentuser->$key = $value;
				$em->flush();
			}

		}


		return new Response("saved");
	}


	public function manageUsersAddUserAction()
	{


		return $this->render('ManageManageBundle:AdminControlPanel:manageusersadduser.html.twig');
	}
	public function manageUsersEditUserAction()
	{

		$request = Request::createFromGlobals();
		$request->getPathInfo();
		$manageid = $request->query->get("id","");
		$repository = $this->getDoctrine()->getRepository('classesclassBundle:manageUsers');
		$currentuser = $repository->findOneBy(array("id" => $manageid));

		$currentuser->adminChecked = "";

		if ($currentuser->account == 1)
		$currentuser->adminChecked = "checked";

		return $this->render('ManageManageBundle:AdminControlPanel:manageusersedituser.html.twig',array("currentuser" => $currentuser));
	}

	public function manageUsersDeleteUserAction()
	{

		$request = Request::createFromGlobals();
		$request->getPathInfo();
		$manageid = $request->query->get("id","");
		$repository = $this->getDoctrine()->getRepository('classesclassBundle:manageUsers');
		$currentuser = $repository->findOneBy(array("id" => $manageid));

		return $this->render('ManageManageBundle:AdminControlPanel:manageusersdeleteuser.html.twig',array("currentuser" => $currentuser));
	}

	public function manageUsersDeleteUserSaveAction()
	{
		$request = Request::createFromGlobals();
		$request->getPathInfo();
		$manageid = $request->request->get("id","");
		$em = $this->getDoctrine()->getManager();

		//delete manageAccounts records
		$repository = $this->getDoctrine()->getRepository('classesclassBundle:manageUsersAccounts');
		$userAccounts = $repository->findBy(array("manageid" => $manageid));

		foreach ($userAccounts as $account)
		{
			$em->remove($account);
			$em->flush();
		}

		$repository = $this->getDoctrine()->getRepository('classesclassBundle:manageUsers');
		$currentuser = $repository->findOneBy(array("id" => $manageid));

		$em->remove($currentuser);
		$em->flush();

		return new Response("deleted");
	}


}
?>
