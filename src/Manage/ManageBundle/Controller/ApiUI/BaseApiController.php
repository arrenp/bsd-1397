<?php
namespace Manage\ManageBundle\Controller\ApiUI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
class BaseApiController extends Controller
{   
    public function getButtonAttrs($action)
    {
        $button['description'] = "Save";
        $button['class'] = "primary";
        if ($action == "add")
        {
            $button['description'] = "Add";
            $button['class'] = "info";
        }
        return $button;
    }        
}
