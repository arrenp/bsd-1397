<?php

namespace Manage\ManageBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\General\GeneralMethods;
use classes\classBundle\Entity\CTAAccountList;
class CTAController extends Controller
{
    public function indexAction(Request $request)
    {
        $userid = $request->query->get("userid");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:CTA');
        $ctas = $repository->findAll(); 
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:CTAAccountList');
        foreach ($ctas as &$cta)
        {
            $ctaExists = $repository->findOneBy(array("userid" => $userid,"ctaid" => $cta->id));
            if ($ctaExists != null)
            $cta->checked = "checked";
            else
            $cta->checked = "";
        }
        return $this->render('ManageManageBundle:CTA:index.html.twig',array("ctas" => $ctas,"userid" => $userid));
    }
    public function savedAction(Request $request)
    {
        $ctaids = $request->request->get("ctaids");
        $userid = $request->request->get("userid");
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:CTAAccountList');       
        $ctas= $repository->findBy(array("userid" => $userid)); 
        foreach ($ctas as $cta)
        $em->remove($cta); 
        echo $ctaids;
        if ($ctaids != "")
        {
            $ctaidArray = explode(",",$ctaids);
            foreach ($ctaidArray as $ctaid)
            {
                $cta = new CTAAccountList();
                $cta->userid = $userid;
                $cta->ctaid = $ctaid;
                $em->persist($cta);
            }              
        }     
        $em->flush();
        return new Response("Saved");
    }
}

