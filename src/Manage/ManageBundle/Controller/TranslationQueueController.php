<?php
namespace Manage\ManageBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
class TranslationQueueController extends Controller
{
    private $inputFields = 
    [
        [
            "name" => "sourceKey",
            "description" => "Source Key"
        ],
        [
            "name" => "domain",
            "description" => "Domain"            
        ],
        [
            "name" => "language",
            "description" => "Language"            
        ],
        [
            "name" => "content",
            "description" => "Content"            
        ],
        [
            "name" => "active",
            "description" => "Active"            
        ],        
    ];
    public function indexAction()
    {
        $this->get("session")->set("Section","Manage");
        return $this->render("index.html.twig");
    }   
    public function listAction(Request $request)
    {
        $table = "TranslationQueue";
        $result = $this->get("QueryBuilderDataTables")->jsonObject($table,["a.id","a.sourceKey","a.domain","a.language","a.content"],$request->request->all());
        $data = [];
        foreach ($result['rows'] as $translation)
        {
            $data[] = [
                $translation->id,
                $translation->sourceKey,
                $translation->domain,
                $translation->language,
                $translation->content,
                $translation->active,
                $this->render("buttons.html.twig",["id" => $translation->id ])->getContent()
            ];
        }      
        $response = $result['response'];
        $response['data'] = $data;
        return new JsonResponse($response);
    }    
    public function addAction(Request $request)
    {
        return $this->render("add.html.twig",["inputFields" => $this->inputFields]);
    }
    public function addSavedAction(Request $request)
    {
        $this->get("TranslationQueueService")->add($request->request->all());
        return new Response("");
    }
    public function editAction(Request $request)
    {
        $translation = $this->get("TranslationQueueService")->findOneBy(["id" => $request->query->get("id")]);
        $this->inputFieldsWithTranslation($translation);
        return $this->render("edit.html.twig",["inputFields" => $this->inputFields,"id" => $request->query->get("id")]);
    }
    public function editSavedAction(Request $request)
    {
        $translation = $this->get("TranslationQueueService")->findOneBy(["id" => $request->request->get("id")]);
        $this->get("TranslationQueueService")->save($translation,$request->request->all());
        return new Response("");
    }
    public function inputFieldsWithTranslation($translation)
    {
        foreach ($this->inputFields as &$field)
        {
            $field['value'] = $translation->{$field['name']};
        }        
    }
    public function deleteAction(Request $request)
    {
        $translation = $this->get("TranslationQueueService")->findOneBy(["id" => $request->query->get("id")]);
        $this->inputFieldsWithTranslation($translation);
        return $this->render("delete.html.twig",["inputFields" => $this->inputFields,"id" => $request->query->get("id")]);
    }
    public function deleteSavedAction(Request $request)
    {
        $translation = $this->get("TranslationQueueService")->findOneBy(["id" => $request->request->get("id")]);
        $this->get("TranslationQueueService")->delete($translation);
        return new Response("");
    }
    public function render($file,$params = array())
    {
        return parent::render('ManageManageBundle:TranslationQueue:'.$file,$params);
    }
}