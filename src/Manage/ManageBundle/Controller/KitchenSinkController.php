<?php
namespace Manage\ManageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Common\Annotations\AnnotationReader;
class KitchenSinkController extends Controller
{
    private $kitchenSinkService;   
    public function indexAction()
    {
        $this->get("session")->set("section","Manage");
        return $this->render("index.html.twig");
    }    
    public function getAccountsAction(Request $request)
    {    
        $table = "accounts";
        $queryBuilder = $this->get("QueryBuilderDataTables")->queryBuilder($table);
        $queryBuilder->where("a.visible = :visible");
        $queryBuilder->setParameter("visible",1);
        $data = $this->get("QueryBuilderDataTables")->jsonObjectQueryBuilder($table,["a.id","a.company","a.partnerid"],$request->request->all(),$queryBuilder);
        $accounts = $data['rows'];
        $response = $data['response'];
        $data = [];
        foreach ($accounts as $account)
        {
            $data[] =[
                $account->id,
                $this->render("accountlist/linkfield.html.twig",["account" => $account,"description" => $account->company])->getContent(),
                $this->render("accountlist/linkfield.html.twig",["account" => $account,"description" => $account->partnerid])->getContent(),
                $this->render("accountlist/runreport.html.twig",["account" => $account])->getContent()
            ];
        }
        $response['data'] = $data;
        return new JsonResponse($response);
    }
    public function getPlansAction(Request $request)
    {
        $this->get("session")->set("kitchenSinkUserid",$request->request->get("userid"));
        $this->get("session")->set("kitchenSinkDeleted",$request->request->get("deleted"));
        return $this->render("getplans.html.twig");
    }
    public function getPlansDataTablesAction(Request $request)
    {
        $table = "plans";
        $queryBuilder = $this->get("QueryBuilderDataTables")->queryBuilder($table,$table);   
        $queryBuilder = $this->get("KitchenSinkService")->addWhereQueryBuilder($queryBuilder, $this->get("session")->get("kitchenSinkUserid"), $this->get("session")->get("kitchenSinkDeleted"));
        $data = $this->get("QueryBuilderDataTables")->jsonObjectQueryBuilder($table,["plans.id","plans.name","plans.planid"],$request->request->all(),$queryBuilder,"plans.id");
        $plans = $data['rows'];
        $response = $data['response'];
        $data = [];
        foreach ($plans as $plan)
        {
            $data[] =[
                $plan->id,
                $this->render("planlist/linkfield.html.twig",["plan" => $plan,"description" => $plan->name])->getContent(),
                $this->render("planlist/linkfield.html.twig",["plan" => $plan,"description" => $plan->planid])->getContent(),
            ];
        }
        $response['data'] = $data;    
        return new JsonResponse($response);

    }
    public function showUserDataAction(Request $request)
    {
        $this->get("session")->set("kitchenSinkSelectedUserid",$request->request->get("userid"));
        $this->get("session")->set("kitchenSinkDeleted",$request->request->get("deleted"));
        return $this->render("showplandata.html.twig",["calls" => $this->calls()]);        
    }
    public function showPlanDataAction(Request $request)
    {   
        $this->get("session")->remove("kitchenSinkSelectedUserid");
        $this->get("session")->set("kitchenSinkPlanid",$request->request->get("planid"));
        return $this->render("showplandata.html.twig",["calls" => $this->calls()]);
    }
    public function resetPageNumberAction(Request $request)
    {
        $this->get("session")->set("kitchenSinkPagenumber",0);
        return new Response("");
    }
    private function calls()
    {
        $calls = 
        [
            "plans" =>
            [
                "description" => "Plans",                
                "route" => "_manage_kitchenSink_showPlanData_plansData",
                "name" => "plans"
            ],
            "plansModules" =>
            [
                "description" => "Modules",                
                "route" => "_manage_kitchenSink_showPlanData_plansModulesData",
                "name" => "plansModules"
            ],
            "plansMessaging" =>
            [
                "description" => "Messaging",                
                "route" => "_manage_kitchenSink_showPlanData_plansMessagingData",
                "name" => "plansMessaging"
            ],
            "plansFunds" =>
            [
                "description" => "Funds",                
                "route" => "_manage_kitchenSink_showPlanData_plansFundsData",
                "name" => "plansFunds"
            ],
             "plansPortfolios" =>
            [
                "description" => "Portfolios",                
                "route" => "_manage_kitchenSink_showPlanData_plansPortfoliosData",
                "name" => "plansPortfolios"
            ],
            "portfoliosWithFunds" =>
            [
                "description" => "Portfolios With Funds",                
                "route" => "_manage_kitchenSink_showPlanData_portfoliosWithFunds",
                "name" => "portfoliosWithFunds"
            ],
            "plansRiskBasedFunds" =>
            [
                "description" => "Risk Based Funds",                
                "route" => "_manage_kitchenSink_showPlanData_plansRiskBasedFundsData",
                "name" => "plansRiskBasedFunds"
            ],
            "plansDocuments" =>
            [
                "description" => "Documents",                
                "route" => "_manage_kitchenSink_showPlanData_plansDocuments",
                "name" => "plansDocuments"
            ],
            "plansVideos" =>
            [
                "description" => "Videos",                
                "route" => "_manage_kitchenSink_showPlanData_plansVideos",
                "name" => "plansVideos"
            ]   
        ];     
        return $calls;
    }
    public function plansDataAction()
    {
        $rows = $this->kitchenSinkService->getPlanRowsDoctrineArray("plans",["field" => "id"]);
        return $this->reportResponse($rows);
    }
    public function plansModulesDataAction()
    {
        $rows =  $this->kitchenSinkService->getPlanRowsDoctrineArray("plansModules");
        return $this->reportResponse($rows);
    }
    public function plansMessagingDataAction()
    {
        $rows =  $this->kitchenSinkService->plansMessagingData();
        return $this->reportResponse($rows);
    }
    public function plansFundsDataAction()
    {
        $rows =  $this->kitchenSinkService->getPlanRowsDoctrineArray("plansFunds");
        return $this->reportResponse($rows);
    }
    public function plansPortfoliosDataAction()
    {
        $rows =  $this->kitchenSinkService->getPlanRowsDoctrineArray("plansPortfolios");
        return $this->reportResponse($rows);
    }
    public function plansRiskBasedFundsDataAction()
    {
        $rows =  $this->kitchenSinkService->getPlanRowsDoctrineArray("plansRiskBasedFunds");
        return $this->reportResponse($rows);
    }
    public function portfoliosWithFundsAction()
    {
        $rows = $this->kitchenSinkService->portfoliosWithFunds();
        return $this->reportResponse($rows);
    }
    public function plansDocumentsAction()
    {
        $rows = $this->kitchenSinkService->getPlanRowsDoctrineArray("plansDocuments");
        return $this->reportResponse($rows);
    }    
    public function plansVideosAction()
    {
        $rows = $this->kitchenSinkService->plansVideosData();
        return $this->reportResponse($rows);
    }  
    public function plansMixedAction(Request $request)
    {
        $data = [];
        if (in_array("plans",$request->request->get("types")))
        {
            $plans = $this->kitchenSinkService->getPlanRowsDoctrineArray("plans",["field" => "id"]);
            foreach ($plans as $plan)
            {
                $data[$plan['id']]['plans'][] = $plan;
            }
        }
        if (in_array("plansModules",$request->request->get("types")))
        {
            $modules = $this->kitchenSinkService->getPlanRowsDoctrineArray("plansModules");
            foreach ($modules as $module)
            {
                $data[$module['planid']]['plansModules'][] = $module;
            }
        }
        if (in_array("plansMessaging",$request->request->get("types")))
        {
            $messages =  $this->kitchenSinkService->plansMessagingData();
            foreach ($messages as $message)
            {
                $data[$message['PlansMessages_planid']]['plansMessaging'][] = $message;
            } 
        }
        if (in_array("plansFunds",$request->request->get("types")))
        {
            $funds =  $this->kitchenSinkService->getPlanRowsDoctrineArray("plansFunds");
            foreach ($funds as $fund)
            {
                $data[$fund['planid']]['plansFunds'][] = $fund;
            }
        }
        if (in_array("plansPortfolios",$request->request->get("types")))
        {
            $portfolios =  $this->kitchenSinkService->getPlanRowsDoctrineArray("plansPortfolios");
            foreach ($portfolios as $portfolio)
            {
                $data[$portfolio['planid']]['plansPortfolios'][] = $portfolio;
            } 
        }
        if (in_array("portfoliosWithFunds",$request->request->get("types")))
        {
            $portfolioFunds = $this->kitchenSinkService->portfoliosWithFunds();
            foreach ($portfolioFunds as $portfolioFund)
            {
                $data[$portfolioFund['plansPortfolioFunds_planid']]['portfoliosWithFunds'][] = $portfolioFund;
            }
        }
        if (in_array("plansRiskBasedFunds",$request->request->get("types")))
        {
            $riskBasedFunds = $this->kitchenSinkService->getPlanRowsDoctrineArray("plansRiskBasedFunds");
            foreach ($riskBasedFunds as $riskBasedFund)
            {
                $data[$riskBasedFund['planid']]['plansRiskBasedFunds'][] = $riskBasedFund;       
            }  
        }
        if (in_array("plansDocuments",$request->request->get("types")))
        {
            $documents= $this->kitchenSinkService->getPlanRowsDoctrineArray("plansDocuments");
            foreach ($documents as $document)
            {
                $data[$document['planid']]['plansDocuments'][] = $document;      
            }  
        }
        if (in_array("plansVideos",$request->request->get("types")))
        {
            $videos = $this->kitchenSinkService->plansVideosData();
            foreach ($videos as $video)
            {
                $data[$video['PlansLibraryUsers_planid']]['plansVideos'][] = $video;      
            } 
        }
        $response['csv'] = $this->convertToCsvRaw($this->getRows($data,1));       
        $response['html'] = $this->render("reports/tabledatalist.html.twig",["rows" => $this->getRows($data,0)])->getContent();
        if (!empty($this->get("session")->get("kitchenSinkSelectedUserid")))
        {
            $response['loader'] = $this->render("progressbar.html.twig",["percent" => $this->get("session")->get("kitchenSinkPercent")])->getContent();;
        }
        return new JsonResponse($response);
    }  
    public function getRows($data,$useHeader)
    {
        $rows = [];
        foreach ($data as $planid => $planData)
        {
            foreach ($planData as $key => $planSection)
            {          
                $rows[] = ["csvHeader" => $this->calls()[$key]['description']." - Planid: ".$this->kitchenSinkService->getRecordKeeperPlanid($planid)];
                if ($useHeader)
                {
                    $rows[] =  array_keys(current($planSection));
                }
                foreach ($planSection as $sectionRow)
                {
                    $rows[]  = $sectionRow;
                }
            }
        }
        return $rows;
    }
    public function reportResponse($rows)
    {
        $response['csv'] = $this->convertToCsv($rows);
        $response['html'] = $this->render("reports/tabledatalist.html.twig",["rows" => $rows])->getContent();
        if (!empty($this->get("session")->get("kitchenSinkSelectedUserid")))
        {
            $response['loader'] = $this->render("progressbar.html.twig",["percent" => $this->get("session")->get("kitchenSinkPercent")])->getContent();;
        }
        return new JsonResponse($response);
    }
    public function render($file,$params = [])
    {        
        return parent::render("ManageManageBundle:KitchenSink:".$file,$params);
    }
    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null) 
    {
        parent::setContainer($container);
        $this->kitchenSinkService = $this->get("KitchenSinkService");
        if (!empty($this->get("session")->get("kitchenSinkPlanid")))
        {
            $this->kitchenSinkService->setPlanId($this->get("session")->get("kitchenSinkPlanid"));
        }
        if (!empty($this->get("session")->get("kitchenSinkSelectedUserid")))
        {
            if (empty($this->get("session")->get("kitchenSinkPagenumber")))
            {
                $this->get("session")->set("kitchenSinkPagenumber",1);
            }
            else
            {
                $this->get("session")->set("kitchenSinkPagenumber",$this->get("session")->get("kitchenSinkPagenumber")+1);
            }
            $this->kitchenSinkService->setPlans($this->get("session")->get("kitchenSinkSelectedUserid"),$this->get("session")->get("kitchenSinkPagenumber"),$this->get("session")->get("kitchenSinkDeleted"));
            $this->get("session")->set("kitchenSinkPercent",$this->kitchenSinkService->getPercent($this->get("session")->get("kitchenSinkPagenumber")));
        }
        $this->get("session")->set("kitchenSinkFinished",$this->kitchenSinkService->getFinished());
    }
    public function csvObject($rows)
    {
        $csv = [];
        if ($this->get("session")->get("kitchenSinkPagenumber") <= 1)
        {
            $csv[] = array_keys(current($rows));
        }
        foreach ($rows as $row)
        {
            $csv[] = array_values($row);
        }
        return $csv;
    }
    public function convertToCsv($rows)
    {
        $csv = $this->csvObject($rows);
        return $this->convertToCsvRaw($csv);
    }
    public function convertToCsvRaw($csv)
    {
        $temp = tmpfile();
        foreach ($csv as $row)
        {
            fputcsv($temp,$row);
        }
        rewind($temp);
        $response =  stream_get_contents($temp);
        fclose($temp);       
        return strip_tags($response);        
    }
}
