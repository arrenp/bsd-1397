	if (document.getElementById("account_partnerid").value == "")
	{
		alert("Partner Id cannot be blank");
		return;
	}	
	if (document.getElementById("account_connectionType").value == "" || document.getElementById("account_connectionType").value == null)
	{
		alert("A Connection Type must be selected");
		return;
	}

	var querystring = "";
	{% for section in sections %}
	if (querystring == "")
	querystring = "{{section.name}}=" + document.getElementById("account_{{section.name}}").value;
	else
	querystring = querystring + "&{{section.name}}=" + document.getElementById("account_{{section.name}}").value;
	{% endfor %}
	
	var ajaxRequest = getAjaxVariable();
	ajaxRequest.onreadystatechange = function()
  	{
	    if(ajaxRequest.readyState == 4)
	    {
	    	if (ajaxRequest.responseText == "Added")
	    	{
		    	newsearch('accounts','accounts','ManageManageBundle:Accounts:Search/accountlist.html.twig');
		    	closeLightBox();
	    	}
	    	else
	    	alert(ajaxRequest.responseText);

	    }
	}

	postAjax(ajaxRequest,page,querystring);