<?php
namespace Manage\ManageBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use classes\classBundle\Entity\TranslationSources;
use classes\classBundle\Entity\DefaultTranslations;
class TranslationUpdateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:TranslationUpdate')->setDescription('Updates default translations')->setHelp('Read description');
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $TranslationQueue = $em->getRepository('classesclassBundle:TranslationQueue')->findBy(["active" => 1]);
        foreach ($TranslationQueue as $translation)
        {
            $language = $em->getRepository('classesclassBundle:languages')->findOneBy(["name" => $translation->language ]);
            $TranslationSource = $em->getRepository('classesclassBundle:TranslationSources')->findOneBy(["sourceKey" => $translation->sourceKey,"domain" => $translation->domain]);
            if ($TranslationSource == null)
            {
                $TranslationSource = new TranslationSources();
                $TranslationSource->sourceKey = $translation->sourceKey;
                $TranslationSource->domain = $translation->domain;  
                $em->persist($TranslationSource);
                $em->flush();
            }
            $DefaultTranslation = $em->getRepository('classesclassBundle:DefaultTranslations')->findOneBy(["sourceId" => $TranslationSource->id,"languageId" => $language->id]);
            if ($DefaultTranslation == null)
            {
                $DefaultTranslation = new DefaultTranslations();
                $DefaultTranslation->translationSource = $TranslationSource;
                $DefaultTranslation->languageId = $language->id;
                $DefaultTranslation->content = $translation->content;
                $em->persist($DefaultTranslation);
            } else {
                $DefaultTranslation->content = $translation->content;
            }
            $translation->active = 0;
            $em->flush();
        }
        $output->writeln("Moved over translations");
    }
}
