<?php

namespace Permissions\RolesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('PermissionsRolesBundle:Default:index.html.twig', array('name' => $name));
    }
}
