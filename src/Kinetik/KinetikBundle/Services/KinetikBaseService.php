<?php

namespace Kinetik\KinetikBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Shared\RecordkeepersBundle\Classes\recordKeeper;

class KinetikBaseService {
    
    private $baseDomain;
    private $lastError;
    private $em;
    private $container;
    
    public function __construct($baseDomain, containerInterface $container) {
        $this->baseDomain = $baseDomain;
        $this->container = $container;
        $this->em = $this->container->get("doctrine")->getManager();
    }
    
    protected function doCall($endpoint, $method = "GET", $data = null, $query = array(), $postAsJson = true) {
        $recordKeeper = new Recordkeeper($this->em);
        $session = $this->container->get('session');
        $generalfunctions = $this->container->get('generalfunctions');
        $planid = $generalfunctions->getPlanId($session);
        $userid = $generalfunctions->getUserId($session);
        $profileId = $session->get('profileId');
        $data_source = $session->get('common')['data_source'];
        if (empty($data_source)) {
            $data_source = 'admin';
        }
        $ch = curl_init();

		if (in_array($method, array("ADD", "UPDATE")) && $postAsJson) {
			$data = !empty($data) ? json_encode($data) : '{}';
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		}

		$url = $this->baseDomain . $endpoint;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        switch ($method) {
            case "ADD":
                curl_setopt($ch, CURLOPT_POST, true); 
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                break;
            case "UPDATE":
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                break;
            default: // "GET"
                curl_setopt($ch, CURLOPT_HTTPGET, true);
                $data = $url;
                break;
        }

        $recordKeeper->writeXmlLog("unknown", NULL, $profileId,$data, $planid, $userid, 'send', "advice", $data_source);
        $response = curl_exec($ch);
        $recordKeeper->writeXmlLog("unknown", NULL, $profileId,$response, $planid, $userid, 'receive',"advice", $data_source);

        if ($response === false) {
            $this->lastError = curl_error($ch);
            curl_close($ch);
            return false;
        }
        
        $response = json_decode($response, true);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        if ($httpCode < 200 || $httpCode >= 300) {
            // Anything that is not 2xx is an error
            $this->lastError = !empty($response['message']) ? $response['message'] : 'Unknown Error';
            curl_close($ch);
            return false;
        }
        curl_close($ch);
        return $response;
    }

    public function getLastError() {
        return $this->lastError;
    }
    
}
