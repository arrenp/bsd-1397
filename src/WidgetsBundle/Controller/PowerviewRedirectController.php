<?php
namespace WidgetsBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PowerviewRedirectController extends Controller
{
    public function indexAction($request)
    {
		$session = $this->get('session');
		$lang = $this->get('WidgetService')->setLang();
		$mobile = $this->get('WidgetService')->isMobile();
                
		$tpl = [
			'data' => $session->get('DATA'),
			'lang' => $lang,
		];

		if ($mobile) {
			return $this->render('WidgetsBundle:PowerViewRedirect:mobile.html.twig', $tpl);
		}
		else {
			return $this->render('WidgetsBundle:PowerViewRedirectNew:index.html.twig', $tpl);
		}
    }
}
