<?php

namespace WidgetsBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class PowerviewOffTheBenchController extends Controller {
    
    protected function init() {
        $this->directory = $this->get('WidgetService')->isMobile() ? 'mobile' : 'desktop';
        $this->get('WidgetService')->setLang();
    }
    
    public function indexAction($request) {
        $this->init();
        $data = $this->get("session")->get("DATA");
        $hasMatching = $data['maxMatch'] > 0;
        return $this->render("WidgetsBundle:PowerViewOffTheBench/{$this->directory}:index.html.twig", array("data" => $data, "hasMatching" => $hasMatching));
    }
    
    public function selectContributionAction() {
        $this->init();
        $data = $this->get('session')->get("DATA");
        return $this->render("WidgetsBundle:PowerViewOffTheBench/{$this->directory}:selectContribution.html.twig", array('data' => $data));
    }

    public function confirmationConnectedAction() {
        $this->init();
        $data = $this->get('session')->get("DATA");
        return $this->render("WidgetsBundle:PowerViewOffTheBench/{$this->directory}:confirmationConnected.html.twig", array('data' => $data));
    }
    
    public function confirmationCtaAction() {
        $this->init();
        $data = $this->get('session')->get("DATA");
        return $this->render("WidgetsBundle:PowerViewOffTheBench/{$this->directory}:confirmationCta.html.twig", array('data' => $data));
    }
    
    public function confirmationThankYouAction() {
        $this->init();
        return $this->render("WidgetsBundle:PowerViewOffTheBench/{$this->directory}:confirmationThankYou.html.twig");
    }
    
    public function startSavingAction() {
        $this->init();
        return $this->render("WidgetsBundle:PowerViewOffTheBench/mobile:startSaving.html.twig");
    }
}
