<?php
namespace WidgetsBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use classes\classBundle\Entity\surveyResults;
use Symfony\Component\HttpFoundation\Session\Session;

class NationalsurveyController extends Controller {
    
    public function indexAction($request){
        
        $this->get('translator')->setLocale($request['lang']);
        
        $session = $this->getRequest()->getSession();
        
        $session->set("partner_id", $request['partner_id']);
        
        $partId = $this->getRequest()->getSession()->get('partner_id'); 
        
        if($partId === '' || !isset($partId)){
            return $this->render('WidgetsBundle:NationalsurveyNew:templates/error.html.twig');
        }else{
            return $this->render('WidgetsBundle:NationalsurveyNew:index.html.twig', array('partner' => $partId));
        }
    
    }
    
    function answerFilterAction(Request $data){
        
        $identifier = $data->request->get('indentifier');
        
        switch($identifier){
            case 'state':
                return $this->getCounties($data->request->get('answer'));
                break;
            case 'employerName':
                return $this->getPlanType($data->request->get('answer'));
                break;
            case 'end':
                return $this->planResults($data->request->get('answer'));
                break; 
            case 'save':
                return $this->saveResults($data->request->get('plan'), $data->request->get('querystring'), $data->request->get('partner'));
                break;             
            default:
                //return $this->render('WidgetsBundle:Nationalsurvey:templates/error.html.twig');
                return new response('<h1>'.$identifier.'</h1>');
                break;
        }
    }
    
    function firstQuestionAction(){
        $em       = $this->getDoctrine()->getManager();
        $query    = $em->createQuery('SELECT p.state as id, p.state as label FROM classesclassBundle:SurveyEmployerPlans p WHERE p.state != :null group by p.state')                                    
                                    ->setParameter('null', 1);  
        $states   = $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $question = 'Please select the State of your Employer: ';
        
    return $this->render('WidgetsBundle:NationalsurveyNew:templates/select.html.twig', 
                          array('question' => $question,
                                'answers'  => $states,
                                'type'     => 'state'));        
    }
    
    function getCounties($answer){
        $em       = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare('SELECT e.id, e.employerName as label FROM surveyEmployers e INNER JOIN SurveyEmployerPlans p ON p.surveyEmployerId = e.id WHERE p.state = :location GROUP BY e.id');
        $stmt->execute(['location' => $answer]);
        $counties = $stmt->fetchAll();
        $question = 'survey_two_select_from_list';
        
    return $this->render('WidgetsBundle:NationalsurveyNew:templates/select.html.twig',  
                          array('question' => $question,
                                'answers'  => $counties,
                                'type'     => 'employerName'));        
    }
    
    function getPlanType($answer){
        $em       = $this->getDoctrine()->getManager();
        $query    = $em->createQuery('SELECT p.type, p.planid FROM classesclassBundle:SurveyEmployerPlans p WHERE p.surveyEmployerId = :surveyEmployerId')                                   
                                    ->setParameter('surveyEmployerId', $answer);
        $counties = $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $question = 'survey_two_select_Plan_type';
        
        
        $answers = $counties;
    return $this->render('WidgetsBundle:NationalsurveyNew:templates/radio.html.twig',  
                          array('question' => $question,
                                'answers'  => $answers,
                                'identifier' => 'end'));        
    }
    
    function planResults(){

    return $this->render('WidgetsBundle:NationalsurveyNew:templates/planResults.html.twig');     
    
    }    
    
    
    function hashArray(&$object,$fieldname)
    {
        foreach ($object as $key => $value)
        {
           if (is_int($key))
           {
            $object[$value[$fieldname]] = $value;
            unset ($object[$key]); 
           }
        }        
    }    
    
    function saveResults($plan, $json, $partner){   
        
        $session               = $this->getRequest()->getSession();
        $em                    = $this->getDoctrine()->getManager();
        $newaccount            = new surveyResults();
        $newaccount->partnerId = $partId = $this->getRequest()->getSession()->get('partner_id'); 
        $newaccount->planId    = $plan;
        $newaccount->results   = $json;
        $newaccount->dateStamp = new \DateTime('now');
        $newaccount->userIp    = $this->getRequest()->getClientIp();
              
        
        $partId    = $partner; 
        $planId    = $plan;        
        
        $em->persist($newaccount);
        $em->flush();
        
        $redirect = $this->container->getParameter('app_domain');
        $appurl = $this->container->getParameter('appurl');
        return new response($appurl.'?id='.$planId.'&partner_id='.$partId."&type=smartenroll");
    }

}