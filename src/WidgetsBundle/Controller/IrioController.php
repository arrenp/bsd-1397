<?php
namespace WidgetsBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
class IrioController extends Controller
{
    public function indexAction($request)
    {
		$tmp = Request::createFromGlobals();

		$session = $this->get('session');
		$this->get('WidgetService')->setLang();
		if (!$session->get('DATA')) {
			return $this->render('WidgetsBundle:Default:loading.html.twig', array('session' => $session));
		} else {
			$tpl = [
				'data' => $session->get('DATA'),
                               ];

			return $this->render('WidgetsBundle:Irio:index.html.twig', $tpl);
		}
    }
}


