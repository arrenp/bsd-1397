<?php
namespace WidgetsBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PowerviewController extends Controller
{
    public function indexAction($request)
    {
		$session = $this->get('session');
                $data = $session->get("DATA");
                if (isset($data['diffDeferral']) && $data['diffDeferral'] <= 0)
                {
                    $session->set("cta","PowerviewSmartPlan");
                    return $this->forward("WidgetsBundle:PowerviewSmartPlan:index", array("request" => $request)); 
                }
		$lang = $this->get('WidgetService')->setLang();
		$mobile = $this->get('WidgetService')->isMobile();

		$tpl = [
			'data' => $session->get('DATA'),
			'lang' => $lang,
		];

		if ($mobile) {
			return $this->render('WidgetsBundle:PowerView:mobile.html.twig', $tpl);
		}
		else {
			return $this->render('WidgetsBundle:PowerViewNew:index.html.twig', $tpl);
		}
    }
}
