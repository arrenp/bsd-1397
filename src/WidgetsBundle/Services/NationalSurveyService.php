<?php
namespace WidgetsBundle\Services;
use Symfony\Component\DependencyInjection\ContainerInterface;
class SurveyService
{
    public $container;
    public function __construct(containerInterface $container)
    {
        $this->container = $container;
        $this->em = $this->container->get("doctrine");
        $this->connection = $this->container->get("doctrine.dbal.default_connection");
    }    
    public function getSurvey($partnerid = "VWISE_EDUCATE_PROD")
    {			
        $repository = $this->em->getRepository('classesclassBundle:surveyQuestions');
        $survey = $repository->findOneBy(array('partnerid' => $partnerid, 'isFirst' => 1,'active' => 1)); 
        $survey->previousid = 0;
        $this->calculateSurvey($survey);
        return $survey;
    }
    public function calculateSurvey(&$survey)
    {
        $repository = $this->em->getRepository('classesclassBundle:surveyAnswers');
        $survey->answers =  $repository->findBy(array('questionid' => $survey->id,'active' => 1)); 
        $repository = $this->em->getRepository('classesclassBundle:surveyQuestions');
        foreach ($survey->answers as &$answer)
        {
            $answer->question = $repository->findOneBy(array('id' => $answer->nextQuestionId)); 
            if ($answer->question != null)
            $answer->question->previousid = $survey->id;
            if ($answer->question != null)
            $this->calculateSurvey($answer->question);
        }
    }
}

