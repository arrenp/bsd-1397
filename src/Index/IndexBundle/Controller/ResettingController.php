<?php

namespace Index\IndexBundle\Controller;


use FOS\UserBundle\Controller\ResettingController as BaseController;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccountStatusException;
use Sessions\AdminBundle\Classes\adminsession;
use Symfony\Component\HttpFoundation\Request;

use FOS\UserBundle\Model\UserInterface;

class ResettingController extends BaseController
{

	public function resetAction(Request $request, $token = null)
    {
        if (empty($token)) { // they used question mark
            $token = $request->query->get('token');
        }

        $user = $this->container->get('fos_user.user_manager')->findUserByConfirmationToken($token);

        if (null === $user) {
            return new RedirectResponse($this->container->get('router')->generate('_index'));
        }

        if (!$user->isPasswordRequestNonExpired($this->container->getParameter('fos_user.resetting.token_ttl'))) {
            $em = $this->container->get("doctrine")->getManager();
            $user->setConfirmationToken(null);
            $user->setPasswordRequestedAt(null);
            $em->flush();
            return $this->container->get('templating')->renderResponse('IndexIndexBundle:Default:resetexpired.html.twig', array( 'token' => $token ));
        }

        $form = $this->container->get('fos_user.resetting.form');
        $formHandler = $this->container->get('fos_user.resetting.form.handler');
        $process = $formHandler->process($user);

        if ($process) {
            $this->setFlash('fos_user_success', 'resetting.flash.success');
            $response = new RedirectResponse($this->getRedirectionUrl($user));
            $this->authenticateUser($user, $response);

            return $response;
        }

        return $this->container->get('templating')->renderResponse('IndexIndexBundle:Default:reset.html.'.$this->getEngine(), array(
            'token' => $token,
            'form' => $form->createView(),
        ));
    }
    public function sendEmailAction()
    {
        $username = $this->container->get('request')->request->get('username');
        $tokenGenerator = $this->container->get('fos_user.util.token_generator');      
        /** @var $user UserInterface */
        $user = $this->container->get('fos_user.user_manager')->findUserByUsernameOrEmail($username);
        $response = new Response();
        $response->headers->set("Content-Security-Policy", "frame-ancestors 'self'");

        if (null === $user) {
            return $this->container->get('templating')->renderResponse('FOSUserBundle:Resetting:request.html.'.$this->getEngine(), array('invalid_username' => $username,'token' => $tokenGenerator->generateToken()),$response);
        }

        if ($user->isPasswordRequestNonExpired($this->container->getParameter('fos_user.resetting.token_ttl'))) {
            return $this->container->get('templating')->renderResponse('FOSUserBundle:Resetting:passwordAlreadyRequested.html.'.$this->getEngine());
        }

        if (null === $user->getConfirmationToken()) {
            /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
            $user->setConfirmationToken($tokenGenerator->generateToken());
        }

        $this->container->get('session')->set(static::SESSION_EMAIL, $this->getObfuscatedEmail($user));
        $this->container->get('fos_user.mailer')->sendResettingEmailMessage($user);
        $user->setPasswordRequestedAt(new \DateTime());
        $this->container->get('fos_user.user_manager')->updateUser($user);

        return new RedirectResponse($this->container->get('router')->generate('fos_user_resetting_check_email'));
    }
    public function requestAction()
    {
        $response = new Response();
        $response->headers->set("Content-Security-Policy", "frame-ancestors 'self'");
        $tokenGenerator = $this->container->get('fos_user.util.token_generator');      
        return $this->container->get('templating')->renderResponse('FOSUserBundle:Resetting:request.html.'.$this->getEngine(),array("token" => $tokenGenerator->generateToken()),$response);
    }
}
?>
