<?php

namespace Sessions\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SessionsAdminBundle:Default:index.html.twig', array('name' => $name));
    }
}
