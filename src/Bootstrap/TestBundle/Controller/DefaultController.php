<?php

namespace Bootstrap\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('BootstrapTestBundle:Default:index.html.twig');
    }

    public function index2Action()
    {
        return $this->render('BootstrapTestBundle:Default:index2.html.twig',array('variables_file' => 'test'));
    }
}
