<?php

namespace classes\classBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Shared\General\SFTPConnection;

class AccountRolloverReportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:AccountRolloverReport')
            ->setDescription('Account rollover report for ameritas')
            ->setHelp('Look at description')
            ->addOption('userid', null, InputOption::VALUE_REQUIRED, 'Account id')
            ->addOption('start', null, InputOption::VALUE_OPTIONAL, 'start date: example 2017-09-13')
            ->addOption('end', null, InputOption::VALUE_OPTIONAL, 'end date: example 2017-11-13')
            ->addOption('skipUpload', null, InputOption::VALUE_NONE, 'Skip upload');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $start = $input->getOption('start');
        $end = $input->getOption('end');

        if ($start == null || $end == null) 
        {
            $date = new \DateTime();
            $end = $date->format('Y-m-d H:i:s');
            $date->sub(new \DateInterval('P1M'));
            $start = $date->format('Y-m-d H:i:s');
        }
        $params = array("start" => $start,"end" => $end,"where" => array("userid=".$input->getOption('userid')));
        $AmeritasCustomAccountRollover = $this->getContainer()->get("AmeritasCustomAccountRollover");
        $data =  
        [
            [
                "Call Us Count",
                "Call Me Count",
                "Email Me Count",
                "Enrollment Countact Viewed Times",
                "Enrollment Countact Viewed Times Distinct"
            ],
            [
                $AmeritasCustomAccountRollover->callUsCount($params),
                $AmeritasCustomAccountRollover->callMeCount($params),
                $AmeritasCustomAccountRollover->emailMeCount($params),
                $AmeritasCustomAccountRollover->enrollmentContactViewedTimes($params),
                $AmeritasCustomAccountRollover->enrollmentContactViewedTimesDistinct($params)
            ]
        ];
        $ftpConfig = $AmeritasCustomAccountRollover->getFtpConfig();
        $filename = "AMRTS_".date("Ym")."_AAR.csv";     
        $fp = fopen($filename, 'w');
        foreach ($data as $row)
        {
            fputcsv($fp, $row);
        }

        $skipUpload = $input->getOption('skipUpload');
        if (!$skipUpload) {
            $sftp = new SFTPConnection($ftpConfig['host'], $ftpConfig['port']);
            $sftp->login($ftpConfig['username'], $ftpConfig['password']);
            $sftp->uploadFile($filename, "/home/vwiseupload/" . $filename);
            unlink($filename);
        }
    }
}
