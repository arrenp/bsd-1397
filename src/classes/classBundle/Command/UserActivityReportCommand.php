<?php

namespace classes\classBundle\Command;

use Manage\ManageBundle\Controller\ReportsController;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UserActivityReportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:UserActivityReport')
            ->setDescription('User Activity Report command')
            ->addOption('startDate', null, InputOption::VALUE_REQUIRED, 'start date: example 2017-09-13')
            ->addOption('endDate', null, InputOption::VALUE_REQUIRED, 'end date: example 2017-11-13')
            ->addOption('age', null, InputOption::VALUE_REQUIRED, 'Retirement imminient age')
            ->addOption('providerId', null, InputOption::VALUE_REQUIRED, 'Provider id to run the report on.')
            ->addOption('adviserId', null, InputOption::VALUE_REQUIRED, 'adviser id to run for')
            ->addOption('planId', null, InputOption::VALUE_REQUIRED, 'plan id to run for')
            ->addOption('outputPath', null, InputOption::VALUE_REQUIRED, 'path to place file')
            ->addOption('q', null, InputOption::VALUE_REQUIRED, 'partial filename (planId assumed to be used)')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $reportsController = new ReportsController();
        $reportsController->setContainer($this->getContainer());

        $startDate = $input->getOption('startDate');
        $endDate = $input->getOption('endDate');
        $age = $input->getOption('age');

        if ($startDate == null || $endDate == null) {
            throw new \Exception("give a start and end date");
        }

        if (empty($age)) {
            $age = 58;
        }

        $parameters = array();
        $parameters['startDate'] = $startDate;
        $parameters['endDate'] = $endDate;
        $parameters['userid'] = $input->getOption('providerId');
        $parameters['adviserid'] = $input->getOption('adviserId');
        $parameters['planid'] = $input->getOption('planId');
        $parameters['outputPath'] = $input->getOption('outputPath');
        $parameters['q'] = $input->getOption('q');
        $parameters['age'] = $age;
        $parameters['filename'] = "";

        if (empty($parameters['q']) || empty($parameters['planid'])) {
            $parameters['filename'] = "UserActivityReport.xlsx";
        } else {
            $repository = $this->getContainer()->get("doctrine")->getRepository("classesclassBundle:plans");
            $planid = $repository->findOneBy(array('id' => $parameters['planid']))->planid;
            $parameters['filename'] = "{$planid}_{$parameters['q']}.xlsx";
        }

        if (!empty($parameters['outputPath'])) {
            $pathChar = ['/', '\\'];
            if (!in_array(substr($parameters['outputPath'], -1), $pathChar)) {
                $parameters['outputPath'] = $parameters['outputPath']."/";
            }
        }

        $unique = ['userid', 'adviserid', 'planid'];
        $count = 0;
        foreach ($unique as $item) {
            if (!empty($parameters[$item])) {
                $count++;
            }
        }
        if ($count != 1) {
            throw new \Exception("choose only one as a parameter - providerId, adviserId, or planId");
        }

        $dateString = $reportsController->getDateSql($parameters);
        $connection = $this->getContainer()->get('doctrine.dbal.default_connection');
        $selectPlanFieldsSql = "SELECT plans.id as smartplanid,plans.name as name, plans.planid as planid, plans.providerLogoImage as providerLogoImage ";
        $reports = array();

        if (isset($parameters['adviserid']))
        {
            $accountsUsers = $connection->executeQuery("SELECT userid,allplans FROM accountsUsers WHERE id=".$parameters['adviserid'])->fetch();
            if ($accountsUsers != null)
            {
                if ($accountsUsers['allplans'] == 0) {
                    $advisersPlans =  $connection->fetchAll($selectPlanFieldsSql."FROM accountsUsersPlans LEFT JOIN plans on accountsUsersPlans.planid = plans.id  LEFT JOIN profiles ON accountsUsersPlans.planid = profiles.planid WHERE accountsUsersPlans.accountsUsersId = ".$parameters['adviserid']." AND profiles.id is not null ".$dateString." AND plans.id is not null group by plans.id");

                }
                else {
                    $advisersPlans =  $connection->fetchAll($selectPlanFieldsSql."FROM plans LEFT JOIN profiles ON plans.id = profiles.planid  WHERE plans.userid = ".$accountsUsers['userid']." AND profiles.id is not null ".$dateString." group by plans.id" );
                }
            }
            foreach ($advisersPlans as $plan)
            {
                $planid = $plan['smartplanid'];
                $parameters['planid'] = $planid;
                $reportsController->addReport($reports,$reportsController->UserActivityReport($parameters),$plan);
            }
        }
        elseif (isset($parameters['userid'])) {
            $accountsPlans = $connection->fetchAll($selectPlanFieldsSql."FROM plans LEFT JOIN profiles ON plans.id = profiles.planid WHERE plans.userid = :userid AND profiles.id is not null group by plans.id", [':userid' => $parameters['userid']]);
            foreach ($accountsPlans as $plan) {
                $parameters['planid'] = $plan['smartplanid'];
                $reportsController->addReport($reports,$reportsController->UserActivityReport($parameters),$plan);
            }
        }
        else
        {
            $plan = $connection->executeQuery($selectPlanFieldsSql."FROM plans LEFT JOIN profiles ON plans.id = profiles.planid WHERE plans.id = ".$parameters['planid']." AND profiles.id is not null  group by plans.id")->fetch();
            $reportsController->addReport($reports,$reportsController->UserActivityReport($parameters),$plan);
        }

        if ((!empty($parameters['planid']) && !empty($plan)) || (empty($parameters['planid']) && !empty($reports))) {
            $reportsController->irioExcelReport($reports, $parameters['outputPath'].$parameters['filename']);
            $output->writeln("done");
        } else {
            throw new \Exception("no results returned.");
        }

    }

}