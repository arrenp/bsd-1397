<?php

namespace classes\classBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendReminderEmailsCommand extends ContainerAwareCommand {


    protected function configure()
    {
        $this
            ->setName('app:SendReminderEmailsCommand')
            ->setDescription('For each plan where the freezeEndDate is not null and transitionPeriodOver is 0, send an email to the account manager')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $qb = $em->createQueryBuilder();
        $plans = $qb->select('p')
                    ->from("classesclassBundle:plans", 'p')
                    ->where('p.deleted = 0 and p.freezeEndDate is not null and p.transitionPeriodOver = 0')
                    ->getQuery()
                    ->getResult();

        $accountsRepo = $em->getRepository("classesclassBundle:accounts");
        $accountManagersRepo = $em->getRepository("classesclassBundle:accountManagers");

        foreach ($plans as $plan) {
            $result = $accountsRepo->findOneBy(array('id' => $plan->userid))->accountManager;
            $accountManagers = explode(',', $result);
            $to = array();
            foreach ($accountManagers as $accountManager) {
                $to[] = $accountManagersRepo->findOneBy(array('id' => $accountManager))->emailAddress;
            }

            $emailContents =  $this->getContainer()->get('twig')->render('classesclassBundle:EmailsCommand:template.html.twig',
                array('partnerid' => $plan->partnerid, 'planname' => $plan->name, 'planid' => $plan->planid));

            $message = \Swift_Message::newInstance()
                ->setSubject('Freeze Date Reminder')
                ->setFrom('info@smartplanenterprise.com')
                ->setTo($to)
                ->setBody(
                    $emailContents,'text/html');
            $this->getContainer()->get('mailer')->send($message);
        }
    }

}
