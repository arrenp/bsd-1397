<?php

namespace classes\classBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Shared\General\SFTPConnection;
use Symfony\Component\Console\Input\InputArgument;

class RemoveDuplicateTranslationsCommand extends ContainerAwareCommand
{
    
    protected function configure()
    {
        $this
            ->setName('app:RemoveDuplicateTranslationsCommand')
            ->setDescription('Remove duplicate entries for account and plan level');
        ;       
    }    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $defaultTranslations = $this->loadTranslations("DefaultTranslations",array());
        foreach ($defaultTranslations as $defaultTranslation)
        {
            $accountTranslations = $this->loadTranslations("AccountsTranslations", array("content" => $defaultTranslation->content,"languageId" => $defaultTranslation->languageId));
            foreach ($accountTranslations as $translation)
            {
                $output->writeln("Removing Account Entry");
                $output->writeln("Content: ".$translation->content);
                $output->writeln("Languageid: ".$translation->languageId);
                $em->remove($translation);
            }
        }
        $em->flush();
    }
    public function loadTranslations($table,$findby)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $repo = $em->getRepository('classesclassBundle:'.$table);
        $translations = $repo->findBy($findby);
        return $translations;
     }    
}
