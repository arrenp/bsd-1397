<?php

namespace classes\classBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class HorsemanSurveyCommand extends ContainerAwareCommand {
    protected function configure()
    {
        $this
            ->setName('app:HorsemanSurveyCommand')
            ->setDescription('Horseman survey csv planid renamer. Use a CSV file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $output->write("Enter Filename: ");
        $filename = rtrim(fgets(STDIN));
        if (!$this->endsWith($filename, ".csv")) {
            $filename .= ".csv";
        }

        if (($handle = fopen($filename, "r")) === FALSE) {
            $output->write("File not found at " . getcwd() . "/{$filename}\n");
            exit();
        }

        $output->write("Pick One:\n1) Migrate the planids from the old to the new\n2) Migrate the surveyEmployers ids from the new to the old (revert): ");
        $option = rtrim(fgets(STDIN));

        if (strpos(strtolower($filename), 'iowa') !== false) {
            $tab = 1;
        } else if (strpos(strtolower($filename), 'national') !== false) {
            $tab = 2;
        }

        if (empty($tab)) {
            $output->write("Pick One:\n1) Iowa\n2) National: ");
            $tab = rtrim(fgets(STDIN));
        }

        if ($tab == 2) { // if national, because no ~ concat
            switch ($option) {
                case 1: $first = 2;
                    $second = 0;
                    break;
                case 2: $first = 0;
                    $second = 2;
                    break;
            }
        }

        $count = 0;
        $repository = $this->getContainer()->get("doctrine")->getRepository("classesclassBundle:surveyEmployers");
        $arraySet = array();

        $output->write("Starting planid rename job\n");
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            if (!$count++) {
                continue;
            }

            if ($tab == 1) {
                if ($option == 1) {
                    $surveyEmployers = $repository->findBy(array('planId' => "{$data[0]}~{$data[1]}"));
                    if (empty($surveyEmployers)) {
                        $output->write("Planid not found: {$data[0]}~{$data[1]}\n");
                        continue;
                    } else if(array_key_exists("{$data[3]}", $arraySet)) {
                        $output->write("Multiple records with this new planid {$data[3]} has been found. Skipping\n");
                        continue;
                    }
                    $surveyEmployers[0]->planId = $data[3];
                    $arraySet[$data[3]] = true;
                    $em->persist($surveyEmployers[0]);
                } else if ($option == 2) {
                    $surveyEmployers = $repository->findBy(array('planId' => "{$data[3]}"));
                    if (empty($surveyEmployers)) {
                        $output->write("Planid not found: {$data[3]}\n");
                        continue;
                    } else if(array_key_exists("{$data[3]}", $arraySet)) {
                        $output->write("Multiple records with this new planid {$data[3]} has been found. Skipping\n");
                        continue;
                    }
                    $surveyEmployers[0]->planId = "{$data[0]}~{$data[1]}";
                    $arraySet[$data[3]] = true;
                    $em->persist($surveyEmployers[0]);

                }
            } else if ($tab == 2) {
                $surveyEmployers = $repository->findBy(array('planId' => "{$data[$second]}"));
                if (empty($surveyEmployers)) {
                    $output->write("Planid not found: {$data[$second]}\n");
                    continue;
                } else if(array_key_exists("{$data[$second]}", $arraySet)) {
                    $output->write("Multiple records with this new planid {$data[$second]} has been found. Skipping\n");
                    continue;
                }
                $surveyEmployers[0]->planId = $data[$first];
                $data[$second] = true;
                $em->persist($surveyEmployers[0]);
            }

            if ($count % 100 == 0) {
                $em->flush();
            }
        }
        $em->flush();
        $output->write("Completed survey planid rename job\n");
        fclose($handle);
    }

    public function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }
        return (substr($haystack, -$length) === $needle);
    }

}
