<?php

namespace classes\classBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PerformanceReportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:PerformanceReport')
            ->setDescription('Only use none or one of either accountId, planId, userId fields')            
            ->addOption('startDate', null, InputOption::VALUE_OPTIONAL, 'start date of report')
            ->addOption('endDate', null, InputOption::VALUE_OPTIONAL, 'end date of report')
            ->addOption('accountId', null, InputOption::VALUE_OPTIONAL, 'account id for report')
            ->addOption('planId', null, InputOption::VALUE_OPTIONAL, 'plan id for report')
            ->addOption('userId', null, InputOption::VALUE_OPTIONAL, 'user id for advisor')
            ->addOption('format', null, InputOption::VALUE_OPTIONAL, 'csv or pdf')
            ->addOption('outputPath', null, InputOption::VALUE_OPTIONAL, 'path for file output (no / at end)')
            ->addOption('q', null, InputOption::VALUE_REQUIRED, 'partial filename (planId assumed to be used)');        
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $params['level'] = "vWise";
        $params['acct'] = "";//accounts.id or accountsUsers.id
        $params['plan'] =  "";//smartplanid
        $params['var2'] = "";//planid_partnerid 
        $params['audience'] = "provider";
        $params['prov'] = "All";//description
        $params['start'] = !empty($input->getOption('startDate')) ? $input->getOption('startDate'):"";
        $params['end'] = !empty($input->getOption('endDate')) ? $input->getOption('endDate'):"";
        $filename = "performanceReport";
        $filenameQUsed = false;
        if (!empty($input->getOption('planId')))
        {
            $repository = $this->getContainer()->get("doctrine")->getRepository("classesclassBundle:plans");
            $plan = $repository->findOneBy(array('id' => $input->getOption('planId'))); 
            
            if(!empty($plan))
            {
                $repository = $this->getContainer()->get("doctrine")->getRepository("classesclassBundle:accounts");
                $account = $repository->findOneBy(array('id' => $plan->userid));
                if(!empty($account))
                {
                    $params['acct'] = $plan->userid;
                    $params['var2'] = $plan->planid."_".$plan->partnerid;
                    $params['prov'] = $account->id."-".$account->company;
                    $params['plan'] = $input->getOption('planId');
                    if (!empty($input->getOption('q')))
                    {
                        $filename = $plan->planid."_".$input->getOption('q');
                        $filenameQUsed = true;
                    }
                    else
                    {
                        $filename = $filename."_plan_".$input->getOption('planId');
                    }
                }
            }
        }
        else if (!empty($input->getOption('accountId')))
        {
            $repository = $this->getContainer()->get("doctrine")->getRepository("classesclassBundle:accounts");
            $account = $repository->findOneBy(array('id' => $input->getOption('accountId')));
            if (!empty($account))
            {
                $params['acct'] = $input->getOption('accountId');
                $params['var2'] = $account->partnerid;
                $params['prov'] = $account->id."-".$account->company;
                $filename = $filename."_account_".$input->getOption('accountId');
            }
        }
        else if (!empty($input->getOption('userId')))
        {
            $repository = $this->getContainer()->get("doctrine")->getRepository("classesclassBundle:accountsUsers");
            $user = $repository->findOneBy(array('id' => $input->getOption('userId')));
            if (!empty($user))
            {
                $repository = $this->getContainer()->get("doctrine")->getRepository("classesclassBundle:accounts");
                $account = $repository->findOneBy(array('id' => $user->userid));     
                $params['acct'] = $user->id;
                $params['var2'] = $account->partnerid;
                $params['prov'] = $account->id."-".$user->id."-".$user->firstname." ".$user->lastname;
                $params['level']  = "advisor";
                $filename = $filename."_adviser_".$input->getOption('userId');
            }
        }
        if (!$filenameQUsed)
        {
            $filename = $filename."_".time();
        }
        $data = $this->getContainer()->get("PerformanceReportService")->vWiseReport($params);
        if (empty($input->getOption('format')) || !in_array($input->getOption('format'),["csv","pdf"]))
        {
            var_dump($data);
        }
        else
        {
            $filename = $filename.".".$input->getOption('format');
            if($input->getOption('format') == "csv")
            {
                $fileContents = $this->getContainer()->get("PerformanceReportService")->reportCsv($data);
            }
            else
            {
                $fileContents = $this->getContainer()->get("PerformanceReportService")->reportPdf($data);
            }
            if (!empty($input->getOption('outputPath')))
            {
                $filename = $input->getOption('outputPath')."/".$filename;
            }
            file_put_contents($filename,$fileContents);
        }

    }    
}