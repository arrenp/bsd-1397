<?php

namespace classes\classBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

class SendBatchFileNotificationEmailsCommand extends ContainerAwareCommand {
    
    protected function configure()
    {
        $this
            ->setName('app:SendBatchFileNotificationEmails')
            ->setDescription('Sends emails to all batch file notification emails assigned the specified account id')
            ->setHelp('Sends emails to all batch file notification emails assigned the specified account id')
            ->addArgument('userid', InputArgument::REQUIRED, "Account id of batch file notification emails")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $userid = $input->getArgument("userid");
		
		/* @var $translator \Shared\TranslationBundle\Translation\Translator */
		$translator = $this->getContainer()->get("translator");
		$translator->setAccountId($userid);
		
		$subject = $translator->trans('email_subject', array(), 'batch');
		$body = $translator->trans("email_body", array(), 'batch');
		
		$repository = $this->getContainer()->get("doctrine")->getRepository("classesclassBundle:AccountsBatchFileNotificationEmails");
		$accountsBatchFileNotificationEmails = $repository->findBy(array('userid' => $userid));
		$emails = [];
		foreach ($accountsBatchFileNotificationEmails as $item) {
			$emails[] = $item->email;
		} 
		
		$message = \Swift_Message::newInstance()
			->setSubject($subject)
			->setFrom("support@smartplanenterprise.com")
			->setTo($emails)
			->setBody($body, 'text/html');
		$sent = $this->getContainer()->get('mailer')->send($message);
		
		$output->writeln("$sent emails sent");
    }
    
}
