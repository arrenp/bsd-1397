<?php

namespace classes\classBundle\Classes;

use classes\classBundle\Entity\campaignsLists;
use classes\classBundle\Entity\campaigns;
use classes\classBundle\Classes\mailgunEngine;
$files =   parse_ini_file(getcwd() . "/../app/config/mailEngines.ini", true);
foreach ($files as $key => $value )
{
   $file = getcwd()."/../src/classes/classBundle/Classes/".$key.".php";
   if (file_exists($file))
   include $file;
}
class mailEngine
{
    public function __construct($name, $controller)
    {
        $paramaters = parse_ini_file(getcwd() . "/../app/config/mailEngines.ini", true)[$name];
        foreach ($paramaters as $key => $value)
        {
            $this->$key = $value;
        }
        $this->mailEngine = $name;
        $this->controller = $controller;
        $this->em = $controller->getDoctrine()->getManager();
        $this->dbal = $this->controller->get('doctrine.dbal.default_connection');
        if ($name == "mailgunEngine")
        $this->mailObject =   new  mailgunEngine();
        else
        $this->mailObject =   new  $name;
        $this->mailObject->init($this);
    }
    public function createCampaign($params)
    {
        if (isset($params['description']) && isset($params['title']) && isset($params['fromEmail']) && isset($params['fromName']) && isset($params['subject']))
        {
            $html = "";
            $text = "";

            $campaign = new campaigns();
            foreach ($params as $key => $value)
            {
                $campaign->$key = $value;
            }
            $campaign->mailEngine = $this->mailEngine;
            $this->addCampaignId($params, $campaign);
            if (isset($params['html']))
            {
                $html = $params['html'];
            }
            if (isset($params['textBody']))
            {
                $text = $params['textBody'];
            }
            $html = $this->campaignHtml($html);
            $this->mailObject->createCampaign($params,$campaign,$html,$text);
            $this->em->persist($campaign);
            $this->em->flush();
            return $campaign->id;
        }
        return -1;
    }
    public function updateCampaign($params)
    {
        if (isset($params['id']))
        {
            $repository = $this->controller->getDoctrine()->getRepository('classesclassBundle:campaigns');
            $campaign = $repository->findOneBy(array('id' => $params['id']));
            if (isset($params['html']))
            $params['html'] = $this->campaignHtml($params['html']);
            foreach ($params as $key => $value)
            {
                $campaign->$key = $value;
            }
            $this->addCampaignId($params, $campaign);
            $this->mailObject->updateCampaign($params,$campaign);
            $this->em->flush();
            return $campaign->id;
        }
        return -1;
    }
    public function campaignHtml($html)
    {
        if (substr_count($html,"<html>") == 0)
        return  "<html>".$html."</html>";    
        else 
        return $html;
    }
    public function calculateCampaignid($campaign)
    {
        $repository = $this->controller->getDoctrine()->getRepository('classesclassBundle:plans');
        $plan = $repository->findOneBy(array('id' => $campaign->planid));
        return $plan->planid."_".$plan->partnerid."_".$campaign->gacampaignid;
    }
    public function addCampaignId(&$params,&$campaign)
    {
        $campaignString = "campaignid=".$this->calculateCampaignid($campaign);
        $params['html'] = str_replace($campaignString,"",$params['html']);
        $params['html'] = str_replace("smartplanid=",$campaignString."&"."smartplanid=",$params['html']);
    }
    public function createList($params)
    {        
        $emailHash = array();
        if (isset($params['campaignid']) && isset($params['emails']) && count($params['emails']) > 0 )
        {
            
            $repository = $this->controller->getDoctrine()->getRepository('classesclassBundle:campaigns');
            $currentCampaign = $repository->findOneBy(array('id' => $params['campaignid']));
            $this->clearList($params,$currentCampaign);
            

            foreach ($params['emails'] as &$email)
            {
                $email['emailAddress'] = trim($email['emailAddress']);
                
                if ($email['emailAddress'] != "" && !isset($emailHash[$email['emailAddress']]))
                {
                    $repository = $this->controller->getDoctrine()->getRepository('classesclassBundle:campaignsLists');
                    $contact = $repository->findOneBy(array('emailAddress' => trim(str_replace(" ","",$email['emailAddress']))));
                    $contactAdded = false;
                    if ($contact == null)
                    {
                        $contact = new campaignsLists();
                        $contactAdded = true;
                    }
                    foreach ($email as $key => $value)
                    {
                        $contact->$key = $value;
                    }
                    if ($contactAdded) $this->em->persist($contact);
                    $contact->campaigns = str_replace("-" . $params['campaignid'] . "-", "", $contact->campaigns);
                    $contact->campaigns = $contact->campaigns . "-" . $params['campaignid'] . "-";
                    $emailHash[$email['emailAddress']] = 1;
                }
            }
            if (isset($contact))
            {
                $campaignidsFilter = str_replace("--", ",", $contact->campaigns);
                $campaignidsFilter = str_replace("-", "", $campaignidsFilter);
                $campaignidsExplode = explode(",", $campaignidsFilter);
                $campaignidString = "";
                foreach ($campaignidsExplode as $campaignid)
                {
                    $repository = $this->controller->getDoctrine()->getRepository('classesclassBundle:campaigns');
                    $campaign = $repository->findOneBy(array('id' => $campaignid));
                    if ($campaign != null && $campaign->mailEngineCampaignid != "")
                    {
                        $campaignidString = $campaignidString . "-" . $campaign->mailEngineCampaignid . "-";
                    }
                }

                $this->mailObject->createList($params,$contact,$currentCampaign,$campaignidString);
                $this->em->flush();
                return 1;
            }
        }
      
        return -1;
    }
    public function clearList($params,$currentCampaign)
    {
        $sql = "SELECT * FROM campaignsLists WHERE campaigns LIKE '%-".$currentCampaign->id."-%'";
        $users = $this->dbal->fetchAll($sql);
        
        $this->mailObject->clearList($params,$currentCampaign,$users);
        
        $sql = "UPDATE campaignsLists set campaigns =  REPLACE(campaigns,'-".$currentCampaign->id."-','');";
        $this->dbal->executeQuery($sql); 
    }
    public function sendCampaign($params)
    {
        $return = -1;
        $scheduled = false;
        $notifyEmail = "";
        $currentTime =  new \DateTime('now');
        if (isset($params['id']))
        {
            $repository = $this->controller->getDoctrine()->getRepository('classesclassBundle:campaigns');
            $campaign = $repository->findOneBy(array('id' => $params['id']));
            $repository = $this->controller->getDoctrine()->getRepository('classesclassBundle:plans');
            $plan = $repository->findOneBy(array('id' => $campaign->planid));              
            $send['html'] = "Details:<br/><br/>"
                    ."<b>Plan Name</b>: ".$plan->name
                    ."<br/><b>Smartplanid</b>: ".$plan->id
                    ."<br/><b>Userid</b>: ".$plan->userid
                    ."<br/><b>Campaign Title</b>: ".$campaign->title
                    ."<br/><b>Sent date</b>: ".$currentTime->format("Y-m-d H:i:s");
            $send['id'] = $campaign->id;
                    
            if (!isset($params['scheduledTime']))
            {
                $return = $this->mailObject->sendCampaign($params,$campaign);
                $campaign->sent = $return;
                $currenTime = new \DateTime('now');
                $params['scheduledTime'] = $currenTime->format("Y-m-d H:i:s");
                if ($return)
                {
                    $repository = $this->controller->getDoctrine()->getRepository('classesclassBundle:sponsorConnectRequests');
                    $sponsorConnectRequests = $repository->findOneBy(array('campaignid' => $params['id']));
                    if ($sponsorConnectRequests != null)
                    {
                        $sponsorConnectRequests->requestStatus = "done";
                        $repository = $this->controller->getDoctrine()->getRepository('classesclassBundle:plans');
                        $plan = $repository->findOneBy(array('id' => $sponsorConnectRequests->planid));
                        if ($sponsorConnectRequests->notifiyEmailAddress != "")
                        {
//                            $message = \Swift_Message::newInstance()
//                            ->setSubject("Notification of SmartConnect delievery")
//                            ->setFrom('info@smartplanenterprise.com')
//                            ->setTo($sponsorConnectRequests->notifiyEmailAddress)
//                            ->setBody("<h3>Plan: ".$plan->name."</h3>".
//                            '<h3>Userid: '.$plan->userid.'</h3>'.
//                             '<h3>Subject: '.$campaign->subject.'</h3>','text/html');
//                            $this->controller->get('mailer')->send($message);
                            $notifyEmail = $sponsorConnectRequests->notifiyEmailAddress;
                        }
                    }
                }
            }
            else
            {
                $scheduled =true;
                $return = 1;
            }
            $campaign->sentDate = $params['scheduledTime'];
            $this->em->flush();      
        }
        else
        {
            $send['title'] = "Campaign does not exist";
            $send['html'] = " ";
            $this->emailToCommunicationSpecialist($send, $notifyEmail);
            return $return;
        }
        if ($return == 1)
        $send['title'] = "Campaign has successfully Sent";
        
        else
        $send['title'] = "Campaign has failed to  Send";    
        if (!$scheduled)
        $this->emailToCommunicationSpecialist($send, $notifyEmail);
        return $return;
    }
    public function getStats($params)
    {
        if (isset($params['id']))
        {
            $repository = $this->controller->getDoctrine()->getRepository('classesclassBundle:campaigns');
            $campaign = $repository->findOneBy(array('id' => $params['id']));
            $stats = $this->mailObject->getStats($campaign);
            return $stats;
        }
        return -1;
    }
    public function escapeXml($string)
    {
        return "<![CDATA[" . $string . "]]>";   
    }
    public function viewUsers($params)
    {
        $repository = $this->controller->getDoctrine()->getRepository('classesclassBundle:campaigns');
        $campaign = $repository->findOneBy(array('id' => $params['campaignid']));
        return $this->mailObject->viewUsers($params,$campaign);
    }
    public function cancelCampaign($params)
    {
        if (isset($params['id']))
        {
            $repository = $this->controller->getDoctrine()->getRepository('classesclassBundle:campaigns');
            $campaign = $repository->findOneBy(array('id' => $params['id']));
            if ($campaign->sentDate == "0000-00-00 00:00:00")
            return -1;
            $campaign->sentDate = "0000-00-00 00:00:00";
            $campaign->sent = 0;
            $campaign->mailEngineSendid = "";
            $this->em->flush();
            return 1;
        }
    }
    public function previewTemplate($params)
    {
        if (isset($params['id']))
        {
            $repository = $this->controller->getDoctrine()->getRepository('classesclassBundle:campaigns');
            $campaign = $repository->findOneBy(array('id' => $params['id']));
            return $this->mailObject->previewTemplate($params,$campaign);
        }
        return -1;
    }
    public function previewProperites($params)
    {
        if (isset($params['id']))
        {
            $repository = $this->controller->getDoctrine()->getRepository('classesclassBundle:campaigns');
            $campaign = $repository->findOneBy(array('id' => $params['id']));
            return $this->mailObject->previewProperites($params,$campaign);
        }     
        return -1;
    }
    public function campaignStatus($params)
    {
        if (isset($params['id']))
        {
            $repository = $this->controller->getDoctrine()->getRepository('classesclassBundle:campaigns');
            $campaign = $repository->findOneBy(array('id' => $params['id']));
            return $this->mailObject->campaignStatus($params,$campaign);
        }
        return -1;
    }
    public function emailToCommunicationSpecialist($params, $notifyEmail="")
    {
        if (isset($params['id']))
        {
            $repository = $this->controller->getDoctrine()->getRepository('classesclassBundle:campaigns');
            $campaign = $repository->findOneBy(array('id' => $params['id']));
            $repository = $this->controller->getDoctrine()->getRepository('classesclassBundle:accounts');
            $account = $repository->findOneBy(array('id' => $campaign->userid));
            
            $repository = $this->controller->getDoctrine()->getRepository('classesclassBundle:communicationSpecialist');
            $communicationSpecialist = $repository->findOneBy(array('id' => $campaign->communicationSpecialist));
            if ($communicationSpecialist == null)
            $communicationSpecialist = $repository->findOneBy(array('id' => $account->communicationSpecialist));                
            
            $message = \Swift_Message::newInstance()
            ->setSubject($params['title'])
            ->setFrom('info@smartplanenterprise.com')
            ->setTo($communicationSpecialist->emailAddress)
            ->setBody(
            $params['html'],'text/html');

            $this->controller->get('mailer')->send($message);

            //If user wants notify email send email again to notify email address
            if($notifyEmail != "")
            {
                $message->setTo($notifyEmail);
                $this->controller->get('mailer')->send($message);
            }
        }
        return -1;
    }
}
