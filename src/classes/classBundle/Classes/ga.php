<?php
namespace classes\classBundle\Classes;
use Plans\PlansBundle\Controller\ga\GoogleAnalyticsAPI;
class ga
{
    public $ga;
    public function __construct($accountid)
    {
        $ga = new GoogleAnalyticsAPI('service');
        $ga->auth->setClientId('891041475744-ttakqc55rr0d0k2so2likptb554bfck7.apps.googleusercontent.com');
        $ga->auth->setEmail('891041475744-ttakqc55rr0d0k2so2likptb554bfck7@developer.gserviceaccount.com');
        $ga->auth->setPrivateKey('../src/Plans/PlansBundle/Controller/ga/spe_analytics.p12');
        $auth = $ga->auth->getAccessToken();
        // Try to get the AccessToken
        if ($auth['http_code'] == 200) 
        {
            $accessToken  = $auth['access_token'];
            $tokenExpires = $auth['expires_in'];
            $tokenCreated = time();
        } 
        else 
        {
            echo "Session Expired.";
        }
        // Set the accessToken and Account-Id
        $ga->setAccessToken($accessToken);
        $ga->setAccountId('ga:'.$accountid);
        $this->ga = $ga;
    }
    public function query($params)
    {        
        return $this->ga->query($params);
    } 
    public function topBrowser($params)
    {     
        $params['metrics'] = "ga:sessions";
        $params['dimensions'] = "ga:browser";
        $params['sort'] = "-ga:sessions";
        $data = $this->query($params);
        if (isset($data['rows'][0][0]) && $data['rows'][0][0] != "(not set)")
        return $data['rows'][0][0];
        else
        return "N/A";
    }
    public function topCities($params)
    {
        $params['metrics'] = "ga:sessions";
        $params['dimensions'] = "ga:city";
        $params['sort'] = "-ga:sessions";
        $data = $this->query($params);
        if (array_key_exists('rows', $data)) 
        {
            $result = $data['rows'];
            $top    = array_slice($result, 0, 25);
            foreach($top as $place)
            $topc[] = $place[0];            
            $markup = implode(' / ',$topc);
        }
        else
        $markup = "N/A";       
        return $markup;
    }
    public function topMobileDevice($params)
    {
        $params['metrics'] = "ga:sessions";
        $params['dimensions'] = "ga:mobileDeviceInfo";
        $params['sort'] = "-ga:sessions";
        $data = $this->query($params);
        if (isset($data['rows'][0][0]) && $data['rows'][0][0] != "(not set)")
        return $data['rows'][0][0];
        else
        return "N/A";
    }
    public function totalVisits($params)
    {
        $params['metrics'] = "ga:visits";
        $data = $this->query($params);
        if (isset($data['rows'][0][0]))
        return $data['rows'][0][0];
        else
        return 0;
    }
    public function totalUniqueVisits($params)
    {
        $params['metrics'] = "ga:users";
        $data = $this->query($params);
        if (isset($data['rows'][0][0]))
        return $data['rows'][0][0];
        else
        return 0;
    }
    public function percentageOfUniqueVisits($params)
    {
        $uniqueVisits = $this->totalUniqueVisits($params);
        $totalVisits = $this->totalVisits($params);
        if ($totalVisits == 0)
        return 0;
        else
        return ($uniqueVisits/$totalVisits) * 100;
    }
    public function returningUsersPercent($params)
    {
        if ($this->totalVisits($params) == 0)
        return 0;
        return 100- $this->percentageOfUniqueVisits($params);
    }
}
