<?php
class constantcontact 
{
    public $mailEngine;
    public $clientID;
    public $clientSecret;
    public $accessToken;
    public $userID;
   
    public function __construct()
    {
    }
    public function init($mailEngine)
    {
        $this->mailEngine = $mailEngine;
        $this->clientID = $this->mailEngine->clientID;
        $this->clientSecret = $this->mailEngine->clientSecret;
        $this->accessToken = $this->mailEngine->accessToken;
        $this->userID = $this->mailEngine->userID;
        $this->headers = array(
            'Authorization: Bearer 0e338fdb-34d4-4678-9dfb-ec3cb203b6ec',
            'Content-Type:application/json'
        );
    }
    public function createCampaign(&$params,&$campaign,&$html,&$text)
    {
       $data = array();
       $data['name'] = $params['title'];
       $data['subject'] = $params['subject'];
       $data['from_name'] = $params['fromName'];
       $data['from_email'] = $params['fromEmail'];
       $data['reply_to_email'] = $params['fromEmail'];
       $data['email_content'] = '<html>'.$html.'</html>';
       $data['text_content'] = $text;
       $data['message_footer'] = array(
         "city" => "Aliso Viejo",
         "state" => "CA",
         "country" => "US",
         "organization_name" => "vWise, Inc.",
         "address_line_1" => "85 Enterprise",
         "address_line_2" => "Suite 320",
         "address_line_3" => "",
         "international_state" => "",
         "postal_code" => "92656",
         "include_forward_email" => true,
         "forward_email_link_text" => "Click here to forward this message",
         "include_subscribe_link" => true,
         "subscribe_link_text" => "Subscribe to Our Newsletter!"
       );
       $campaign->fromEmail = $params['fromEmail'];

       $json = json_encode($data);
       $url = "https://api.constantcontact.com/v2/emailmarketing/campaigns?api_key=".$this->clientID;
       $buf = $this->curlCall($url, $json, 'null', 'POST');
       $response = json_decode($buf);
       //print_r($response);
       $campaign->mailEngineCampaignid = $response->id;
    }
    public function updateCampaign(&$params,&$campaign)
    {
       $data = array();
       $data['name'] = $params['title'];
       $data['subject'] = $params['subject'];
       $data['from_name'] = $params['fromName'];
       $data['from_email'] = $params['fromEmail'];
       $data['reply_to_email'] = $params['fromEmail'];
       $data['email_content'] = '<html>'.$params['html'].'</html>';
       $data['text_content'] = 'body text';
       $data['message_footer'] = array(
         "city" => "Aliso Viejo",
         "state" => "CA",
         "country" => "US",
         "organization_name" => "vWise, Inc.",
         "address_line_1" => "85 Enterprise",
         "address_line_2" => "Suite 320",
         "address_line_3" => "",
         "international_state" => "",
         "postal_code" => "92656",
         "include_forward_email" => true,
         "forward_email_link_text" => "Click here to forward this message",
         "include_subscribe_link" => true,
         "subscribe_link_text" => "Subscribe to Our Newsletter!"
       );
       $campaign->fromEmail = $params['fromEmail'];

       $json = json_encode($data);
       $url = "https://api.constantcontact.com/v2/emailmarketing/campaigns/".$campaign->mailEngineCampaignid."?api_key=".$this->clientID;
       $buf = $this->curlCall($url, $json, 'null', 'PUT');
       $response = json_decode($buf);
       //print_r($response);
    }
    public function createList(&$params,&$contact,&$currentCampaign,&$campaignidString) 
    {
        $url = "https://api.constantcontact.com/v2/lists?api_key=".$this->clientID;
        $data = array();
        $data['name'] = (string)time();
        $data['status'] = "ACTIVE";
        $json = json_encode($data);
        $buf = $this->curlCall($url, $json, 'null', 'POST');
        $response = json_decode($buf);
        $currentCampaign->mailEngineListid = $response->id; 
        $listId = $response->id;
        $data3 = array();
        $data3["lists"] = array(array("id" => $listId));
        $url3 = "https://api.constantcontact.com/v2/contacts?api_key=".$this->clientID;
        foreach ($params['emails'] as $email) {
            if(!empty($email['emailAddress'])) {
                $data3["email_addresses"] = array(array("email_address" => $email['emailAddress']));
                $json3 = json_encode($data3);
                $buf3 = $this->curlCall($url3, $json3, 'null', 'POST');
                $response3 = json_decode($buf3);
                //print_r($response3);
            }
        }
        $idArray = array();
        foreach ($params['emails'] as $email) {
            if(!empty($email['emailAddress'])) {
                $url1 = "https://api.constantcontact.com/v2/contacts?email=".$email['emailAddress']."&api_key=".$this->clientID;
                $buf1 = $this->curlCall($url1, '', 'null', 'GET');
                $response1 = json_decode($buf1);
                //print_r($response1);
                foreach($response1 as $res) {
                    foreach($res as $val) {
                        foreach($val as $key => $val1) {
                            if($key == 'id') {
                                $id = $val1;
                            }
                            if($key == 'email_addresses') {
                                foreach($val1 as $val2) {
                                    $idArray[$val2->email_address] = $id;
                                }
                            }
                        }
                    }
                }
            }
        }
        //print_r($idArray); 
        $data1 = array();
        $data1["lists"] = array(array("id" => $listId));
        //$batch = array();
        foreach ($params['emails'] as $email) {
            if(!empty($email['emailAddress'])) {
               if(array_key_exists($email['emailAddress'], $idArray)) {
                    $url4 = "https://api.constantcontact.com/v2/contacts/".$idArray[$email['emailAddress']]."?api_key=".$this->clientID;
                    $data1["email_addresses"] = array(array("email_address" => $email['emailAddress']));
                    $json1 = json_encode($data1);
                    $buf1 = $this->curlCall($url4, $json1, 'null', 'PUT');
                    $response1 = json_decode($buf1);
                    //print_r($response1);
                    //echo $url4;
               }
            }
        }
        //print_r($idArray);
        $data2 = array();
        $data2['name'] = $currentCampaign->title;
        $data2['subject'] = $currentCampaign->subject;
        $data2['from_name'] = $currentCampaign->fromName;
        $data2['from_email'] = $currentCampaign->fromEmail;
        $data2['reply_to_email'] = $currentCampaign->fromEmail;
        $data2['email_content'] = '<html>'.$currentCampaign->html.'</html>';
        $data2['text_content'] = 'body text';
        $data2['sent_to_contact_lists'] = array(array("id" => $response->id));
        $data2['message_footer'] = array(
         "city" => "Aliso Viejo",
         "state" => "CA",
         "country" => "US",
         "organization_name" => "vWise, Inc.",
         "address_line_1" => "85 Enterprise",
         "address_line_2" => "Suite 320",
         "address_line_3" => "",
         "international_state" => "",
         "postal_code" => "92656",
         "include_forward_email" => true,
         "forward_email_link_text" => "Click here to forward this message",
         "include_subscribe_link" => true,
         "subscribe_link_text" => "Subscribe to Our Newsletter!"
       );

       $json2 = json_encode($data2);
       $url2 = "https://api.constantcontact.com/v2/emailmarketing/campaigns/".$currentCampaign->mailEngineCampaignid."?api_key=".$this->clientID;
       $buf2 = $this->curlCall($url2, $json2, 'null', 'PUT');
       $response2 = json_decode($buf2);
       //print_r($response2);
    }
    public function sendCampaign(&$params,&$campaign)
    {
        $url = "https://api.constantcontact.com/v2/emailmarketing/campaigns/".$campaign->mailEngineCampaignid."/schedules?api_key=".$this->clientID;
        $json = json_encode (new stdClass);
        $buf = $this->curlCall($url, $json, 'null', 'POST');
        $response = json_decode($buf);
        print_r($response);
    }
    public function clearList($params,$currentCampaign,$users)
    {
        
    }
    public function cancelCampaign($params,$campaign)
    {

    }
    public function getStats(&$campaign)
    {
        $delivered = 0;
        $unique_opens = 0;
        $unique_clicks = 0;
        $bounces = 0;
        $unsubscribes = 0;
        $url = "https://api.constantcontact.com/v2/emailmarketing/campaigns/".$campaign->mailEngineCampaignid."/tracking/reports/summary?api_key=".$this->clientID;
        $buf = $this->curlCall($url, '', 'null', 'GET');
        $response = json_decode($buf);
        $delivered = $response->sends;
        $unique_opens = $response->opens;
        $unique_clicks = $response->clicks;
        $bounces = $response->bounces;
        $unsubscribes = $response->unsubscribes;
        $emailStats = new \stdClass();
        $emailStats->opens = new \stdClass();
        $emailStats->clicks = new \stdClass();
        
        $emailStats->delivered = $delivered;
        $emailStats->opens->unique = $unique_opens;
        $emailStats->clicks->unique = $unique_clicks;
        $emailStats->bounces = $bounces;
        $emailStats->unsubscribes = $unsubscribes;

        return $emailStats;
    }
    public function viewUsers($params,&$campaign)
    {
        $users = array();
        $first_name = '';
        $last_name = '';
        $url = "https://api.constantcontact.com/v2/lists/".$campaign->mailEngineListid."/contacts?api_key=".$this->clientID;
        $buf = $this->curlCall($url, '', 'null', 'GET');
        $response = json_decode($buf);
        foreach($response as $res) {
            foreach($res as $val) {
                foreach($val as $key => $val1) {
                    if($key == 'email_addresses') {
                        foreach($val1 as $val2) {
                             $email = $val2->email_address;
                            $users[$email] = new \stdClass();
                            $users[$email]->email = $email;
                            $users[$email]->firstName = $first_name;
                            $users[$email]->lastName = $last_name;
                            if ($users[$email]->firstName instanceof \stdClass)
                            $users[$email]->firstName = "";
                            if ($users[$email]->lastName instanceof \stdClass)
                            $users[$email]->lastName = "";
                        }
                    }
                }
            }
        }
        
        return $users;
    }    
    public function previewTemplate($params,$campaign)
    {
        return $campaign->html;
    }
    public function previewProperites($params,$campaign)
    {
        $properties = new \stdClass();
        $properties->subject = $campaign->subject;
        $properties->fromName = $campaign->fromName;
        $properties->fromEmail = $campaign->fromEmail;
        return $properties;
    }
    public function curlCall($url, $data, $options = null, $method = 'POST')
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if($method == 'POST') {
            curl_setopt($ch, CURLOPT_POST, 1);
        }
        
        if ($method == 'PUT') {
            curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        }
        if ($method == 'DELETE') {
            curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        }
        if ($data != "")
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $buf = curl_exec($ch);
        curl_close($ch);
        return $buf;
    }     
}