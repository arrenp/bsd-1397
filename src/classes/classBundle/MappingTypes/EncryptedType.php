<?php

namespace classes\classBundle\MappingTypes;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Doctrine\DBAL\Types\Type;

class EncryptedType extends StringType {
    
    const ENCRYPTED = 'encrypted';
    const KEY = '5E6D217336';
    
    public function getName() {
        return self::ENCRYPTED;
    }
    
    public function canRequireSQLConversion() {
        return true;
    }
    
    public function convertToPHPValueSQL($sqlExpr, $platform) {
        return "IFNULL(AES_DECRYPT($sqlExpr, UNHEX('".self::KEY."')),'')";
    }
    
    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform) {
        return "AES_ENCRYPT($sqlExpr, UNHEX('".self::KEY."'))";
    }
}
