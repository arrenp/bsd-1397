<?php

namespace classes\classBundle\Services\HubSpot;

use classes\classBundle\Services\BaseService;
use HubSpot\HubSpotBundle\Services\HubSpotContactsService;
use HubSpot\HubSpotBundle\Services\HubSpotEmailService;
use HubSpot\HubSpotBundle\Services\HubSpotWorkflowService;

class HubSpotEmailResultsService extends BaseService {
    
    const RESULT_REPORT_CREATED = 1;
    const RESULT_ERROR_WORKFLOW_NOT_FOUND = 2;
    const RESULT_ERROR_NO_WORKFLOW_ACTIONS = 3;
    const RESULT_ERROR_NO_CAMPAIGN_ID = 4;
    const RESULT_ERROR_NO_CAMPAIGNS = 5;
    
    const EVENT_TYPE_SENT = "SENT";
    const EVENT_TYPE_BOUNCE = "BOUNCE";
    const EVENT_TYPE_DELIVERED = "DELIVERED";
    const EVENT_TYPE_CLICK = "CLICK";
    const EVENT_TYPE_OPEN = "OPEN";
    
    public function createReport($outputFilePath, $workflowId, $startDate = null, $endDate = null)
    {   
        /**
         * @var HubSpotContactsService
         */
        $hubSpotService = $this->container->get('hub_spot.contacts');

        /**
         * @var HubSpotEmailService
         */
        $hubSpotEmailService = $this->container->get('hub_spot.emails');

        /**
         * @var HubSpotWorkflowService
         */
        $hubSpotWorkflowService = $this->container->get('hub_spot.workflows');

        // check if workflowId provided is valid
        $validWorkflowId = $this->checkValidWorkflowId($workflowId);

        if (!$validWorkflowId) {
            return self::RESULT_ERROR_WORKFLOW_NOT_FOUND;
        }

        $workflow = $hubSpotWorkflowService->getWorkflow($workflowId);

        if (!array_key_exists('actions', $workflow)) {
            return self::RESULT_ERROR_NO_WORKFLOW_ACTIONS;
        }
        $emailCampaignIds = [];
        $actions = $workflow['actions'];
        foreach ($actions as $action) {
            if ($action['type'] == 'EMAIL') {
                if (!array_key_exists('emailCampaignId', $action)) {
                    return self::RESULT_ERROR_NO_CAMPAIGN_ID;
                }
                $emailCampaignIds[] = $action['emailCampaignId'];
            }
        }
        
        $emailCampaignIds = array_unique($emailCampaignIds);
        
        if (count($emailCampaignIds) === 0) {
            return self::RESULT_ERROR_NO_CAMPAIGNS;
        }

        if (!$endDate) {
            $endDate = date('m/d/Y');
        }
        $endDate .= ' 08:00:00 GMT-0700';

        if (!$startDate) {
            $startDate = date('m/d/Y', strtotime($endDate . ' - 1 days'));
        }
        $startDate .= ' 08:00:00 GMT-0700';

        $endTimestamp = strtotime($endDate) * 1000;
        $startTimestamp = strtotime($startDate) * 1000;
        
        $events = [];
        $types = [self::EVENT_TYPE_SENT, self::EVENT_TYPE_DELIVERED, self::EVENT_TYPE_OPEN, self::EVENT_TYPE_CLICK, self::EVENT_TYPE_BOUNCE];
        foreach ($emailCampaignIds as $emailCampaignId) {
            foreach ($types as $type) {
                $newEvents = $hubSpotEmailService->getEmailEvents(null, $emailCampaignId, null, $type, $startTimestamp, $endTimestamp);
                if ($newEvents !== false) {
                    $events = array_merge($events, $newEvents);
                }
            }
        }
        
        $stats = [];
        foreach ($events as $key => $event) {
            if (in_array($event['type'], $types)) {
                $email = $event['recipient'];
                if (!isset($stats[$email][$event['type']])) {
                    $stats[$email][$event['type']] = 0;
                }
                $stats[$email][$event['type']]++;
            }
            unset($events[$key]);
        } 
        $emails = array_keys($stats);

        $members = [];
        // HubSpot can only handle requests for 100 records at a time
        // but googling php array chunk pays off sometimes
        $emailChunks = array_chunk($emails, 100);
        foreach ($emailChunks as $key => $emailChunk) {
            $query = array(
                'formSubmissionMode' => 'none',
                'property' => array(
                    'email',
                    'firstname',
                    'lastname',
                    'participant_id',
                    'plan_id',
                    'partner_id',
                    'status_code',
                    'hubspot_test_last_email_send_date'
                )
            );
            $chunkMembers = $hubSpotService->getGroupByEmail($emailChunk, $query);
            $members = array_merge($members, $chunkMembers);
        }
        
        $data = [];
        if (count($members) > 0) {
            foreach ($members as $key => $member) {
                $properties = $member['properties'];
                $email = $this->getValue('email', $properties);
                
                $data[] = array(
                    $this->getValue('partner_id', $properties),
                    $this->getValue('plan_id', $properties),
                    $email,
                    $this->getValue('firstname', $properties),
                    $this->getValue('lastname', $properties),
                    $this->getValue('status_code', $properties),
                    $this->getStat($email, self::EVENT_TYPE_SENT, $stats),
                    $this->getStat($email, self::EVENT_TYPE_DELIVERED, $stats),
                    $this->getStat($email, self::EVENT_TYPE_OPEN, $stats),
                    $this->getStat($email, self::EVENT_TYPE_CLICK, $stats),
                    $this->getStat($email, self::EVENT_TYPE_BOUNCE, $stats),
                );
                
                // Free up memory
                unset($members[$key]);
            }
            // Sort by planid, then email
            $plans  = array_column($data, 1);
            $emails = array_column($data, 2);
            array_multisort($plans, $emails, $data);
        }
        
        // Write to CSV
        $handle = fopen($outputFilePath, 'w');
        fputcsv($handle, [
            'Partner Id', 
            'Plan Id', 
            'Email', 
            'First Name', 
            'Last Name', 
            'Status Code', 
            'Emails Sent', 
            'Emails Delivered', 
            'Emails Opened', 
            'Emails Clicked', 
            'Emails Bounced'
        ]);
        foreach ($data as $row) {
            fputcsv($handle, $row);
        }
        fclose($handle);
        return self::RESULT_REPORT_CREATED;
    }
    
    public function getValue($name, $properties) {
        $value = null;
        if (array_key_exists($name, $properties) && array_key_exists('value', $properties[$name])) {
            $value = $properties[$name]['value'];
        }
        return $value;
    }

    private function checkValidWorkflowId($workflowId) {
        $hubSpotWorkflowService = $this->container->get('hub_spot.workflows');
        $workflows = $hubSpotWorkflowService->getWorkflows();

        foreach ($workflows as $workflow) {
            if ($workflow['id'] == $workflowId) {
                return true;
            }
        }
        return false;
    }
    
    private function getStat($email, $type, &$stats) {
        return isset($stats[$email][$type]) ? $stats[$email][$type] : 0;
    }
}
