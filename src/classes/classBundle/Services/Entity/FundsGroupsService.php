<?php
namespace classes\classBundle\Services\Entity;
class FundsGroupsService extends BaseEntityService
{
    public function init()
    {
        $this->entityName = "FundsGroups";
    }   
    public function add($array)
    {
        $entity = new $this->classPathAdd;
        $this->copy($entity,$array);
        $lastEntity = $this->findLast(array("deleted" => 0));
        $entity->orderid = $lastEntity->orderid + 1;
        $this->persist($entity);
        $this->flush();
    }
    public function getList()
    {
        return $this->findBy(array('deleted' => 0),array("orderid" => "asc"));
    }
}