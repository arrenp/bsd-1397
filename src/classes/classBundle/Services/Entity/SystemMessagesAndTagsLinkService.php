<?php
namespace classes\classBundle\Services\Entity;
class SystemMessagesAndTagsLinkService extends BaseEntityService
{
    public function init()
    {
        $this->entityName = "SystemMessagesAndTagsLink";
    }      
    public function addLinks($message,$tags)
    {
        $links = array();
        foreach ($tags as $tag)
        {
            $link['systemMessageId'] = $message->id;
            $link['systemMessageTagsId'] = $tag->id;
            $links[] = $this->add($link);
        }
        return $links;
    }
}
