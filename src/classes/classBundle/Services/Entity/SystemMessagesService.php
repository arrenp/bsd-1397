<?php
namespace classes\classBundle\Services\Entity;
class SystemMessagesService extends BaseEntityService
{
    public function init()
    {
        $this->entityName = "SystemMessages";
    }
    public function deleteMessage($messageid)
    {
        $message = $this->findOneBy(array("id" => $messageid));
        $this->em->remove($message);
        $links = $this->container->get("SystemMessagesAndTagsLinkService")->findBy(array("systemMessageId" => $messageid));
        foreach ($links as $link)
        {
            $this->em->remove($link);
        }
        $this->em->flush();        
    } 
    public function dataTablesQuery($request,$count,$id = null)
    {
        $sqlParams['base'] = "SELECT *,sm.id as smid ";  
        if ($id != null)
        {
            $sqlParams['base'] = $sqlParams['base']." ,ausm.id as ausmid ";
        }       
        $sqlParams['base'] = $sqlParams['base']." FROM SystemMessages sm ";
        if ($count)
        {
            $sqlParams['base'] = "SELECT COUNT(DISTINCT(sm.id)) as count FROM SystemMessages sm";  
        }
        $sqlParams['join'] = " LEFT JOIN SystemMessagesAndTagsLink smlink ON sm.id = smlink.systemMessageId LEFT JOIN SystemMessagesTags smtags ON smtags.id = smlink.systemMessageTagsId ";
        {
            if ($id != null)
            {
                $sqlParams['join'] = $sqlParams['join']." LEFT JOIN AccountsUsersSystemMessages  ausm ON ausm.systemMessageId = sm.id ";
            }
        }
        $sqlParams['limit']= " LIMIT :start,:length ";  
        $sqlParams['where'] = " WHERE sm.id > 0 ";
        $sqlParams['search'] = " AND (sm.message LIKE :search OR smtags.name LIKE :search) ";
        $sqlParams['groupby'] = " GROUP BY sm.id ";
        $order = explode(",",$request->request->get("types"))[$request->request->get("order")[0]['column']];  
        $sqlParams['orderby'] = " ORDER BY ".$order." ".$request->request->get("order")[0]['dir']." ";
        $sql = $sqlParams['base'].$sqlParams['join'];
        if ($request->request->get("search")['value'] != "")
        {
            $sqlParams['where'] = $sqlParams['where'].$sqlParams['search'];
        }
        if ($id != null)
        {
            $sqlParams['where'] = $sqlParams['where']." AND ausm.accountsUsersId = :accountsUsersId AND ausm.deleted = 0 AND sm.expirationDate > :currentDate";
        }
        $sql = $sql.$sqlParams['where'];
        if (!$count)
        {
            $sql = $sql.$sqlParams['groupby'];
            if (in_array($order,array("sm.id","sm.message","MAX(smtags.name)","sm.expirationDate")) && in_array($request->request->get("order")[0]['dir'],array("asc","desc")))
            {
                $sql = $sql.$sqlParams['orderby'];
            }
            $sql = $sql.$sqlParams['limit'];
        }
        $systemMessagesQuery = $this->em->getConnection()->prepare($sql);
        if (!$count)
        {
            $systemMessagesQuery->bindValue("start",  (int)$request->request->get("start"),\PDO::PARAM_INT);
            $systemMessagesQuery->bindValue("length", (int)$request->request->get("length"),\PDO::PARAM_INT);
        }
        if ($request->request->get("search")['value'] != "")
        {
            $systemMessagesQuery->bindValue("search",  "%".$request->request->get("search")['value']."%",\PDO::PARAM_STR);
        }
        if ($id != null)
        {
            $systemMessagesQuery->bindValue("accountsUsersId",$id);
            $systemMessagesQuery->bindValue("currentDate",date("Y-m-d H:i:s"));
        }
        $systemMessagesQuery->execute();       
        return $systemMessagesQuery;        
    }
    public function dataTablesTotalCount()
    {
        $sqlCount = "SELECT COUNT(id) as count FROM SystemMessages";
        $count = $this->connection->executeQuery($sqlCount)->fetch()['count'];
        return $count;
    }
    public function dataTablesMessage($message)
    {
        if ($message != substr($message,0,50))
        {
            return substr($message,0,50)."...";
        }
        return $message;
    }
}


