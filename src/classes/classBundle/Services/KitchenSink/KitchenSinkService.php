<?php
namespace classes\classBundle\Services\KitchenSink;
use classes\classBundle\Services\BaseService;
class KitchenSinkService extends BaseService
{
    private $planids;
    private $lastId;
    private $finished;
    private $totalResults;
    private $resultLimit = 10;
    private $recordkeeperPlanids;
    public function setPlanId($planid)
    {
        $this->planids = [];
        $this->planids[] = $planid;        
        $this->recordkeeperPlanids[$planid] = $this->em->getRepository($this->classPath . "plans")->findOneBy(["id" => $planid])->planid;
        $this->finished = true;
    }
    public function getFinished()
    {
        return $this->finished;
    }
    public function getRecordKeeperPlanid($planid)
    {
        return $this->recordkeeperPlanids[$planid];
    }
    public function getPercent($pagenumber)
    {
       $percent =  (($pagenumber * $this->resultLimit)/$this->totalResults) * 100;
       if ($percent > 100)
       {
           $percent = 100;
       }  
       return round($percent);
    }
    public function addWhereQueryBuilder($queryBuilder,$userid,$deleted)
    {
        $queryBuilder->where('plans.userid = :userid');
        $queryBuilder->setParameter('userid', $userid);
        if (!$deleted)
        {
            $queryBuilder->andWhere("plans.deleted = 0");
        }
        $queryBuilder->andWhere("plans.smartEnrollAllowed=1");
        $queryBuilder->andWhere("plans.smartPlanAllowed=1");
        return $queryBuilder;
    }
    public function setPlans($userid,$pagenumber,$deleted)
    {
        $em = $this->em->getManager();
        $queryBuilder = $em->createQueryBuilder();        
        $queryBuilder->from($this->classPath.'plans', 'plans');
        $queryBuilder = $this->addWhereQueryBuilder($queryBuilder, $userid, $deleted);
        $queryBuilderTotalResults = clone $queryBuilder;
        $queryBuilderTotalResults->select("COUNT(plans.id)");
        $queryBuilder->select('plans.id,plans.planid');
        $queryBuilderLastId = clone $queryBuilder;        
        $queryBuilder->setMaxResults($this->resultLimit);
        $queryBuilder->setFirstResult(($this->resultLimit * ($pagenumber-1)));
        $queryBuilder->orderBy("plans.id","asc");
        $rows = $queryBuilder->getQuery()->getScalarResult();
        $this->planids = [];
        foreach ($rows as $row)
        {
            $this->planids[] = $row['id'];
            $this->recordkeeperPlanids[$row['id']] = $row['planid'];
            $lastId = $row['id'];
        }        
        $queryBuilderLastId->setMaxResults(1);
        $queryBuilderLastId->orderBy("plans.id","desc");
        $this->lastId =  $queryBuilderLastId->getQuery()->getScalarResult()[0]['id'];
        $this->finished = $this->lastId == $lastId;
        $this->totalResults = $queryBuilderTotalResults->getQuery()->getSingleScalarResult();
    }
    public function getPlanRowDoctrine($table,$params = ['field' => "planid"])
    {
        $em = $this->em->getManager();
        $repository = $em->getRepository($this->classPath.$table);
        $row = $repository->findOneBy(array($params['field'] => $this->planids));  
        return $row;
    }  
    public function getPlanRowsDoctrine($table,$params = ['field' => "planid"])
    {
        $em = $this->em->getManager();
        $repository = $em->getRepository($this->classPath.$table);
        $row = $repository->findBy(array($params['field'] => $this->planids));  
        return $row;
    }
    public function convertDoctrineRowToArray($row)
    {
        $return = [];
        foreach($row as $key => $value)
        {
            if (!is_array($value))
            {
                try
                {
                    if ($value instanceof \DateTime && !empty($value))//twig doesn't have default print for DateTime ree
                    {
                        $value = $value->format('Y-m-d H:i:s');
                        if (strtotime($value) < 0)
                        {
                            $value = "";
                        }
                    }
                    $return[$key] = (string)$value;                
                } 
                catch (\Exception $ex) 
                {

                }
            }                       
        }
        return $return;        
    }   
    public function getPlanRowDoctrineArray($table,$params = ['field' => "planid"])
    {
        $row = $this->getPlanRowDoctrine($table,$params);
        return $this->convertDoctrineRowToArray($row);
    }
    public function getPlanRowsDoctrineArray($table,$params = ['field' => "planid"])
    {
        $rows = $this->getPlanRowsDoctrine($table,$params);
        $return = [];
        foreach ($rows as $row)
        {
            $return[] = $this->convertDoctrineRowToArray($row);
        }
        return $return;
    }  
    public function portfoliosWithFunds()
    {
        $em = $this->em->getManager();
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->select('plansPortfolioFunds','plansPortfolios','plansFunds');     
        $queryBuilder->from($this->classPath.'plansPortfolioFunds', 'plansPortfolioFunds');
        $queryBuilder->where('plansPortfolioFunds.planid in (:planids)');
        $queryBuilder->leftJoin(
            $this->classPath.'plansPortfolios',
            'plansPortfolios',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'plansPortfolios.id = plansPortfolioFunds.portfolioid'
        );
        $queryBuilder->leftJoin(
            'classesclassBundle:plansFunds',
            'plansFunds',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'plansFunds.id = plansPortfolioFunds.fundid'
        );        
        $queryBuilder->setParameter('planids', $this->planids);
        return $queryBuilder->getQuery()->getScalarResult();
    }
    public function plansMessagingData()
    {
        $em = $this->em->getManager();
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->select('PlansMessages', 'MessagesPanes','MessagesPanesGroups');
        $queryBuilder->from($this->classPath.'PlansMessages', 'PlansMessages');
        $queryBuilder->leftJoin(
            $this->classPath.'MessagesPanes',
            'MessagesPanes',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'PlansMessages.messagesPanesId = MessagesPanes.id'
        );
        $queryBuilder->leftJoin(
            $this->classPath.'MessagesPanesGroups',
            'MessagesPanesGroups',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'MessagesPanes.messagesPanesGroupsId = MessagesPanesGroups.id'
        );
        $queryBuilder->where('PlansMessages.planid in (:planids)');
        $queryBuilder->setParameter('planids', $this->planids);
        return  $queryBuilder->getQuery()->getScalarResult();        
    }
    public function plansVideosData()
    {
        $em = $this->em->getManager();
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->select('PlansLibraryUsers','PlansLibraryVideos','PlansLibraryDirectories');
        $queryBuilder->from($this->classPath.'PlansLibraryUsers', 'PlansLibraryUsers');
        $queryBuilder->leftJoin(
            $this->classPath.'PlansLibraryVideos',
            'PlansLibraryVideos',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'PlansLibraryVideos.id = PlansLibraryUsers.videoid'
        );
        $queryBuilder->leftJoin(
            $this->classPath.'PlansLibraryDirectories',
            'PlansLibraryDirectories',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'PlansLibraryDirectories.id = PlansLibraryVideos.directoryid'
        );
        $queryBuilder->where('PlansLibraryUsers.planid in (:planid)');
        $queryBuilder->setParameter('planid', $this->planids);
        return  $queryBuilder->getQuery()->getScalarResult();        
    }
}