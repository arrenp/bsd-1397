<?php
namespace classes\classBundle\Services\AmeritasCustom;

use classes\classBundle\Services\BaseService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sessions\AdminBundle\Classes\adminsession;
use classes\classBundle\Classes\ga;
//use Permissions\RolesBundle\Classes\rolesPermissions;
//use Plans\PlansBundle\Controller\ga\GoogleAnalyticsAPI;
ini_set("memory_limit","2000M");
set_time_limit(0);

class UserRolloverActivityReportService extends BaseReportService
{

    public function getProfiles($account, $start, $end) {
        $sql = "
select 
  AES_DECRYPT(participants.firstName, '^m!s6') as firstName, 
  AES_DECRYPT(participants.lastName, '^m!s6') as lastName, 
  plans.planid, 
  profiles.callUs, 
  profiles.callMe, 
  profiles.emailMe 
from 
  profiles
left join
  plans on plans.id = profiles.planid 
left join 
  participants on profiles.participantid = participants.id 
where 
  profiles.reportDate is not null 
  and profiles.userid = '". $account . "' 
  and reportDate >= '" . $start . "' and reportDate <= '" . $end . "';
  ";

        $result = $this->connection->executeQuery($sql)->fetchAll();
        $arrayOfProfiles = array();

        foreach($result as $row) {
            $rowArr = array();
            $userSelection = null;
            foreach ($row as $key => $value) {
                $rowArr[] = $value;
                if ($userSelection == null) {
                    $userSelection = $this->checkUserSelection($key, $value);
                }
            }

            $rowArr[] = $userSelection;
            $arrayOfProfiles[] = $rowArr;
        }
        return $arrayOfProfiles;
    }
    
    public function getProfilesFromHubSpotList($listId, $start, $end) {
        $data = [];
        
        /* @var $contactListService \HubSpot\HubSpotBundle\Services\HubSpotContactListsService */
        $contactListService = $this->container->get("hub_spot.contactlists");
        $startAsTimestamp = strtotime($start) * 1000;
        $endAsTimestamp = strtotime($end) * 1000;
        $options = [
            'count' => 100,
            'property' => ['firstname','lastname','email','email_me_amt','phone_number_amt'], 
            'propertyMode' => 'value_and_history'
        ];
        do {
            $result = $contactListService->getContactsInAList($listId, $options);

            foreach ($result['contacts'] as $contact) { 

                # This logic assumes last updated field is the user's selected rollover option for [emailMe|callMe]
                $lastTimestamp = 0;
                foreach (['email_me_amt','phone_number_amt'] as $field) {
                    if (isset($contact['properties'][$field])) {
                        foreach ($contact['properties'][$field]['versions'] as $version) {
                            if ($version['timestamp'] > $lastTimestamp) {
                                if ($field === "email_me_amt" && $version['value'] === "true") {
                                    $emailMe = $contact['properties']['email']['value'];
                                    $callMe = '';
                                    $lastTimestamp = $version['timestamp'];
                                    $userSelection = "emailMe";
                                }
                                if ($field === "phone_number_amt") {
                                    $emailMe = '';
                                    $callMe = $version['value'];
                                    $lastTimestamp = $version['timestamp'];
                                    $userSelection = "callMe";
                                }
                            }
                        }
                    }
                }

                if ($lastTimestamp >= $startAsTimestamp && $lastTimestamp <= $endAsTimestamp) {
                    $data[] = [
                        $contact['properties']['firstname']['value'],
                        $contact['properties']['lastname']['value'],
                        '',
                        '',
                        $callMe,
                        $emailMe,
                        $userSelection
                    ];
                }
            }
            $options['vidOffset'] = $result['vid-offset'];
        } while ($result['has-more'] === true);
        
        return $data;
    }

    private function checkUserSelection($key, $value) {
        $userSelection = null;
        $checkColumns = array('callUs', 'callMe', 'emailMe');
        $nonValues = array('0', '', null);

        if (in_array($key, $checkColumns)) {
            if (!in_array($value, $nonValues)) {
                $userSelection = $key;
            }
        }
        return $userSelection;
    }

}
