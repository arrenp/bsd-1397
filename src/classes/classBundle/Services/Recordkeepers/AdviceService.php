<?php
namespace classes\classBundle\Services\Recordkeepers;
use classes\classBundle\Services\BaseService;
use classes\classBundle\Entity\recordkeeperAdviceFunds;
use classes\classBundle\Classes\mailgunEngine;
class AdviceService extends BaseService
{
    public function getFunds($userid,$planid)
    {
        $account = $this->container->get("AccountsService")->findOneBy(["id" => $userid]);
        $connectionType = strtolower($account->connectionType);

        if (in_array($connectionType,["srt","srt2"]))
        {          
            return $this->container->get("SRTServiceAdmin")->advicelist($planid,$userid);
        }
        else if (in_array($connectionType,["relius"]))
        {
            return $this->container->get("ReliusService")->advicelist($planid,$userid);
        }
        return [];
    }    
    public function getFundsHashed($userid,$planid)
    {
        $hashedFunds = [];
        $funds = $this->getFunds($userid,$planid);
        foreach($funds as $fund)
        {
            $hashedFunds[$fund->ticker] = $fund;
        }
        return $hashedFunds;
    }
    public function getRecordkeeperAdviceFundsHashed($smartplanid)
    {
        $hashedFunds = [];
        $recordkeeperAdviceFunds = $this->container->get("RecordkeeperAdviceFundsService")->findBy(["planid" => $smartplanid]);
        foreach ($recordkeeperAdviceFunds as $fund)
        {
            $hashedFunds[$fund->ticker] = $fund;
        }
        return $hashedFunds;
    }
    public function populateAndTagFunds($userid,$planid)
    {
        $plan = $this->em->getRepository($this->classPath."plans")->findOneBy(["planid" => $planid,"userid" => $userid, "deleted" => 0]);
        $recordkeeperFunds  = $this->getFundsHashed($userid,$planid);
        $recordkeeperAdviceFunds = $this->getRecordkeeperAdviceFundsHashed($plan->id);      
        $presence = ["NEW" => "NEW","CURRENT" => "CURRENT", "OLD" => "OLD"];
        $oldDetected = false;
        $em = $this->em->getManager();
        if (empty($recordkeeperAdviceFunds))
        {
            $presence["NEW"] =  "CURRENT";
        }
        foreach ($recordkeeperFunds as $recordkeeperFund)
        {
            if (!isset($recordkeeperAdviceFunds[$recordkeeperFund->ticker]))
            {
                $newFund =  new recordkeeperAdviceFunds();
                $newFund->name = (string)$recordkeeperFund->name;
                $newFund->ticker = (string)$recordkeeperFund->ticker;
                $newFund->cusip = (string)$recordkeeperFund->cusip;
                $newFund->planid = $plan->id;
                $newFund->userid = $userid;
                $newFund->presence = $presence["NEW"];
                $em->persist($newFund);
            }
        }
        foreach ($recordkeeperAdviceFunds as &$recordkeeperAdviceFund)
        {
            if (!isset($recordkeeperFunds[$recordkeeperAdviceFund->ticker]))
            {
                $recordkeeperAdviceFund->presence = $presence["OLD"];
                $oldDetected = true;
            }            
        }
        if ($oldDetected)
        {
            $plan->adviceStatus = 2;
        }
        $em->flush();
    }
    public function populateAndTagFundsAll()
    {
        ini_set('memory_limit','512M');
        $plans = $this->em->getRepository($this->classPath."plans")->findBy(["adviceStatus" => [1,2],"deleted" => 0]);
        foreach ($plans as $plan)
        {
            try
            {
                $this->populateAndTagFunds($plan->userid,$plan->planid);
                
            } 
            catch (\Exception $ex)
            {
                
            }
        }
    }

    public function createHTML($new, $old, $firstName, $lastName,$partnerid,$company,$planid,$name) {
        $oldDetected = false;
        if (!empty($old)) {
            $oldDetected = true;
        }

        $text =  $this->container->get('twig')->render('classesclassBundle:Advice:emailTemplate.html.twig',
            array('firstName' => $firstName, 'lastName' => $lastName, 'old' => $old, 'new' => $new, 'oldDetected' => $oldDetected,"company" => $company,"partnerid" => $partnerid, "planid" => $planid,"name" => $name));

        return $text;
    }

    public function sendEmail($changedFunds, $email, $firstName, $lastName,$partnerid,$company,$planid,$name)
    {
        $new = array();
        $old = array();
        foreach ($changedFunds as $funds) {
            if ($funds->presence == "NEW") {
                $new[] = $funds->ticker;
            } else {
                $old[] = $funds->ticker;
            }
        }

        $html = $this->createHTML($new, $old, $firstName, $lastName,$partnerid,$company,$planid,$name);

        $params['subject'] = "The funds associated with planid $planid have changed";
        $params['to'] = $email;
        $params['html'] = $html;
        $params['from'] = "info@smartplanenterprise.com";
        $email = new mailgunEngine();
        $email->sendEmail($params);
    }

    public function notifyRecipientsOfChanges() {
        $repo = $this->em->getRepository('classesclassBundle:PlansAdviceNotificationRecipients');
        $qb = $repo->createQueryBuilder("p");
        $qb->select("p.planid, p.email, p.firstName, p.lastName,plans.planid as smartplanid,accounts.company,accounts.partnerid,plans.name");
        $qb->leftJoin(
            $this->classPath.'plans',
            'plans',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'p.planid = plans.id'
        );        
        $qb->leftJoin(
            $this->classPath.'accounts',
            'accounts',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'plans.userid = accounts.id'
        );                     
        $planids =$qb->distinct(true)->getQuery()->getResult();
           
           
        foreach ($planids as $item) {
            $repo = $this->em->getRepository('classesclassBundle:recordkeeperAdviceFunds');
            $changedFunds = $repo->findBy(array("presence" => ['NEW', 'OLD'], "planid" => $item['planid']));

            if (!empty($changedFunds)) {
                $this->sendEmail($changedFunds, $item['email'], $item['firstName'], $item['lastName'],$item['partnerid'],$item['company'],$item['smartplanid'],$item['name']);
            }
        }
    }
}