<?php

namespace classes\classBundle\Services\Recordkeepers;

use classes\classBundle\Services\BaseService;

class ReliusService extends BaseService {
    
    public function advicelist($planid, $userid)
    {
        $Soap = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $this->plandataXML($planid, $userid));
        $Soap = new \SimpleXMLElement($Soap);
        $PlanInvestProd = $Soap->soapBody->GetPlanDataResponse->a_objPlanDataDS->diffgrdiffgram->PlanData->PlanInvest;
        $i = 0;
        foreach ($PlanInvestProd as $data)
        {
            $da = (array) $data;
            foreach ($da as $name => $value)
            {
                if (isset($name) && isset($value) && $name == "DESCRIPTION")
                {
                    $riskBasedFunds[$i]->name = $value;
                }
                if (isset($name) && isset($value) && $name == "FUNDID")
                {
                    $riskBasedFunds[$i]->ticker = $value;
                }

                if (isset($name) && isset($value) && $name == "CUSIPNUM")
                {
                    $riskBasedFunds[$i]->cusip = $value;
                }
            }
            $i++;
        }
        return $riskBasedFunds;
    }
    
    public function plansXML($userid)
    {
        $planId = "";
        try
        {
            $namespace = $this->space();
            $params = $this->parameters($planId);
            $endpoint = $this->endpoint($userid);
            $authHeaders = $this->authenticationHeader($userid);
            $objClient = new \SoapClient("datalink.wsdl", array('trace' => true, 'exceptions' => false));
            $header = new \SoapHeader($namespace, "AuthenticationHeader", $authHeaders, false);
            $objClient->__setSoapHeaders($header);
            $newlocation = $objClient->__setLocation($endpoint);
            $objResponse = $objClient->__soapCall("GetPlanList", $params);
            return $objClient->__getLastResponse();
        }
        catch (Exception $e)
        {
            return -1;
        }
    }
    
    public function plandataXML($planid, $userid)
    {

        try
        {
            $namespace = $this->space();
            $params = $this->parameters($planid);
            $endpoint = $this->endpoint($userid);
            $authHeaders = $this->authenticationHeader($userid);
            $objClient = new \SoapClient("datalink.wsdl", array('trace' => true, 'exceptions' => false));
            $header = new \SoapHeader($namespace, "AuthenticationHeader", $authHeaders, false);
            $objClient->__setSoapHeaders($header);
            $newlocation = $objClient->__setLocation($endpoint);
            $objResponse = $objClient->__soapCall("GetPlanData", $params);
            return trim($objClient->__getLastResponse());
        }
        catch (Exception $e)
        {
            return -1;
        }
    }
    
    private function parameters($planid)
    {
        $params = array('AuthenticationHeader' => array(
                'Content-Type' => 'text/xml; charset=UTF-8',
                'a_strPlanID' => $planid,
                'a_strSsNum' => '999999999'
            )
        );

        return $params;
    }

    private function authenticationHeader($userid)
    {

        $repository = $this->em->getRepository('classesclassBundle:accounts');
        $currentmember = $repository->findOneBy(array('id' => $userid));
        $user = $currentmember->recordkeeperUser;
        $pass = $currentmember->recordkeeperPass;
        $tpaid = $currentmember->recordkeeperTpaid;
        $advicep = $currentmember->recordkeeperAdviceProviderCd;




        $authHeaders["UserName"] = $user;
        $authHeaders["Password"] = $pass;
        $authHeaders["TPAID"] = $tpaid;
        $authHeaders["AdviceProviderCd"] = $advicep;
        $authHeaders["AgentID"] = "";
        $authHeaders["ServiceUrl"] = "datalink.wsdl";

        return $authHeaders;
    }

    private function space()
    {
        return "http://tempuri.org/RAWebPronvestService/PronVest";
    }

    private function endpoint($userid)
    {
        $repository = $this->em->getRepository('classesclassBundle:accounts');
        $currentmember = $repository->findOneBy(array('id' => $userid));
        $endpoint = $currentmember->recordkeeperUrl;
        return $endpoint;
    }
}
