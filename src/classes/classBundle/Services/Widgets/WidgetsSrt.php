<?php
namespace classes\classBundle\Services\Widgets;
use Symfony\Component\DependencyInjection\ContainerInterface;
use classes\classBundle\Services\Widgets\WidgetsRecordkeeper;
use Shared\RecordkeepersBundle\Classes\recordKeeper;
class WidgetsSrt extends WidgetsRecordkeeper
{
    
    public $table = "accountsRecordkeepersSrt";
    public function __construct(containerInterface $container)
    {
        $this->container = $container;
        $this->em = $this->container->get("doctrine");
        $this->recordkeeper = new recordKeeper($this->em);
    }
    public function sid($account)
    {
        $this->connectionType = $account['connectionType'];
        $this->baseurl = 'https://' . $account['url']."/";
        if (!isset($account['recordkeeperVersion']))
        {
            $account['recordkeeperVersion'] = 2;
        }
        if ($account['recordkeeperVersion'] != 3)
        {
            $this->baseurl .= "SDDAv2/";
        }
        $this->baseurl .= "pas.asmx/";
        $url = $this->baseurl."GetOperatorSessionID";
        $username = "vwisesnapshotsponsor";
        $password = "password123";
        $type = "6";
        $siteToken = "114";
        $fields = array("userName" => $account['userName'],"password" => $account['password'], "userType" => $account['type'], "siteToken" => $account['siteToken']);
        $ch = curl_init();
        $fieldString = "";
        foreach ($fields as $key => $value)
        {
            if ($fieldString != "")
            $fieldString = $fieldString."&";
            $fieldString = $fieldString.$key."=".$value;
        }
        
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fieldString);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        $result = curl_exec($ch);
        $this->recordkeeper->writeXmlLog("none", "none", "none",$url."/?".$fieldString,null,$account['id'],"send","sid",$this->connectionType);
        $this->recordkeeper->writeXmlLog("none", "none", "none",$result,null,$account['id'],"receive","sid",$this->connectionType);
        $xml = new \DOMDocument();
        $xml->preserveWhiteSpace = false;
        $xml->formatOutput = true;
        $xml->loadXML($result);
        $this->session_id = $xml->textContent;
        curl_close($ch);     
        $this->url = $account['url'];
    }
    public function callPlan($apiCall,$parameters = array())
    {
        $parameters['planID'] = $this->getCurrentPlanid();
        return $this->call($apiCall,$parameters);
    }
    public function call($apiCall,$parameters = array(),$userid = null,$smartplanid = null)
    {
        $url = $this->baseurl. $apiCall . '?sessionId=' . $this->session_id . ($parameters ? '&' . http_build_query($parameters): '');
        try 
        {
            $this->recordkeeper->writeXmlLog("none", "none", "none",$url,$smartplanid,$userid,"send",$apiCall,$this->connectionType);
            $rawXml = file_get_contents($url, FALSE);
            $this->recordkeeper->writeXmlLog("none", "none", "none",$rawXml,$smartplanid,$userid,"receive",$apiCall,$this->connectionType);
            if ($rawXml) 
            {
                $xml = simplexml_load_string($rawXml);
                return json_decode(json_encode($xml));
            }
        }
        catch (\Exception $e)
        {
                /* TODO: Error Handling */
        }
        return FALSE;
    }   

    public function getPlanEmailList()
    {
        $data = $this->callPlan("getVwiseParticipantDetails");
        return $data;
    }
    public function getPlanList()
    {
        $plans = $this->call('getVwisePlanDetails');
        return $plans;
    }
    public function getParticipantDetails($planId,$userid = null,$smartplanid = null)
    {
        $participants = $this->call('getVwiseParticipantDetails', array('planID' => $planId),$userid,$smartplanid );
        return $participants;
    }

}




