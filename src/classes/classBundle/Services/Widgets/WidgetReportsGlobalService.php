<?php
namespace classes\classBundle\Services\Widgets;
use Symfony\Component\DependencyInjection\ContainerInterface;
use classes\classBundle\Services\BaseService;
class WidgetReportsGlobalService extends BaseService
{
    public function returnCTAList($type)
    {
        switch ($type)
        {
            case 'irio':
            return array("Irio");
            case 'powerview':
            return array("Powerview","PowerviewRedirect","PowerviewSmartPlan");           
        }
    }
    public function returnCTAListQuery($type)
    {
        $ctas = $this->returnCTAList($type);
        foreach ($ctas as &$cta)
        {
            $cta = "'".$cta."'";
        }
        return implode(",",$ctas);
    }    
}

