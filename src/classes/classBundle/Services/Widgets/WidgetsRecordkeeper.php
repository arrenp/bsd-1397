<?php
namespace classes\classBundle\Services\Widgets;
use Symfony\Component\DependencyInjection\ContainerInterface;
use classes\classBundle\Services\BaseService;
use classes\classBundle\MappingTypes\EncryptedType;
class WidgetsRecordkeeper extends BaseService
{
    public function sid($account){}
    public function getLoggedInSid()
    {
        return $this->sid($this->getAccount($this->session->get("userid")));      
    }
    public function getAccount($userid)
    {
        $sql = "SELECT *,AES_DECRYPT(userName, UNHEX('".EncryptedType::KEY."')) as userName,AES_DECRYPT(password, UNHEX('".EncryptedType::KEY."')) as password  FROM ".$this->table." LEFT JOIN accounts on accounts.id = ".$this->table.".userid WHERE accounts.id = ".$userid;
        $data = $this->connection->executeQuery($sql)->fetch();
        return $data;
    }
    public function getPlanid($smartplanid)
    {
        $sql = "SELECT * FROM plans WHERE id = ".$smartplanid;
        $data = $this->connection->executeQuery($sql)->fetch();
        return $data['planid'];                
    }
    public function getCurrentPlanid()
    {
        return $this->getPlanid($this->session->get("planid"));    
    }
    public function getWidgetBasedOffLoggedInAccount()
    {
        //more cases if needed
        return $this->container->get("WidgetsSrt");
    }
    public function call($apiCall,$parameters = array()){}
    public function callPlan($apiCall,$parameters = array()){}
}
