<?php

namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * planuploadTableColumnMappings
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class planuploadTableColumnMappings {
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="tableName", type="string",length=100)
     */
    public $tableName;
    /**
     * @var string
     *
     * @ORM\Column(name="columnName", type="string",length=100)
     */
    public $columnName;
    /**
     * @var string
     *
     * @ORM\Column(name="mappings", type="text")
     */
    public $mappings;
}
