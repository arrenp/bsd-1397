<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;



/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class surveyEmployerGroups{
    
    
     /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    
     /**
     * @var string
     *
     * @ORM\Column(name="groupName", type="string", length=45)
     */
    public $groupName;
    
     /**
     * @var string
     *
     * @ORM\Column(name="employerGroupId", type="string", length=45, nullable=true)
     */
    public $employerGroupId;
    
    /**
     * @var string
     *
     * @ORM\Column(name="partnerId", type="string", length=45)
     */
    public $partnerId;
    
     /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=45)
     */
    public $description;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="parentId", type="integer", nullable=true)
     */
    public $parentId;
    /**
     * @var bool
     *
     * @ORM\Column(name="isEmployer", type="boolean")
     */
    public $isEmployer;
    /**
     * @var integer
     *
     * @ORM\Column(name="displayOrder", type="integer")
     */
    public $displayOrder;
    public function __construct()//do not remove, will fail for doctrine add function
    {

        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            if ($this->$key != "id") $this->$key = "";

        }
        $this->parentId = null;
        $this->isEmployer = false;
        $this->employerGroupId = null;
        
    }    
    
}

