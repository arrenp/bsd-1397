<?php
namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * auditLog
 *
 * @ORM\Table()
 * @ORM\Entity
 *
 */
class auditLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
	/**
     * @var integer
     *
     * @ORM\Column(name="accountsUsersId", type="integer")
     */    
    public $accountsUsersId;
    /**
     * @var string
     *
     * @ORM\Column(name="tablename", type="string", length=50)
     */
    public $tablename;
    /**
     * @var integer
     *
     * @ORM\Column(name="recordId", type="integer", nullable=true)
     */
    public $recordId;
    /**
     * @var string
     *
     * @ORM\Column(name="record", type = "text")
     */
    public $record; 
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50)
     */
    public $type;
    /**
     * @var string
     *
     * @ORM\Column(name="section", type="string", length=50)
     */
    public $section;
    /**
     * @var string
     *
     * @ORM\Column(name="page", type="string", length=50)
     */
    public $page;
    /**
    * @var string
    *
    * @ORM\Column(name="time", type="datetime")
    */
   public $time; 
}

