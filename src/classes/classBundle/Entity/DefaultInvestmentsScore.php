<?php
namespace classes\classBundle\Entity;
use classes\classBundle\Entity\abstractclasses\InvestmentsScore;
use Doctrine\ORM\Mapping as ORM;
/**
 * DefaultInvestmentsScore
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class DefaultInvestmentsScore extends InvestmentsScore 
{
    
}
