<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class CTAStat
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
        /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;

    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
    /**
     * @var float
     *
     * @ORM\Column(name="ipaddress", type="string", length = 100)
     */
    public $ipaddress;
    /**
     * @var string
     *
     * @ORM\Column(name="uniqid",type="string", length = 100)
     */
    public $uniqid;
    /**
     * @var string
     *
     * @ORM\Column(name="sessionid", type="string",length = 255)
     */
    public $sessionid;     
    /**
     * @var text
     *
     * @ORM\Column(name="firstName", type="encrypted",length = 100 )
     */
    public $firstName; 
    /**
     * @var text
     *
     * @ORM\Column(name="lastName", type="encrypted",length=100)
     */
    public $lastName;
    /**
     * @var string
     *
     * @ORM\Column(name="cta", type="string",length = 100)
     */
    public $cta;  
    /**
     * @var text
     *
     * @ORM\Column(name="email", type="encrypted",length=100)
     */
    public $email;    
    /**
     * @var text
     *
     * @ORM\Column(name="age", type="integer")
     */
    public $age;    
    /**
     * @var float
     *
     * @ORM\Column(name="salary", type="float")
     */
    public $salary;       
    /**
     * @var datetime
     *
     * @ORM\Column(name="dropoff", type="string",length=100)
     */
    public $dropoff;       
    /**
     * @var datetime
     *
     * @ORM\Column(name="predeferral", type="float")
     */
    public $predeferral;    
    /**
     * @var datetime
     *
     * @ORM\Column(name="curdeferral", type="float")
     */
    public $curdeferral;
    /**
     * @var float
     *
     * @ORM\Column(name="employerMatch", type="float")
     */
    public $employerMatch;
    /**
     * @var float
     *
     * @ORM\Column(name="maxMatch", type="float")
     */
    public $maxMatch;    
    /**
     * @var integer
     *
     * @ORM\Column(name="deferral", type="integer")
     */
    public $deferral;   
    /**
     * @var integer
     *
     * @ORM\Column(name="selectedDeferral", type="integer")
     */    
    public $selectedDeferral;
    /**
     * @var float
     *
     * @ORM\Column(name="totalSpouseAccountBalance", type="float")
     */    
    public $totalSpouseAccountBalance;    
	/**
     * @var boolean
     *
     * @ORM\Column(name="postContributions", type="boolean")
     */    
    public $postContributions;    

    /**
     * @var string
     *
     * @ORM\Column(name="requestData", type="text")
     */
    public $requestData;
    /**
     * @var string
     *
     * @ORM\Column(name="responseData", type="text")
     */
    public $responseData;
        /**
     * @var text
     *
     * @ORM\Column(name="complete", type="integer")
     */
    public $complete;
    /** 
     * @var datetime
     * 
     * @ORM\Column(type="datetime", name="createDate") 
     */
    public $createDate;   
     /** 
     * @var datetime
     * 
     * @ORM\Column(type="datetime", name="modifiedDate") 
     */
    public $modifiedDate;    
     /** 
     * @var datetime
     * 
     * @ORM\Column(type="float", name="otherRetirementAccountBalance") 
     */
    public $otherRetirementAccountBalance;
     /** 
     * @var datetime
     * 
     * @ORM\Column(type="float", name="totalSpouseAnnualIncome") 
     */
    public $totalSpouseAnnualIncome;
     /** 
     * @var datetime
     * 
     * @ORM\Column(type="integer", name="spouseAge") 
     */
    public $spouseAge;            
     /**
     * @var datetime
     *
     * @ORM\Column(name="diffDeferral", type="float")
     */
    public $diffDeferral; 
    /**
     * @var datetime
     *
     * @ORM\Column(name="balance", type="float")
     */
    public $balance; 
    /**
     * @var datetime
     *
     * @ORM\Column(name="projectedMonthlyIncome", type="float",nullable=true)
     */
    public $projectedMonthlyIncome; 
    /**
     * @var datetime
     *
     * @ORM\Column(name="projectedIncome", type="float",nullable=true)
     */
    public $projectedIncome; 
    /**
     * @var datetime
     *
     * @ORM\Column(name="json", type="encrypted")
     */
    public $json;     
    
    public function __construct()//do not remove, will fail for doctrine add function
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            if (!in_array("projectedMonthlyIncome","projectedIncome"))
            $this->$key = "";
        }
    }
}
