<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use stdClass;


/**
 * EmailCampaignEmployees
 *
 * @ORM\Table(name="emailCampaignEmployees")
 * @ORM\Entity
 */
class EmailCampaignEmployees
{

    public $fieldMap = [
        'participantId' => 'participant_id',
        'planId' => 'plan_id',
        'partnerId' => 'partner_id',
        'firstName' => 'firstname',
        'lastName' =>'lastname',
        'statusCode' => 'status_code',
        'statusCodeDesc' => 'status_code_description',
        'mobilePhoneNumber' => 'mobilephone',
        'email' => 'email',
        'firstEligibleStatusDate' => 'first_eligible_status_date',
        'lastEligibleStatusDate' => 'last_eligible_status_date',
        'statusCodeAtEnrollment' => 'status_code_at_enrollment',
        'providerLogo' => 'provider_logo',
        'sponsorLogo' => 'sponsor_logo',
        'smartPlan' => 'smart_plan',
        'smartEnroll' => 'smart_enroll',
        'smartPlanLogin' => 'smart_plan_participant_login_website_address',
        'smartEnrollLogin' =>'smart_enroll_participant_login_website_address',
        'HRFirst' => 'hr_contact_first_name',
        'HRLast' => 'hr_contact_last_name',
        'HREmail' => 'hr_contact_email'
    ];

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="participantId", type="integer")
     */
    private $participantId;

    /**
     * @var integer
     *
     * @ORM\Column(name="planId", type="integer")
     */
    private $planId;

    /**
     * @var integer
     *
     * @ORM\Column(name="partnerId", type="integer")
     */
    private $partnerId;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="encrypted", length=255)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="encrypted", length=255)
     */
    private $lastName;

    /**
     * @var integer
     *
     * @ORM\Column(name="statusCode", type="integer")
     */
    private $statusCode;

    /**
     * @var string
     *
     * @ORM\Column(name="statusCodeDesc", type="string", length=255)
     */
    private $statusCodeDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="mobilePhoneNumber", type="encrypted", length=255)
     */
    private $mobilePhoneNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="encrypted", length=255)
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreated", type="datetime")
     */
    private $dateCreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateModified", type="datetime")
     */
    private $dateModified;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="firstEligibleStatusDate", type="datetime")
     */
    private $firstEligibleStatusDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastEligibleStatusDate", type="datetime")
     */
    private $lastEligibleStatusDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="statusCodeAtEnrollment", type="integer")
     */
    private $statusCodeAtEnrollment;

    /**
     * @var string
     *
     * @ORM\Column(name="providerLogo", type="string", length=255)
     */
    private $providerLogo;

    /**
     * @var string
     *
     * @ORM\Column(name="sponsorLogo", type="string", length=255)
     */
    private $sponsorLogo;

    /**
     * @var string
     *
     * @ORM\Column(name="smartPlan", type="string", length=1)
     */
    private $smartPlan;

    /**
     * @var string
     *
     * @ORM\Column(name="smartEnroll", type="string", length=1)
     */
    private $smartEnroll;

    /**
     * @var string
     *
     * @ORM\Column(name="smartPlanLogin", type="string", length=255)
     */
    private $smartPlanLogin;

    /**
     * @var string
     *
     * @ORM\Column(name="smartEnrollLogin", type="string", length=255)
     */
    private $smartEnrollLogin;

    /**
     * @var string
     *
     * @ORM\Column(name="HRFirst", type="string", length = 255)
     */
    private $HRFirst;
    /**
     * @var string
     *
     * @ORM\Column(name="HRLast", type="string", length = 255)
     */
    private $HRLast;
    /**
     * @var string
     *
     * @ORM\Column(name="HREmail", type="string", length = 255)
     */
    private $HREmail;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set participantId
     *
     * @param integer $participantId
     *
     * @return EmailCampaignEmployees
     */
    public function setParticipantId($participantId)
    {
        $this->participantId = $participantId;

        return $this;
    }

    /**
     * Get participantId
     *
     * @return integer
     */
    public function getParticipantId()
    {
        return $this->participantId;
    }

    /**
     * Set planId
     *
     * @param integer $planId
     *
     * @return EmailCampaignEmployees
     */
    public function setPlanId($planId)
    {
        $this->planId = $planId;

        return $this;
    }

    /**
     * Get planId
     *
     * @return integer
     */
    public function getPlanId()
    {
        return $this->planId;
    }

    /**
     * Set partnerId
     *
     * @param integer $partnerId
     *
     * @return EmailCampaignEmployees
     */
    public function setPartnerId($partnerId)
    {
        $this->partnerId = $partnerId;

        return $this;
    }

    /**
     * Get partnerId
     *
     * @return integer
     */
    public function getPartnerId()
    {
        return $this->partnerId;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return EmailCampaignEmployees
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return EmailCampaignEmployees
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set statusCode
     *
     * @param integer $statusCode
     *
     * @return EmailCampaignEmployees
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Get statusCode
     *
     * @return integer
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Set statusCodeDesc
     *
     * @param string $statusCodeDesc
     *
     * @return EmailCampaignEmployees
     */
    public function setStatusCodeDesc($statusCodeDesc)
    {
        $this->statusCodeDesc = $statusCodeDesc;

        return $this;
    }

    /**
     * Get statusCodeDesc
     *
     * @return string
     */
    public function getStatusCodeDesc()
    {
        return $this->statusCodeDesc;
    }

    /**
     * Set mobilePhoneNumber
     *
     * @param string $mobilePhoneNumber
     *
     * @return EmailCampaignEmployees
     */
    public function setMobilePhoneNumber($mobilePhoneNumber)
    {
        $this->mobilePhoneNumber = $mobilePhoneNumber;

        return $this;
    }

    /**
     * Get mobilePhoneNumber
     *
     * @return string
     */
    public function getMobilePhoneNumber()
    {
        return $this->mobilePhoneNumber;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return EmailCampaignEmployees
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return EmailCampaignEmployees
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dateModified
     *
     * @param \DateTime $dateModified
     *
     * @return EmailCampaignEmployees
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;

        return $this;
    }

    /**
     * Get dateModified
     *
     * @return \DateTime
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }

    /**
     * Set firstEligibleStatusDate
     *
     * @param \DateTime $firstEligibleStatusDate
     *
     * @return EmailCampaignEmployees
     */
    public function setFirstEligibleStatusDate($firstEligibleStatusDate)
    {
        $this->firstEligibleStatusDate = $firstEligibleStatusDate;

        return $this;
    }

    /**
     * Get firstEligibleStatusDate
     *
     * @return \DateTime
     */
    public function getFirstEligibleStatusDate()
    {
        return $this->firstEligibleStatusDate;
    }

    /**
     * Set lastEligibleStatusDate
     *
     * @param \DateTime $lastEligibleStatusDate
     *
     * @return EmailCampaignEmployees
     */
    public function setLastEligibleStatusDate($lastEligibleStatusDate)
    {
        $this->lastEligibleStatusDate = $lastEligibleStatusDate;

        return $this;
    }

    /**
     * Get lastEligibleStatusDate
     *
     * @return \DateTime
     */
    public function getLastEligibleStatusDate()
    {
        return $this->lastEligibleStatusDate;
    }

    /**
     * Set statusCodeAtEnrollment
     *
     * @param integer $statusCodeAtEnrollment
     *
     * @return EmailCampaignEmployees
     */
    public function setStatusCodeAtEnrollment($statusCodeAtEnrollment)
    {
        $this->statusCodeAtEnrollment = $statusCodeAtEnrollment;

        return $this;
    }

    /**
     * Get statusCodeAtEnrollment
     *
     * @return integer
     */
    public function getStatusCodeAtEnrollment()
    {
        return $this->statusCodeAtEnrollment;
    }

    /**
     * @return string
     */
    public function getProviderLogo()
    {
        return $this->providerLogo;
    }

    /**
     * @param string $providerLogo
     */
    public function setProviderLogo($providerLogo)
    {
        $this->providerLogo = $providerLogo;
    }

    /**
     * @return string
     */
    public function getSponsorLogo()
    {
        return $this->sponsorLogo;
    }

    /**
     * @param string $sponsorLogo
     */
    public function setSponsorLogo($sponsorLogo)
    {
        $this->sponsorLogo = $sponsorLogo;
    }

    /**
     * @return string
     */
    public function getSmartPlan()
    {
        return $this->smartPlan;
    }

    /**
     * @param string $smartPlan
     */
    public function setSmartPlan($smartPlan)
    {
        $this->smartPlan = $smartPlan;
    }

    /**
     * @return string
     */
    public function getSmartEnroll()
    {
        return $this->smartEnroll;
    }

    /**
     * @param string $smartEnroll
     */
    public function setSmartEnroll($smartEnroll)
    {
        $this->smartEnroll = $smartEnroll;
    }

    /**
     * @return string
     */
    public function getSmartPlanLogin()
    {
        return $this->smartPlanLogin;
    }

    /**
     * @param string $smartPlanLogin
     */
    public function setSmartPlanLogin($smartPlanLogin)
    {
        $this->smartPlanLogin = $smartPlanLogin;
    }

    /**
     * @return string
     */
    public function getSmartEnrollLogin()
    {
        return $this->smartEnrollLogin;
    }

    /**
     * @param string $smartEnrollLogin
     */
    public function setSmartEnrollLogin($smartEnrollLogin)
    {
        $this->smartEnrollLogin = $smartEnrollLogin;
    }

    /**
     * @return string
     */
    public function getHRFirst()
    {
        return $this->HRFirst;
    }

    /**
     * @param string $HRFirst
     */
    public function setHRFirst($HRFirst)
    {
        $this->HRFirst = $HRFirst;
    }

    /**
     * @return string
     */
    public function getHRLast()
    {
        return $this->HRLast;
    }

    /**
     * @param string $HRLast
     */
    public function setHRLast($HRLast)
    {
        $this->HRLast = $HRLast;
    }

    /**
     * @return string
     */
    public function getHREmail()
    {
        return $this->HREmail;
    }

    /**
     * @param string $HREmail
     */
    public function setHREmail($HREmail)
    {
        $this->HREmail = $HREmail;
    }

    public function valueOrNull($object, $field) {
        if (property_exists($object, $field)) {
            if (is_object($object->$field) && (get_class($object->$field) == \stdClass::class)) {
                return null;
            } else {
                return trim($object->$field);
            }
        } else {
            return null;
        }
    }

    public function loadCommonFields(\stdClass $xml)
    {
        $this->firstName = $this->valueOrNull($xml, 'FirstName');
        $this->lastName = $this->valueOrNull($xml, 'LastName');
        $this->participantId = $this->valueOrNull($xml, 'ParticipantID');
        $this->statusCode = $this->valueOrNull($xml, 'StatusCode');
        $this->statusCodeDesc = $this->valueOrNull($xml, 'StatusCodeDesc');
        $this->mobilePhoneNumber = $this->valueOrNull($xml, 'MobilePhoneNumber');
        $this->email = $this->valueOrNull($xml, 'Email');
    }

    public function loadFromXml(\stdClass $xml, $eligibleStatusCode)
    {
        $this->loadCommonFields($xml);
        $this->dateCreated = new \DateTime();
        $this->dateModified = new \DateTime();

        if ($this->statusCode == $eligibleStatusCode) {
            $this->firstEligibleStatusDate = new \DateTime();
            $this->lastEligibleStatusDate = null;
            $this->statusCodeAtEnrollment = null;
        } else {
            $this->lastEligibleStatusDate = new \DateTime();
            $this->statusCodeAtEnrollment = $this->valueOrNull($xml, 'StatusCode');
        }
    }

    public function updateFromXml(\stdClass $xml, $eligibleStatusCode)
    {
        if ($this->statusCode != $eligibleStatusCode) {
            $this->lastEligibleStatusDate = $this->dateModified;
            $this->statusCodeAtEnrollment = $this->valueOrNull($xml,'StatusCode');
        }
        $this->loadCommonFields($xml);
        $this->dateModified = new \DateTime();
    }

    /**
     * HubSpot wants dates in UTC midnight
     */
    public function calculateUTCMilli($date) {
        return date_create( date('Y-m-d', $date->getTimestamp()), timezone_open( 'UTC' ) )->getTimestamp() * 1000;
    }

    public function getPropertyArray()
    {
        $exemptArray = array('lastEligibleStatusDate', 'firstEligibleStatusDate');
        $properties = array();
        foreach($this->fieldMap as $field => $emailField) {
            if (in_array($field, $exemptArray) && $this->$field instanceof \DateTime) {
                $properties[$emailField] = $this->calculateUTCMilli($this->$field);
            } else {
                $properties[$emailField] = $this->$field;
            }
        }
        return $properties;
    }

    public function generateJSONForEmailSystem()
    {
        $fields = array();
        foreach ($this->fieldMap as $field => $emailField) {
            $fields[] = '{"property" : "' . $emailField . '", "value" : "' . $this->$field . '"}';
        }
        $json = '{"properties" : [';
        $json .= join(',', $fields);
        $json .= ']}';
        return $json;
    }

}

