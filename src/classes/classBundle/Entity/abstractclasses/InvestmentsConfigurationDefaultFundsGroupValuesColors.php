<?php
namespace classes\classBundle\Entity\abstractclasses;
use Doctrine\ORM\Mapping as ORM;
abstract class InvestmentsConfigurationDefaultFundsGroupValuesColors
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
    * @var integer
    *
    * @ORM\Column(name="color", type="string",length = 40)
     */
    public $color;      
    /**
    * @var integer
    *
    * @ORM\Column(name="hoverColor", type="string",length = 40)
     */
    public $hoverColor;
    /**
     * @var integer
     *
     * @ORM\Column(name="fundGroupValueId", type="integer")
     */
    public $fundGroupValueId;
}

