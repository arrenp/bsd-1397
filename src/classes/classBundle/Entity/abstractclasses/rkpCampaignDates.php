<?php
namespace classes\classBundle\Entity\abstractclasses;
use Doctrine\ORM\Mapping as ORM;
abstract class rkpCampaignDates
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;   
    /**
     * @var text
     *
     * @ORM\Column(name="scheduled", type="string",length=10)
     */
    public $scheduled;      
    public function __construct()
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
                
            $this->$key = "";
        }
    }       
}


