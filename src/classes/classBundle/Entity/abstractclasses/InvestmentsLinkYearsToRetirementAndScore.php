<?php
namespace classes\classBundle\Entity\abstractclasses;
use Doctrine\ORM\Mapping as ORM;
abstract class InvestmentsLinkYearsToRetirementAndScore
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid; 
    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="string")
     */
    public $type;    
    /**
     * @var integer
     *
     * @ORM\Column(name="InvestmentsYearsToRetirementId", type="integer")
     */
    public $InvestmentsYearsToRetirementId; 
    /**
     * @var integer
     *
     * @ORM\Column(name="InvestmentsScoreId", type="integer")
     */
    public $InvestmentsScoreId;   
}
