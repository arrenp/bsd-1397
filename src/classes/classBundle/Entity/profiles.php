<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class profiles
{

    /**
     * @ORM\ManyToOne(targetEntity="participants")
     * @ORM\JoinColumn(name="participantid", referencedColumnName="id")
     */
    public $participant;
    /**
     * @ORM\OneToMany(targetEntity="profilesBeneficiaries", mappedBy="profile")
     **/
    public $beneficiaries;
    /**
     * @ORM\OneToMany(targetEntity="profilesContributions", mappedBy="profile")
     **/
    public $contributions;
    /**
     * @ORM\OneToMany(targetEntity="profilesInvestments", mappedBy="profile")
     **/
    public $investments;
    /**
     * @ORM\OneToMany(targetEntity="profilesRetirementNeeds", mappedBy="profile")
     **/
    public $retirementNeeds;
    /**
     * @ORM\OneToMany(targetEntity="profilesRiskProfile", mappedBy="profile")
     **/
    public $riskProfile;
    /**
     * @ORM\OneToMany(targetEntity="profilesAutoIncrease", mappedBy="profile")
     **/
    public $autoIncrease;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */

    public $id;

 	/**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
	public $userid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="planid", type="integer")
	 */
    public $planid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="planName", type="string", length=255)
	 */
    public $planName;
 	/**
	 * @var string
	 *
	 * @ORM\Column(name="currentBalance", type="float",length = 255)
	 */
    public $currentBalance;
    /**
	 * @var string
	 *
	 * @ORM\Column(name="beneficiaryStatus", type="text",length = 65535)
	 */
    public $beneficiaryStatus;
    /**
	 * @var string
	 *
	 * @ORM\Column(name="investmentsStatus", type="text",length = 65535)
	 */
    public $investmentsStatus;
    /**
	 * @var string
	 *
	 * @ORM\Column(name="realignmentStatus", type="text",length = 65535)
	 */
    public $realignmentStatus;
    /**
	 * @var string
	 *
	 * @ORM\Column(name="contributionsStatus", type="text",length = 65535)
	 */
    public $contributionsStatus;
    /**
	 * @var string
	 *
	 * @ORM\Column(name="catchupContributionStatus", type="text",length = 65535)
	 */
    public $catchupContributionStatus;
    /**
     * @var string
     *
     * @ORM\Column(name="ATTransactStatus", type="text",length = 65535)
     */
    public $ATTransactStatus;
    /**
	 * @var string
	 *
	 * @ORM\Column(name="annualSalary", type="float",length = 255)
	 */
    public $annualSalary;
     /**
	 * @var string
	 *
	 * @ORM\Column(name="retireAge", type="integer")
	 */
    public $retireAge;
     /**
	 * @var string
	 *
	 * @ORM\Column(name="preTaxSavingRate", type="float",length = 255)
	 */
    public $preTaxSavingRate;
     /**
	 * @var string
	 *
	 * @ORM\Column(name="postTaxSavingRate", type="float",length = 255)
	 */
    public $postTaxSavingRate;
     /**
	 * @var string
	 *
	 * @ORM\Column(name="rothTaxSavingRate", type="float",length = 255)
	 */
    public $rothTaxSavingRate;
     /**
	 * @var string
	 *
	 * @ORM\Column(name="mStarContribution", type="text",length = 65535)
	 */
    public $mStarContribution;
     /**
	 * @var string
	 *
	 * @ORM\Column(name="mStarStatus", type="text",length = 65535)
	 */
    public $mStarStatus;
     /**
	 * @var string
	 *
	 * @ORM\Column(name="smart401kStatus", type="text",length = 65535)
	 */
    public $smart401kStatus;
	 /**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="reportDate", type="datetime", nullable=true)
	 */
    public $reportDate;
	 /**
	 * @var string
	 *
	 * @ORM\Column(name="profileId", type="string", length = 50, unique=true)
	 */
    public $profileId;
	 /**
	 * @var string
	 *
	 * @ORM\Column(name="acceptedAdvice", type="text",length = 65535)
	 */
    public $acceptedAdvice;
	 /**
	 * @var string
	 *
	 * @ORM\Column(name="ACAOptOutStatus", type="text",length = 65535)
	 */
    public $ACAOptOutStatus;
    /**
     * @var string
     *
     * @ORM\Column(name="enrollmentStatus", type="text",length = 65535)
     */
    public $enrollmentStatus;
	 /**
	 * @var string
	 *
	 * @ORM\Column(name="profilestatus", type="smallint")
	 */
	public $profilestatus;
 	 /**
	 * @var string
	 *
	 * @ORM\Column(name="availability", type="string",length = 250)
	 */
    public $availability;
 	 /**
	 * @var string
	 *
	 * @ORM\Column(name="clientCalling", type="string",length = 50)
	 */
    public $clientCalling;
 	 /**
	 * @var string
	 *
	 * @ORM\Column(name="mstarQuit", type="smallint" )
	 */
    public $mstarQuit;
 	 /**
	 * @var string
	 *
	 * @ORM\Column(name="rkdData", type="text",length = 65535 )
	 */
 	public $rkdData;
 	 /**
	 * @var string
	 *
	 * @ORM\Column(name="welcomeVideo", type="string",length = 63 )
	 */
    public $welcomeVideo;
 	 /**
	 * @var string
	 *
	 * @ORM\Column(name="planBasicVideo", type="string",length = 127 )
	 */
    public $planBasicVideo;
 	 /**
	 * @var string
	 *
	 * @ORM\Column(name="uniqid", type="string",length = 63 )
	 */
    public $uniqid;
 	 /**
	 * @var string
	 *
	 * @ORM\Column(name="migration", type="smallint" )
	 */
    public $migration;
     /**
     * @var string
     *
     * @ORM\Column(name="investmentType", type="string",length = 15 )
     */
    public $investmentType;
    /**
     * @var boolean
     *
     * @ORM\Column(name="flexPlan", type="boolean", options={"default"=false}, nullable=true)
     */
    public $flexPlan = false;
    /**
    * @var string
    *
    * @ORM\Column(name="ECOMMModal", type="smallint" )
    */
    public $ECOMMModal;
    /**
    * @var string
    *
    * @ORM\Column(name="sessionData", type="encrypted",length = 65535 )
    */
    public $sessionData; 
    /**
    * @var string
    *
    * @ORM\Column(name="userAgent", type="text")
    */
    public $userAgent;  
    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string" ,length = 45)
     */
    public $language;
    /**
     * @var string
     *
     * @ORM\Column(name="smartEnrollPartId", type="string" ,length=25, nullable=true)
     */
    public $smartEnrollPartId;
    /**
     * @var int
     *
     * @ORM\Column(name="callUs", type="smallint", nullable=true)
     */
    public $callUs;  
    /**
     * @var int
     *
     * @ORM\Column(name="callMe", type="string", length = 50, nullable=true)
     */
    public $callMe; 
    /**
     * @var int
     *
     * @ORM\Column(name="emailMe", type="string", length =255,nullable=true )
     */
    public $emailMe; 
    /**
     * @var int
     *
     * @ORM\Column(name="enrollmentContactViewedTimes", type="integer" )
     */
    public $enrollmentContactViewedTimes; 
    /**
     * @var int
     *
     * @ORM\Column(name="trustedContactStatus", type="smallint", nullable=true )
     */
    public $trustedContactStatus;     
    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
                    if (!in_array($key,array("callUs","callMe","emailMe","trustedContactStatus")))
                    {
                        $this->$key = "";
                    }
   		}
   		$this->beneficiaries= new ArrayCollection();
   		$this->contributions= new ArrayCollection();
   		$this->investments= new ArrayCollection();
   		$this->retirementNeeds= new ArrayCollection();
   		$this->riskProfile= new ArrayCollection();
        $this->autoIncrease = new ArrayCollection();

	}


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     *
     * @return profiles
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set planid
     *
     * @param integer $planid
     *
     * @return profiles
     */
    public function setPlanid($planid)
    {
        $this->planid = $planid;

        return $this;
    }

    /**
     * Get planid
     *
     * @return integer
     */
    public function getPlanid()
    {
        return $this->planid;
    }

    /**
     * Set planName
     *
     * @param string $planName
     *
     * @return profiles
     */
    public function setPlanName($planName)
    {
        $this->planName = $planName;

        return $this;
    }

    /**
     * Get planName
     *
     * @return string
     */
    public function getPlanName()
    {
        return $this->planName;
    }

    /**
     * Set currentBalance
     *
     * @param float $currentBalance
     *
     * @return profiles
     */
    public function setCurrentBalance($currentBalance)
    {
        $this->currentBalance = $currentBalance;

        return $this;
    }

    /**
     * Get currentBalance
     *
     * @return float
     */
    public function getCurrentBalance()
    {
        return $this->currentBalance;
    }

    /**
     * Set beneficiaryStatus
     *
     * @param string $beneficiaryStatus
     *
     * @return profiles
     */
    public function setBeneficiaryStatus($beneficiaryStatus)
    {
        $this->beneficiaryStatus = $beneficiaryStatus;

        return $this;
    }

    /**
     * Get beneficiaryStatus
     *
     * @return string
     */
    public function getBeneficiaryStatus()
    {
        return $this->beneficiaryStatus;
    }

    /**
     * Set investmentsStatus
     *
     * @param string $investmentsStatus
     *
     * @return profiles
     */
    public function setInvestmentsStatus($investmentsStatus)
    {
        $this->investmentsStatus = $investmentsStatus;

        return $this;
    }

    /**
     * Get investmentsStatus
     *
     * @return string
     */
    public function getInvestmentsStatus()
    {
        return $this->investmentsStatus;
    }

    /**
     * Set realignmentStatus
     *
     * @param string $realignmentStatus
     *
     * @return profiles
     */
    public function setRealignmentStatus($realignmentStatus)
    {
        $this->realignmentStatus = $realignmentStatus;

        return $this;
    }

    /**
     * Get realignmentStatus
     *
     * @return string
     */
    public function getRealignmentStatus()
    {
        return $this->realignmentStatus;
    }

    /**
     * Set contributionsStatus
     *
     * @param string $contributionsStatus
     *
     * @return profiles
     */
    public function setContributionsStatus($contributionsStatus)
    {
        $this->contributionsStatus = $contributionsStatus;

        return $this;
    }

    /**
     * Get contributionsStatus
     *
     * @return string
     */
    public function getContributionsStatus()
    {
        return $this->contributionsStatus;
    }

    /**
     * Set catchupContributionStatus
     *
     * @param string $catchupContributionStatus
     *
     * @return profiles
     */
    public function setCatchupContributionStatus($catchupContributionStatus)
    {
        $this->catchupContributionStatus = $catchupContributionStatus;

        return $this;
    }

    /**
     * Set ATTransactStatus
     *
     * @param string $ATTransactStatus
     *
     * @return profiles
     */
    public function setATTransactStatus($ATTransactStatus)
    {
        $this->ATTransactStatus = $ATTransactStatus;

        return $this;
    }

    /**
     * Get catchupContributionStatus
     *
     * @return string
     */
    public function getCatchupContributionStatus()
    {
        return $this->catchupContributionStatus;
    }

    /**
     * Set annualSalary
     *
     * @param float $annualSalary
     *
     * @return profiles
     */
    public function setAnnualSalary($annualSalary)
    {
        $this->annualSalary = $annualSalary;

        return $this;
    }

    /**
     * Get annualSalary
     *
     * @return float
     */
    public function getAnnualSalary()
    {
        return $this->annualSalary;
    }

    /**
     * Set retireAge
     *
     * @param integer $retireAge
     *
     * @return profiles
     */
    public function setRetireAge($retireAge)
    {
        $this->retireAge = $retireAge;

        return $this;
    }

    /**
     * Get retireAge
     *
     * @return integer
     */
    public function getRetireAge()
    {
        return $this->retireAge;
    }

    /**
     * Set preTaxSavingRate
     *
     * @param float $preTaxSavingRate
     *
     * @return profiles
     */
    public function setPreTaxSavingRate($preTaxSavingRate)
    {
        $this->preTaxSavingRate = $preTaxSavingRate;

        return $this;
    }

    /**
     * Get preTaxSavingRate
     *
     * @return float
     */
    public function getPreTaxSavingRate()
    {
        return $this->preTaxSavingRate;
    }

    /**
     * Set postTaxSavingRate
     *
     * @param float $postTaxSavingRate
     *
     * @return profiles
     */
    public function setPostTaxSavingRate($postTaxSavingRate)
    {
        $this->postTaxSavingRate = $postTaxSavingRate;

        return $this;
    }

    /**
     * Get postTaxSavingRate
     *
     * @return float
     */
    public function getPostTaxSavingRate()
    {
        return $this->postTaxSavingRate;
    }

    /**
     * Set rothTaxSavingRate
     *
     * @param float $rothTaxSavingRate
     *
     * @return profiles
     */
    public function setRothTaxSavingRate($rothTaxSavingRate)
    {
        $this->rothTaxSavingRate = $rothTaxSavingRate;

        return $this;
    }

    /**
     * Get rothTaxSavingRate
     *
     * @return float
     */
    public function getRothTaxSavingRate()
    {
        return $this->rothTaxSavingRate;
    }

    /**
     * Set mStarContribution
     *
     * @param string $mStarContribution
     *
     * @return profiles
     */
    public function setMStarContribution($mStarContribution)
    {
        $this->mStarContribution = $mStarContribution;

        return $this;
    }

    /**
     * Get mStarContribution
     *
     * @return string
     */
    public function getMStarContribution()
    {
        return $this->mStarContribution;
    }

    /**
     * Set mStarStatus
     *
     * @param string $mStarStatus
     *
     * @return profiles
     */
    public function setMStarStatus($mStarStatus)
    {
        $this->mStarStatus = $mStarStatus;

        return $this;
    }

    /**
     * Get mStarStatus
     *
     * @return string
     */
    public function getMStarStatus()
    {
        return $this->mStarStatus;
    }

    /**
     * Set smart401kStatus
     *
     * @param string $smart401kStatus
     *
     * @return profiles
     */
    public function setSmart401kStatus($smart401kStatus)
    {
        $this->smart401kStatus = $smart401kStatus;

        return $this;
    }

    /**
     * Get smart401kStatus
     *
     * @return string
     */
    public function getSmart401kStatus()
    {
        return $this->smart401kStatus;
    }

    /**
     * Set reportDate
     *
     * @param \DateTime $reportDate
     *
     * @return profiles
     */
    public function setReportDate($reportDate)
    {
        $this->reportDate = $reportDate;

        return $this;
    }

    /**
     * Get reportDate
     *
     * @return \DateTime
     */
    public function getReportDate()
    {
        return $this->reportDate;
    }

    /**
     * Set profileId
     *
     * @param string $profileId
     *
     * @return profiles
     */
    public function setProfileId($profileId)
    {
        $this->profileId = $profileId;

        return $this;
    }

    /**
     * Get profileId
     *
     * @return string
     */
    public function getProfileId()
    {
        return $this->profileId;
    }

    /**
     * Set acceptedAdvice
     *
     * @param string $acceptedAdvice
     *
     * @return profiles
     */
    public function setAcceptedAdvice($acceptedAdvice)
    {
        $this->acceptedAdvice = $acceptedAdvice;

        return $this;
    }

    /**
     * Get acceptedAdvice
     *
     * @return string
     */
    public function getAcceptedAdvice()
    {
        return $this->acceptedAdvice;
    }

    /**
     * Set aCAOptOutStatus
     *
     * @param string $aCAOptOutStatus
     *
     * @return profiles
     */
    public function setACAOptOutStatus($aCAOptOutStatus)
    {
        $this->ACAOptOutStatus = $aCAOptOutStatus;

        return $this;
    }

    /**
     * Get aCAOptOutStatus
     *
     * @return string
     */
    public function getACAOptOutStatus()
    {
        return $this->ACAOptOutStatus;
    }

    /**
     * Set profilestatus
     *
     * @param integer $profilestatus
     *
     * @return profiles
     */
    public function setProfilestatus($profilestatus)
    {
        $this->profilestatus = $profilestatus;

        return $this;
    }

    /**
     * Get profilestatus
     *
     * @return integer
     */
    public function getProfilestatus()
    {
        return $this->profilestatus;
    }

    /**
     * Set availability
     *
     * @param string $availability
     *
     * @return profiles
     */
    public function setAvailability($availability)
    {
        $this->availability = $availability;

        return $this;
    }

    /**
     * Get availability
     *
     * @return string
     */
    public function getAvailability()
    {
        return $this->availability;
    }

    /**
     * Set clientCalling
     *
     * @param string $clientCalling
     *
     * @return profiles
     */
    public function setClientCalling($clientCalling)
    {
        $this->clientCalling = $clientCalling;

        return $this;
    }

    /**
     * Get clientCalling
     *
     * @return string
     */
    public function getClientCalling()
    {
        return $this->clientCalling;
    }

    /**
     * Set mstarQuit
     *
     * @param integer $mstarQuit
     *
     * @return profiles
     */
    public function setMstarQuit($mstarQuit)
    {
        $this->mstarQuit = $mstarQuit;

        return $this;
    }

    /**
     * Get mstarQuit
     *
     * @return integer
     */
    public function getMstarQuit()
    {
        return $this->mstarQuit;
    }

    /**
     * Set rkdData
     *
     * @param string $rkdData
     *
     * @return profiles
     */
    public function setRkdData($rkdData)
    {
        $this->rkdData = $rkdData;

        return $this;
    }

    /**
     * Get rkdData
     *
     * @return string
     */
    public function getRkdData()
    {
        return $this->rkdData;
    }

    /**
     * Set welcomeVideo
     *
     * @param string $welcomeVideo
     *
     * @return profiles
     */
    public function setWelcomeVideo($welcomeVideo)
    {
        $this->welcomeVideo = $welcomeVideo;

        return $this;
    }

    /**
     * Get welcomeVideo
     *
     * @return string
     */
    public function getWelcomeVideo()
    {
        return $this->welcomeVideo;
    }

    /**
     * Set planBasicVideo
     *
     * @param string $planBasicVideo
     *
     * @return profiles
     */
    public function setPlanBasicVideo($planBasicVideo)
    {
        $this->planBasicVideo = $planBasicVideo;

        return $this;
    }

    /**
     * Get planBasicVideo
     *
     * @return string
     */
    public function getPlanBasicVideo()
    {
        return $this->planBasicVideo;
    }

    /**
     * Set uniqid
     *
     * @param string $uniqid
     *
     * @return profiles
     */
    public function setUniqid($uniqid)
    {
        $this->uniqid = $uniqid;

        return $this;
    }

    /**
     * Get uniqid
     *
     * @return string
     */
    public function getUniqid()
    {
        return $this->uniqid;
    }

    /**
     * Set migration
     *
     * @param integer $migration
     *
     * @return profiles
     */
    public function setMigration($migration)
    {
        $this->migration = $migration;

        return $this;
    }

    /**
     * Get migration
     *
     * @return integer
     */
    public function getMigration()
    {
        return $this->migration;
    }

    /**
     * Set participant
     *
     * @param \classes\classBundle\Entity\participants $participant
     *
     * @return profiles
     */
    public function setParticipant(\classes\classBundle\Entity\participants $participant = null)
    {
        $this->participant = $participant;

        return $this;
    }

    /**
     * Get participant
     *
     * @return \classes\classBundle\Entity\participants
     */
    public function getParticipant()
    {
        return $this->participant;
    }

    /**
     * Add beneficiary
     *
     * @param \classes\classBundle\Entity\profilesBeneficiaries $beneficiary
     *
     * @return profiles
     */
    public function addBeneficiary(\classes\classBundle\Entity\profilesBeneficiaries $beneficiary)
    {
        $this->beneficiaries[] = $beneficiary;

        return $this;
    }

    /**
     * Remove beneficiary
     *
     * @param \classes\classBundle\Entity\profilesBeneficiaries $beneficiary
     */
    public function removeBeneficiary(\classes\classBundle\Entity\profilesBeneficiaries $beneficiary)
    {
        $this->beneficiaries->removeElement($beneficiary);
    }

    /**
     * Get beneficiaries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBeneficiaries()
    {
        return $this->beneficiaries;
    }

    /**
     * Add contribution
     *
     * @param \classes\classBundle\Entity\profilesContributions $contribution
     *
     * @return profiles
     */
    public function addContribution(\classes\classBundle\Entity\profilesContributions $contribution)
    {
        $this->contributions[] = $contribution;

        return $this;
    }

    /**
     * Remove contribution
     *
     * @param \classes\classBundle\Entity\profilesContributions $contribution
     */
    public function removeContribution(\classes\classBundle\Entity\profilesContributions $contribution)
    {
        $this->contributions->removeElement($contribution);
    }

    /**
     * Get contributions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContributions()
    {
        return $this->contributions;
    }

    /**
     * Add investment
     *
     * @param \classes\classBundle\Entity\profilesInvestments $investment
     *
     * @return profiles
     */
    public function addInvestment(\classes\classBundle\Entity\profilesInvestments $investment)
    {
        $this->investments[] = $investment;

        return $this;
    }

    /**
     * Remove investment
     *
     * @param \classes\classBundle\Entity\profilesInvestments $investment
     */
    public function removeInvestment(\classes\classBundle\Entity\profilesInvestments $investment)
    {
        $this->investments->removeElement($investment);
    }

    /**
     * Get investments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInvestments()
    {
        return $this->investments;
    }

    /**
     * Add retirementNeed
     *
     * @param \classes\classBundle\Entity\profilesRetirementNeeds $retirementNeed
     *
     * @return profiles
     */
    public function addRetirementNeed(\classes\classBundle\Entity\profilesRetirementNeeds $retirementNeed)
    {
        $this->retirementNeeds[] = $retirementNeed;

        return $this;
    }

    /**
     * Remove retirementNeed
     *
     * @param \classes\classBundle\Entity\profilesRetirementNeeds $retirementNeed
     */
    public function removeRetirementNeed(\classes\classBundle\Entity\profilesRetirementNeeds $retirementNeed)
    {
        $this->retirementNeeds->removeElement($retirementNeed);
    }

    /**
     * Get retirementNeeds
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRetirementNeeds()
    {
        return $this->retirementNeeds;
    }

    /**
     * Add riskProfile
     *
     * @param \classes\classBundle\Entity\profilesRiskProfile $riskProfile
     *
     * @return profiles
     */
    public function addRiskProfile(\classes\classBundle\Entity\profilesRiskProfile $riskProfile)
    {
        $this->riskProfile[] = $riskProfile;

        return $this;
    }

    /**
     * Remove riskProfile
     *
     * @param \classes\classBundle\Entity\profilesRiskProfile $riskProfile
     */
    public function removeRiskProfile(\classes\classBundle\Entity\profilesRiskProfile $riskProfile)
    {
        $this->riskProfile->removeElement($riskProfile);
    }

    /**
     * Get riskProfile
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRiskProfile()
    {
        return $this->riskProfile;
    }

    /**
     * Set enrollmentStatus
     *
     * @param string $enrollmentStatus
     *
     * @return profiles
     */
    public function setEnrollmentStatus($enrollmentStatus)
    {
        $this->enrollmentStatus = $enrollmentStatus;

        return $this;
    }

    /**
     * Get enrollmentStatus
     *
     * @return string
     */
    public function getEnrollmentStatus()
    {
        return $this->enrollmentStatus;
    }

    /**
     * Get investmentType
     *
     * @return string
     */
    public function getInvestmentType()
    {
        return $this->investmentType;
    }
    /**
     * Set investmentType
     *
     * @param string $investmentType
     *
     * 
     */
    public function setInvestmentType($investmentType)
    {
        $this->investmentType = $investmentType;
    }

    /**
     * Set flexPlan
     *
     * @param boolean $flexPlan
     *
     * @return profiles
     */
    public function setFlexPlan($flexPlan)
    {
        $this->flexPlan = $flexPlan;

        return $this;
    }

    /**
     * Get flexPlan
     *
     * @return boolean
     */
    public function getFlexPlan()
    {
        return $this->flexPlan;
    }
    public function getECOMMModal()
    {
        return $this->ECOMMModal;        
    }
    public function setECOMMModal($ECOMMModal)
    {       
        if (!isset($ECOMMModal) || $ECOMMModal == null || $ECOMMModal == "")
        $ECOMMModal = 0;
        if ($ECOMMModal === "E")
        $ECOMMModal = 1;
        else if ($ECOMMModal === "P")
        $ECOMMModal = 0;
        $this->ECOMMModal = $ECOMMModal;   
        
    }   
}
