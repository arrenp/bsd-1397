<?php
namespace classes\classBundle\Entity;
use classes\classBundle\Entity\abstractclasses\InvestmentsConfiguration;
use Doctrine\ORM\Mapping as ORM;
/**
 * PlansInvestmentsConfiguration
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class PlansInvestmentsConfiguration extends InvestmentsConfiguration
{ 
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;  
}

