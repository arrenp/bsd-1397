<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class LNCLNKeys
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=50)
	 */
    public $name;
  /**
   * @var string
   *
   * @ORM\Column(name="value", type="string", length=255)
   */
    public $value;

}
