<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class participantsCommunication
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="participantid", type="string", length = 255)
     */
    public $participantid;
    /**
     * @var integer
     *
     * @ORM\Column(name="msgType", type="string", length = 45)
     */
    public $msgType;
    /**
     * @var integer
     *
     * @ORM\Column(name="sourceApplication", type="string", length = 45)
     */
    public $sourceApplication; 
    /**
     * @var integer
     *
     * @ORM\Column(name="emailAddress", type="string", length = 255)
     */
    public $emailAddress;     
    /**
     * @var integer
     *
     * @ORM\Column(name="mailEngine", type="string", length = 45)
     */
    public $mailEngine;         
    
    public function __construct()
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            if ($key != "id") $this->$key = "";
        }
    }

}
