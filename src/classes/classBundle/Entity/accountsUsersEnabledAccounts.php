<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class accountsUsersEnabledAccounts
{
    /** 
     * 
     * @ORM\ManyToOne(targetEntity="accounts", inversedBy="accountsUsersEnabledAccounts")
     * @ORM\JoinColumn(name="userid", referencedColumnName="id")
     */
    public $account;	
	 /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="userid",type="integer")
     */
    public $userid;
    /**
     * @var string
     *
     * @ORM\Column(name="accountsUsersId",type="integer")
     */
    public $accountsUsersId;


	public function __construct()//do not remove, will fail for doctrine add function
	{

		$class_vars = get_class_vars(get_class($this));
		foreach ($class_vars as $key => $value)
		{
			if ($this->$key != "id")
			$this->$key = "";
		}
			
	}

}
