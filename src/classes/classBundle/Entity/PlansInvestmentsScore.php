<?php
namespace classes\classBundle\Entity;
use classes\classBundle\Entity\abstractclasses\InvestmentsScore;
use Doctrine\ORM\Mapping as ORM;
/**
 * PlansInvestmentsScore
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class PlansInvestmentsScore extends InvestmentsScore 
{
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
}
