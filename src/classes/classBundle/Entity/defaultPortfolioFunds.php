<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class defaultPortfolioFunds
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
  	/**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
     * @var string
     *
     * @ORM\Column(name="fundid", type="integer")
     */
    public $fundid;
    /**
     * @var string
     *
     * @ORM\Column(name="portfolioid", type="integer")
     */
    public $portfolioid;
 	/**
     * @var integer
     *
     * @ORM\Column(name="orderid", type="integer")
     */
    public $orderid;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="percent", type="float")
	 */
    public $percent;

    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($this->$key != "id")
     		$this->$key = "";
   		}
	}

}
