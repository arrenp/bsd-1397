<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class HubSpotLog {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="accountsUsersId", type="integer")
     */
    public $accountsUsersId;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestamp", type="datetime")
     */
    private $timestamp;
    /**
     * @var string
     *
     * @ORM\Column(name="data", type="text")
     */
    public $data; 
    
    public function __construct() {
        $this->timestamp = new \DateTime();
    }

}
