<?php
namespace classes\classBundle\Entity;
use classes\classBundle\Entity\abstractclasses\InvestmentsLinkYearsToRetirementAndScore;
use Doctrine\ORM\Mapping as ORM;
/**
 * DefaultInvestmentsLinkYearsToRetirementAndScore
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class DefaultInvestmentsLinkYearsToRetirementAndScore extends InvestmentsLinkYearsToRetirementAndScore 
{

}
