<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class PlansAdviceNotificationRecipients {
	/**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
	/**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
	public $userid;
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
	public $planid;
    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=50)
     */
	public $firstName;
    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=50)
     */
	public $lastName;
	/**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
	public $email;
}
