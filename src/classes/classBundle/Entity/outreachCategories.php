<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class outreachCategories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="name", type="string",length = 100)
	 */
	public $name;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="orderid", type="integer")
	 */
	public $orderid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="integrated", type="integer")
	 */
	public $integrated;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="accounts", type="string", length = 1000)
	 */
	public $accounts;

    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($this->$key != "id")
     		$this->$key = "";
   		}
	}

}
