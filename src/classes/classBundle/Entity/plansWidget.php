<?php

namespace classes\classBundle\Entity;
use classes\classBundle\Entity\abstractclasses\rkp;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class plansWidget
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;    
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;     
     /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=45)
     */
    public $type;   
    public function __construct()
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
                
            $this->$key = "";
        }
    }    
}

