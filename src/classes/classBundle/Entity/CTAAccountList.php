<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class CTAAccountList
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

     /**
     * @var text
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;   
    /**
     * @var integer
     *
     * @ORM\Column(name="ctaid", type="integer")
     */
    public $ctaid;   
    public function __construct()//do not remove, will fail for doctrine add function
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            $this->$key = "";
        }
    }
}
