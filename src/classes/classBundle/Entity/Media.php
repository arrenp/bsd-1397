<?php

namespace classes\classBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="media")
 * @ORM\EntityListeners({"entityListener"})
 */
class Media
{
    public static $PRODUCT_OPTIONS = array(
        0       => "Smartplan Enterprise",
        2       => "Promotional Documents",
        9       => "Tutorials"
    );

    public static $TYPE_OPTIONS = array(
        'e'     => "Standard Type",
        'n'     => "Non-Integrated Type",
        'a'     => "Advice Type"
    );

    public static $CLIENT_OPTIONS = array(
        'SP'    => "SmartPlan",
        'FD'    => "Fiduciary Type",
        'AA'    => "Advisors Access",
        'AB'    => 'Alliance Benefit Group',
        'AX'    => "AXA Equitable",
        'LFG'   => "LFG",
        'MG'    => "MGM Resorts",
        'MO'    => "Mutual of Omaha"
    );

    public static $MEDIA_TYPE_OPTIONS = array(
        1       => "Audio",
        2       => "Video",
        3       => "Other"
    );

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * The product that the media belongs to
     * Refer self::$PRODUCT_OPTIONS for the available options
     * @var integer
     *
     * @ORM\Column(name="product", type="integer")
     */
    public $product;

    /**
     * The module under which the media falls
     * @var string
     *
     * @ORM\Column(name="module", type="string", length=7)
     */
    public $module;

    /**
     * The client / owner of this media
     * Refer self::$CLIENT_OPTIONS for the available options
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=7)
     */
    public $client;

    /**
     * The type of the media
     * Refer self::$TYPE_OPTIONS for the available options
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=1)
     */
    public $type;

    /**
     * Media Title
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    public $title;

    /**
     * The legacy name used by the CodeIgnitor / Flash app
     * @var string
     *
     * @ORM\Column(name="legacy_name", type="string", length=31)
     */
    public $legacyName;

    /**
     * The format of the media
     * Refer self::$MEDIA_TYPE_OPTIONS for the available options
     * @var integer
     *
     * @ORM\Column(name="media_type", type="integer")
     */
    public $mediaType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    public $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    public $updatedAt;
    /**
     * Media Title
     * @var string
     *
     * @ORM\Column(name="transcriptFile", type="string", length=255)
     */
    public $transcriptFile;
    /**
     * Media Title
     * @var string
     *
     * @ORM\Column(name="smartplanSection", type="string", length=50)
     */
    public $smartplanSection;    
    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="MediaLocalization", mappedBy="media", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    protected $localizations;
    /**
     * @var string
     *
     * @ORM\Column(name="source", type="string", length = 100)
     */
    public $source;      
    
    public function __construct()
    {
        $this->localizations = new ArrayCollection();
        $this->transcriptFile = "";
        $this->smartplanSection = "";
        $this->source = "";
    }

    /**
     * Returns the unique string representation to identify this media
     * @return string
     */
    public function getUid(){
        return $this->getProduct() . $this->getModule() . $this->getClient() . $this->getType();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set product
     *
     * @param integer $product
     *
     * @return Media
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return integer
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set module
     *
     * @param string $module
     *
     * @return Media
     */
    public function setModule($module)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return Media
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Media
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Media
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set legacyName
     *
     * @param string $legacyName
     *
     * @return Media
     */
    public function setLegacyName($legacyName)
    {
        $this->legacyName = $legacyName;

        return $this;
    }

    /**
     * Get legacyName
     *
     * @return string
     */
    public function getLegacyName()
    {
        return $this->legacyName;
    }

    /**
     * Set mediaType
     *
     * @param integer $mediaType
     *
     * @return Media
     */
    public function setMediaType($mediaType)
    {
        $this->mediaType = $mediaType;

        return $this;
    }

    /**
     * Get mediaType
     *
     * @return integer
     */
    public function getMediaType()
    {
        return $this->mediaType;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Media
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Media
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add localizations
     *
     * @param MediaLocalization $localization
     * @return Media
     */
    public function addLocalization(MediaLocalization $localization)
    {
        $localization->setMedia($this);
        $this->localizations[] = $localization;

        return $this;
    }

    /**
     * Remove localizations
     *
     * @param MediaLocalization $localization
     */
    public function removeLocalization(MediaLocalization $localization)
    {
        $this->localizations->removeElement($localization);
    }

    /**
     * @return ArrayCollection
     */
    public function getLocalizations()
    {
        return $this->localizations;
    }

    /**
     * @param ArrayCollection $localizations
     */
    public function setLocalizations($localizations)
    {
        foreach ($localizations as $localization) {
            $this->addLocalization($localization);
        }
    }
    public function getTranscriptFile()
    {
        return $this->transcriptFile;
    }
    public function setTranscriptFile($file)
    {
        $this->transcriptFile = $file;
    }
    public function getSmartplanSection()
    {
        return $this->smartplanSection;
    }
    public function setSmartplanSection($section)
    {
        $this->smartplanSection = $section;
    }
    public function getSource()
    {
        return $this->source;
    }
    public function setSource($source)
    {
        $this->source = $source;     
    }
}

