<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * messages
 *
 * @ORM\Table(name="defaultMessages")
 * @ORM\Entity
 *  @ORM\EntityListeners({"entityListener"})
 */
class DefaultMessages extends Messages {
    
}
