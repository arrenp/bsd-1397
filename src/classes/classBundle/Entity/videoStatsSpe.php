<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 *  @ORM\EntityListeners({"entityListener"})
 */
class videoStatsSpe
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;

    /**
     * @var string
     *
     * @ORM\Column(name="partnerid", type="string",length=255)
     */
    public $partnerid;
    /**
     * @var string
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
    /**
     * @var string
     *
     * @ORM\Column(name="recordkeeperPlanid", type="string",length=255)
     */
    public $recordkeeperPlanid;
    /**
     * @var string
     *
     * @ORM\Column(name="participantid", type="integer")
     */
    public $participantid;    
    /**
     * @var string
     *
     * @ORM\Column(name="sessionid", type="string",length=100)
     */
    public $sessionid;    
    /**
     * @var string
     *
     * @ORM\Column(name="videoPlayed", type="string", length = 255)
     */
    public $videoPlayed;    
    /**
     * @var string
     *
     * @ORM\Column(name="playDuration", type="float")
     */
    public $playDuration;     
    /**
     * @var string
     *
     * @ORM\Column(name="videoLength", type="float")
     */
    public $videoLength;       
    /**
     * @var string
     *
     * @ORM\Column(name="ipaddress", type="string",length=100)
     */
    public $ipaddress;      
    /**
     * @var string
     *
     * @ORM\Column(name="percentageViewed", type="float")
     */
    public $percentageViewed;     
    /**
    * @var string
    *
    * @ORM\Column(name="uniqid", type="string",length = 63 )
    */
    public $uniqid;    
    
    public function __construct()
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            if ($key != "id") $this->$key = "";
        }
    }

}
