<?php
namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class ATWsAccounts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="profileid", type="integer")
     */
    public $profileid;
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
     * @var string
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
    /**
     * @var string
     *
     * @ORM\Column(name="accountNumber", type="string", nullable=true, length = 255)
     */    
    public $accountNumber;
    /**
     * @var string
     *
     * @ORM\Column(name="accountName", type="string", nullable=true, length = 255)
     */    
    public $accountName;
    /**
     * @var string
     *
     * @ORM\Column(name="accountType", type="string", nullable=true, length = 255)
     */    
    public $accountType;
    /**
     * @var string
     *
     * @ORM\Column(name="accountCustodian", type="string", nullable=true, length = 255)
     */    
    public $accountCustodian;
    /**
     * @var string
     *
     * @ORM\Column(name="isSpouseAccount", type="string", length = 255)
     */
    public $isSpouseAccount;
    /**
     * @var string
     *
     * @ORM\Column(name="isIncomeAccount", type="string", length = 255)
     */
    public $isIncomeAccount;
    /**
     * @var float
     *
     * @ORM\Column(name="monthlyIncome", type="string", options={"default"="0.00"}, length = 255)
     */    
    public $monthlyIncome;
    /**
     * @var float
     *
     * @ORM\Column(name="balance", type="string", options={"default"="0.00"}, length = 255)
     */
    public $balance;
    /**
     * @var float
     *
     * @ORM\Column(name="equityBalance", type="string", options={"default"="0.00"}, length = 255)
     */
    public $equityBalance;
    /**
     * @var string
     *
     * @ORM\Column(name="balanceAsOf", type="string", nullable=true, length = 255)
     */
    public $balanceAsOf;
    /**
     * @var float
     *
     * @ORM\Column(name="eeAdditions", type="string", options={"default"="0.00"}, length = 255)
     */
    public $eeAdditions;
    /**
     * @var float
     *
     * @ORM\Column(name="erAdditions", type="string", options={"default"="0.00"}, length = 255)
     */
    public $erAdditions;
    /**
     * @var string
     *
     * @ORM\Column(name="defRateMethod", type="string", nullable=true, length = 255)
     */
    public $defRateMethod;
    /**
     * @var string
     *
     * @ORM\Column(name="preTaxPct", type="string", options={"default"="0.00"}, length = 255)
     */
    public $preTaxPct;
    /**
     * @var float
     *
     * @ORM\Column(name="preTaxDol", type="float")
     */
    public $preTaxDol;
    /**
     * @var string
     *
     * @ORM\Column(name="rothPct", type="string", options={"default"="0.00"}, length = 255)
     */
    public $rothPct;
    /**
     * @var float
     *
     * @ORM\Column(name="rothDol", type="float")
     */
    public $rothDol;
    /**
     * @var string
     *
     * @ORM\Column(name="payrollFrequency", type="string", nullable=true, length = 255)
     */
    public $payrollFrequency;
    /**
     * @var string
     *
     * @ORM\Column(name="blueprintDefaultEePct", type="string", length = 255)
     */
    public $blueprintDefaultEePct;
    /**
     * @var string
     *
     * @ORM\Column(name="blueprintDefaultMatchPct", type="string", length = 255)
     */
    public $blueprintDefaultMatchPct;
    /**
     * @var string
     *
     * @ORM\Column(name="blueprintDefaultErPct", type="string", length = 255)
     */
    public $blueprintDefaultErPct;
    /**
     * @var float
     *
     * @ORM\Column(name="expectedReturn", type="float")
     */
    public $expectedReturn;
    /**
     * @var string
     *
     * @ORM\Column(name="useExpectedReturn", type="string", length = 255)
     */
    public $useExpectedReturn;
    /**
     * @var string
     *
     * @ORM\Column(name="includeInCalcs", type="string", length = 255)
     */
    public $includeInCalcs;
    public function getXmlArray()
    {
        $return = array();
        foreach ($this as $key => $value)
        {
            if (!in_array($key,["id","planid","userid","profileid"]))
            {
                if(in_array($key,['erAdditions', 'eeAdditions'])) {
                    $return[ucfirst($key)] = str_replace(',', '', $value);
                } else {
                    $return[ucfirst($key)] = $value;
                }

            }
        }
        return $return;
    }
    public function __construct()//do not remove, will fail for doctrine add function
    {

        $class_vars = get_class_vars(get_class($this));
        $notNullArray = ['monthlyIncome', 'eeAdditions', 'erAdditions', 'preTaxPct', 'rothPct', 'blueprintDefaultEePct', 'blueprintDefaultMatchPct', 'blueprintDefaultErPct'];
        foreach ($class_vars as $key => $value)
        {
            if (in_array($key, $notNullArray) && $value == null) {
                $this->$key = "0.00";
            }
        }
    }
}
