<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class outreachVwiseVideoTemplates
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="headerimage", type="string", length=200, options = {"default" = "images/vWiseLogoNoTagRGB.png"} )
     */
    public $headerimage;

    /**
     * @var string
     *
     * @ORM\Column(name="footerdescription", type="string", length=200)
     */
    public $footerdescription;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="footerurl", type="string", length=200)
	 */
    public $footerurl;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="youtubeid", type="string", length=200)
	 */
	public $youtubeid;
  /**
   * @var string
   *
   * @ORM\Column(name="redirect", type="integer")
   */
  public $redirect;
  /**
   * @var string
   *
   * @ORM\Column(name="showfooter", type="integer")
   */
  public $showfooter;



    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($this->$key != "id")
     		$this->$key = "";
   		}
	}

}
