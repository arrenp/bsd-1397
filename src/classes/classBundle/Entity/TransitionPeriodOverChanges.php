<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class TransitionPeriodOverChanges {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
    /**
     * @var integer
     *
     * @ORM\Column(name="accountsUsersId", type="integer")
     */
    public $accountsUsersId;
    /**
     * @var bool
     *
     * @ORM\Column(name="changedTo", type="boolean")
     */
    public $changedTo;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestamp", type="datetime")
     */
    public $timestamp;
    
    public function __construct() {
        $this->timestamp = new \DateTime('now');
    }
}
