<?php
namespace classes\classBundle\Entity;
use classes\classBundle\Entity\abstractclasses\InvestmentsConfiguration;
use Doctrine\ORM\Mapping as ORM;
/**
 * DefaultInvestmentsConfiguration
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class DefaultInvestmentsConfiguration extends InvestmentsConfiguration
{
         
}

