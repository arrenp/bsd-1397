<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ApiUser
 *
 * @ORM\Table(name="api_user")
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class ApiUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="account_id", type="integer")
     */
    private $accountId;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="encrypted", length=50)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="encrypted", length=50)
     */
    private $password;

    /**
     * @var integer
     *
     * @ORM\Column(name="enabled", type="smallint")
     */
    private $enabled;

    /**
     * @var integer
     *
     * @ORM\Column(name="rate_limit", type="integer")
     */
    private $rateLimit;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set accountId
     *
     * @param integer $accountId
     *
     * @return ApiUser
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;

        return $this;
    }

    /**
     * Get accountId
     *
     * @return integer
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return ApiUser
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return ApiUser
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set enabled
     *
     * @param integer $enabled
     *
     * @return ApiUser
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return integer
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set rateLimit
     *
     * @param integer $rateLimit
     *
     * @return ApiUser
     */
    public function setRateLimit($rateLimit)
    {
        $this->rateLimit = $rateLimit;

        return $this;
    }

    /**
     * Get rateLimit
     *
     * @return integer
     */
    public function getRateLimit()
    {
        return $this->rateLimit;
    }
}

