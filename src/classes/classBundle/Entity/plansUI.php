<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class plansUI
{
 
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
 	/**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
	public $userid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="planid", type="string", length=50)
	 */
    public $planid;
    /**
     * @var string
     *
     * @ORM\Column(name="deferralTypeUI", type="smallint")
     */
    public $deferralTypeUI;
    /**
     * @var string
     *
     * @ORM\Column(name="matchingContributionsUI", type="smallint")
     */
    public $matchingContributionsUI;
    /**
     * @var string
     *
     * @ORM\Column(name="catchupUI", type="smallint")
     */
    public $catchupUI; 
    /**
     * @var string
     *
     * @ORM\Column(name="loansUI", type="smallint")
     */
    public $loansUI;            
    /**
     * @var string
     *
     * @ORM\Column(name="enrollmentUI", type="smallint")
     */
    public $enrollmentUI;
    /**
     * @var string
     *
     * @ORM\Column(name="vestingUI", type="smallint")
     */
    public $vestingUI;       	

    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($this->$key != "id")
     		$this->$key = "";

   		}
   		
	}

}
