<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * messagesPanes
 *
 * @ORM\Table()
 * @ORM\Entity
 *  @ORM\EntityListeners({"entityListener"})
 */
class messagesPanes {
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    public $name;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=100)
     */
    public $description;
    /**
     * @var integer
     *
     * @ORM\Column(name="messagesPanesGroupsId", type="integer")
     */
    public $messagesPanesGroupsId;
    /**
     * @var integer
     *
     * @ORM\Column(name="displayOrder", type="smallint")
     */
    public $displayOrder;
}
