<?php
namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class ATArchitect
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */

    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
     * @var string
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
    /**
     * @var string
     *
     * @ORM\Column(name="profileid", type="integer")
     */
    public $profileid;    
    /**
     * @var string
     *
     * @ORM\Column(name="takeAdvantage", type="smallint", nullable=true)
     */    
    public $takeAdvantage;   
    /**
     * @var string
     *
     * @ORM\Column(name="agreement", type="smallint", nullable=true)
     */    
    public $agreement;
    /**
     * @var string
     *
     * @ORM\Column(name="currentInvestment", type="string", nullable=true, length = 255)
     */    
    public $currentInvestment;
    /**
     * @var string
     *
     * @ORM\Column(name="currentInvestmentPercent", type="integer", nullable=true)
     */    
    public $currentInvestmentPercent;  
        /**
     * @var string
     *
     * @ORM\Column(name="suggestedInvestment", type="string", nullable=true, length = 255)
     */    
    public $suggestedInvestment;
    /**
     * @var string
     *
     * @ORM\Column(name="suggestedInvestmentPercent", type="integer", nullable=true)
     */    
    public $suggestedInvestmentPercent; 
    /**
     * @var string
     *
     * @ORM\Column(name="acceptedInvestment", type="smallint", nullable=true)
     */    
    public $acceptedInvestment;  
}
