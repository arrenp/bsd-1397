<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DefaultLibraryUsers
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class DefaultLibraryUsers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;

    /**
     * @var integer
     *
     * @ORM\Column(name="videoid", type="integer")
     */
    public $videoid;

}

