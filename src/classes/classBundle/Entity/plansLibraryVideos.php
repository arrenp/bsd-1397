<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * plansLibraryVideos
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class plansLibraryVideos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="directoryid", type="integer")
     */
    public $directoryid;

    /**
     * @var integer
     *
     * @ORM\Column(name="typeid", type="integer")
     */
    public $typeid;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    public $name;

    /**
     * @var string
     *
     * @ORM\Column(name="displayname", type="string", length=255)
     */
    public $displayname;

    /**
     * @var string
     *
     * @ORM\Column(name="video", type="string", length=255)
     */
    public $video;
    
    /**
     * @var string
     *
     * @ORM\Column(name="thumbnail", type="string", length=255)
     */
    public $thumbnail;
    /**
     * @var string
     *
     * @ORM\Column(name="runtime", type="string", length=45)
     */
    public $runtime;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set directoryid
     *
     * @param integer $directoryid
     *
     * @return plansLibraryVideos
     */
    public function setDirectoryid($directoryid)
    {
        $this->directoryid = $directoryid;

        return $this;
    }

    /**
     * Get directoryid
     *
     * @return integer
     */
    public function getDirectoryid()
    {
        return $this->directoryid;
    }

    /**
     * Set typeid
     *
     * @param integer $typeid
     *
     * @return plansLibraryVideos
     */
    public function setTypeid($typeid)
    {
        $this->typeid = $typeid;

        return $this;
    }

    /**
     * Get typeid
     *
     * @return integer
     */
    public function getTypeid()
    {
        return $this->typeid;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return plansLibraryVideos
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set displayname
     *
     * @param string $displayname
     *
     * @return plansLibraryVideos
     */
    public function setDisplayname($displayname)
    {
        $this->displayname = $displayname;

        return $this;
    }

    /**
     * Get displayname
     *
     * @return string
     */
    public function getDisplayname()
    {
        return $this->displayname;
    }

    /**
     * Set video
     *
     * @param string $video
     *
     * @return plansLibraryVideos
     */
    public function setVideo($video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return string
     */
    public function getVideo()
    {
        return $this->video;
    }
}

