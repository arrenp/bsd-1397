<?php

namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
class groups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255)
     */
    public $firstName;
    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255)
     */
    public $lastName;  
     /**
     * @var string
     *
     * @ORM\Column(name="emailAddress", type="string", length=255)
     */
    public $emailAddress;   
     /**
     * @var string
     *
     * @ORM\Column(name="phoneNumber", type="string", length=255)
     */
    public $phoneNumber;     
    
    public function __construct()//do not remove, will fail for doctrine add function
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
             $this->$key = "";

        }
    }
}

