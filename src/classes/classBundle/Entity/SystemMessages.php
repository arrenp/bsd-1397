<?php

namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class SystemMessages extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;    
    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text" ,length = 65535)
     */
    public $message;    
    /**
     * @var string
     *
     * @ORM\Column(name="expirationDate", type="datetime",nullable=true)
     */
    public $expirationDate;       
}

