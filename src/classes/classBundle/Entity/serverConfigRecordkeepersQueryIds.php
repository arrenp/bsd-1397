<?php

namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * serverConfigRecordkeepersQueryIds
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class serverConfigRecordkeepersQueryIds {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=25)
     */
    public $type;
    /**
     * @var string
     *
     * @ORM\Column(name="queryid", type="string", length=100)
     */
    public $queryid;
}
