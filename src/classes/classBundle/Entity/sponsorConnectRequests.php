<?php

namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class sponsorConnectRequests
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;    
    /**
     * @var string
     *
     * @ORM\Column(name="fromFirstName", type="string", length=255)
     */
    public $fromFirstName;
    /**
     * @var string
     *
     * @ORM\Column(name="fromLastName", type="string", length=255)
     */
    public $fromLastName;
    /**
     * @var string
     *
     * @ORM\Column(name="fromEmailAddress", type="string", length=255)
     */
    public $fromEmailAddress;
    /**
     * @var string
     *
     * @ORM\Column(name="fromPhone", type="string", length=255)
     */
    public $fromPhone;
    /**
     * @var string
     *
     * @ORM\Column(name="toFirstName", type="encrypted", length=255)
     */
    public $toFirstName;
    /**
     * @var string
     *
     * @ORM\Column(name="toLastName", type="encrypted", length=255)
     */
    public $toLastName;
    /**
     * @var string
     *
     * @ORM\Column(name="toEmailAddress", type="encrypted", length=255)
     */
    public $toEmailAddress;     
    /**
     * @var string
     *
     * @ORM\Column(name="toPhone", type="encrypted", length=255)
     */
    public $toPhone;
    /**
     * @var integer
     *
     * @ORM\Column(name="templateid", type="integer")
     */
    public $templateid;
    /**
     * @var string
     *
     * @ORM\Column(name="requestStatus", type="string", length=50)
     */
    public $requestStatus = 'pending';    
    /**
    * @var string
    *
    * @ORM\Column(name="requestDate", type="datetime", nullable=true)
    */
    public $requestDate = null;
    /**
    * @var string
    *
    * @ORM\Column(name="scheduleDate", type="datetime", nullable=true)
    */
    public $scheduleDate = null;    
    /**
     * @var integer
     *
     * @ORM\Column(name="campaignid", type="integer")
     */
    public $campaignid;   
    /**
     * @var string
     *
     * @ORM\Column(name="notifiyEmailAddress", type="string", length=255)
     */
    public $notifiyEmailAddress; 
    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="text")
     */
    public $subject;     
    public function __construct()//do not remove, will fail for doctrine add function
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            if (empty($this->$key) && !in_array($key, array("requestDate", "scheduleDate"))) 
            {
                $this->$key = "";
            }
        }
    }
}

