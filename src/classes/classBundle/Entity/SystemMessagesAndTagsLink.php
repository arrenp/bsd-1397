<?php

namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class SystemMessagesAndTagsLink extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;  
    /**
     * @var string
     *
     * @ORM\Column(name="systemMessageId", type="integer")
     */
    public $systemMessageId;
    /**
     * @var string
     *
     * @ORM\Column(name="systemMessageTagsId", type="integer")
     */
    public $systemMessageTagsId;    
    
}
