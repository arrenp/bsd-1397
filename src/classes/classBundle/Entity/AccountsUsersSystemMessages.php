<?php

namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class AccountsUsersSystemMessages extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="accountsUsersId",type="integer")
     */
    public $accountsUsersId;
    /**
     * @var string
     *
     * @ORM\Column(name="systemMessageId", type="integer")
     */
    public $systemMessageId;
    /**
     * @var string
     *
     * @ORM\Column(name="isRead", type="smallint")
     */
    public $isRead;    
    /**
     * @var string
     *
     * @ORM\Column(name="deleted", type="smallint")
     */
    public $deleted;    
    public function __construct()
    {
        $this->isRead = 0;
        $this->deleted = 0;
    }
}

