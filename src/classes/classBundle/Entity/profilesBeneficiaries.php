<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class profilesBeneficiaries
{
	/**
     * @ORM\ManyToOne(targetEntity="profiles", inversedBy="beneficiaries")
     * @ORM\JoinColumn(name="profileid", referencedColumnName="id")
     **/
	public $profile;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
 	/**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
	public $userid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="planid", type="integer")
	 */
    public $planid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="profileid", type="integer")
	 */
    public $profileid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="type", type="string", length = 15)
	 */
    public $type;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="firstName", type="encrypted")
	 */
    public $firstName;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="lastName", type="encrypted")
	 */
    public $lastName;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="gender", type="encrypted")
	 */
    public $gender;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="relationship", type="string", length = 15)
	 */
    public $relationship;
    /**
	 * @var string
	 *
	 * @ORM\Column(name="maritalStatus", type="encrypted")
	 */
    public $maritalStatus;//checked
	/**
	 * @var string
	 *
	 * @ORM\Column(name="ssn", type="encrypted")
	 */
    public $ssn;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="percent", type="float", length = 255)
	 */
    public $percent;
    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length = 50)
     */
    public $city;
    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length = 50)
     */
    public $state;
    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length = 10)
     */
    public $zip;
    /**
     * @var string
     * @ORM\Column(name="dob", type="encrypted")
     */
    public $dob;
    /**
     * @var string
     *
     * @ORM\Column(name="address1", type="encrypted")
     */
    public $address1;
    /**
     * @var string
     *
     * @ORM\Column(name="address2", type="encrypted")
     */
    public $address2;
    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="encrypted")
     */
    public $phone;    
    /**
     * @var string
     *
     * @ORM\Column(name="level", type="string", length = 15)
     */
    public $level;
    /**
     * @var string
     *
     * @ORM\Column(name="spousalWaiverForm", type="string", length = 255)
     */
    public $spousalWaiverForm;
    /**
     * @var string
     *
     * @ORM\Column(name="participantsId", type="integer")
     */
    public $participantsId;    
    /**
     * @var string
     *
     * @ORM\Column(name="trustEstateCharityOther", type="smallint")
     */
    public $trustEstateCharityOther;  
    /**
     * @var string
     *
     * @ORM\Column(name="foreignaddress", type="smallint")
     */
    public $foreignaddress;
    /**
     * @var string
     *
     * @ORM\Column(name="country", type="encrypted")
     */
    public $country;
    /**
     * @var string
     *
     * @ORM\Column(name="middleInitial", type="encrypted")
     */
    public $middleInitial;     
    
    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($this->$key != "id")
     		$this->$key = "";
   		}
	}


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     *
     * @return profilesBeneficiaries
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set planid
     *
     * @param integer $planid
     *
     * @return profilesBeneficiaries
     */
    public function setPlanid($planid)
    {
        $this->planid = $planid;

        return $this;
    }

    /**
     * Get planid
     *
     * @return integer
     */
    public function getPlanid()
    {
        return $this->planid;
    }

    /**
     * Set profileid
     *
     * @param integer $profileid
     *
     * @return profilesBeneficiaries
     */
    public function setProfileid($profileid)
    {
        $this->profileid = $profileid;

        return $this;
    }

    /**
     * Get profileid
     *
     * @return integer
     */
    public function getProfileid()
    {
        return $this->profileid;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return profilesBeneficiaries
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return profilesBeneficiaries
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return profilesBeneficiaries
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return profilesBeneficiaries
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set relationship
     *
     * @param string $relationship
     *
     * @return profilesBeneficiaries
     */
    public function setRelationship($relationship)
    {
        $this->relationship = $relationship;

        return $this;
    }

    /**
     * Get relationship
     *
     * @return string
     */
    public function getRelationship()
    {
        return $this->relationship;
    }

    /**
     * Set maritalStatus
     *
     * @param string $maritalStatus
     *
     * @return profilesBeneficiaries
     */
    public function setMaritalStatus($maritalStatus)
    {
        $this->maritalStatus = $maritalStatus;

        return $this;
    }

    /**
     * Get maritalStatus
     *
     * @return string
     */
    public function getMaritalStatus()
    {
        return $this->maritalStatus;
    }

    /**
     * Set ssn
     *
     * @param string $ssn
     *
     * @return profilesBeneficiaries
     */
    public function setSsn($ssn)
    {
        $this->ssn = $ssn;

        return $this;
    }

    /**
     * Get ssn
     *
     * @return string
     */
    public function getSsn()
    {
        return $this->ssn;
    }

    /**
     * Set percent
     *
     * @param float $percent
     *
     * @return profilesBeneficiaries
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * Get percent
     *
     * @return float
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * Set profile
     *
     * @param \classes\classBundle\Entity\profiles $profile
     *
     * @return profilesBeneficiaries
     */
    public function setProfile(\classes\classBundle\Entity\profiles $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile
     *
     * @return \classes\classBundle\Entity\profiles
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return profilesBeneficiaries
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return profilesBeneficiaries
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set zip
     *
     * @param string $zip
     *
     * @return profilesBeneficiaries
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set dob
     *
     * @param string $dob
     *
     * @return profilesBeneficiaries
     */
    public function setDob($dob)
    {
        $this->dob = $dob;

        return $this;
    }

    /**
     * Get dob
     *
     * @return string
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * Set address1
     *
     * @param string $address1
     *
     * @return profilesBeneficiaries
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address1
     *
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set address2
     *
     * @param string $address2
     *
     * @return profilesBeneficiaries
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param string $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return string
     */
    public function getSpousalWaiverForm()
    {
        return $this->spousalWaiverForm;
    }

    /**
     * @param string $spousalWaiverForm
     */
    public function setSpousalWaiverForm($spousalWaiverForm)
    {
        $this->spousalWaiverForm = $spousalWaiverForm;
    }
    /**
     * @return string
     */
    public function getParticipantsId()
    {
        return $this->participantsId;
    }

    public function setParticipantsId($participantsId)
    {
        $this->participantsId = $participantsId;
    }
    public function getPhone()
    {
        return $this->phone;
    }
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }    
    public function getTrustEstateCharityOther()
    {
        return $this->trustEstateCharityOther;
    }
    public function setTrustEstateCharityOther($trustEstateCharityOther)
    {
        $this->trustEstateCharityOther = $trustEstateCharityOther;
    }  
    public function getForeignaddress()
    {
        return $this->foreignaddress;
    }
    public function setForeignaddress($foreignaddress)
    {
        $this->foreignaddress = $foreignaddress;
    }
    public function getCountry()
    {
        return $this->country;
    }
    public function setCountry($country)
    {
        $this->country = $country;
    }
    public function getMiddleInitial()
    {
        return $this->middleInitial;
    }
    public function setMiddleInitial($middleInitial)
    {
        $this->middleInitial = $middleInitial;
    }    
}
