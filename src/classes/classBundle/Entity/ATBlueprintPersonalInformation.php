<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ATBlueprintPersonalInformation
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class ATBlueprintPersonalInformation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="profileid", type="integer")
     */
    public $profileid;

    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;

    /**
     * @var string
     *
     * @ORM\Column(name="planid", type="string", length=255)
     */
    public $planid;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="encrypted", length=255)
     */
    public $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="encrypted", length=255)
     */
    public $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="encrypted", length=255)
     */
    public $email;

    /**
     * @var string
     *
     * @ORM\Column(name="mobilePhone", type="encrypted", length=255)
     */
    public $mobilePhone;

    /**
     * @var string
     *
     * @ORM\Column(name="annualCompensation", type="string", length=255)
     */
    public $annualCompensation;

    /**
     * @var string
     *
     * @ORM\Column(name="dateOfBirth", type="encrypted", length=255)
     */
    public $dateOfBirth;

    /**
     * @var string
     *
     * @ORM\Column(name="spouseFirstName", type="encrypted", length=255)
     */
    public $spouseFirstName;

    /**
     * @var string
     *
     * @ORM\Column(name="spouseLastName", type="encrypted", length=255)
     */
    public $spouseLastName;

    /**
     * @var string
     *
     * @ORM\Column(name="spouseAnnualCompensation", type="string", length=255)
     */
    public $spouseAnnualCompensation;

    /**
     * @var string
     *
     * @ORM\Column(name="spouseDateOfBirth", type="encrypted", length=255)
     */
    public $spouseDateOfBirth;

    /**
     * @var string
     *
     * @ORM\Column(name="retirementAge", type="string", length=255)
     */
    public $retirementAge;

    /**
     * @var string
     *
     * @ORM\Column(name="yearsInRetirement", type="string", length=255)
     */
    public $yearsInRetirement;

    /**
     * @var string
     *
     * @ORM\Column(name="socialSecurityBeginAge", type="string", length=255)
     */
    public $socialSecurityBeginAge;

    /**
     * @var string
     *
     * @ORM\Column(name="monthlyRetirementGoal", type="string", length=255)
     */
    public $monthlyRetirementGoal;

    /**
     * @var string
     *
     * @ORM\Column(name="socialSecurityMonthlyAmount", type="string", length=255)
     */
    public $socialSecurityMonthlyAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="spouseSocialSecurityMonthlyAmount", type="string", length=255)
     */
    public $spouseSocialSecurityMonthlyAmount;

    /**
     * @var boolean
     *
     * @ORM\Column(name="includeSpousalInformation", type="string")
     */
    public $includeSpousalInformation;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set profileid
     *
     * @param integer $profileid
     *
     * @return ATBlueprintPersonalInformation
     */
    public function setProfileid($profileid)
    {
        $this->profileid = $profileid;

        return $this;
    }

    /**
     * Get profileid
     *
     * @return integer
     */
    public function getProfileid()
    {
        return $this->profileid;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     *
     * @return ATBlueprintPersonalInformation
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set planid
     *
     * @param integer $planid
     *
     * @return ATBlueprintPersonalInformation
     */
    public function setPlanid($planid)
    {
        $this->planid = $planid;

        return $this;
    }

    /**
     * Get planid
     *
     * @return integer
     */
    public function getPlanid()
    {
        return $this->planid;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return ATBlueprintPersonalInformation
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return ATBlueprintPersonalInformation
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return ATBlueprintPersonalInformation
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set mobilePhone
     *
     * @param string $mobilePhone
     *
     * @return ATBlueprintPersonalInformation
     */
    public function setMobilePhone($mobilePhone)
    {
        $this->mobilePhone = $mobilePhone;
        $this->mobilePhone = str_replace(array("-"," ","(",")"),"",$this->mobilePhone);
        return $this;
    }

    /**
     * Get mobilePhone
     *
     * @return string
     */
    public function getMobilePhone()
    {
        return $this->mobilePhone;
    }

    /**
     * Set annualCompensation
     *
     * @param string $annualCompensation
     *
     * @return ATBlueprintPersonalInformation
     */
    public function setAnnualCompensation($annualCompensation)
    {
        $this->annualCompensation = $annualCompensation;

        return $this;
    }

    /**
     * Get annualCompensation
     *
     * @return string
     */
    public function getAnnualCompensation()
    {
        return $this->annualCompensation;
    }

    /**
     * Set dateOfBirth
     *
     * @param string $dateOfBirth
     *
     * @return ATBlueprintPersonalInformation
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    /**
     * Get dateOfBirth
     *
     * @return string
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * Set spouseFirstName
     *
     * @param string $spouseFirstName
     *
     * @return ATBlueprintPersonalInformation
     */
    public function setSpouseFirstName($spouseFirstName)
    {
        $this->spouseFirstName = $spouseFirstName;

        return $this;
    }

    /**
     * Get spouseFirstName
     *
     * @return string
     */
    public function getSpouseFirstName()
    {
        return $this->spouseFirstName;
    }

    /**
     * Set spouseLastName
     *
     * @param string $spouseLastName
     *
     * @return ATBlueprintPersonalInformation
     */
    public function setSpouseLastName($spouseLastName)
    {
        $this->spouseLastName = $spouseLastName;

        return $this;
    }

    /**
     * Get spouseLastName
     *
     * @return string
     */
    public function getSpouseLastName()
    {
        return $this->spouseLastName;
    }

    /**
     * Set spouseAnnualCompensation
     *
     * @param string $spouseAnnualCompensation
     *
     * @return ATBlueprintPersonalInformation
     */
    public function setSpouseAnnualCompensation($spouseAnnualCompensation)
    {
        $this->spouseAnnualCompensation = $spouseAnnualCompensation;

        return $this;
    }

    /**
     * Get spouseAnnualCompensation
     *
     * @return string
     */
    public function getSpouseAnnualCompensation()
    {
        return $this->spouseAnnualCompensation;
    }

    /**
     * Set spouseDateOfBirth
     *
     * @param string $spouseDateOfBirth
     *
     * @return ATBlueprintPersonalInformation
     */
    public function setSpouseDateOfBirth($spouseDateOfBirth)
    {
        $this->spouseDateOfBirth = $spouseDateOfBirth;

        return $this;
    }

    /**
     * Get spouseDateOfBirth
     *
     * @return string
     */
    public function getSpouseDateOfBirth()
    {
        return $this->spouseDateOfBirth;
    }

    /**
     * Set retirementAge
     *
     * @param string $retirementAge
     *
     * @return ATBlueprintPersonalInformation
     */
    public function setRetirementAge($retirementAge)
    {
        $this->retirementAge = $retirementAge;

        return $this;
    }

    /**
     * Get retirementAge
     *
     * @return string
     */
    public function getRetirementAge()
    {
        return $this->retirementAge;
    }

    /**
     * Set yearsInRetirement
     *
     * @param string $yearsInRetirement
     *
     * @return ATBlueprintPersonalInformation
     */
    public function setYearsInRetirement($yearsInRetirement)
    {
        $this->yearsInRetirement = $yearsInRetirement;

        return $this;
    }

    /**
     * Get yearsInRetirement
     *
     * @return string
     */
    public function getYearsInRetirement()
    {
        return $this->yearsInRetirement;
    }

    /**
     * Set socialSecurityBeginAge
     *
     * @param string $socialSecurityBeginAge
     *
     * @return ATBlueprintPersonalInformation
     */
    public function setSocialSecurityBeginAge($socialSecurityBeginAge)
    {
        $this->socialSecurityBeginAge = $socialSecurityBeginAge;

        return $this;
    }

    /**
     * Get socialSecurityBeginAge
     *
     * @return string
     */
    public function getSocialSecurityBeginAge()
    {
        return $this->socialSecurityBeginAge;
    }

    /**
     * Set monthlyRetirementGoal
     *
     * @param string $monthlyRetirementGoal
     *
     * @return ATBlueprintPersonalInformation
     */
    public function setMonthlyRetirementGoal($monthlyRetirementGoal)
    {
        $this->monthlyRetirementGoal = $monthlyRetirementGoal;

        return $this;
    }

    /**
     * Get monthlyRetirementGoal
     *
     * @return string
     */
    public function getMonthlyRetirementGoal()
    {
        return $this->monthlyRetirementGoal;
    }

    /**
     * Set socialSecurityMonthlyAmount
     *
     * @param string $socialSecurityMonthlyAmount
     *
     * @return ATBlueprintPersonalInformation
     */
    public function setSocialSecurityMonthlyAmount($socialSecurityMonthlyAmount)
    {
        $this->socialSecurityMonthlyAmount = $socialSecurityMonthlyAmount;

        return $this;
    }

    /**
     * Get socialSecurityMonthlyAmount
     *
     * @return string
     */
    public function getSocialSecurityMonthlyAmount()
    {
        return $this->socialSecurityMonthlyAmount;
    }

    /**
     * Set spouseSocialSecurityMonthlyAmount
     *
     * @param string $spouseSocialSecurityMonthlyAmount
     *
     * @return ATBlueprintPersonalInformation
     */
    public function setSpouseSocialSecurityMonthlyAmount($spouseSocialSecurityMonthlyAmount)
    {
        $this->spouseSocialSecurityMonthlyAmount = $spouseSocialSecurityMonthlyAmount;

        return $this;
    }

    /**
     * Get spouseSocialSecurityMonthlyAmount
     *
     * @return string
     */
    public function getSpouseSocialSecurityMonthlyAmount()
    {
        return $this->spouseSocialSecurityMonthlyAmount;
    }

    /**
     * Set includeSpousalInformation
     *
     * @param boolean $includeSpousalInformation
     *
     * @return ATBlueprintPersonalInformation
     */
    public function setIncludeSpousalInformation($includeSpousalInformation)
    {
        $this->includeSpousalInformation = $includeSpousalInformation;

        return $this;
    }

    /**
     * Get includeSpousalInformation
     *
     * @return boolean
     */
    public function getIncludeSpousalInformation()
    {
        return $this->includeSpousalInformation;
    }
}

