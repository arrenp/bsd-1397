<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class landingStats {
     /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="ipAddress", type="string", length=45)
     */
    public $ipAddress;
    /**
     * @var string
     *
     * @ORM\Column(name="videoLength", type="string", length=45)
     */
    public $videoLength;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="totalVideoLength", type="string", length=45)
	 */
    public $totalVideoLength;
    /**
     * @var string 
     *
     * @ORM\Column(name="weight", type="string", length=45)
     */
    public $weight;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="mousePosition", type="string", length=45)
     */
    public $mousePosition;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="dateSubmitted", type="string", length=45)
     */
    public $dateSubmitted;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="isSubmitted", type="string", length=45)
     */
    public $isSubmitted;
        /**
	 * @var string
	 *
	 * @ORM\Column(name="timeSpent", type="float", length=45)
     */
    public $timeSpent;
        /**
	 * @var string
	 *
	 * @ORM\Column(name="campaignID", type="string", length=45)
     */
    public $campaignID;
        /**
	 * @var string
	 *
	 * @ORM\Column(name="emailID", type="string", length=45)
     */
    public $emailID;

   public function __construct()//do not remove, will fail for doctrine add function
	 {

		$class_vars = get_class_vars(get_class($this));
			foreach ($class_vars as $key => $value)
			{
				if ($this->$key != "id")
				$this->$key = "";

			}
			$this->landingStats = new ArrayCollection();
	}

}
