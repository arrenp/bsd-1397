<?php
namespace classes\classBundle\Entity;
use classes\classBundle\Entity\abstractclasses\InvestmentsConfiguration;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class AddedPlansFromCommand
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
    /**
     * @var string
     *
     * @ORM\Column(name="command", type="string", length=50)
     */
    public $command;
}

