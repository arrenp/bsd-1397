<?php
namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use classes\classBundle\Entity\abstractclasses\rkpEmail;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class plansRkpEmail extends rkpEmail
{       
    /**
     * @var text
     *
     * @ORM\Column(name="partnerid", type="string", length = 255)
     */
    public $partnerid;   
    /**
     * @var text
     *
     * @ORM\Column(name="recordkeeperPlanid", type="string", length = 255)
     */
    public $recordkeeperPlanid; 
}
