<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class profilesInvestments
{
	/**
     * @ORM\ManyToOne(targetEntity="profiles", inversedBy="investments")
     * @ORM\JoinColumn(name="profileid", referencedColumnName="id")
     **/
	public $profile;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
 	/**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer",nullable=true)
     */
	public $userid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="planid", type="integer",nullable=true)
	 */
    public $planid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="profileid", type="integer",nullable=true)
	 */
    public $profileid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="type", type="string",length = 15,nullable=true)
	 */
    public $type;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="pname", type="string",length = 255,nullable=true)
	 */
    public $pname;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="fname", type="string",length = 255,nullable=true)
	 */
    public $fname;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="percent", type="decimal",  scale = 2, precision = 18,nullable=true)
	 */
    public $percent;
    /**
     * @var string
     *
     * @ORM\Column(name="fundFactLink", type="string",length = 255,nullable=true)
     */
    public $fundFactLink;
    /**
     * @var string
     *
     * @ORM\Column(name="prospectusLink", type="string",length = 255,nullable=true)
     */
    public $prospectusLink;
    /**
     * @var string
     *
     * @ORM\Column(name="participantsId", type="integer")
     */
    public $participantsId;
    /**
     * @var integer
     *
     * @ORM\Column(name="customid", type="string",length=100)
     */
    public $customid;     
    
    /**
    * @var integer
    *
    * @ORM\Column(name="fundGroupValueId", type="integer")
     */
    public $fundGroupValueId;    

    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($this->$key != "id")
     		$this->$key = "";
   		}
	}


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     *
     * @return profilesInvestments
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set planid
     *
     * @param integer $planid
     *
     * @return profilesInvestments
     */
    public function setPlanid($planid)
    {
        $this->planid = $planid;

        return $this;
    }

    /**
     * Get planid
     *
     * @return integer
     */
    public function getPlanid()
    {
        return $this->planid;
    }

    /**
     * Set profileid
     *
     * @param integer $profileid
     *
     * @return profilesInvestments
     */
    public function setProfileid($profileid)
    {
        $this->profileid = $profileid;

        return $this;
    }

    /**
     * Get profileid
     *
     * @return integer
     */
    public function getProfileid()
    {
        return $this->profileid;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return profilesInvestments
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set pname
     *
     * @param string $pname
     *
     * @return profilesInvestments
     */
    public function setPname($pname)
    {
        $this->pname = $pname;

        return $this;
    }

    /**
     * Get pname
     *
     * @return string
     */
    public function getPname()
    {
        return $this->pname;
    }

    /**
     * Set fname
     *
     * @param string $fname
     *
     * @return profilesInvestments
     */
    public function setFname($fname)
    {
        $this->fname = $fname;

        return $this;
    }

    /**
     * Get fname
     *
     * @return string
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set percent
     *
     * @param string $percent
     *
     * @return profilesInvestments
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * Get percent
     *
     * @return string
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * Set profile
     *
     * @param \classes\classBundle\Entity\profiles $profile
     *
     * @return profilesInvestments
     */
    public function setProfile(\classes\classBundle\Entity\profiles $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile
     *
     * @return \classes\classBundle\Entity\profiles
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @return string
     */
    public function getFundFactLink()
    {
        return $this->fundFactLink;
    }

    /**
     * @param string $fundFactLink
     */
    public function setFundFactLink($fundFactLink)
    {
        $this->fundFactLink = $fundFactLink;
    }

    /**
     * @return string
     */
    public function getProspectusLink()
    {
        return $this->prospectusLink;
    }

    /**
     * @param string $prospectusLink
     */
    public function setProspectusLink($prospectusLink)
    {
        $this->prospectusLink = $prospectusLink;
    }
    /**
     * @return string
     */
    public function getParticipantsId()
    {
        return $this->participantsId;
    }

    /**
     * @param string $spousalWaiverForm
     */
    public function setParticipantsId($participantsId)
    {
        $this->participantsId = $participantsId;
    }    
}
