<?php
namespace classes\classBundle\Entity;
use classes\classBundle\Entity\abstractclasses\InvestmentsLinkYearsToRetirementAndScore;
use Doctrine\ORM\Mapping as ORM;
/**
 * PlansInvestmentsLinkYearsToRetirementAndScore
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class PlansInvestmentsLinkYearsToRetirementAndScore extends InvestmentsLinkYearsToRetirementAndScore 
{
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
}
