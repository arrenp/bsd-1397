<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class launchUsers {
     /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="partnerid", type="string", length=100)
     */
    public $partnerid;
    /**
     * @var string
     *
     * @ORM\Column(name="smartplanid", type="string", length=50)
     */
    public $planid;
    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="encrypted", length=45)
     */
    public $firstName;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="lastName", type="encrypted", length=45)
	 */
    public $lastName;
    /**
     * @var string 
     *
     * @ORM\Column(name="email", type="encrypted", length=45)
     */
    public $email;
    /**
     * @var string 
     *
     * @ORM\Column(name="telephone", type="encrypted", length=45)
     */
    public $telephone;
    /**
     * @var string
     *
     * @ORM\Column(name="uploadListDate", type="datetime")
     */
    public $uploadDate; 
    /**
     * @var string 
     *
     * @ORM\Column(name="accountStatus", type="smallint")
     */
    public $accountStatus;
     /**
     * @var string 
     *
     * @ORM\Column(name="logoFilePath", type="string", length=255)
     */
    public $logoFilePath;   
     /**
     * @var string 
     *
     * @ORM\Column(name="tableName", type="string", length=45)
     */
    public $tableName; 
     /**
     * @var string 
     *
     * @ORM\Column(name="password", type="string", length=250)
     */
    public $password;    
     /**
     * @var string 
     *
     * @ORM\Column(name="lastModified", type="datetime", length=250)
     */
    public $lastModified;
    /**
     * @var string
     *
     * @ORM\Column(name="urlPass", type="string", length=45)
     */
    public $urlPass;


    public function __construct()//do not remove, will fail for doctrine add function
	 {

		$class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            $this->$key = "";
        }
            $this->lastModified = new \dateTime('now');
	}
    
    public static function getRandSalt() {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*?";
        $size = strlen($chars);
        $str = '';
        for ($i = 0; $i < 6; $i++) {
            $str .= $chars[rand(0, $size - 1)];
        }
        return $str;
    }
}
