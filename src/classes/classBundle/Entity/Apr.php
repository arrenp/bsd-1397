<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class Apr
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

     /**
     * @var text
     *
     * @ORM\Column(name="age", type="integer")
     */
    public $age;   
    /**
     * @var text
     *
     * @ORM\Column(name="mortality_rate", type="float")
     */
    public $mortality_rate;
   /**
     * @var text
     *
     * @ORM\Column(name="lx", type="float")
     */
    public $lx;
   /**
     * @var text
     *
     * @ORM\Column(name="dx", type="float")
     */
    public $dx;
   /**
     * @var text
     *
     * @ORM\Column(name="nx", type="float")
     */
    public $nx;
   /**
     * @var text
     *
     * @ORM\Column(name="apr", type="float")
     */
    public $apr;
   /**
     * @var text
     *
     * @ORM\Column(name="apr_join", type="float")
     */
    public $apr_join;
   /**
     * @var text
     *
     * @ORM\Column(name="year", type="integer")
     */
    public $year;
   






    public function __construct()//do not remove, will fail for doctrine add function
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            $this->$key = "";
        }
    }
}
