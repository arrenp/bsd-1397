<?php
namespace classes\classBundle\Entity;
class BaseEntity
{
    protected final function copyArray($array)//keep seperate just in case want to copy whole array then do additional logic
    {
        foreach ($array as $key => $value)
        {
           if (property_exists($this,$key))
            {
                $this->$key = $value;
            }
        }
    }
    public function copy($array)//override if necessary
    {
        $this->copyArray($array);
    }
}

