<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class printMaterials
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
  	/**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="adviserid", type="integer")
	 */
	public $adviserid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=100)
	 */
    public $name;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="link", type="string", length=200)
	 */
    public $link;
    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($this->$key != "id")
     		$this->$key = "";
   		}
	}

}
