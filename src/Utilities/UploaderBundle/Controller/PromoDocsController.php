<?php

namespace Utilities\UploaderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
require_once('cloudfiles.php');
use classes\classBundle\Entity\managePromoDocs;
class PromoDocsController extends Controller
{
    public function uploadAction()
	{

		$request = Request::createFromGlobals();
		$request->getPathInfo();

		$name = $request->request->get("name","");
		$parentid = $request->request->get("parentid","");
		$categoryid = $request->request->get("categoryid","");
		$additionalinfo = $request->request->get("additionalinfo","");
		$documentid =  $request->request->get("documentid","");

		if ($name != "")
		{
			$container2 = "promo docs";

			if ($documentid == "")
			$promoDoc = new managePromoDocs();
			else
			{
				$repository = $this->getDoctrine()->getRepository('classesclassBundle:managePromoDocs');
				$promoDoc = $repository->findOneBy(array('id' => $documentid));;
			}

			$promoDoc->name = $name;
			$promoDoc->parentid = $parentid;
			$promoDoc->additionalinfo = $additionalinfo;

  			if(count($request->files) > 0)
  			{

		    	$rackspace_user = "gnewman"; // this is your rackspace user name
		    	$rackspace_api_key = "105c0ad26c9c60b13bf21e458fc8fae8"; // get it from my account tab

		   	 	// Lets connect to Rackspace
		    	$authentication = new CF_Authentication($rackspace_user, $rackspace_api_key);
		    	$authentication->authenticate();
		    	$connection = null;
		    	try
		    	{
		       		$connection = new CF_Connection($authentication);
		    	}
		    	catch(AuthenticationException $e)
		    	{
		       		echo "Unable to authenticate ".$e->getMessage();
		        	die(0);
		    	}
		    	$container = null;
		    	// create a new container if you like uncomment the line below
		    	// $container = new CF_Container($authentication,$connection,"testcontainer");
		   	 	// Or use an already exsting container
		    	// $container = $connection->get_container('testcontainer');
		    	// or better way to handle this according to me is
		    	try
		    	{
					$container = $connection->get_container($container2);
					$container->make_public();
		    	}
		    	catch(NoSuchContainerException $e)
		    	{
		      	//$container=   $connection->create_container($container2);
				//$container->make_public();
		   		}
				catch(InvalidResponseException $res)
				{
					// let your users know or try again or just store the file locally and try again later to push it to the Cloud
				}

				$file_to_be_uploaded = 	$request->files->get("PromoDoc","");

				if ($file_to_be_uploaded != null &&  $file_to_be_uploaded != "")
				{
					$filename = $file_to_be_uploaded->getClientOriginalName();
					$filename = "0".$filename;
		    		// upload file to Rackspace
		    		$object = $container->create_object($filename);
		    		$object->load_from_filename($file_to_be_uploaded);
			 		$theurl =  str_replace("http","https",$object->public_uri());
					$theurlarray = explode(".",$theurl);
					$theurl2 = "";
					for ($i = 0; $i < count($theurlarray); $i++)
					if (($i+1) == count($theurlarray))
					$theurl2 = $theurl2.$theurlarray[$i];
					else if ($i != 1)
					$theurl2 = $theurl2.$theurlarray[$i].'.';
					else
					$theurl2 = $theurl2.'ssl.';
    				$theurl = $theurl2;
					$promoDoc->url = $theurl;
				}
			}
		if ($documentid == "")
		$this->getDoctrine()->getManager()->persist($promoDoc);
		$this->getDoctrine()->getManager()->flush();
		$page = $this->generateUrl('_manage_promo_docs_manage_manage_category');
		$page =  $page."?id=".$categoryid;
		echo '<script>parent.window.location  = "'.$page.'";</script>';
        return new Response("document added");


    }

	return new Response("failed to add document");
}


}
