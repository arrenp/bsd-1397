<?php
namespace Utilities\UploaderBundle\Controller;
require_once('cloudfiles.php');
class cdnUploader
{
    public function container($name)
    {
        $rackspace_user = "gnewman"; // this is your rackspace user name
        $rackspace_api_key = "105c0ad26c9c60b13bf21e458fc8fae8"; // get it from my account tab
        // Lets connect to Rackspace
        $authentication = new CF_Authentication($rackspace_user, $rackspace_api_key);
        $authentication->authenticate();
        $connection = null;
        try
        {
            $connection = new CF_Connection($authentication);
        }
        catch (AuthenticationException $e)
        {
            echo "Unable to authenticate " . $e->getMessage();
            die(0);
        }

        $container = null;
        // create a new container if you like uncomment the line below
        // $container = new CF_Container($authentication,$connection,"testcontainer");
        // Or use an already exsting container
        // $container = $connection->get_container('testcontainer');
        // or better way to handle this according to me is
        try
        {

            $container = $connection->get_container($name);
            $container->make_public();
        }
        catch (NoSuchContainerException $e)
        {
            //$container=   $connection->create_container($container2);
            //$container->make_public();
        }
        catch (InvalidResponseException $res)
        {
            // let your users know or try again or just store the file locally and try again later to push it to the Cloud
        }   
        return $container;
        
    }
    public function uploadFile($container,$tmpfilename,$filename)
    {
        $object = $container->create_object($filename);
        $object->load_from_filename($tmpfilename);
        $theurl = str_replace("http", "https", $object->public_uri());
        $theurlarray = explode(".", $theurl);
        $theurl2 = "";
        for ($j = 0; $j < count($theurlarray); $j++) if (($j + 1) == count($theurlarray)) $theurl2 = $theurl2 . $theurlarray[$j];

            else if ($j != 1) $theurl2 = $theurl2 . $theurlarray[$j] . '.';
            else $theurl2 = $theurl2 . 'ssl.';
        $theurl = $theurl2;
        return $theurl;
        
    }
    
    public function containerByHostName($httpHost) {
        if ($httpHost == "dev-admin.smartplanenterprise.com" || $httpHost == "symfony-admin.smartplanenterprise.com") {
            $container2 = "plans_images_dev";
        }
        if ($httpHost == "qa-admin.smartplanenterprise.com") {
            $container2 = "plans_images_qa";
        }
        if ($httpHost == "admin.smartplanenterprise.com") {
            $container2 = "plans_images";
        }
        if (!isset($container2)) {
            $container2 = "plans_images_dev";
        }
        return $this->container($container2);
    }
    
}

