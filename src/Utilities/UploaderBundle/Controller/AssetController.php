<?php

namespace Utilities\UploaderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use classes\classBundle\Entity\assets;
use Utilities\UploaderBundle\Controller\cdnUploader;
class AssetController extends Controller
{
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $cdnUploader = new cdnUploader();
        $container = $cdnUploader->container("admin_assets");
        $file_to_be_uploaded = $request->files->get("fileContent");
        $filename = "_".time()."_".$file_to_be_uploaded->getClientOriginalName();
        $theurl = $cdnUploader->uploadFile($container, $file_to_be_uploaded, $filename);
        $assets = new assets();
        $assets->url = $theurl;
        $assets->name = $request->request->get("name");
        $em->persist($assets);
        $em->flush();
        return new Response("");
    }
    public function editAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:assets');
        $assets = $repository->findOneBy(array('id' => $request->request->get("id")));
        if ($request->files->get("fileContent") != null)
        {    
            $cdnUploader = new cdnUploader();
            $container = $cdnUploader->container("admin_assets");
            $file_to_be_uploaded = $request->files->get("fileContent");
            $filename = "_".time()."_".$file_to_be_uploaded->getClientOriginalName();
            $theurl = $cdnUploader->uploadFile($container, $file_to_be_uploaded, $filename);
            $assets->url = $theurl;
        }
        $assets->name = $request->request->get("name");
        $em->flush();
        return new Response("");       
    }
}