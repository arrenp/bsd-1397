<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;
use classes\ClassBundle\Entity\ApiWhitelist;
use classes\ClassBundle\Entity\ApiUser;
use WidgetsBundle\Services\FormulaService;

class IrioController extends Controller
{
    public function widgetAction(Request $request)
    {
        $formulaService = $this->get('FormulaService');

        $data = $this->callFormulaService($request, $formulaService);
        $amount = $data['projectedMonthlyRetirement_0'];

        $html = $this->get('twig')->render('ApiBundle:Irio:widget.html.twig', $data);
        $this->get("session")->set("application","irio");
        return new Response(json_encode(['html' => $html]));
    }

    public function formulaAction(Request $request)
    {
        $formulaService = $this->get('FormulaService');

        $returnData = $this->callFormulaService($request, $formulaService);

        $amount = $returnData['projectedMonthlyRetirement_0'];

        return new Response(json_encode(['amount' => $amount]));
    }

    public function callFormulaService(Request $request, FormulaService $formulaService)
    {
        $age = $request->headers->get('age');
        $preCurrent = $request->headers->get('preCurrent');
        $rothCurrent = $request->headers->get('rothCurrent');
        $balance = $request->headers->get('balance');
        $maxMatchDollar = $request->headers->get('maxMatchDollar');
        $compensationTotals = $request->headers->get('compensationTotals');
        $matchTier1Percent = $request->headers->get('matchTier1Percent');
        $matchTier1Cap = $request->headers->get('matchTier1Cap');
        $matchTier2Percent = $request->headers->get('matchTier2Percent');
        $matchTier2Cap = $request->headers->get('matchTier2Cap');
        $salary = $request->headers->get('salary');

        $data = [];
        $data['age'] = $age;
        $data['preCurrent'] = $preCurrent;
        $data['rothCurrent'] = $rothCurrent;
        $data['balance'] = $balance;
        $data['maxmatchdollar'] = $maxMatchDollar;
        $data['retireAge'] = 65;
        $data['salary'] = $salary;
        $data['balance'] = $balance;
        $data['matchTier1Percent'] = $matchTier1Percent;
        $data['matchTier1Cap'] = $matchTier1Cap;
        $data['matchTier2Percent'] = $matchTier2Percent;
        $data['matchTier2Cap'] = $matchTier2Cap;

        $em = $this->container->get('doctrine')->getManager();
        $repository = $em->getRepository('classesclassBundle:Apr');
        $apr = $repository->findOneBy(array('age' => $data['retireAge'], 'year' => date('Y')));

        $data['apr'] = isset($apr->apr) ? $apr->apr : 0;
        $data['apr_join'] = isset($apr->apr_join) ? $apr->apr_join : 0;

        $matchInformation = new \stdClass();
        $matchInformation->maxmatchdollar = $maxMatchDollar;
        $matchInformation->compensationtotals = $compensationTotals;
        $matchInformation->matchdetails = new \stdClass();
        $matchInformation->matchdetails->matchstep1 = $matchTier1Percent;
        $matchInformation->matchdetails->matchrate1 = $matchTier1Cap;
        $matchInformation->matchdetails->matchstep2 = $matchTier2Percent;
        $matchInformation->matchdetails->matchrate2 = $matchTier2Cap;
        $data['matchInformation'] = $matchInformation;

        $formulaService->_calcMatch($data);
        $returnData = $formulaService->_calculateNew($data);

        return $returnData;
    }

}