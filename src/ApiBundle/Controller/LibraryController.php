<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;
use classes\ClassBundle\Entity\ApiWhitelist;
use classes\ClassBundle\Entity\ApiUser;

class LibraryController extends Controller
{
    public function videosAction(Request $request)
    {
        $this->get("session")->set("application","video library");
        $library = $this->get('spe.app.plan')->getLibraryData();

        $mainDirectory = "https://c06cc6997b3d297a6674-653de9dab23a7201285f2586065c1865.ssl.cf1.rackcdn.com/video/library/";
        $m4v = "m4v/";
        $webmv = "webmv/";
        $mp4 = ".mp4";
        $webm = ".webm";
        $thumbs = "thumbnails/";

        $libraryFiles = [];
        foreach ($library as $lib) {
            $file = array(
                'title' => $lib['title'],
                'value' => $lib['value'],
                'mp4' => $mainDirectory . $lib['directory'] . "/" . $m4v . $lib['video'] . $mp4,
                'webm' => $mainDirectory . $lib['directory'] . "/" . $webmv . $lib['video'] . $webm,
                'thumbnail' => $mainDirectory . $thumbs . $lib['thumbnail'],
                'id' => $lib['id'],
                'thumbnail' => $mainDirectory . $thumbs . $lib['thumbnail'],
                'runtime' => $lib['runtime']
            );
            $libraryFiles[] = $file;
        }


        return new Response(json_encode($libraryFiles));
    }
}