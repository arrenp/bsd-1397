<?php

namespace Settings\SettingsBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Shared\ContactBundle\Classes\contact;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
class ContactController extends Controller
{
	public $contact;
	public $userid;
    public function indexAction()
    {
    	$this->init();
		$contact = $this->contact->indexAction();
		$userid = $this->userid;
		$permissions = new rolesPermissions($this);
		$writeable = $permissions->writeable("SettingsContact");
		//return new Response("test");
		return $this->render('SettingsSettingsBundle:Contact:index.html.twig', array('userid' => $userid, 'contact' => $contact,'writeable' => $writeable));
    }

    public function init()
    {
		$session = new adminsession($this);
		$session->set("section","Settings");
		$session->set("currentpage","SettingsContact");
		$userid = $session->userid;
		$em = $this->getDoctrine();
		$this->userid = $userid;
		$this->contact = new contact($em,"settings",$userid);
    }
}
