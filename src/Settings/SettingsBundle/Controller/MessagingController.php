<?php

namespace Settings\SettingsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Shared\MessagingBundle\Classes\Messaging;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
class MessagingController extends Controller
{
    public function indexAction()
    {
	$messaging = new Messaging($this,"settings");
	$messages = $messaging->messages();
  	$session = new adminsession($this);
	$session->set("section","Settings");
	$session->set("currentpage","SettingsMessaging");
	$userid = $session->userid;
	$adviser = new rolesPermissions($this);
	$writeable = $adviser->writeable("SettingsMessaging");

	
    return $this->render('SettingsSettingsBundle:Messaging:index.html.twig', array( "userid" => $userid,"messages" => $messages, "writeable" => $writeable));
  

    }
}
