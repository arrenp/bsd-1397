<?php

namespace Spe\AppBundle\EventListener;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Spe\AppBundle\Services\SmartService;


class SmartRequestListener
{

    public function __construct(Session $session, SmartService $smartService)
    {
        $this->session = $session;
        $this->smartService = $smartService;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        //Apply Locale setting on Ajax request
        if (strpos($request->get('_controller'), 'AppBundle') !== false) {
            if ($request->isXmlHttpRequest()) {

                if($request->get('switch')){
                    $session = $this->session->get('REQUEST');
                    $session['lang'] = $request->get('switch');
                    $this->session->set('REQUEST', $session);
                }

                if (isset($this->session->get('REQUEST')['lang']) && $this->session->get('language_files')) {
                    $this->smartService->setLocale();
                }
            }
        }
    }

}
