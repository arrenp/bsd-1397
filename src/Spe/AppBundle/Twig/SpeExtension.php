<?php
/**
 * Created by PhpStorm.
 * User: tanuj
 * Date: 7/14/2015
 * Time: 2:03 PM
 */

namespace Spe\AppBundle\Twig;

class SpeExtension extends \Twig_Extension
{
    public function getFilters(){
        return array(
            new \Twig_SimpleFilter('price', array($this, 'priceFilter')),
        );
    }

    public function priceFilter($number, $decimals = 0, $decPoint = '.', $thousandsSep = ','){
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        return $price;
    }

    public function getName(){
        return 'spe_extension';
    }
}