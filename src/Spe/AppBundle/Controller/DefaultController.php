<?php

namespace Spe\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use \classes\classBundle\MappingTypes\EncryptedType;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="loading")
     * @Template("SpeAppBundle:Layout:loading.html.twig")
     */
    public function loadingAction(Request $request)
    {

        $counter = 0;
        $countRequest = count($_REQUEST);
        $newArray = array();
        if ($countRequest > 0)
        {
            foreach ($_REQUEST as $key => $value)
            {
                
                $newArray[$counter] = $key;
                $counter++;
            }

            $newArrayCount = count($newArray);
            $requestLastKey = $newArray[$newArrayCount-1];
            $_REQUEST[$requestLastKey] = rtrim($_REQUEST[$requestLastKey],'/');
        }
        
        $session = $this->get("session");
        $session->clear();
        // Set Environment
        $session->set('environment', $this->container->getParameter('kernel.environment'));

        if(isset($_SERVER['HTTP_REFERER'])){
            extract(parse_url($_SERVER['HTTP_REFERER']));
            $session->set('referer', $scheme .'://'. $host);
        }
        if (!isset($_REQUEST['id']) && isset($_REQUEST['planid']))
        $_REQUEST['id'] = $_REQUEST['planid'];
        if (!isset($_REQUEST['id']) && isset($_REQUEST['plan_id']))
        $_REQUEST['id'] = $_REQUEST['plan_id'];        
        
        $session->set('REQUEST', $_REQUEST);
        $host = explode("?",$request->getUri())[0]."?";
        $hostcounter = 0;
        foreach ($_REQUEST as $key => $value)
        {
            if ($hostcounter > 0)
            $host = $host."&";
            $host = $host.$key."=".$value;
            $hostcounter++;
        }
        $session->set('originalUrl',$host); 
        return array("request" => $_REQUEST);
    }

    /**
     * @Route("/style", name="style")
     */
    public function styleAction()
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'text/css');
        return $this->render('SpeAppBundle:Layout:style.css.php', array(), $response);
    }

    /**
     * @Route("/pdf/{filename}", name="pdf")
     */
    public function pdfAction($filename = null){
        if($filename){
            $path = $this->get("session")->get('translation_path');
            return new BinaryFileResponse($path.'pdf/'.$filename.'.pdf');
        }
    }

    /**
     * @Route("/profile/report/{profileId}", name="profileReport")
     */
    public function profileReportAction($profileId = null,Request $request){
        $profile = array();
        if ($profileId) {
            /* @var $em \Doctrine\ORM\EntityManager */
            $em = $this->get('doctrine')->getManager();

            $sql = "SELECT
                        p.id as pid, p.profileId, p.reportDate, p.planName,p.currentBalance,p.investmentsStatus,p.realignmentStatus,p.beneficiaryStatus,p.contributionsStatus,p.catchupContributionStatus,p.enrollmentStatus,p.mstarQuit,p.annualSalary,p.retireAge,p.preTaxSavingRate,p.postTaxSavingRate,p.rothTaxSavingRate,p.mStarStatus,p.flexPlan,p.clientCalling, AES_DECRYPT(p.sessionData, UNHEX('" . EncryptedType::KEY . "')) as sessionData,p.language,
                        p.callMe,p.emailMe,p.callUs,p.trustedContactStatus,
                        pt.*,
                        pl.IRSCode, pl.riskType, pl.providerLogoImage, pl.morningstar, pl.id as planId, pl.userid as userId,
                        LCASE(a.connectionType) as connectionType,
                        LCASE(a.languagePrefix) as languagePrefix,
                        pm.spousalWaiverUrl,pm.matchingContributions,
                        COALESCE( NULLIF(pm.retirementNeedsVersion, ''), NULLIF(dm.retirementNeedsVersion, ''), 'standard') AS retirementNeedsVersion, pm.riskBasedQuestionnaire, pm.investmentsGraph
                    FROM profiles p
                        JOIN participants pt ON pt.id = p.participantid
                        JOIN plans pl ON pl.id = p.planid
                        JOIN accounts a ON a.id = pl.userid
                        LEFT JOIN defaultModules dm ON dm.userid = pl.userid
                        LEFT JOIN plansModules pm ON pm.planid = pl.id && pm.userid = pl.userid
                    WHERE p.profileId = :profileid";

            $stmt = $em->getConnection()->prepare($sql);
            $stmt->bindValue("profileid", $profileId);
            $stmt->execute();
            $profile = $stmt->fetch(\PDO::FETCH_ASSOC);


            if (!$profile) {
                return new Response("");
            }
            
            // Load Plan Messages
            $planMessages = $this->get('spe.app.plan')->getPlanMessages($profile['planId'], $profile['userId'],$profile['language']);
            $profile['investorProfileDisclosure'] = $planMessages['investorProfileDisclosure'];
            
            //Load Plan Documents
            $profile['planDocuments'] = $this->get('spe.app.plan')->getPlanDocuments($profile['planId'], $profile['userId']);

            /* Set Locale and get RiskProfile Path */
            $locales = $this->get('spe.app.plan')->getLanguageFiles();
            $language = isset($profile['language']) && $profile['language'] ? $profile['language'] : $this->get('request')->getLocale();
            
            $prefix = '';
            if ($profile['languagePrefix']) {
                // Strip language prefix from profile's locale if exists
                $prefix = $profile['languagePrefix'] . '_';
                if (substr($language, 0, strlen($prefix)) === $prefix) {
                    $language = substr($language, strlen($prefix));
                }
            }

            if (isset($locales[strtolower($language)])) {
                $this->get('request')->setLocale($locales[strtolower($language)]);
                $this->get('translator')->setLocale($locales[strtolower($language)]);
            }
            $riskProfilePath = $this->get('kernel')->locateResource('@SpeAppBundle/Resources/translations/' . strtolower($prefix).$this->get('request')->getLocale() . '/riskProfile/');

            $sql = "SELECT r.* FROM profilesRiskProfile r WHERE r.profileid = '" . $profile['pid'] . "'";
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $profile['risk_profile'] = $stmt->fetch(\PDO::FETCH_ASSOC);

            $sql = "SELECT i.*,fgv.value as fundsGroupsValue FROM profilesInvestments i LEFT JOIN fundsGroupsValues fgv ON i.fundGroupValueId = fgv.id  WHERE i.profileid = '" . $profile['pid'] . "'";
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $profile['investments'] = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            $profile['sessionData'] = json_decode($profile['sessionData']);
            $investmentsGraph = isset($profile['sessionData']->investmentsGraphComplete) && $profile['sessionData']->investmentsGraphComplete;
            if (isset($profile['sessionData']->investmentsGraphData) && !$investmentsGraph)
            {
                unset($profile['sessionData']->investmentsGraphData);
            }
            
            $profile['auto_increase'] = $em->getConnection()->fetchAll("SELECT * FROM profilesAutoIncrease WHERE profileid = :profileid", [":profileid" => $profile['pid']]);
            
            //fundGroupValueId
            if ($profile['morningstar'] && !$profile['mstarQuit']) {
                return $this->render('SpeAppBundle:Template:report_mstar.html.php', array('profile' => $profile));
            } else {
                $sql = "SELECT r.* FROM profilesRetirementNeeds r WHERE r.profileid = '" . $profile['pid'] . "'";
                $stmt = $em->getConnection()->prepare($sql);
                $stmt->execute();
                $profile['retirement_needs'] = $stmt->fetch(\PDO::FETCH_ASSOC);

                if ($profile['retirement_needs']) {
                    $profile['retirement_needs']['qa'] = $this->get('spe.app.functions')->getRNQuestionAnswer($profile['retirement_needs'], $profile['retirementNeedsVersion']);
                }

                if ($profile['risk_profile'] && $profile['risk_profile']['xml'] && $profile['risk_profile']['ans']) {
                    $profile['risk_profile']['qa'] = $this->get('spe.app.functions')->getRPQuestionAnswer($profile['risk_profile']['xml'], $profile['risk_profile']['ans'], $riskProfilePath);
                }

                $sql = "SELECT c.* FROM profilesContributions c WHERE c.profileid = '" . $profile['pid'] . "'";
                $stmt = $em->getConnection()->prepare($sql);
                $stmt->execute();
                $profile['contributions'] = $stmt->fetch(\PDO::FETCH_ASSOC);

                $sql = "SELECT c.* FROM profilesBeneficiaries c WHERE c.profileid = '" . $profile['pid'] . "'";
                $stmt = $em->getConnection()->prepare($sql);
                $stmt->execute();
                $profile['beneficiaries'] = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                $documents = array();
                foreach ($profile['planDocuments'] as $key => $document)
                    $documents[$this->autoEscapeField($key)] = $document;
                $profile['planDocuments'] = $documents;

                $profile['fromAdmin'] = $request->query->get("fromAdmin",0);
                $profile['ATArchitect'] = $this->ATArchitect($profile)->getContent();
                $profile['ATBlueprint'] = $this->ATBlueprint($profile)->getContent();
                $planid = $profile['planId'];
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
                $plan = $repository->findOneBy(array('id' => $planid));
                if (!$request->query->get("ajax",0)) 
                {
                    $template = "report.html.php";
                }
                else
                {
                    $template = "report-inner.html.php";
                }   
                $profile['_extras']['showConfirmationCodes'] = true;
                if ($profile['reportDate'] == null)
                {
                    $date = new \DateTime('now');                
                    $profile['reportDate'] = $date->format('Y-m-d H:i:s');
                    $profile['_extras']['showConfirmationCodes'] = false;
                }
                $profile['currentYearlyIncome'] = $profile['contributions']['currentYearlyIncome'];
                if ($profile['currentYearlyIncome'] == null)
                {
                    $profile['currentYearlyIncome'] = $profile['retirement_needs']['currentYearlyIncome'];
                }
                $profile['_extras']['ajax'] = $request->query->get("ajax",0);
                $profile['dayOfWeek'] = $profile['reportDate'] ? date("l", strtotime($profile['reportDate'])) : '';
                $profile['month'] = $profile['reportDate'] ? date("F", strtotime($profile['reportDate'])) : '';       
                $profile['month'] = strtolower($profile['month']);

                // AT
                if ($profile['sessionData']->module->ATBlueprint) {
                    if ($profile['sessionData']->ACBluePrint != null) {
                        $type = 'ACBluePrint';
                    } else {
                        $type = 'ACInitialize';
                    }
                    $profile['contributions']['mode'] = 'PCT';
                    $profile['currentYearlyIncome'] = $profile['sessionData']->$type->WsIndividual->AnnualCompensation;

                    $profile['contributions']['projected'] = $profile['sessionData']->$type->WsBlueprint->DefRateDol;

                    $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATWsAccounts');
                    $plan = $repository->findOneBy(array('profileid' => $profile['pid'], 'accountType' => 'Plan'));
                    if ($profile['sessionData']->common->roth) {
                        $profile['contributions']['roth'] = $plan->rothPct * 100.0;
                    }
                    $profile['contributions']['pre'] = $plan->preTaxPct * 100.0;
                }
                return $this->render('SpeAppBundle:Template:'.$template, array('profile' => $profile, 'plan' => $plan));

            }
        }
    }
    public function ATArchitect($profile)
    {
        if ((array)$profile['sessionData']->ACBluePrint != null) {
            $ATArchitect = (array)$profile['sessionData']->ACBluePrint->WsArchitectTask;
        } else {
            $ATArchitect = (array)$profile['sessionData']->ACInitialize->WsArchitectTask;
        }

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATArchitect');
        $architect = $repository->findOneBy(array('profileid' => $profile['pid']));

        if ($ATArchitect == null || ($architect != null && !$architect->acceptedInvestment) || $profile['sessionData']->ACInitialize->WsPlan->AllowArchitect === 'false' || $profile['sessionData']->module->ArchitectIsEnrolled === 'false') {
            return new Response("");
        } else {
            $session = $this->get('session');
            $rkp = $session->get('rkp');
            $reliusFund = null;
            foreach ($rkp['plan_data']->funds as $fund) {
                if ($fund[3] > 0) {
                    $reliusFund = $fund[1];
                    break;
                }
            }
            if (explode('^', $session->get('investments')['I_SelectedInvestmentDetails'])[0] !== $reliusFund) {
                $common = $session->get('common');
                $common['updateElections'] = true;
                $common['updateRealignment'] = true;
                $session->set('common', $common);
            }
            return $this->render("SpeAppBundle:Template:atarchitect.html.twig",array("ATArchitect" => $ATArchitect));
        }
    }
    public function ATBlueprint($profile)
    {
        if ((array)$profile['sessionData']->ACBluePrint != null) {
            $individual = (array)$profile['sessionData']->ACBluePrint->WsIndividual;
            $blueprint = (array)$profile['sessionData']->ACBluePrint->WsBlueprint;
        } else {
            $individual = (array)$profile['sessionData']->ACInitialze->WsIndividual;
            $blueprint = (array)$profile['sessionData']->ACInitialize->WsBlueprint;
        }
        
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintPersonalInformation');
        $personalInfo = $repository->findOneBy(array('profileid' => $profile['pid']));

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintOutlook');
        $outlook = $repository->findOneBy(array('profileid' => $profile['pid']));
        
        if ($personalInfo == null || $outlook == null)
        {
            return new Response("");
        }
        return $this->render('SpeAppBundle:Template:ATBlueprint/atblueprint.html.twig', array('personalInfo' => $personalInfo, 'outlook' => $outlook,'individual' => $individual, 'blueprint' => $blueprint,"profile" => $profile));        
    }
    /**
     * @param $profileId
     * @return Response
     * @Route("/profile/graphReport", name="graphReport")
     */    
    public function graphReportAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:profiles');
        $profile = $repository->findOneBy(array('id' => $request->request->get("id")));   
        return $this->render('SpeAppBundle:Template:investments/graph/profilegraph.html.twig',(array)json_decode($profile->sessionData)->investmentsGraphData);
    }
    
    function autoEscape(&$data)
    {
        foreach ($data as $key=> &$row)
        {
            if (is_array($row))
            $this->autoEscape($data[$key]);
            else
            $data[$key] = $this->autoEscapeField ($row);
        }
    }
    function autoEscapeField($row)
    {
        return htmlspecialchars($row, ENT_QUOTES, 'ini_get("default_charset")', false);
    }


    protected  function toJSON($data){
        return new Response(json_encode($data));
    }
}
