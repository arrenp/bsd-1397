<?php

namespace Spe\AppBundle\Controller;

use classes\classBundle\Entity\ATBlueprintOutlook;
use classes\classBundle\Entity\ATBlueprintPersonalInformation;
use classes\classBundle\Entity\ATBlueprintPersonalInformationOtherAccounts;
use classes\classBundle\Entity\ATBlueprintPersonalInformationOtherIncome;
use classes\classBundle\Entity\ATWsAccounts;
use Doctrine\ORM\EntityManager;
use classes\classBundle\Entity\participants;
use classes\classBundle\Entity\profiles;
use classes\classBundle\Entity\videoStatsSpe;
use classes\classBundle\Entity\ATArchitect;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Shared\General\GeneralMethods;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * Ajax controller.
 * @Route("/", name="ajax", condition="request.headers.get('X-Requested-With') == 'XMLHttpRequest'")
 */

class AjaxController extends Controller
{
    /**
     * @Route("/initialize", name="initialize")
     * @Template("SpeAppBundle:Layout:layout.html.twig")
     */
    public function initializeAction()
    {
        //try{
            if($this->get('request')->get('switch')){
                $this->get('spe.app.smart')->setDefaultData();
                $this->get('spe.app.smart')->setPlaylist();
                $this->get('spe.app.smart')->setCommonVariables();
                return array();
            }

            $session = $this->get("session");
            $session->set('api_url',$this->container->getParameter( 'api_url' ));
			$session->set('profileId', uniqid('SPE_'));
            $this->get('spe.app.rkp')->getRKPData();
            $rkp = $session->get('rkp');
            if($rkp && isset($rkp['plan_data']) && count($rkp['plan_data'])) {
                $session->set('spe_plan_id',$rkp['plan_id']);
                $this->get('spe.app.plan')->setPlanData();
                $this->get('spe.app.smart')->setLocale();
                $this->get('spe.app.plan')->setPlanMessages();
                $this->get('spe.app.plan')->setLibraryData();
                $this->get('spe.app.smart')->setDefaultData();
                $this->get('spe.app.smart')->setSmart401kData();
                $this->get('spe.app.smart')->setClientCalling();
                $this->get('spe.app.smart')->setPlaylist();
                $this->get('spe.app.smart')->setRetirementNeeds();
                $this->get('spe.app.smart')->setRiskProfile();
                $this->get('spe.app.smart')->setInvestments();
                $this->get('spe.app.smart')->setContributions();
                $this->get('spe.app.smart')->setMyProfiles();
                $this->get('spe.app.smart')->setBeneficiaries();
                $this->get('spe.app.profile')->createProfile();
                $this->get('spe.app.smart')->setCommonVariables();
                $module = $session->get("module");
                if ($module['ATBlueprint'])
                {
                    $ACInitialize = $this->get("spe.app.rkp")->ACInitialize();
                    if ($ACInitialize['WsPlan']['AllowBlueprint'] == 'true') {

                        $session->set("ACInitialize", $ACInitialize);
                        $this->setATBlueprintPersonalInformation();
                        $module['ATArchitect'] = false;
                        if ($ACInitialize['WsPlan']['AllowArchitect'] == "true") {
                            $module['ATArchitect'] = true;
                        }
                        $module['BlueprintRecordExist'] = $ACInitialize['WsIndividual']['BlueprintRecordExist'];
                        $module['ArchitectIsEnrolled'] = $ACInitialize['WsIndividual']['ArchitectIsEnrolled'];

                        if ($module['BlueprintRecordExist'] == 'true') {
                            $this->get("session")->set("ATLastPath", "/atBlueprint/outlook/main");
                        }

                        if ($module['ArchitectIsEnrolled'] == 'true') {
                            $this->getArchitect();
                            $this->get("session")->set("ArchitectLastPath", "/investments/atarchitect/4");
                        }
                        $this->get("session")->set("VisitedOutlook", 0);
                        $session->set("module", $module);
                        $this->getOrCreateOutlook();
                        $common = $session->get('common');
                        $common['ATemail'] = $ACInitialize['WsIndividual']['Email'];
                        $session->set('common', $common);
                    } else {
                        $module['ATBlueprint'] = false;
                    }

                }
                $session->set("module",$module);
                $this->get('spe.app.smart')->setPlaylist();

            }else{
                $this->get('spe.app.plan')->setErrorEmail();
                
                if (isset($rkp['app_error']) || isset($rkp['error']))
                    $status = array('error' => 1, 'data' => $this->get('spe.app.plan')->getErrorMessage($rkp['app_error']));
                else
                    $status = array('error' => 1, 'data' => $this->get('spe.app.plan')->getErrorMessage('connection'));   
                
                $emailStatus = array('success' => false, 'message' => $this->get('spe.app.plan')->getErrorMessage('connection'));
               
                $data = (isset($session->get('REQUEST')['plan'])) ? $session->get('REQUEST')['plan'] : array();
                
                $this->get('spe.app.functions')->sendProfileErrorEmail($emailStatus, $data, FALSE);
                
                return $status;
            }
            /*
        }catch (\Exception $e){
            $error = ($this->container->get('kernel')->getEnvironment() == 'dev' || isset($_GET['debug'])) ? '<br><p class="error-detail">'. $e->getMessage() .'</p>' : null;
            return array('error' => 1, 'data' => $this->get('spe.app.plan')->getErrorMessage('other', $error));
        }
        */

        //var_dump($session->all()); exit;
        return array();
    }




    /**
     * @Route("/welcome", name="welcome")
     */
    public function WelcomeAction(){
        $session = $this->get("session");
        $translator = $this->get('translator')->getMessages();
        if ($session->get('partBeneficiaries') != null)
        $benevalue = $session->get('partBeneficiaries');
        else
        $benevalue = $session->get('beneficiary'); 
        $additionalFundHeader = "";
        $session->get("rkp")['plan_data']->rkpExtras->additionalFundHeader = $session->get("rkp")['plan_data']->rkpExtras->additionalFundHeader;
        if (isset($translator['investments'][$session->get("rkp")['plan_data']->rkpExtras->additionalFundHeader]))
        {
            $additionalFundHeader  = $translator['investments'][$session->get("rkp")['plan_data']->rkpExtras->additionalFundHeader];
        }
        else if (isset($session->get("rkp")['plan_data']->rkpExtras->additionalFundHeader))
        {
            $additionalFundHeader = $session->get("rkp")['plan_data']->rkpExtras->additionalFundHeader;
        }
        return $this->toJSON(array(
            'library' =>  $session->get('library'),
            'playlist' => $session->get('playlist'),
            'mediaPath'  => array(
                'video_path' => $session->get('video_path'),
                'audio_path' => $session->get('audio_path'),
                'media_base' => $session->get('media_base'),
                'library_path' => $session->get('library_video_path'),
                'popup_path' => $session->get('popup_video_path'),
            ),
            'common' => $session->get('common'),
            'messaging' => $session->get('messaging'),
            'retirement_needs' => $session->get('retirement_needs'),
            'risk_profile' => $session->get('risk_profile'),
            'investments' => $session->get('investments'),
            'contributions' => $session->get('contributions'),
            'my_profiles' => $session->get('my_profiles'),
            'partBeneficiaries' => $session->get('partBeneficiaries'),
            'partBeneficiariesCopy' => $session->get('partBeneficiariesCopy'),
            'partBeneficiariesDeleted' => $session->get('partBeneficiariesDeleted'),
            'beneficiary' => $benevalue,
            'profile_transaction' => null,
            'translation' => $this->get('translator')->getMessages()['alert'],
            'morningstar' => null,
            'serverConfig' => $session->get('serverConfig'),
            'participantid' => $session->get('participantid'),
            'originalUrl' => $session->get('originalUrl'),
            "module" => $session->get('module'),
            "translations" => $translator,
            "additionalFundHeader" => $additionalFundHeader,
            "pretaxslidercheckbox" => true,
            "rothslidercheckbox" => true
        ));
        
    }

    /**
     * @Route("/investments", name="investments")
     */
    public function investmentsAction(Request $request)
    {
        $module = $this->get("session")->get("module");
        if ($module['ATArchitect'])
        {
            return $this->atBlueprintMainAction($request, "ArchitectLastPath");
        }
        $em = $this->get('doctrine')->getManager();
        $session = $this->get("session");
        $rkpfunds = $session->get('rkp')['plan_data']->funds;
        $rkpfundsHash = array();
        foreach ($rkpfunds as $fund)
        {
            $rkpfundsHash[$fund[1]] = $fund;
        }
        $plan = $session->get('plan');
        $source = $session->get('rkp')['rkp'];
        $investments = $session->get('investments');

        $data['clientCalling'] = $session->get('client_calling');
        $data['IRScode'] = $plan['IRSCode'];
        $data['isACA'] = $session->get('isACA');
        $data['risk_type'] = $plan['riskType'];

        $smart401k = $session->get('smart401k_completed');

        $data['i_audios'] = $i_audios = $session->get('common')['I_Audio'];
        $data['volume'] = $session->get('volume');
        $messaging = $session->get('messaging');
        $page = (isset($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
        $a = (isset($_REQUEST['a'])) ? $_REQUEST['a'] : 0;
        $type = (isset($_REQUEST['type'])) ? $_REQUEST['type'] : 'RISK';

        if ($smart401k) {
            $data['smart401k'] = 1;
            $type = 'ADVICE';
            if ($page == 1) {
                $page = 2;
            }
        }

        //Check For Default Investments
        if ($plan['defaultInvestment']) {
            $this->get('spe.app.plan')->setDefaultInvestmentsFunds();
        }

        $data['DIFund'] = $session->get('defaultInvestmentFund');
        $data['isDIF'] = is_array($data['DIFund']) ? 1 : 0;

        //$data['DIFund'] = array(001,001,'Test1','Test2',null,null,'0100','http://google.com');
        //$data['isDIF'] = 1;

        switch ($page) {
            default:
            case 1:
                $data['disclosure_text'] = trim($messaging['investmentSelector']);
                break;
            case 3:
                if($type == 'DEFAULT') {
                    if($data['isACA']){
                        $ACAFund = $session->get('rkp')['plan_data']->rkpExtras->ACAFund;
                        $a = $ACAFund[1].'^100^'.$ACAFund[0];
                    }elseif($data['isDIF']){
                        $DIFund = $session->get('defaultInvestmentFund');
                        $a = $DIFund[1].'^100^'.$DIFund[0];
                    }
                }
                $investments['investmentsStatus'] = 2;
            case 2:
                $investments['I_SelectedInvestmentOption'] = $type;
                $investments['I_SelectedInvestmentDetails'] = $a;
                if($type == 'RISK' && $page == 2) {
                    $data['disclosure_text'] = trim($messaging['portfolioSelector']);
                }
                if($type == 'CUSTOM' || $type == 'ADVICE') {
                    if($page == 2) {
                        $data['disclosure_text'] = trim($messaging['customChoice']);
                        $session->set('editRecommended', 0);
                    }
                }

                $session->set('investmentsType', $type);

                if($type == 'ADVICE') {
                    if($page == 3) {
                        if($session->get('editRecommended')) {
                            $investments['I_SelectedInvestmentOption'] = 'CUSTOM';
                            $session->set('investmentsType', 'CUSTOM');
                        }
                    }
                }

                break;
        }
        $data['current_section'] = $page;
        if (($type == "TARGET" || $type == "CUSTOM")  && $source == "educate")
        {
            if (isset($investments['investmentDetails']))
            unset($investments['investmentDetails']);
            $fundCounter = 0;
            $fundExplode = explode("|",$investments['I_SelectedInvestmentDetails']);
            foreach ($fundExplode as $fund)
            {
                if (substr_count($fund,"^") > 0)
                {
                    $fundData = explode("^",$fund);
                    $plansFunds = $em->getRepository('classesclassBundle:plansFunds')->findOneBy(array('id' => $fundData[0]));
                    if ($plansFunds != null)
                    {
                        $investments['investmentDetails'][$fundCounter]['fname'] = $plansFunds->name;
                        $investments['investmentDetails'][$fundCounter]['percent'] =$fundData[1];
                        $investments['investmentDetails'][$fundCounter]['rkp'] = $rkpfundsHash[$plansFunds->id];
                        $fundCounter++;
                    }
                }
            }
        }
        
        if ($type == "RISK" && $source == "educate")
        {
            if (isset($investments['investmentDetails']))
            unset($investments['investmentDetails']);
            $fundCounter = 0;
            $portfolioFunds = $em->getRepository('classesclassBundle:plansPortfolioFunds')->findBy(array('portfolioid' => $investments['I_SelectedInvestmentDetails']));
            foreach ($portfolioFunds as $portfolioFund)
            {
                //var_dump($portfolioFund);
                $plansFunds = $em->getRepository('classesclassBundle:plansFunds')->findOneBy(array('id' => $portfolioFund->fundid));
                $plansPortfolios = $em->getRepository('classesclassBundle:plansPortfolios')->findOneBy(array('id' => $portfolioFund->portfolioid));
                if ($plansFunds != null && $plansPortfolios != null)
                {
                    $investments['investmentDetails'][$fundCounter]['pname'] = $plansPortfolios->name;
                    $investments['investmentDetails'][$fundCounter]['fname'] = $plansFunds->name;
                    $investments['investmentDetails'][$fundCounter]['percent'] =$portfolioFund->percent;
                    $fundCounter++;
                }
            }
        }
        $session->set('investments', $investments);
        
        if (!in_array($investments['I_SelectedInvestmentOption'],array("","RISK")) &&  $session->get('module')['investmentsGraph'])
        {
            $session->remove('graphScores');
            $session->remove('PlansInvestmentsGraphSelectedMinScore');            
            $session->remove("investmentsGraphComplete");
        }
               
        if ($investments['I_SelectedInvestmentOption']== "RISK" && $session->get('module')['investmentsGraph'])
        {
            $session->remove('investments');
            return $this->investmentsTableAction($request);
        }
        else
        {
            return $this->render('SpeAppBundle:Template:investments.html.php', $data);
        }
    }

    /**
     * @Route("/saveInvestments", name="saveInvestments")
     */
    public function saveInvestmentsAction(Request $request){
        $data = $request->get("data");
        if($data){
            foreach($data as $key => $item){
                if(is_array($item)){
                    $section = $this->get("session")->get($key);
                    foreach($item as $skey => $sitem)
                        $section[$skey]  = $sitem;
                    $this->get("session")->set($key, $section);
                }
                else{
                    $this->get("session")->set($key, $item);
                }
            }
        }
        //$this->updateSession($data);
        $this->updateProfile('investments');
        $investments = $this->get("session")->get('investments');
        $investments['defaultInvestmentFund'] = $this->get("session")->get('defaultInvestmentFund');
        return $this->toJSON($investments);
        //return $this->toJSON(array('status' => 'success'));
    }

    /**
     * @Route("/updateSession", name="updateSession")
     */
    public function updateSessionAction(Request $request){
        $data = $request->get("data");
        $this->updateSession($data);
        return $this->toJSON(array('status' => 'success'));
    }

    /**
     * @Route("/saveRetirementNeeds", name="saveRetirementNeeds")
     */
    public function saveRetirementNeedsAction(Request $request){
        $data = $request->get("data");
        $this->updateSession($data);
        $this->get("session")->set("currentYearlyIncome",$data['retirement_needs']['RN_CurrentYearlyIncome']);
        $this->updateProfile('retireNeeds');
        return $this->toJSON(array('status' => 'success'));
    }

    /**
     * @Route("/saveContributions", name="saveContributions")
     */
    public function saveContributionsAction(Request $request){
        $data = $request->get("data");
        $this->updateSession($data);
        $this->get("session")->set("currentYearlyIncome",$data['contributions']['C_CurrentYearlyIncome']);
        $this->updateProfile('contributions');
        $flexPlan = $this->get('session')->get('common')['flexPlan'];
        if($flexPlan) {
            $this->flexPlanInvestor();
        }

        if ($this->get("session")->get("contributions")["C_ACAoptOut"]) {
            $this->get("session")->remove("autoIncreaseSave");
        }

        return $this->toJSON(array('status' => 'success'));
    }

    /**
     * @Route("/saveMyProfile", name="saveMyProfile")
     */
    public function saveMyProfileAction(Request $request){
        $data = $request->get("data");
        $session = $this->get("session")->all();
        isset($data['common']['email']) ? $session['common']['email'] = $data['common']['email'] : '';
        isset($data['common']['edelivery']) ? $session['common']['edelivery'] = $data['common']['edelivery'] : '';
        isset($data['common']['updateEPProfile']) ? $session['common']['updateEPProfile'] = $data['common']['updateEPProfile'] : '';
        isset($data['common']['updateBeneficiary']) ? $session['common']['updateBeneficiary'] = $data['common']['updateBeneficiary'] : '';
        isset($data['common']['updateProfileBeneficiary']) ? $session['common']['updateProfileBeneficiary'] = $data['common']['updateProfileBeneficiary'] : '';
        isset($data['common']['spousalWaiverFormStatus']) ? $session['common']['spousalWaiverFormStatus'] = $data['common']['spousalWaiverFormStatus'] : '';
        isset($data['my_profiles']['updateProfile']) ? $session['my_profiles']['updateProfile'] = $data['my_profiles']['updateProfile'] : '';
        $session['partBeneficiaries'] = isset($data['partBeneficiaries']) ?  $data['partBeneficiaries'] : '';
        $session['beneficiary'] = isset($data['beneficiary']) ? $data['beneficiary'] : '';
        isset($data['partBeneficiariesDeleted']) ? $session['partBeneficiariesDeleted'] = $data['partBeneficiariesDeleted'] : '';

        $fixedPartBeneficiariesFormat = function($data){
            if(isset($data['BeneficiaryBirthDate']) && $data['BeneficiaryBirthDate'] != '') {
                $data['BeneficiaryBirthDate'] = date('Y-m-d', strtotime($data['BeneficiaryBirthDate']));
            }
            return $data;
        };

        if($session['partBeneficiaries'] && !empty($session['partBeneficiaries'])){
            $session['partBeneficiaries'] = array_map($fixedPartBeneficiariesFormat, $session['partBeneficiaries']);
        }

        if($session['partBeneficiariesDeleted'] && !empty($session['partBeneficiariesDeleted'])){
            $session['partBeneficiariesDeleted'] = array_map($fixedPartBeneficiariesFormat, $session['partBeneficiariesDeleted']);
        }

        $this->get('session')->set('common', $session['common']);
        $this->get('session')->set('my_profiles', $session['my_profiles']);
        $this->get('session')->set('partBeneficiaries', $session['partBeneficiaries']);
        $this->get('session')->set('partBeneficiariesDeleted', $session['partBeneficiariesDeleted']);
        $this->get('session')->set('beneficiary', $session['beneficiary']);
        return $this->toJSON(array('status' => 'success'));
    }

    /**
     * @Route("/saveProfileInfo", name="saveProfileInfo")
     */
    public function saveProfileInfoAction(Request $request){
        $em = $this->get('doctrine')->getManager();
        $data = $request->get("data");
        $session = $this->get("session");
        $this->updateSession($data);

        try {
            $profileId = $session->get('profileId') ? $session->get('profileId') : uniqid('SPE_');
            $data_user = array(
                'userId' => isset($session->get('plan')['userid']) ? $session->get('plan')['userid'] : null,
                'planId' => isset($session->get('rkp')['plan_id']) ? $session->get('rkp')['plan_id'] : null,
                'planName' => isset($session->get('rkp')['plan_data']->planName) ? $session->get('rkp')['plan_data']->planName : null,
                'phone' => isset($session->get('common')['phone']) ? $session->get('common')['phone'] : null,
                'availability' => isset($session->get('common')['availability']) ? $session->get('common')['availability'] : null,
                'profilestatus' => 0,
                'profileId' => $profileId,
                'firstName' => $session->get('common')['firstName'] ? strtoupper($session->get('common')['firstName']) : null,
                'lastName' => $session->get('common')['lastName'] ? strtoupper($session->get('common')['lastName']) : null,
                'dateOfBirth' => $session->get('common')['dateOfBirth'] ? $session->get('common')['dateOfBirth'] : null,
                'email' => $session->get('common')['email'] ? $session->get('common')['email'] : null,
                'address' => isset($session->get('common')['address']) && $session->get('common')['address'] ? $session->get('common')['address']: null,
                'city' => isset($session->get('common')['city']) ? $session->get('common')['city'] : null,
                'state' => isset($session->get('common')['state']) ? $session->get('common')['state'] : null,
                'zip' => isset($session->get('common')['postalCode']) ? $session->get('common')['postalCode'] : null,
                'maritalStatus' => isset($session->get('common')['maritalStatus']) && $session->get('common')['maritalStatus'] ? $session->get('common')['maritalStatus']: null,
                'gender' => isset($session->get('common')['gender']) ? $session->get('common')['gender']: null,
                'ssn' => isset($session->get('common')['ssn']) && $session->get('common')['ssn'] ? $session->get('common')['ssn']: null,
                'email2' => isset($session->get('common')['email2']) && $session->get('common')['email2'] ? $session->get('common')['email2'] : null,
                'preferredEmail' => isset($session->get('common')['preferredEmail']) && $session->get('common')['preferredEmail'] ? $session->get('common')['preferredEmail'] : null,
                'mobile' => isset($session->get('common')['mobile']) ? $session->get('common')['mobile'] : null,
                'location' => isset($session->get('common')['location']) ? $session->get('common')['location'] : null,
            );

            if ($session->get('profileId') && $profileId) {
                $profiles = $em->getRepository('classesclassBundle:profiles')->findOneBy(array('profileId' => $profileId));
                $participants = $em->getRepository('classesclassBundle:participants')->findOneBy(array('id' => $profiles->getParticipant()));
            } else {
                $participants = new participants();
                $profiles = new profiles();
            }

            $gender = $data_user['gender'] ? $data_user['gender'] : ($session->get('common')['firstName'] ? $this->get('spe.app.plan')->findParticipantGender($session->get('common')['firstName']) : null);

            $participants->setFirstName($data_user['firstName']);
            $participants->setLastName($data_user['lastName']);
            $participants->setBirthDate($data_user['dateOfBirth']);
            $participants->setGender($gender ? $gender : null);
            $participants->setEmail($data_user['email']);
            $participants->setEmail2($data_user['email2']);
            $participants->setPreferredEmail($data_user['preferredEmail']);
            $participants->setAddress($data_user['address']);
            $participants->setCity($data_user['city']);
            $participants->setState($data_user['state']);
            $participants->setZip($data_user['zip']);
            $participants->setMaritalStatus($data_user['maritalStatus']);
            $participants->setEmployeeId($data_user['ssn']);
            $participants->setPhone($data_user['phone']);
            $participants->setMobile($data_user['mobile']);
            $participants->setLocation($data_user['location']);
            $em->persist($participants);

            $profiles->setParticipant($participants);
            $profiles->setUserid($data_user['userId']);
            $profiles->setPlanid($data_user['planId']);
            $profiles->setPlanName($data_user['planName']);
            $profiles->setAvailability($data_user['availability']);
            $profiles->setProfilestatus($data_user['profilestatus']);
            $profiles->sessionData = json_encode($request->getSession()->all());
            $profiles->userAgent = $request->headers->get('User-Agent');
            $profiles->smartEnrollPartId = isset($session->get("REQUEST")["smartenrollpartid"]) ? $session->get("REQUEST")["smartenrollpartid"] : null;
            $em->persist($profiles);

            $em->flush();
            $session->set('profileId', $profileId);
        }catch (\Exception $e){
            return $this->toJSON(array('status' => 'fail'));
        }
        return $this->toJSON(array('status' => 'success'));
    }

    public function updateSession($data){
        if($data){
            foreach($data as $key => $d){
                $this->get("session")->set($key, $d);
            }
        }
    }

    /**
     * @Route("/saveMStarProfile", name="saveMStarProfile")
     */
    public function saveMStarProfileAction(){
        $this->updateProfile('mStarConfirmInfo');
        return $this->toJSON(array('success' => 1));
    }

    public function updateProfile($section) {
        $session = $this->get("session");
        $em = $this->get('doctrine')->getManager();

        if($session->get('rkp')['rkp'] == 'educate' && !$session->get('plan')['morningstar']){
            return;
        }

        $profile = $em->getRepository('classesclassBundle:profiles')->findOneBy(array('id' => $session->get('pid')));
        if($section == 'retireNeeds'){
            $this->get('spe.app.profile')->addRetirementNeeds($profile);
        }elseif($section == 'riskProfile'){
            $this->get('spe.app.profile')->addRiskProfile($profile);
        }elseif($section == 'investments'){
            $this->get('spe.app.profile')->addInvestments($profile);
        }elseif($section == 'contributions'){
            $this->get('spe.app.profile')->addContributions($profile);
        }elseif($section == 'mStarQuit'){
            $this->get('spe.app.mStar')->mStarQuit($profile);
        }elseif($section == 'mStarConfirmInfo'){
            $this->get('spe.app.mStar')->addMStarProfileInfo($profile);
        }elseif($section == 'mStarAssetMix'){
            $this->get('spe.app.mStar')->addMStarInvestments($profile);
        }elseif($section == "autoIncrease"){
            $this->get('spe.app.profile')->addAutoIncrease($profile);
        }
    }
    /**
     * @Route("/silentSaveProfile", name="silentSaveProfile")
     */
    public function SilentSaveProfileAction(){
        $this->get('spe.app.profile')->updateProfile(0);
        return $this->toJSON(array('success' => 1));
    }
    /**
     * @Route("/riskProfileData", name="riskProfile")
     */
    public function RiskProfileAction(Request $request){
        $session = $this->get("session");
        $fileName = $request->request->get("filename", 'default');
        $filePath = $path = $session->get('translation_path').'/riskProfile/'. $fileName.".xml";
        if(!file_exists($filePath)){
            $filePath = $path = $session->get('translation_path').'/riskProfile/'.'default.xml';
            $fileName = 'default';
        }
        $riskProfile_data = simplexml_load_file($filePath);
        $records = array();
        $i=0;
        $translator = $this->get('translator')->getMessages();
        foreach($riskProfile_data->questions as $questions){
            foreach($questions as $question){
                $records[$i]['showPoints'] = false;
                if (isset($question->showPoints))
                {
                    $records[$i]['showPoints'] = (bool)(string)$question->showPoints;
                }                
                $records[$i]['text'] = (string)$question->text;
                if (isset($question->kinetikDynamic))
                {
                    $currentYearlyIncome = $session->get('retirement_needs')['RN_CurrentYearlyIncome'];
                    if (!empty($currentYearlyIncome)) {
                        $records[$i]['text'] = str_replace("$300","$".number_format($currentYearlyIncome*0.10, 0, '.', ','),$records[$i]['text']);
                        $records[$i]['text'] = str_replace("$200","$".number_format($currentYearlyIncome*0.08, 0, '.', ','),$records[$i]['text']);
                    }
                }

                $j = 0;
                if($question->options->children()){
                    foreach($question->options->children() as $option){
                        $translationLabel = $translator['investments']['points'];
                        $records[$i]['options'][$j] = (string)$option;
                        $records[$i]['points'] = $option->attributes();
                        if ($records[$i]['points'] == 1)
                        {
                            $translationLabel  = $translator['investments']['point'];
                        }
                        $translationLabel = ucfirst($translationLabel);
                        $records[$i]['optionsPoints'][$j] = (string)$option;
                        if ($records[$i]['showPoints'])
                        {
                            $records[$i]['optionsPoints'][$j] = (string)$option." (".$records[$i]['points']." ".$translationLabel.")";
                        }
                        $j++;
                    }
                } else{
                    $records[$i]['options'] = (string)$question->options;
                    $records[$i]['optionsPoints'] = $records[$i]['options'];
                }
                $records[$i]['audio'] = (string)$question->audio;
                $i++;
            }
        }
        $records['total'] = count($riskProfile_data->xpath('/riskprofile/questions/question') );

        $risk_profile = $this->get("session")->get('risk_profile');
        $risk_profile['RP_xml'] = $fileName;
        $this->get("session")->set('risk_profile', $risk_profile);
        return $this->toJSON($records);
    }

    private function flexPlanInvestor(){
        $session = $this->get("session");
        $retirement_needs =  $session->get('retirement_needs');
        $risk_profile = $session->get('risk_profile');
        $contribution = $session->get('contributions');

        $age = isset($session->get('rkp')['plan_data']->dateOfBirth) ? floor((strtotime(date('m/d/Y')) - strtotime($session->get('rkp')['plan_data']->dateOfBirth)) / 31556926) : 0;
        $yearUntilRetirement = $retirement_needs['RN_NumberOfYearsBeforeRetirement'];
        $annualIncome = $retirement_needs['RN_CurrentYearlyIncome'];
        $totalAssets = $retirement_needs['RN_TotalAssets'];
        $deferralPercent = ($contribution['C_PreTaxContributionPct'] + $contribution['C_RothContributionPct'] + $contribution['C_PostTaxContributionPct'])/100;

        // For Testing
//        $age = 30;
//        $yearUntilRetirement = 35;
//        $annualIncome = 100000;
//        $totalAssets = 400000;
//        $deferralPercent = 0/100;
//        $retirement_needs['return'] = 7.95;
//        $retirement_needs['inflation'] = 2.41;
//        $retirement_needs['RN_RetirementAge'] = 65;
//        echo $this->get('spe.app.functions')->futureValue(7.95/100,35,-(0*100000),-400000);
//        echo "age: ".$age."<br>yearUntilRetirement: ".$yearUntilRetirement."<br>annualIncome: ".$annualIncome."<br>totalAssets: ".$totalAssets."<br>deferralPercent: ".$deferralPercent;
//        var_dump($retirement_needs);

        $fv = $yearUntilRetirement > 0 ?
            $this->get('spe.app.functions')->futureValue($retirement_needs['return']/100,$yearUntilRetirement,-($deferralPercent*$annualIncome),-$totalAssets) :
            $totalAssets;
        $pmt = $yearUntilRetirement > 0 ?
            $this->get('spe.app.functions')->payment($retirement_needs['inflation']/100,$retirement_needs['RN_LifeExpectancy']-$retirement_needs['RN_RetirementAge'],-$fv,0) :
            $this->get('spe.app.functions')->payment($retirement_needs['inflation']/100,$retirement_needs['RN_LifeExpectancy']-$age,-$fv,0);

        $need = $yearUntilRetirement > 0 ?
            $this->get('spe.app.functions')->futureValue($retirement_needs['inflation']/100,$yearUntilRetirement,0,-$annualIncome) :
            $annualIncome;

        $ratio = $pmt/$need*100;

        $plan_portfolios = $session->get('plan_portfolios');
        $investorType = 0;
        if(is_array($plan_portfolios) && count($plan_portfolios)){
            foreach ($plan_portfolios as $port) {
                if ($port['min_score'] <= $ratio && $ratio <= $port['max_score'] && $port['th_min_score'] <= $yearUntilRetirement && $yearUntilRetirement <= $port['th_max_score']) {
                    $investorType = (int) $port['profile'];
                    $risk_profile['recommendedPortfolioName'] = $port['name'];
                }
            }
        }
        $risk_profile['RP_InvestorType'] = (int)$investorType;
        $risk_profile['RP_Score'] = round($ratio);
        $risk_profile['RP_THScore'] = $yearUntilRetirement;
        $session->set('risk_profile', $risk_profile);
        //var_dump($risk_profile); exit;
    }

    /**
     * @Route("/riskProfileInvestor", name="riskProfileInvestor")
     */
    public function riskProfileInvestorAction(Request $request){
        $session = $this->get("session");
        $risk_profile = $session->get('risk_profile');
        $investor_available = array(0,1,2,3,4);
        $answers = $request->get("answers");
        //$answers = array(2,1,0,2,1,0,2);
        $xml = $risk_profile['RP_xml'];
        
        $points = array();
        $score = $th_score = 0;

        if($xml && count($answers)) {
            $filePath = $session->get('translation_path');
            $riskProfile_data = simplexml_load_file($filePath.'/riskProfile/'.$xml.'.xml');
            foreach($riskProfile_data->questions as $questions){
                $q = 0;
                foreach($questions as $question){
                    if($question->options->children()){
                        $o = 0;
                        foreach($question->options->children() as $option){
                            if($o++ == $answers[$q]) {
                                $points[] = (int) $option->attributes();
                                break;
                            }
                        }
                    }
                    $q++;
                }
            }

            $scorings = array();
            foreach($riskProfile_data->scoring as $scoring) {
                $scorings = $scoring;
            }

            //Score Distribution
            if (in_array($xml, array('riskProfileAXA', 'riskProfileLNCLN7'))) {
                for ($i = 0; $i < count($answers); $i++) {
                    if ($i < 6)
                        $score += $points[$i];
                    else
                        $th_score += $points[$i];
                }
            }
            elseif ($xml == 'riskProfileAXA2013') {
                for ($i = 0; $i < count($answers); $i++) {
                    if ($i < 4)
                        $score += $points[$i];
                    else
                        $th_score += $points[$i];
                }
            }
            elseif ($xml == 'riskProfileAXA2014_Ibbotson') {
                for ($i = 0; $i < count($answers); $i++) {
                    if ($i > 1)
                        $score += $points[$i];
                    else
                        $th_score += $points[$i];
                }
            }
            else if ($xml == 'riskProfileABGIL') {
                for ($i = 0; $i < count($answers); $i++) {                       
                    if ($i == 5)
                    $th_score += $points[$i];
                    else
                    $score += $points[$i];
                }
            }
            else if ($xml == "riskProfileADP")
            {
                for ($i = 0; $i < count($answers); $i++) {                       
                    if ($i < 6)
                    {
                        $score += $points[$i];
                    }
                    if ($i == 6)
                    {
                        $th_score += $points[$i];
                    }
                }                              
            }
            else {
                for ($i = 0; $i < count($answers); $i++) {
                    $score += isset($points[$i]) ? $points[$i] : 0;
                }
            }

            if($session->get('module')['investmentSelectorRiskBasedPortfolio'] == 2) {
                switch($xml){
                    case 'smart401k':
                        $maxScore = 445;
                        $minScore = 0;
                        break;
                    case 'riskProfileWIL6':
                        $maxScore = 176;
                        $minScore = 0;
                        break;
                    case 'riskProfileAA':
                        $maxScore = 45;
                        $minScore = 0;
                        break;
                    case 'loringWard':
                    case 'default':
                        $maxScore = 54;
                        $minScore = 6;
                        break;
                }
                //$investor_type = (isset($investor_available[0]))? $investor_available[0] : 0;

                if(isset($maxScore) && isset($minScore)) {
                    
                    $low = $minScore;
                    $high = 0;
                    $mul1 = $maxScore/count($investor_available);
                    $mul2 = $minScore/count($investor_available);
                    $investor_type = (isset($investor_available[0]))? $investor_available[0] : 0;

                    foreach($investor_available as $investor){
                        $high = $low + $mul1 - $mul2;
                        //$scoring[] = array('investor' => $investor, 'low' => ceil($low),'high' => ceil($high));
 
                        
                        if($score > ceil($low) && $score <= ceil($high)) {
                            $investor_type = $investor;
                            break;
                        }
                        $low = $high;
                    }
                }
            }
            else if ($xml !== "smart401k") {  // Get Investor type, name from plan Portfolios (DB). Do not do for Kinetik RTQ as we want to use defaults in smart401k.xml
                $plan_portfolios = $session->get('plan_portfolios');
                if(is_array($plan_portfolios) && count($plan_portfolios)){
                    if(in_array($xml, array('riskProfileAXA', 'riskProfileAXA2013', 'riskProfileAXA2014_Ibbotson', 'riskProfileLNCLN7','riskProfileABGIL','riskProfileADP'))){
                        foreach ($plan_portfolios as $port) {
                            if ($port['min_score'] <= $score && $score <= $port['max_score'] && $port['th_min_score'] <= $th_score && $th_score <= $port['th_max_score']) {
                                $investor_type = (int) $port['profile'];
                                $risk_profile['recommendedPortfolioName'] = $port['name']; // Session variable
                                //break;
                            }
                        }
                    }
                    else{
                        foreach ($plan_portfolios as $port) {
                            if ($port['min_score'] == 0) $port['min_score'] = -1;
                            if ($port['min_score'] <= $score && $score <= $port['max_score']) {
                                $investor_type = (int) $port['profile'];
                                $risk_profile['recommendedPortfolioName'] = $port['name']; // Session variable
                                break;
                            }
                        }
                    }
                }
            }

            ### Default Scoring logic Based on XML ###
            if(!isset($investor_type)) {
                $scoring_attr = $scorings->defaultScoring->attributes();
                if ($xml === "smart401k") { // don't bother doing the below if we using kinetik advice
                    $score_section = 0;
                }
				else if ($session->get('module')['investmentSelectorRiskBasedPortfolio'] == 0) {
					$tmp_count = 0;
					foreach(['riskProfileConservative','riskProfileModeratelyConservative','riskProfileModerate','riskProfileModeratelyAggressive','riskProfileAggressive'] as $key) {
						if ($session->get('module')[$key] == 1) {
							$tmp_count++;
						}
					}
					if($tmp_count <= 3) $score_section = 1;
					if(3 < $tmp_count && $tmp_count <= 5) $score_section = 0;
					if(5 < $tmp_count && $tmp_count <= 10) $score_section = 2;
				}
                elseif($scoring_attr['portfolioCount'] == 'yes'){
                    // count portfolios
                    $portfolios = $session->get('rkp')['plan_data']->portfolios;
                    if(count($portfolios) <= 3) $score_section = 1;
                    if(3 < count($portfolios) && count($portfolios) <= 5) $score_section = 0;
                    if(5 < count($portfolios) && count($portfolios) <= 10) $score_section = 2;
                } else{
                    $score_section = 0;
                }

                if(in_array($xml, array('riskProfileAXA', 'riskProfileAXA2013', 'riskProfileAXA2014_Ibbotson', 'riskProfileLNCLN7','riskProfileADP'))){
                    foreach($scorings->defaultScoring->scoreSection[$score_section]->profiles->profile as $profile){
                        $profile_attr = $profile->attributes();
                        if($profile_attr['low'] == '0') $profile_attr['low'] = -1;
                        if($profile_attr['low'] <= $score && $score <= $profile_attr['high'] && $profile_attr['tlow'] <= $th_score && $th_score <= $profile_attr['thigh']){
                            $investor_type = (int)$profile_attr['id'];
                            $investor_portfolio = (string)$profile_attr['investment'];
                            $risk_profile['recommendedPortfolioName'] = $investor_portfolio; // Session variable
                            break;
                        }
                    }
                }else{
                    foreach($scorings->defaultScoring->scoreSection[$score_section]->profiles->profile as $profile){
                        $profile_attr = $profile->attributes();
                        if(!$profile->ranges){
                            if($profile_attr['low'] == '0') $profile_attr['low'] = -1;
                            if($profile_attr['low'] < $score && $score <= $profile_attr['high']){
                                $investor_type = (int)$profile_attr['id'];
                                $investor_portfolio = (string)$profile_attr['investment'];
                                break;
                            }
                        } else {
                            foreach($profile->ranges->range as $range ){
                                $range_attr = $range->attributes();
                                if($range_attr['low'] == '0')$range_attr['low'] = -1;
                                if($range_attr['low'] < $score && $score <= $range_attr['high']){
                                    $investor_type = (int)$profile_attr['id'];
                                    $investor_portfolio = (string)$profile_attr['investment'];
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            ### Custom scoring logic based on XML ###
            switch ($xml) {
                case 'default':  // no custom logic
                    break;

                case 'loringWard':
                    foreach ($scorings->customScoring->investments->investment as $investment) {
                        $invest_attr = $investment->attributes();

                        if ($invest_attr['low'] == '0')
                            $invest_attr['low'] = -1;
                        if ($invest_attr['low'] < $score && $score <= $invest_attr['high']) {
                            $cust_investor_type = $invest_attr['idArray'];
                            $idArray = explode(',', $invest_attr['idArray']);
                            break;
                        }
                        $cust_investor_type_name = $invest_attr['name'];
                    }

                    $portfolio_match = 0;
                    $portfolios = $session->get('rkp')['plan_data']->portfolios;
                    foreach ($portfolios as $portfolio) {
                        if (in_array($portfolio[0], $idArray)) {
                            $portfolio_match = $portfolio[0];
                            break;
                        }
                    }
                    $risk_profile['RP_Portfolio_Match'] = $portfolio_match; // Session variable
                    break;

                case 'smart401k':
                    $smart401k_advice_profile_id = 0;
                    foreach ($scorings->customScoring->investments->investment as $investment) {
                        $invest_attr = $investment->attributes();
                        if ($invest_attr['low'] == '0') $invest_attr['low'] = -1;
                        if ($score > $invest_attr['low'] && $score <= $invest_attr['high']) {
                            $smart401k_advice_profile_id = (int) $invest_attr['id'];
                            break;
                        }
                    }
                    $risk_profile['smart401k_advice_profile_id'] = $smart401k_advice_profile_id; // Session variable
                    break;

                default:
                    break;
            }

//            echo "<br>score:".$score;
//            echo "<br>th_score:".$th_score;
//            echo "<br>type:".$investor_type;
//            echo "<br>smart401k_advice_profile_id:".$smart401k_advice_profile_id;
//            exit;

            if (!isset($investor_type)) $investor_type = 0;
            $risk_profile['RP_InvestorType'] = (int)$investor_type; // Session variable
            $risk_profile['RP_Score'] = $score; // Session variable
            $risk_profile['RP_THScore'] = $th_score; // Session variable
            $risk_profile['RP_Points'] = implode("|",$points); // Session variable
            $risk_profile['RP_Answers'] = $answers; // Session variable
            //$risk_profile['RP_xml'] = $xml; // Session variable

            $session->set('smart401k_completed', $risk_profile['RP_xml'] == 'smart401k' ? 1:0 );
            $session->set('risk_profile', $risk_profile );

            //var_dump($risk_profile);
            //var_dump($session->get('risk_profile'));
            if ($this->get("session")->get("smart401k"))
            {
                $this->kinetikAddParticipant();
                $kinetikParticipant = $this->get("kinetik.participant");
                $adviceFunds = $kinetikParticipant->getParticipant($this->kinetikPartnerParticipantId());
                $sessionAdviceFunds = [];
                foreach ($adviceFunds as $adviceFund)
                {
                    $sessionAdviceFunds[$adviceFund['ticker']] = $adviceFund;
                }
                $this->get("session")->set("sessionAdviceFunds",$sessionAdviceFunds);
            }
            $risk_profile = $this->get('spe.app.smart')->setRiskProfileInvestors();
            $this->updateProfile('riskProfile');
        }

        return $this->toJSON($risk_profile);
    }
    public function kinetikPartnerParticipantId()
    {
        $session = $this->get("session");
        return "{$session->get('plan')['partnerid']}*sep*{$session->get('rkp')['participant_uid']}";
    }
    public function kinetikAddParticipant($profileId = "")
    {
        $session = $this->get("session");
        $risk_profile = $session->get("risk_profile");
        $kinetikParticipant = $this->get("kinetik.participant");
        $partnerParticipantId = $this->kinetikPartnerParticipantId();
        $firstName = $session->get('rkp')['plan_data']->firstName;
        $lastName = $session->get('rkp')['plan_data']->lastName;
        $birthDate = $session->get('rkp')['plan_data']->dateOfBirth;
        $birthDate = date('Y-m-d', strtotime($birthDate));
        $partnerPlanId = "{$session->get('plan')['partnerid']}*sep*{$session->get('plan')['planid']}";
        $prePct = $rothPct = null;
        $salary = "";
        $employerContributionPct = ""; // always send empty string for now

        if (isset($session->get('retirement_needs')['RN_CurrentYearlyIncome']) && $session->get('retirement_needs')['RN_CurrentYearlyIncome'] != "")
        {
            $salary = (float)$session->get('retirement_needs')['RN_CurrentYearlyIncome'];
        }
        if (isset($session->get('contributions')['C_PreTaxContributionPct']) && $session->get('contributions')['C_PreTaxContributionPct'] != "")
        {
            $prePct = (float)$session->get('contributions')['C_PreTaxContributionPct'];
        }
        if (isset($session->get('contributions')['C_RothContributionPct']) && $session->get('contributions')['C_RothContributionPct'] != "")
        {
            $rothPct = (float)$session->get('contributions')['C_RothContributionPct'];
        }
        $contributionPct = $prePct + $rothPct;
        if (empty($contributionPct)) { // need to send "" instead of null if prePct and rothPct are both null
            $contributionPct = "";
        }
        $risk = $risk_profile['RP_Score'];

        $response = $kinetikParticipant->addParticipant($partnerParticipantId, $firstName, $lastName, $birthDate, $partnerPlanId, $profileId,
            $contributionPct, $employerContributionPct, $salary, $risk, array());
        return $response;
    }
    /**
     * @Route("/updateTransaction", name="updateTransaction")
     */
    public function updateTransactionAction(Request $request){
        $data = $request->get('data');
        $session = $this->get("session");
        $translator = $this->get('translator')->getMessages();

        $t = isset($data['id']) ? $data['id'] : null;
        $isTest = $session->get('testing') ? 1 : 0;
        $actions = array(
            'Personal' => 'personalConfirmationStatus',
            'Elections' => 'investmentsConfirmationStatus',
            'Realignment' => 'realignmentConfirmationStatus',
            'Contributions' => 'contributionsConfirmationStatus',
            'Catch Up' => 'catchupContributionConfirmationStatus',
            'Beneficiary' => 'beneficiaryConfirmationStatus',
            'Enrollment' => 'enrollmentConfirmationStatus',
            'Selections' => 'investmentsConfirmationStatus',
            'ATFutureBuilder' => 'ATTransactStatus'
        );
        $response = array();

        if($t == '0' && !$isTest){
            $response = $this->get('spe.app.rkp')->postPersonal();
        }elseif($t == '1' && !$isTest){
            $response = $this->get('spe.app.rkp')->postElections();
        }elseif($t == '2' && !$isTest){
            $response = $this->get('spe.app.rkp')->postRealignment();
        }elseif($t == '3' && !$isTest){
            $response = $this->get('spe.app.rkp')->postContributions();
        }elseif($t == '4' && !$isTest){
            $response = $this->get('spe.app.rkp')->postCatchUp();
        }elseif($t == '5' && !$isTest){
            $response = $this->get('spe.app.rkp')->postBeneficiary();
        }elseif($t == '6' && !$isTest){
            sleep(5);
            $response = $this->get('spe.app.rkp')->postEnrollment();
        }elseif($t == '7' && !$isTest){
            $responses = $this->get('spe.app.rkp')->postCombined();
            $responses = json_decode($responses,true);
            if(count($responses) == 1) {
                $response = json_encode(current($responses));
            }else {
                $response = json_encode($responses);
            }
        }elseif($t == '8' && !$isTest) {
            $profileid = $session->get('pid');
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintOutlook');
            $outlook= $repository->findOneBy(array('profileid' => $profileid));
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATArchitect');
            $architect = $repository->findOneBy(array('profileid' => $profileid));
            $transData = $this->getWsBlueprint();

            if (($outlook != null && $outlook->complete))
            {
                $transData['WsAccounts'] = $this->getCurrentATWsAccountsAsXmlArray($profileid);
            }

            $architectIsEnrolled = $session->get("ACInitialize")['WsIndividual']['ArchitectIsEnrolled'];
            $transData['WsIndividual']['OverrideAnnualCompensation'] =  'true';

            if ($architectIsEnrolled == 'true') {
                if ($architect->acceptedInvestment == 0) {
                    $transData['ArchitectOptOut'] = 'true';
                } else {
                    $transData['ArchitectOptOut'] = 'false';
                }
            } else {
                if ($architect->acceptedInvestment == 1) {
                    $transData['ArchitectOptIn'] = 'true';
                } else {
                    $transData['ArchitectOptIn'] = 'false';
                }
            }

            $response = $this->get("spe.app.rkp")->ACTransact($transData);
            $response['success'] = 1;
            $response['message'] = 'Transaction request processed';
            $response['confirmation'] = $response['ConfirmationId'];
            $response = json_encode($response);

        }
        elseif($t == "9")
        {
            $kinetikResponse = $this->kinetikAddParticipant($session->get('profileId'));
            $response['success']= $kinetikResponse == null;
            $response['message'] = ($response['success'] ? "Advice funds processed successfully." : "Advice funds failed.");
            $response['confirmation'] = "";
            $response = json_encode($response);
        }
        elseif($t == '10'){
            $response = $this->get('spe.app.profile')->updateProfile();
        }

        if($isTest && $t != '10' && $t != "9"){
            $response = json_encode(array(
                'success' => 1,
                'message' => array_keys($actions)[$t] . ' '.$translator['my_profile']['change_processed_successfully'],
                'confirmation' => time()
            ));
        }       
        
        $checkResponse = json_decode($response, true);
        $emailData = json_decode(json_encode($session->get('rkp')['plan_data']), true);
        
        if (count($checkResponse) > 1) {
            foreach($checkResponse as $resp){
                if(isset($resp['success']) &&  !$resp['success']){
                   $this->get('spe.app.functions')->sendProfileErrorEmail($resp, $emailData);
                   break;
                }
            }
        }else if(isset($checkResponse['success']) ||  !$checkResponse['success']) {
            
            $this->get('spe.app.functions')->sendProfileErrorEmail($checkResponse, $emailData);
          
        }        

        if(isset(array_values($actions)[$t])){
            $session->set(array_values($actions)[$t], $response);
        }
        return $this->toJSON(json_decode($response,true));
    }

    /**
     * @Route("/sendProfile", name="sendProfile")
     */
    public function sendProfileAction(Request $request){
        $response = $this->sendProfileEmail();
        if($response === null){
            return $this->toJSON(array('status' => 'null'));
        }
        else if($response === false){
            return $this->toJSON(array('status' => 'fail'));
        }
        return $this->toJSON(array('status' => 'success'));
    }

    /**
     * @Route("/sendProfileEmail", name="sendProfileEmail")
     */
    public function sendProfileEmailAction(Request $request){
        $session = $this->get("session");
        $em = $this->get('doctrine')->getManager();
        $email = $request->get('email');
        $profileId = $session->get('profileId');
        if($email && $profileId) {
            if (!$this->sendProfileEmail($email)) {
                return $this->toJSON(array('status' => 'fail'));
            }
            $profiles = $em->getRepository('classesclassBundle:profiles')->findOneBy(array('profileId' => $profileId));
            $participants = $em->getRepository('classesclassBundle:participants')->findOneBy(array('id' => $profiles->getParticipant()));
            $participants->setEmail($email);
            $em->persist($participants);
            $em->flush();
        }
        return $this->toJSON(array('status' => 'success'));
    }

    /**
     * @Route("/postACAOptOut", name="postACAOptOut")
     */
    public function postACAOptOutAction(){
        if($this->get('session')->get('testing')){
            $response = json_encode(array('success' => 1, 'message' => 'ACA change processed sucessfully.', 'confirmation' => time()));
        }else{
            $response = $this->get('spe.app.rkp')->postACAOptOut();
        }
        $this->get('session')->set('ACARequestResponse', $response);
        return $this->toJSON(array('status' => 'success', 'response' => $response));
    }

    private function sendProfileEmail($email = null, $bcc = null){
        $session = $this->get("session");
        $translator = $this->get('translator')->getMessages();
        $planName = isset($session->get('plan')['name']) && $session->get('plan')['name'] ? $session->get('plan')['name'] : $session->get('rkp')['plan_data']->planName;
        $emails = array();
        if ($email != null){
            $emails[] = trim($email);
        }
        else if(isset($session->get('plan')['profileNotificationEmail']) && $session->get('plan')['profileNotificationEmail'] != ""){
            $emailSplit = explode(",",$session->get('plan')['profileNotificationEmail']);
            foreach ($emailSplit as $email){
                $emails[] = trim($email);
            }
        }
        else 
        return null;
        

        $emailer = $this->get('spe.app.mailer');
        foreach ($emails as $email){
            $from = $session->get("plan")['investorProfileEmail'];
            $subject = 'Your Investor Profile';
            $content = "<div style='margin-top:30px;font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height: 25px;color: black;'>
                                <span style='font-size: 20px;line-height: 30px;'>". $translator['my_profile']['download_your_investor_profile_for'] ." <br> $planName <br></span>
                                <a href='".$this->get('spe.app.functions')->getBaseUrl()."/profile/report/".$session->get('profileId')."' target='_blank'>". $translator['my_profile']['your_investor_profile'] ."</a>
                                <br><br>Thank you!<br>
                                <span style='padding-top:10px;margin-top:10px;font-family: Arial, Helvetica, sans-serif;font-size: 12px;line-height: 10px;'>&copy; ".date('Y')." ". $translator['my_profile']['smartplan_enterprise_is_trademark_of_vwise'] ."</span>
                            </div>";
            if(!$emailer->sendMail($email, $from, $subject, $content, null, $bcc)){
                return false;
            }
        }
        return true;
    }

    /**
     * @Route("/mStarProfileInfo", name="mStarProfileInfo")
     */
    public function mStarProfileInfoAction(){
        $data = $this->get('spe.app.mStar')->getParticipantData();
        return $this->toJSON(array('success' => 1, 'data' => $data));
    }

    /**
     * @Route("/updateMStarProfileInfo", name="updateMStarProfileInfo")
     */
    public function updateMStarProfileInfoAction(Request $request){
        $data = $request->get('data');
        $session = $this->get("session");
        $mStarData = $session->get('morningstar');

        if($data){
            $catchUpContribution = $session->get('morningstar')['participant']['CatchUpContribution'];
            if($catchUpContribution){
                foreach($catchUpContribution as $type => $contribution){
                    foreach($data as $index => $d){
                        if($d['name'] == 'contribution['.$type.'][ContriDollar]'){
                            $catchUpContribution[$type]['ContriDollar'] = isset($data[$index]) ? $data[$index]['value'] : $catchUpContribution[$type]['ContriDollar'];
                            unset($data[$index]);
                        }
                        if($d['name'] == 'contribution['.$type.'][Length]'){
                            $catchUpContribution[$type]['Length'] = isset($data[$index]) ? $data[$index]['value'] : $catchUpContribution[$type]['Length'];
                            unset($data[$index]);
                        }
                    }
                }
                $mStarData['participant']['CatchUpContribution'] = $catchUpContribution;
            }
            foreach($data as $key => $mstar){
                $mStarData['participant'][$mstar['name']] = $mstar['value'];
            }
            $session->set('morningstar', $mStarData);
        }
        return $this->toJSON(array('success' => 1, 'data' => $mStarData['participant']));
    }

    /**
     * @Route("/mStarAssetMix", name="mStarAssetMix")
     */
    public function mStarAssetMixAction(){
        $data = $this->get('spe.app.mStar')->getStrategyData();
        $this->updateProfile('mStarAssetMix');
        return $this->toJSON(array('success' => 1, 'data' => $data));
    }

    /**
     * @Route("/updateReviewDetail", name="updateReviewDetail")
     */
    public function updateReviewDetailAction(){
        $session = $this->get("session");
        $session->set('mStarUpdate', 1);
        return $this->toJSON(array('success' => 1));
    }

    /**
     * @Route("/updateMStarTransaction", name="updateMStarTransaction")
     */
    public function updateMStarTransactionAction(Request $request){
        $data = $request->get('data');
        $session = $this->get("session");
        $response = array();

        $t = isset($data['id']) ? $data['id'] : null;
        if($t == 0){
            $response = $this->get('spe.app.mStar')->updateStrategy();
        }elseif($t == 1){
            $response = $this->get('spe.app.mStar')->updateProfile();
        }
        return $this->toJSON($response);
    }

    /**
     * @Route("/quitMStar", name="quitMStar")
     */
    public function quitMStarAction(Request $request){
        $data = $request->get('data');
        $this->updateSession($data);
        $this->updateProfile('mStarQuit');
        return $this->toJSON(array('success' => 1));
    }

    /**
     * @Route("/ping", name="ping")
     */
    public function pingAction(){
        $this->get("session")->set('last_activity',time());
        $response = $this->get('spe.app.rkp')->ping();
        return $this->toJSON(json_decode($response,true));
    }

    /**
     * @Route("/sessionTimeLeft", name="sessionTimeLeft")
     */
    public function sessionTimeLeftAction(){
        $session = $this->get("session");
        $exp_time = $session->get('session_expiration');
        $last_activity = $session->get('last_activity');
        $cur_time = time();
        $time_left = ($exp_time - ($cur_time - $last_activity)) * 1000;
        return $this->toJSON(array('time_left' => $time_left));
    }

    protected function toJSON($data){
        $request = $this->get("session")->get('REQUEST');
        if(isset($request['server']) && $request['server'] == 1) {
            $data['ip'] = $_SERVER['SERVER_ADDR'];
        }
        return new Response(json_encode($data));
    }

    /**
     * @Route("/postVideoStats", name="postVideoStats")
     */
    public function postVideoStatsAction(Request $request){
        $em = $this->get('doctrine')->getManager();
        $videoStatsSpe = new videoStatsSpe();
        $arr =  $request->request->all();
        $generalMethods = new GeneralMethods($this);
        $session = $this->get("session");
        $repository = $em->getRepository('classesclassBundle:profiles');
        $profile = $repository->findOneBy(array("profileId" => $session->get("profileId")));        
        while (list($key, $value) = each($arr))
        {
            $videoStatsSpe->$key = $value;
        }
        $videoStatsSpe->sessionid = $session->getId();
        $videoStatsSpe->ipaddress = $generalMethods->get_client_ip();
        $videoStatsSpe->planid = $session->get("plan")['id'];
        $videoStatsSpe->recordkeeperPlanid = $session->get("plan")['planid'];
        $videoStatsSpe->userid = $session->get("plan")['userid'];
        $videoStatsSpe->partnerid = $session->get("plan")['partnerid'];
        $videoStatsSpe->participantid = $profile->participant->id;
        $videoStatsSpe->uniqid = $profile->uniqid;
        $em->persist($videoStatsSpe);
        $em->flush();
        return new Response("");
    }
    /**
     * @Route("investments/table", name="investmentsTable")
     */
    public function investmentsTableAction(Request $request)
    {
        if (!$request->request->get("ajax",false))
        {
            return $this->render('SpeAppBundle:Template:investments/graph/table.html.twig');
        }
        else
        {
            return $this->render('SpeAppBundle:Template:investments/graph/tableajax.html.twig');
        }
    }
    /**
     * @Route("investments/graph", name="investmentsGraph")
     */
    public function investmentsGraphAction()
    {       
        return $this->render('SpeAppBundle:Template:investments/graph/graph.html.twig');
    }    
    /**
     * @Route("investments/graph2", name="investmentsGraph2")
     */
    public function investmentsGraph2Action(Request $request)
    {
        if ($request->request->get("id","") != "")
        {
            $this->get("session")->set("PlansInvestmentsGraphSelectedMinScore",$request->request->get("id"));
        }
        return $this->render('SpeAppBundle:Template:investments/graph/graph2.html.twig',$this->calculateInvestmentsGraphTable());
    } 
    /**
     * @Route("investments/graph3", name="investmentsGraph3")
     */    
    public function investmentsGraph3Action(Request $request)
    {
        $this->get("session")->set("graphScores",$request->request->get("scores"));
        return $this->render('SpeAppBundle:Template:investments/graph/graph3.html.twig',$this->calculateInvestmentsGraphTable());
    }
    /**
     * @Route("investments/completedGraph", name="completedGraph")
     */    
    public function completedGraphAction(Request $request)
    {
        $this->get("session")->set("investmentsGraphComplete",1);
        return new Response("");
    }

    private function _populateEntity ($input, $extras, $entity)
    {
        $exemptComma = ['monthlyRetirementGoal', 'socialSecurityMonthlyAmount', 'spouseSocialSecurityMonthlyAmount','annualCompensation','spouseAnnualCompensation','accountBalance','projectedAnnualReturn','estimatedMonthlyIncome'];

        if ($input != null) {
            foreach ($input as $item) {
                if (in_array($item['name'], $exemptComma)) {
                    $entity->$item['name'] = str_replace(',', '', $item['value']);
                } else {
                    $entity->$item['name'] = $item['value'];
                }
                //exceptions time
                if ($item['name'] == "mobilePhone") {
                    $entity->setMobilePhone($item['value']);
                } else if ($item['name'] == "balanceAsOf") {
                    $time = strtotime($item['value']);
                    $newformat = date('Y-m-d', $time);
                    $newformat .= 'T00:00:00';
                    $entity->$item['name'] = $newformat;
                } else if (($item['name'] == "eeAdditions" || $item['name'] == "erAdditions") && $item['value'] == "") {
                    $entity->$item['name'] = "0.00";
                }
            }
        }

        if ($extras != null) {
            foreach ($extras as $key => $value) {
                $entity->$key = $value;
            }
        }

        return $entity;
    }

    private function _checkIfSpouseExists()
    {
        $spouseInfo = $this->ATBlueprintPersonalInformation();
        $hidden = true;
        if ($spouseInfo->spouseFirstName != '' || $spouseInfo->spouseLastName != '') {
            $hidden = false;
        }
        return $hidden;
    }

    /**
     * @Route("/atBlueprint/personalInfo/update", name="atBlueprintPersonalInfoUpdate")
     */
    public function atBlueprintPersonalInfoUpdateAction(Request $request)
    {
        $em = $this->get('doctrine')->getManager();
        $table = $request->request->get('table');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $table);
        $id = $request->request->get('id');
        $toedit = $repository->findOneBy(array('id' => $id));

        $input = $request->request->get("fields");
        $extras = $request->request->get("extras");

        $toedit = $this->_populateEntity($input, $extras, $toedit);

        $em->persist($toedit);
        $em->flush();

        return new Response('saved');
    }

    /**
     * @Route("/atBlueprint/personalInfo/save", name="atBlueprintPersonalInfoSave")
     */
    public function atBlueprintPersonalInfoSaveAction(Request $request)
    {
        $em = $this->get('doctrine')->getManager();
        $table = $request->request->get('table');
        $fullclass = 'classes\\classBundle\\Entity\\' . $table;
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $table);
        $profileid = $this->get("session")->get("pid");
        $userid = $this->get("session")->get("plan")['userid'];
        $planid = $this->get("session")->get("plan")['id'];

        $oneToMany = $request->request->get('oneToMany');
        $toadd = null;
        if ($oneToMany != true) {
            $toadd = $repository->findOneBy(array('profileid' => $profileid));
        }
        if ($toadd == null) {
            $toadd = new $fullclass;
            $toadd->profileid = $profileid;
            $toadd->userid = $userid;
            $toadd->planid = $planid;
        }
        $input = $request->request->get("fields");
        $extras = $request->request->get("extras");

        $toadd = $this->_populateEntity($input, $extras, $toadd);

        foreach ($input as $item)
        {
            if ($item['name'] == "includeSpousalInformation")
            {
                if ($item['value'] == 'on') {
                    $toadd->includeSpousalInformation = "true";
                } else {
                    $toadd->includeSpousalInformation = "false";
                }
            }
        }
        if ($table == 'ATWsAccounts' && $toadd->balanceAsOf == null) { // null balanceAsOf messes with call
            $dateObject = new \DateTime();
            $toadd->balanceAsOf = $dateObject->format("Y-m-d")."T00:00:00";
        }
        $em->persist($toadd);
        $em->flush();

        return new Response('saved');
    }

    /**
     * @Route("/atBlueprint/personalInfo/delete", name="atBlueprintPersonalInfoDelete")
     */
    public function atBlueprintPersonalInfoDeleteAction(Request $request)
    {
        $em = $this->get('doctrine')->getManager();
        $table = $request->request->get('table');
        $id = $request->request->get('id');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $table);
        $userid = $this->get("session")->get("plan")['userid'];

        $personalInfo = $repository->findOneBy(array('id' => $id));
        if ($userid == $personalInfo->userid) {
            $em->remove($personalInfo);
            $em->flush();
        }

        return new Response('saved');
    }
    function ATBlueprintPersonalInformationHash($ATBlueprintPersonalInformation)
    {
        $individual = $this->get("session")->get("ACInitialize")['WsIndividual'];
        $rkp = $this->get("session")->get('rkp');
        $params= [
            "firstName" =>
            [
                "entityField" => "firstName",
                "entityValue" => $ATBlueprintPersonalInformation->firstName,
                "rkpValue" => $this->get("session")->get("rkp")["plan_data"]->firstName
            ],
            "lastName" =>
            [
                "entityField" => "lastName",
                "entityValue" => $ATBlueprintPersonalInformation->lastName,
                "rkpValue" => $this->get("session")->get("rkp")["plan_data"]->lastName
            ],
            "email" =>
            [
                "entityField" => "email",
                "entityValue" => $ATBlueprintPersonalInformation->email,
                "rkpValue" => $individual['Email'],
                "rkpField" => "Email"
            ],
            "mobilePhone" =>
            [
                "entityField" => "mobilePhone",
                "entityValue" => $ATBlueprintPersonalInformation->mobilePhone,
                "rkpValue" => $individual['MobilePhone'],
                "rkpField" => "MobilePhone"
            ],
            "annualCompensation" =>
            [
                "entityField" => "annualCompensation",
                "entityValue" => $ATBlueprintPersonalInformation->annualCompensation,
                "rkpValue" => $individual['AnnualCompensation'],
                "rkpField" => "AnnualCompensation"
            ],
            "dateOfBirth" =>
            [
                "entityField" => "dateOfBirth",
                "entityValue" => $ATBlueprintPersonalInformation->dateOfBirth,
                "rkpValue" => $rkp['plan_data']->dateOfBirth,
                "rkpField" => "BirthDate"
            ],
            "spouseFirstName" =>
            [
                "entityField" => "spouseFirstName",
                "entityValue" => $ATBlueprintPersonalInformation->spouseFirstName,
                "rkpValue" => $individual['SpouseFirstName'],
                "rkpField" => "SpouseFirstName"
            ],
            "spouseLastName" =>
            [
                "entityField" => "spouseLastName",
                "entityValue" => $ATBlueprintPersonalInformation->spouseLastName,
                "rkpValue" => $individual['SpouseLastName'],
                "rkpField" => "SpouseLastName"
            ],
            "spouseAnnualCompensation" =>
            [
                "entityField" => "spouseAnnualCompensation",
                "entityValue" => $ATBlueprintPersonalInformation->spouseAnnualCompensation,
                "rkpValue" => (float)$individual['SpouseAnnualCompensation'],
                "rkpField" => "SpouseAnnualCompensation"
            ],
            "spouseDateOfBirth" =>
            [
                "entityField" => "spouseDateOfBirth",
                "entityValue" => $ATBlueprintPersonalInformation->spouseDateOfBirth,
                "rkpValue" => $individual['SpouseBirthDate'],
                "rkpField" => "SpouseBirthDate"
            ],
            "includeSpousalInformation" =>
            [
                "entityField" => "includeSpousalInformation",
                "entityValue" => $ATBlueprintPersonalInformation->includeSpousalInformation,
                "rkpValue" => $individual['SpouseIncludeInBlueprint'],
                "rkpField" => "SpouseIncludeInBlueprint"
            ]
        ];
        if ($individual['OverrideReplacementIncome'] == 'true') {
            $params['monthlyRetirementGoal'] = array(
                "entityField" => "monthlyRetirementGoal",
                "entityValue" => $ATBlueprintPersonalInformation->monthlyRetirementGoal,
                "rkpValue" => $individual['ReplacementIncomeNeeded'],
                "rkpField" => "ReplacementIncomeNeeded"
            );
        }
        if ($individual['OverrideRetirementAge'] == 'true') {
            $params['retirementAge'] = array(
                "entityField" => "retirementAge",
                "entityValue" => $ATBlueprintPersonalInformation->retirementAge,
                "rkpValue" => $individual['RetirementAge'],
                "rkpField" => "RetirementAge"
            );
        }
        if ($individual['OverrideSsRetirementAge'] == 'true') {
            $params['socialSecurityBeginAge'] = array(
                "entityField" => "socialSecurityBeginAge",
                "entityValue" => $ATBlueprintPersonalInformation->socialSecurityBeginAge,
                "rkpValue" => $individual['SsRetirementAge'],
                "rkpField" => "SsRetirementAge"
            );
        }
        if ($individual['OverrideSsBenefit'] == 'true') {
            $params['socialSecurityMonthlyAmount'] = array(
                "entityField" => "socialSecurityMonthlyAmount",
                "entityValue" => $ATBlueprintPersonalInformation->socialSecurityMonthlyAmount,
                "rkpValue" => $individual['SsBenefit'],
                "rkpField" => "SsBenefit"
            );
        }
        if ($individual['OverrideSsSpousalBenefit'] == 'true') {
            $params['spouseSocialSecurityMonthlyAmount'] = array(
                "entityField" => "spouseSocialSecurityMonthlyAmount",
                "entityValue" => $ATBlueprintPersonalInformation->spouseSocialSecurityMonthlyAmount,
                "rkpValue" => $individual['SsSpousalBenefit'],
                "rkpField" => "SsSpousalBenefit"
            );
        }
        if ($individual['OverrideYearsInRetirement'] == 'true') {
            $params['yearsInRetirement'] = array(
                "entityField" => "yearsInRetirement",
                "entityValue" => $ATBlueprintPersonalInformation->yearsInRetirement,
                "rkpValue" => $individual['YearsInRetirement'],
                "rkpField" => "YearsInRetirement"
            );
        }
        $dateFields = array("dateOfBirth","spouseDateOfBirth");
        foreach ($dateFields as $field)
        {
            $params[$field]['rkpValue'] = $this->atConvertRkpDate($params[$field]['rkpValue']);
            $params[$field]['entityValue'] = $this->atConvertEntityDate($params[$field]['entityValue']);
        }
        $numberFields = array("annualCompensation","spouseAnnualCompensation");
        foreach($numberFields as $field)
        {
            $params[$field]['entityValue'] = $this->atConvertEntityNumber($params[$field]['entityValue']);
        }
        return $params;
    }
    function atConvertEntityNumber($number)
    {
        return number_format($number, 2, '.', '');
    }
    function atConvertRkpDate($date)
    {
        if ($date == null)
        {
            return "";
        }
        $dateObject = new \DateTime($date);
        return $dateObject->format("m/d/Y");
    }
    function initializeATWsAccounts()
    {
        $em = $this->get('doctrine')->getManager();
        foreach($this->get("session")->get('ACInitialize')['WsAccounts'] as $arr) {
            $account = new ATWsAccounts();
            $account->profileid = $this->get("session")->get("pid");
            $account->userid = $this->get("session")->get("plan")['userid'];
            $account->planid = $this->get("session")->get("plan")['id'];
            foreach ($arr as $key => $value) {
                $theKey = lcfirst($key);
                $account->$theKey = $value;
            }
            $em->persist($account);
        }
        $em->flush();
    }

    function atConvertEntityDate($date)
    {
        if ($date == null)
        {
            return "";
        }
        $dateObject = new \DateTime($date);
        return $dateObject->format("Y-m-d")."T00:00:00";
    }
    function initializeATBlueprintPersonalInformation()
    {
        $personalInfo = new ATBlueprintPersonalInformation();
        $initializeHash = $this->ATBlueprintPersonalInformationHash($personalInfo);
        foreach ($initializeHash as $row)
        {
            $personalInfo->$row['entityField'] = $row['rkpValue'];
        }
        $personalInfo->profileid = $this->get("session")->get("pid");
        $personalInfo->userid = $this->get("session")->get("plan")['userid'];
        $personalInfo->planid = $this->get("session")->get("plan")['id'];

        $this->initializeATWsAccounts();

        return $personalInfo;
    }
    function ATBlueprintPersonalInformation()
    {

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintPersonalInformation');
        $profileid = $this->get("session")->get("pid");
        $personalInfo = $repository->findOneBy(array('profileid' => $profileid));
        if ($personalInfo == null) {
            $personalInfo = $this->initializeATBlueprintPersonalInformation();
        }

        return $personalInfo;
    }
    function setATBlueprintPersonalInformation()
    {
        $em = $this->get('doctrine')->getManager();
        $personalInfo = $this->initializeATBlueprintPersonalInformation();
        $em->persist($personalInfo);
        $em->flush();
    }
    /**
     * @Route("/atBlueprint/personalInfo/required", name="atBlueprintPersonalInfoRequired")
     */
    public function atBlueprintPersonalInfoRequiredAction(Request $request)
    {
        $this->setATLastPath($request);
        return $this->render('SpeAppBundle:Template:ATBlueprint/personalInformation/required.html.twig', array('personalInfo' => $this->ATBlueprintPersonalInformation()));
    }
    /**
     * @Route("/atBlueprint/personalInfo/bridge", name="atBlueprintPersonalInfoBridge")
     */
    public function atBlueprintPersonalInfoBridgeAction(Request $request)
    {
        $this->setATLastPath($request);
        $personalInfo = $this->ATBlueprintPersonalInformation();
        $filledInMeasurements = false;
        if ($personalInfo != null) {
            if ($personalInfo->retirementAge != null || $personalInfo->yearsInRetirement != null || $personalInfo->socialSecurityBeginAge != null ||
                $personalInfo->monthlyRetirementGoal != null || $personalInfo->socialSecurityMonthlyAmount != null || $personalInfo->spouseSocialSecurityMonthlyAmount != null) {
                $filledInMeasurements = true;
            }
        }

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATWsAccounts');
        $profileid = $this->get("session")->get("pid");
        $accounts = $repository->findBy(array('profileid' => $profileid, 'isIncomeAccount' => 'false', 'includeInCalcs' => 'true'));

        $filledInAccounts = false;
        if ($accounts != null) {
            $blueprint = $this->getWsBlueprint()['WsBlueprint'];
            foreach($accounts as $account) {
                if($account->accountNumber != $blueprint['AccountNumber']) {
                    $filledInAccounts = true;
                    break;
                }
            }
        }

        $income = $repository->findBy(array('profileid' => $profileid, 'isIncomeAccount' => 'true', 'includeInCalcs' => 'true'));
        $filledInIncome = false;
        if ($income != null) {
            $filledInIncome = true;
        }

        return $this->render('SpeAppBundle:Template:ATBlueprint/personalInformation/bridge.html.twig', array('filledInMeasurements' => $filledInMeasurements, 'filledInAccounts' => $filledInAccounts, 'filledInIncome' => $filledInIncome));
    }

    /**
     * @Route("/atBlueprint/personalInfo/measurements", name="atBlueprintPersonalInfoMeasurements")
     */
    public function atBlueprintPersonalInfoMeasurementsAction(Request $request)
    {
        $this->setATLastPath($request);
        $personalInfo = $this->ATBlueprintPersonalInformation();
        $hidden = $this->_checkIfSpouseExists();

        $birthDate = $personalInfo->dateOfBirth;
        $birthDate = explode("/", $birthDate);
        $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
            ? ((date("Y") - $birthDate[2]) - 1)
            : (date("Y") - $birthDate[2]));
        return $this->render('SpeAppBundle:Template:ATBlueprint/personalInformation/measurements.html.twig', array('personalInfo' => $personalInfo, 'hidden' => $hidden, 'age' => $age));
    }

    /**
     * @Route("/atBlueprint/personalInfo/otheraccounts", name="atBlueprintPersonalInfoOtherAccounts")
     */
    public function atBlueprintPersonalInfoOtherAccountsAction(Request $request)
    {
        $this->setATLastPath($request);
        $playAudio = $request->request->get('playAudio');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATWsAccounts');
        $profileid = $this->get("session")->get("pid");

        $blueprint = $this->getWsBlueprint();

        $personalInfo = $repository->findBy(array('profileid' => $profileid, 'isIncomeAccount' => 'false', 'includeInCalcs' => 'true'));
        foreach ($personalInfo as $key => $value) {
            if ($value->accountNumber == $blueprint['WsBlueprint']['AccountNumber']) {
                unset($personalInfo[$key]);
                break;
            }
        }
        return $this->render('SpeAppBundle:Template:ATBlueprint/personalInformation/otherretirementaccounts.html.twig', array('personalInfo' => $personalInfo, 'playAudio' => $playAudio));
    }

    /**
     * @Route("/atBlueprint/personalInfo/otherRetirementIncome", name="atBlueprintPersonalInfoOtherRetirementIncome")
     */
    public function atBlueprintPersonalInfoOtherRetirementIncomeAction(Request $request)
    {
        $this->setATLastPath($request);
        $playAudio = $request->request->get('playAudio');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATWsAccounts');
        $profileid = $this->get("session")->get("pid");

        $personalInfo = $repository->findBy(array('profileid' => $profileid, 'isIncomeAccount' => 'true', 'includeInCalcs' => 'true'));

        return $this->render('SpeAppBundle:Template:ATBlueprint/personalInformation/otherretirementincome.html.twig', array('personalInfo' => $personalInfo, 'playAudio' => $playAudio));
    }

    /**
     * @Route("/atBlueprint/personalInfo/addretirementaccount", name="atBlueprintPersonalInfoAddRetirementAccount")
     */
    public function atBlueprintPersonalInfoAddRetirementAccountAction(Request $request)
    {
        $hidden = $this->_checkIfSpouseExists();

        return $this->render('SpeAppBundle:Template:ATBlueprint/personalInformation/addretirementaccounts.html.twig', array('hidden' => $hidden));
    }

    /**
     * @Route("/atBlueprint/personalInfo/viewretirementaccount", name="atBlueprintPersonalInfoViewRetirementAccount")
     */
    public function atBlueprintPersonalInfoViewRetirementAccountAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATWsAccounts');
        $id = $request->query->get('id');
        $readonly = $request->query->get('readonly');
        $personalInfo = $repository->findOneBy(array('id' => $id));

        $hidden = $this->_checkIfSpouseExists();

        $returnArr = array('personalInfo' => $personalInfo, 'edit' => true, 'hidden' => $hidden);

        if ($readonly == 'true') {
            $returnArr['view'] = true;
        }

        return $this->render('SpeAppBundle:Template:ATBlueprint/personalInformation/addretirementaccounts.html.twig', $returnArr);
    }

    /**
     * @Route("/atBlueprint/personalInfo/viewotherretirementincome", name="atBlueprintPersonalInfoViewOtherRetirementIncome")
     */
    public function atBlueprintPersonalInfoViewOtherRetirementIncomeAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATWsAccounts');
        $id = $request->query->get('id');
        $readonly = $request->query->get('readonly');
        $personalInfo = $repository->findOneBy(array('id' => $id));

        $returnArr = array('personalInfo' => $personalInfo, 'edit' => true);

        if ($readonly == 'true') {
            $returnArr['view'] = true;
        }

        return $this->render('SpeAppBundle:Template:ATBlueprint/personalInformation/addotherrequirementincome.html.twig', $returnArr);
    }

    /**
     * @Route("/atBlueprint/personalInfo/addotherrequirementincome", name="atBlueprintPersonalInfoAddOtherRequirementIncome")
     */
    public function atBlueprintPersonalInfoAddOtherRequirementIncomeAction(Request $request)
    {
        return $this->render('SpeAppBundle:Template:ATBlueprint/personalInformation/addotherrequirementincome.html.twig');
    }

    private function setATLastPath($request)
    {
        $uri = $request->server->get('REQUEST_URI');
        $this->get("session")->set("ATLastPath",$uri);
    }

    private function setArchitectLastPath($request)
    {
        $uri = $request->server->get('REQUEST_URI');
        $this->get("session")->set("ArchitectLastPath",$uri);
    }

    private function getWsBlueprint()
    {
        $blueprint = $this->get("session")->get("ACBluePrint");
        if ($blueprint == null) {
            return $this->get("session")->get("ACInitialize");
        }
        return $blueprint;
    }

    public function getOrCreateOutlook()
    {
        $wsBlueprint = $this->getWsBlueprint();
        $blueprint = $wsBlueprint['WsBlueprint'];
        $em = $this->get('doctrine')->getManager();
        $profileid = $this->get("session")->get("pid");
        $userid = $this->get("session")->get("plan")['userid'];
        $planid = $this->get("session")->get("plan")['id'];
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintOutlook');
        $outlook = $repository->findOneBy(array('profileid' => $profileid));


        $addOutlook = false;
        if ($outlook == null)
        {

            $outlook = new ATBlueprintOutlook();
            $outlook->profileid = $profileid;
            $outlook->userid = $userid;
            $outlook->planid = $planid;
            $outlook->dollarsDeferred = $blueprint['DefRateDol'];
            if ($this->get("session")->get("module")['BlueprintRecordExist'] == 'true') {
                $outlook->improvedOutlook = true;
            }
            $acc = $this->getATPlanAccount();
            if ($acc) {
                $outlook->pretaxPercent = $acc->preTaxPct * 100;
                $outlook->rothPercent = $acc->rothPct * 100;
            }
            $addOutlook = true;
        }
        $outlook->currentPerMonth = $blueprint['AtIndexTrendDol'];
        if ($outlook->originalDeferralPercent == null || $outlook->originalDeferralPercent == 0) {
            $outlook->originalDeferralPercent = (int)($blueprint['DefRatePct'] * 100);
        }
        $outlook->deferralPercent = $blueprint['DefRatePct'] * 100.0;
        if ($addOutlook)
        {
            $em->persist($outlook);
        }
        $em->flush();

        return $outlook;

    }


    /**
     * @Route("/atBlueprint/outlook/main", name="atBlueprintOutlookMain")
     */
    public function atBlueprintOutlookMainAction(Request $request)
    {
        $this->setATLastPath($request);
        $wsBlueprint = $this->getWsBlueprint();
        $visitedOutlook = false;
        if ($this->get("session")->get('VisitedOutlook')) {
            $visitedOutlook = true;
        }
        $individual = $wsBlueprint['WsIndividual'];
        $blueprint = $wsBlueprint['WsBlueprint'];

        $personalInfo =  $this->ATBlueprintPersonalInformation();

        $outlook = $this->getOrCreateOutlook();

        return $this->render('SpeAppBundle:Template:ATBlueprint/outlook/mainoutlook.html.twig', array('personalInfo' => $personalInfo, 'outlook' => $outlook,'individual' => $individual, 'blueprint' => $blueprint, 'visitedOutlook' => $visitedOutlook));
    }

    /**
     * @Route("/atBlueprint/outlook/saveoutlook", name="atBlueprintOutlookSaveOutlook")
     */
    public function atBlueprintOutlookSaveOutlookAction(Request $request)
    {
        $blueprint = $this->getWsBlueprint()['WsBlueprint'];
        $em = $this->get('doctrine')->getManager();
        $profileid = $this->get("session")->get("pid");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintOutlook');
        $outlook = $repository->findOneBy(array('profileid' => $profileid));
        $outlook->deferralPercent = $blueprint['DefRatePct'] * 100.0;
        $outlook->currentPerMonth = $blueprint['IncomeMedian'];
        $outlook->dollarsDeferred = $blueprint['DefRateDol'];
        $outlook->improvedOutlook = true;
        $em->persist($outlook);
        $em->flush();

        return new Response('success');
    }

    /**
     * @Route("/atBlueprint/outlook/reminderpopup", name="atBlueprintOutlookShowReminderPopup")
     */
    public function atBlueprintOutlookShowReminderPopupAction(Request $request)
    {
        return $this->render('SpeAppBundle:Template:ATBlueprint/outlook/reminder-popup.html.twig');
    }

    /**
     * @Route("/atBlueprint/outlook/popup", name="atBlueprintOutlookShowPopup")
     */
    public function atBlueprintOutlookShowPopupAction(Request $request)
    {
        $profileid = $this->get("session")->get("pid");
        $blueprint = $this->getWsBlueprint()['WsBlueprint'];
        $data = array();
        $data['currentAge'] = $blueprint['Age'];
        $data['retirementAge'] = $blueprint['RetirementAge'];
        $data['socialSecurityStartingAge'] = $blueprint['SsRetirementAge'];
        $data['monthlySocialSecurityIncome'] = '$' . number_format($blueprint['SsBenefit'], 2, '.', ',');

        $wsAccounts = $this->getCurrentATWsAccountsAsXmlArray($profileid, true);
        $accounts = array();
        $i = 0;
        $totalBalance = 0;
        $totalIncome = 0;
        $totalAnnualAdditions = 0;

        $ATPlan = $this->getATPlanAccount();

        foreach ($wsAccounts as $account) {
            $accounts[$i]['name'] = $account['AccountName'];
            $accounts[$i]['currentBalance'] = $account['Balance'];
            $accounts[$i]['monthlyIncome'] = $account['MonthlyIncome'];
            $accounts[$i]['isIncome'] = $account['IsIncomeAccount'];
            if($account['AccountNumber'] == $ATPlan->accountNumber) {
                $accounts[$i]['projectedAnnualAdditions'] = $blueprint['PlanEeAdditions'] + $blueprint['PlanErAdditions'];
                $totalAnnualAdditions += $blueprint['PlanEeAdditions'] + $blueprint['PlanErAdditions'];
            } else {
                $accounts[$i]['projectedAnnualAdditions'] = $account['EeAdditions'] + $account['ErAdditions'];
                $totalAnnualAdditions += $account['EeAdditions'] + $account['ErAdditions'];
            }
            $totalBalance += $account['Balance'];
            $totalIncome += $account['MonthlyIncome'];
            $i++;
        }

        $projectedProgress['high'] = round($blueprint['AtIndexTrendHigh'] * 100, 1);
        $projectedProgress['low'] = round($blueprint['AtIndexTrendLow'] * 100, 1);

        $retirementAge = array();
        if($blueprint['AtIndexTrendHigh'] > 1) {
            $retirementAge['best'] = $blueprint['RetirementAge'] - $blueprint['AtAgeIndexHigh'];
        } else {
            $retirementAge['best'] = $blueprint['RetirementAge'] + $blueprint['AtAgeIndexHigh'];
        }
        if($blueprint['AtIndexTrendLow'] > 1) {
            $retirementAge['worst'] = $blueprint['RetirementAge'] - $blueprint['AtAgeIndexLow'];
        } else {
            $retirementAge['worst'] = $blueprint['RetirementAge'] + $blueprint['AtAgeIndexLow'];
        }

        return $this->render('SpeAppBundle:Template:ATBlueprint/outlook/popup.html.twig', array('data' => $data, 'accounts' => $accounts, 'totalBalance' => $totalBalance,
            'totalAnnualAdditions' => $totalAnnualAdditions, 'blueprint' => $blueprint, 'projectedProgress' => $projectedProgress, 'retirementAge' => $retirementAge, 'totalIncome' => $totalIncome));
    }

    /**
     * @Route("/atBlueprint/outlook/2", name="atBlueprintOutlook2")
     */
    public function atBlueprintOutlook2Action(Request $request)
    {
        $this->setATLastPath($request);
        return $this->render('SpeAppBundle:Template:ATBlueprint/outlook/monthly.html.twig');
    }

    /**
     * @Route("/atBlueprint/outlook/improve", name="atBlueprintOutlookImprove")
     */
    public function atBlueprintOutlookImproveAction(Request $request)
    {
        $this->setATLastPath($request);
        $this->get("session")->set("VisitedOutlook", 1);
        $blueprint = $this->getWsBlueprint()['WsBlueprint'];
        $profileid = $this->get("session")->get("pid");
        $personalInfo = $this->ATBlueprintPersonalInformation();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintOutlook');
        $outlook = $repository->findOneBy(array('profileid' => $profileid));

        return $this->render('SpeAppBundle:Template:ATBlueprint/outlook/improve.html.twig', array('personalInfo' => $personalInfo, 'outlook' => $outlook,'blueprint' => $blueprint));
    }

    /**
     * @Route("/atBlueprint/outlook/distributor", name="atBlueprintOutlookDistribute")
     */
    public function atBlueprintOutlookDistributeAction(Request $request)
    {
        $profileid = $this->get("session")->get("pid");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintPersonalInformation');
        $personalInfo = $repository->findOneBy(array('profileid' => $profileid));
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintOutlook');
        $outlook = $repository->findOneBy(array('profileid' => $profileid));

        $showAsterisk = false;

        if (is_numeric( $outlook->deferralPercent ) && floor( $outlook->deferralPercent ) != $outlook->deferralPercent) {
            $showAsterisk = true;
        }

        return $this->render('SpeAppBundle:Template:ATBlueprint/outlook/distributor.html.twig', array('personalInfo' => $personalInfo, 'outlook' => $outlook, 'showAsterisk' => $showAsterisk));
    }

    /**
     * @Route("/atBlueprint/outlook/savedistribution", name="atBlueprintOutlookSaveDistribute")
     */
    public function atBlueprintOutlookSaveDistributeAction(Request $request)
    {
        $em = $this->get('doctrine')->getManager();
        $profileid = $this->get("session")->get("pid");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintOutlook');
        $outlook = $repository->findOneBy(array('profileid' => $profileid));

        $pretax = $request->request->get('pretax');
        $roth = $request->request->get('roth');

        $common = $this->get('session')->get('common');

        if ($pretax) {
            $outlook->pretaxPercent = $pretax;
            $pretax = $pretax/100.0;
        } else {
            $outlook->pretaxPercent = '0.00';
            $pretax = '0.00';
        }
        if ($roth) {
            $outlook->rothPercent = $roth;
            $roth = $roth/100.0;
        } else {
            $outlook->rothPercent = '0.00';
            $roth = '0.00';
        }

        $ATPlan = $this->getATPlanAccount();

        if ($ATPlan) {
            $ATPlan->preTaxPct = $pretax;
            $ATPlan->rothPct = $roth;
            $ATPlan->defRateMethod = 'Percent';
        }

        $outlook->complete = 1;
        $em->flush();

        $this->setModule('ATPassedBlueprint', true);

        $c = array(
            'contribution_mode' => 'DEFERPCT',
            'C_PreTaxContributionPct' => null,
            'C_PreTaxContributionValue' => null,
            'C_RothContributionPct' => null,
            'C_RothContributionValue' => null,
            'C_PostTaxContributionPct' => null,
            'C_PostTaxContributionValue' => null,
            'updateContributions' => 1,
        );
        if ($outlook->pretaxPercent != null)
        {
            $c['C_PreTaxContributionPct'] = $outlook->pretaxPercent;
        }
        if ($outlook->rothPercent != null && $common['roth'])
        {
            $c['C_RothContributionPct'] = $outlook->rothPercent;
        }
        $this->get("session")->set('contributions', $c);

        return new Response('saved');
    }

    public function getATPlanAccount()
    {
        $profileid = $this->get("session")->get("pid");
        $blueprint = $this->getWsBlueprint()['WsBlueprint'];
        $accountNumber = $blueprint['AccountNumber'];

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATWsAccounts');
        $wsAccounts = $repository->findBy(array('profileid' => $profileid));

        foreach ($wsAccounts as $account) {
            if ($account->accountNumber != $accountNumber) {
                continue;
            } else {
                return $account;
            }
        }

        return null;
    }

    /**
     * @Route("/atBlueprint/outlook/4", name="atBlueprintOutlook4")
     */
    public function atBlueprintOutlook4Action(Request $request)
    {

        return $this->render('SpeAppBundle:Template:ATBlueprint/outlook/submit.html.twig');
    }

    /**
     * @Route("/atBlueprint/outlook/5", name="atBlueprintOutlook5")
     */
    public function atBlueprintOutlook5Action(Request $request)
    {

        return $this->render('SpeAppBundle:Template:ATBlueprint/outlook/contributionselector.html.twig');
    }

    /**
     * @Route("/investments/atarchitect/1", name="atArchitect1")
     */
    public function atArchitect1Action(Request $request)
    {
        $this->setArchitectLastPath($request);
        return $this->render('SpeAppBundle:Template:investments/atarchitect/enablepage.html.twig',array("architect" =>$this->getArchitect(),'optInMessage' => $this->get("spe.app.rkp")->ACInitialize()['WsPlan']['ArchitectOptInMessage']));
    }
    /**
     * @Route("/investments/atarchitect/1/saved", name="atArchitect1Saved")
     */
    public function atArchitect1SavedAction(Request $request)
    {
        $this->updateArchitect(array("takeAdvantage" => $request->request->get("enablepageAnswer")));
        if ($request->request->get("enablepageAnswer") == 0)
        {
            $this->setModule('ATArchitect', false);
            $this->updateArchitect(array("acceptedInvestment" => 0));
        }
        return new Response("");
    }
    /**
     * @Route("/investments/atarchitect/1/enable", name="atArchitect1Enable")
     */
    public function atArchitect1EnableAction(Request $request)
    {
        $this->setModule('ATArchitect', true);
        if ($this->get('session')->get('ATArchitectPreviouslyEnrolled')) {
            $this->updateArchitect(array("acceptedInvestment" => 1));
            $this->setModule('ArchitectIsEnrolled', true);
        }

        return new Response("");
    }

    public function setModule($name, $bool)
    {
        $module = $this->get("session")->get("module");
        $module[$name] = $bool;
        $this->get("session")->set("module",$module);

    }
    public function getArchitect()
    {
        $em = $this->get('doctrine')->getManager();
        $repository = $em->getRepository('classesclassBundle:ATArchitect');
        $ATArchitect = $repository->findOneBy(array("profileid" => $this->get("session")->get("pid")));
        $initialize = $this->get("session")->get("ACInitialize");
        $architectEnrolled = $initialize['WsIndividual']['ArchitectIsEnrolled'];
        if ($ATArchitect == null)
        {
            $ATArchitect = new ATArchitect();
            $ATArchitect->profileid = $this->get("session")->get("pid");
            $ATArchitect->userid = $this->get("session")->get("plan")['userid'];
            $ATArchitect->planid = $this->get("session")->get("plan")['id'];
            if ($architectEnrolled == 'true') {
                $ATArchitect->acceptedInvestment = 1;
                $ATArchitect->currentInvestment = $initialize['WsArchitectTask']['CurrentBalanceToFundId'];
                $ATArchitect->currentInvestmentPercent = $initialize['WsArchitectTask']['CurrentBalancePct'];
                $ATArchitect->suggestedInvestment = $initialize['WsArchitectTask']['NewMoneyToFundId'];
                $ATArchitect->suggestedInvestmentPercent = $initialize['WsArchitectTask']['NewMoneyPct'];
            }

            $em->persist($ATArchitect);
            $em->flush();
        }
        return $ATArchitect;
    }
    public function updateArchitect($array)
    {
        $em = $this->get('doctrine')->getManager();
        $ATArchitect = $this->getArchitect();
        foreach ($array as $key => $value)
        {
            $ATArchitect->$key = $value;
        }
        if ($ATArchitect->id == null)
        {
            $em->persist($ATArchitect);
        }
        $em->flush();
    }
    /**
     * @Route("/investments/atarchitect/2", name="atArchitect2")
     */
    public function atArchitect2Action(Request $request)
    {
        return $this->render('SpeAppBundle:Template:investments/atarchitect/disclaimer.html.twig',array("architect" =>$this->getArchitect()));
    }
    /**
     * @Route("/investments/atarchitect/2/saved", name="atArchitect2Saved")
     */
    public function atArchitect2SavedAction(Request $request)
    {
        $this->updateArchitect(array("agreement" => 1));
        $this->get("session")->set('investments', $this->transactInvestmentsAT());
        return new Response("");
    }
    public function getArchitectInvestments()
    {
        $data = $this->getWsBlueprint()['WsArchitectTask'];
        return
        [
            "currentInvestment" =>
            [
                "name" => $data['CurrentBalanceToFundId'],
                "percent" => $data['CurrentBalancePct'],
                "description" => $data['CurrentBalanceDescription']
            ],
            "suggestedInvestment" =>
            [
                "name" => $data['NewMoneyToFundId'],
                "percent" => $data['NewMoneyPct'],
                "description" => $data['NewMoneyDescription']
            ]
        ];
    }
    /**
     * @Route("/investments/atarchitect/3", name="atArchitect3")
     */
    public function atArchitect3Action(Request $request)
    {
        $this->setArchitectLastPath($request);
        return $this->render('SpeAppBundle:Template:investments/atarchitect/investmentselector.html.twig',array("investments"=> $this->getArchitectInvestments()));
    }
    /**
     * @Route("/investments/atarchitect/3/saved", name="atArchitect3Saved")
     */
    public function atArchitect3SavedAction(Request $request)
    {
        $session = $this->get("session");
        $investments = $this->getArchitectInvestments();
        $params =
        [
            "acceptedInvestment" => 1,
            "currentInvestment" => $investments['currentInvestment']['name'],
            "currentInvestmentPercent" => $investments['currentInvestment']['percent'],
            "suggestedInvestment" => $investments['suggestedInvestment']['name'],
            "suggestedInvestmentPercent" => $investments['suggestedInvestment']['percent'],
        ];
        $this->updateArchitect($params);
        $common = $session->get('common');
        $common['updateElections'] = true;
        $common['updateRealignment'] = true;
        $session->set('common', $common);
        $this->setModule('ArchitectIsEnrolled', true);
        return new Response("");
    }
    /**
     * @Route("/investments/atarchitect/4", name="atArchitect4")
     */
    public function atArchitect4Action(Request $request)
    {
        $this->setArchitectLastPath($request);
        $this->get("session")->set('investments', $this->transactInvestmentsAT());
        return $this->render('SpeAppBundle:Template:investments/atarchitect/perfreport.html.twig', array("chart" => $this->atArchitectChart("All")));
    }
    /**
     * @Route("/investments/atarchitect/4/data", name="atArchitect4Data")
     */
    public function atArchitect4DataAction(Request $request)
    {
        $time = $request->request->get("time");
        $timeTranslated = '';
        switch ($time) {
            case 'All': $timeTranslated = 'ITD';
                        break;
            case '1':   $timeTranslated = 'OneYear';
                        break;
            case '3':   $timeTranslated = 'ThreeYear';
                        break;
            case 'YTD': $timeTranslated = 'YTD';
                        break;
        }
        $returnDataAT = $this->get("session")->get("ACInitialize")['WsArchitectSummary']['ATReturn'.$timeTranslated] * 100;
        $returnDataQdia = $this->get("session")->get("ACInitialize")['WsArchitectSummary']['QdiaReturn'.$timeTranslated] * 100;
        return $this->render('SpeAppBundle:Template:investments/atarchitect/chart.html.twig', array("chart" => $this->atArchitectChart($time), 'ATReturn' => $returnDataAT, 'QdiaReturn' => $returnDataQdia));
    }
    /**
     * @Route("/investments/atarchitect/4/saved", name="atArchitect4Saved")
     */
    public function atArchitect4Saved(Request $request)
    {
        return new Response("");
    }
    public function atArchitectChart($time)
    {
        $generalMethods = new GeneralMethods($this);
        $graphData = $this->get("session")->get("ACInitialize")['WsArchitectSummaryReturns']['WsArchitectSummaryReturn'];
        if ($generalMethods->isAssocArray($graphData)) { // kicks off if there's only one WsArchitectSummaryReturn in $graphData
            $temp[0] = array();
            foreach ($graphData as $key => $value) {
                $temp[0][$key] = $value;
            }
            $graphData = $temp;
        }
        $chart = array();
        foreach ($graphData as $data)
        {
            $dt = \DateTime::createFromFormat('!m', $data["ReturnMonth"]);
            $month = strtoupper($dt->format('M'));
            $date = $month." ".$data["ReturnYear"];
            $yearCompare = $data["ReturnYear"] + ($time-1);
            $yearcondition = date("Y") <= $yearCompare++;
            if (!$yearcondition && (date("Y") <= $yearCompare))
            {
                $yearcondition = $data["ReturnMonth"] >= date("n");
            }
            if ((date("Y") == $data["ReturnYear"] && $time == "YTD") || $time == "All" || $yearcondition )
            {
                $this->addArchitectChartData($chart,$date,number_format($data['ArchitectMonthReturn'] * 100,2),number_format($data['AgeMonthReturn'] * 100,2));
            }
        }
        return $chart;
    }

    public function addArchitectChartData(&$chart,$label,$ATArchitectScore,$ageScore)
    {
        $chart[] = array("label" => $label,"ATArchitectScore" => $ATArchitectScore, "ageScore" => $ageScore);
    }

    /**
     * @Route("/investments/atarchitect/5", name="atArchitect5")
     */
    public function atArchitect5Action(Request $request)
    {

        return $this->render('SpeAppBundle:Template:investments/atarchitect/optout.html.twig');
    }

    /**
     * @Route("/investments/atarchitect/5/saved", name="atArchitect5Saved")
     */
    public function atArchitect5SavedAction(Request $request)
    {
        $this->updateArchitect(array("acceptedInvestment" => 0));
        $this->setModule('ATArchitect', false);
        $this->setModule('ArchitectIsEnrolled', false);
        $this->get('session')->set('ATArchitectPreviouslyEnrolled', true);
        return new Response("");
    }


    /**
     * @Route("/atBlueprint/main", name="atBlueprintMain")
     */
    public function atBlueprintMainAction(Request $request, $path = "ATLastPath")
    {
        if ($path === 'ATLastPath') {
            if ($this->get("session")->get("ATLastPath") == null) {
                $this->get("session")->set("ATLastPath", "/atBlueprint/personalInfo/required");
            }
        } else {
            if ($this->get("session")->get("ArchitectLastPath") == null) {
                $this->get("session")->set("ArchitectLastPath", "/investments/atarchitect/1");
            }
        }
        return $this->render('SpeAppBundle:Template:ATBlueprint/main.html.twig',["path" => $this->get("session")->get($path)]);
    }
    /**
     * @Route("/loadingScreen", name="loadingScreen")
     */
    public function loadingScreenAction(Request $request)
    {
        $common = $this->get('session')->get('common');
        $profileid = $this->get("session")->get('pid');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintPersonalInformation');
        $personalInformation = $repository->findOneBy(array('profileid' => $profileid));
        $common['ATemail'] = $personalInformation->email;
        $this->get('session')->set('common', $common);
        return $this->render('SpeAppBundle:Template:ATBlueprint/at-loading-screen.html.twig',array("loadingTitle" => $request->request->get("loadingTitle")));
    }

    public function updateWsIndividual(&$transData, $personalInformation)
    {
        $array = ['annualCompensation', 'email', 'mobilePhone', 'spouseAnnualCompensation', 'spouseFirstName', 'spouseLastName'];
        foreach ($array as $row) {
            $transData['WsIndividual'][ucfirst($row)] = $personalInformation->$row;
        }
        $transData['WsIndividual']['SpouseIncludeInBlueprint'] = $personalInformation->includeSpousalInformation;

        if(!$this->isNull($personalInformation->spouseDateOfBirth)) {
            $time = strtotime($personalInformation->spouseDateOfBirth);
            $newformat = date('Y-m-d', $time);
            $newformat .= 'T00:00:00';
            $transData['WsIndividual']['SpouseBirthDate'] = $newformat;
        }
    }

    /**
     * @Route("/loadingScreenTransacts", name="loadingScreenTransacts")
     */
    public function loadingScreenTransactsAction(Request $request)
    {
        $profileid = $this->get("session")->get('pid');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintPersonalInformation');
        $personalInformation = $repository->findOneBy(array('profileid' => $profileid));
        $transData = $this->getWsBlueprint();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintOutlook');
        $outlook = $repository->findOneBy(array('profileid' => $profileid));
        if ($outlook != null) {
            $defString = $outlook->deferralPercent/100.0;
            $transData['WsBlueprint']['NewDefRatePct'] = "$defString";
        }

        $this->updateWsIndividual($transData, $personalInformation);
        $transData['WsAccounts'] = $this->getCurrentATWsAccountsAsXmlArray($profileid);

        $transData['WsIndividual']['OverrideAnnualCompensation'] = 'true';
        $transData['WsIndividual']['OverrideReplacementIncome'] = 'false';
        $transData['WsIndividual']['OverrideRetirementAge'] = 'false';
        $transData['WsIndividual']['OverrideYearsInRetirement'] = 'false';
        $transData['WsIndividual']['OverrideSsRetirementAge'] = 'false';
        $transData['WsIndividual']['OverrideSsBenefit'] = 'false';
        $transData['WsIndividual']['OverrideSsSpousalBenefit'] = 'false';

        $transData['WsBlueprint']['OtherMonthlyIncome'] = '0'; // prevent compounding monthly income

        if (!$this->isNull($personalInformation->monthlyRetirementGoal)) {
            $transData['WsIndividual']['OverrideReplacementIncome'] = 'true';
            $transData['WsIndividual']['ReplacementIncomeNeeded'] = $personalInformation->monthlyRetirementGoal;
        }
        if (!$this->isNull($personalInformation->retirementAge)) {
            $transData['WsIndividual']['OverrideRetirementAge'] = 'true';
            $transData['WsIndividual']['RetirementAge'] = $personalInformation->retirementAge;
        }
        if (!$this->isNull($personalInformation->yearsInRetirement)) {
            $transData['WsIndividual']['OverrideYearsInRetirement'] = 'true';
            $transData['WsIndividual']['YearsInRetirement'] = $personalInformation->yearsInRetirement;
        }
        if (!$this->isNull($personalInformation->socialSecurityBeginAge)) {
            $transData['WsIndividual']['OverrideSsRetirementAge'] = 'true';
            $transData['WsIndividual']['SsRetirementAge'] = $personalInformation->socialSecurityBeginAge;
        }
        if (!$this->isNull($personalInformation->socialSecurityMonthlyAmount)) {
            $transData['WsIndividual']['OverrideSsBenefit'] = 'true';
            $transData['WsIndividual']['SsBenefit'] = $personalInformation->socialSecurityMonthlyAmount;
        }
        if (!$this->isNull($personalInformation->spouseSocialSecurityMonthlyAmount)) {
            $transData['WsIndividual']['OverrideSsSpousalBenefit'] = 'true';
            $transData['WsIndividual']['SsSpousalBenefit'] = $personalInformation->spouseSocialSecurityMonthlyAmount;
        }

        $this->get("session")->set("ACBluePrint",$this->get("spe.app.rkp")->ACBluePrint($transData));
        return new Response('hi');
    }

    /**
     * @Route("/transactDeferral", name="transactDeferral")
     */
    public function transactDeferralAction(Request $request)
    {
        $profileid = $this->get("session")->get('pid');
        $deferral = $request->request->get('percentage')/100.0;
        $transData = $this->getWsBlueprint();
        $transData['WsAccounts'] = $this->getCurrentATWsAccountsAsXmlArray($profileid);
        $transData['WsBlueprint']['NewDefRatePct'] = "$deferral";

        $transData['WsBlueprint']['OtherMonthlyIncome'] = '0'; // prevent compounding monthly income

        $this->get("session")->set("ACBluePrint",$this->get("spe.app.rkp")->ACBluePrint($transData));
        return new Response('hi');
    }

    function isNull($tocheck)
    {
        if ($tocheck == null || $tocheck === '') {
            return true;
        }
        return false;
    }

    function getCurrentATWsAccountsAsXmlArray($profileid, $sorted = false)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATWsAccounts');
        if ($sorted) {
            $ATWsAccounts = $repository->findBy(array('profileid' => $profileid), array('isIncomeAccount' => "ASC"));
        } else {
            $ATWsAccounts = $repository->findBy(array('profileid' => $profileid));
        }
        $ATWsAccountsArray = array();
        foreach($ATWsAccounts as $account)
        {
            $ATWsAccountsArray[]  = $account->getXmlArray();
        }
        return $ATWsAccountsArray;
    }
    public function ATBlueprintPersonalInformationFirstNameAction()
    {
        $profileid = $this->get("session")->get("pid");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintPersonalInformation');

        $personalInfo = $repository->findOneBy(array('profileid' => $profileid));
        if ($personalInfo != null)
        {
            return new Response($personalInfo->firstName);
        }
        else
        {
            return new Response($this->get("session")->get("rkp")["plan_data"]->firstName);
        }
    }
    private function calculateInvestmentsGraphTable()
    {
        $graphValues = $this->get("session")->get("PlansInvestmentsConfigurationFundsGroupValuesColorsHash")[$this->get("session")->get("PlansInvestmentsGraphSelectedMinScore")];
        $existingScores = $this->get("session")->get("graphScores");
        $table = array();
        $FundsGroupsValuesHash = $this->get("session")->get("FundsGroupsValuesHash");
        $plansFundsHash = array();
        foreach ($this->get("session")->get("plansFunds") as $fund)
        {
            $fund['percent'] = "";
            $plansFundsHash[$fund['groupValueId']][] = $fund;
        }        
        foreach ($graphValues as $value)
        {
            $FundsGroupsValue = $FundsGroupsValuesHash[$value['fundGroupValueId']];
            $row['header'] = array("description" => $FundsGroupsValue['value'],"id" =>$FundsGroupsValue['id']); 
            $row['funds'] = $plansFundsHash[$value['fundGroupValueId']];
            $row['color'] = $value;
            $row['FundsGroupsValue'] = $FundsGroupsValue;
            $row['total'] = 0;
            foreach ($existingScores['groups'] as $group)
            {
                if ($value['fundGroupValueId'] == $group['id'])
                {
                    $groupFunds = $group['funds'];
                    $groupFundsCounter = 0;
                    foreach ($row['funds'] as &$fund)
                    {
                        $fund['percent'] = $groupFunds[$groupFundsCounter++]['percent'];
                    }
                    $row['total'] = $group['total'];
                }                 
            }
            $table[] = $row;
        }
        $data = array("graphValues"  => $graphValues,"table" => $table);
        $this->get("session")->set("investmentsGraphData",$data);
        return $data;
    }

    public function transactInvestmentsAT()
    {
        $blueprint = $this->getWsBlueprint();

        $investment = array();
        $investment['I_SelectedInvestmentOption'] = "ATCUSTOM";
        $investment['I_SelectedInvestmentDetails'] = $blueprint['WsArchitectTask']['NewMoneyToFundId'] . "^" . $blueprint['WsArchitectTask']['NewMoneyPct'];
        $investment['I_RealignSelectedInvestmentDetails'] = $blueprint['WsArchitectTask']['CurrentBalanceToFundId'] . "^" . $blueprint['WsArchitectTask']['CurrentBalancePct'];
        return $investment;
    }

    /**
     * @Route("/commonData", name="commonData")
     */
    public function commonDataAction(Request $request)
    {
        $attr = $request->request->get('attr');
        return new JsonResponse($this->get("session")->get($attr));
    }

    /**
     * @Route("/getXmlRaw")
     */
    public function getXmlRawAction(Request $request) {
        $session = $this->get("session");
        $xmlUid = isset($session->get("rkp")["xmlUid"]) ? $session->get("rkp")["xmlUid"] : null;
		$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery("SELECT w.ip FROM classesclassBundle:WhitelistedIps w");
		$whitelistedIps = array_column($query->getScalarResult(), 'ip');

        if ($xmlUid && in_array($request->getClientIp(), $whitelistedIps)) {
            $sql = "
                SELECT
                    *
                FROM
                    xmlRaw
                WHERE
                    xmlUid = :xmlUid
                ORDER BY 
                    timestamp ASC
            ";
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute(array(':xmlUid' => $xmlUid));
            $xmlRawData = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        else {
            $xmlRawData = array();
        }
        $response = new  Response(json_encode($xmlRawData));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

	/**
	 * @Route("/saveRolloverOption")
	 */
	public function saveRolloverOption(Request $request) {

		$session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
		$profileId = $session->get('profileId');
		$profiles = $em->getRepository('classesclassBundle:profiles')->findOneBy(array('profileId' => $profileId));

		if ($request->request->has("emailMe")) {
			$profiles->emailMe = $request->request->get("emailMe");
			$profiles->callMe = null;
			$profiles->callUs = null;
		}
		else if ($request->request->has("callMe")) {
			$profiles->callMe = $request->request->get("callMe");
			$profiles->emailMe = null;
			$profiles->callUs = null;
		}
		else {
			$profiles->callUs = 1;
			$profiles->emailMe = null;
			$profiles->callMe = null;
		}
		$em->flush();
        return $this->toJSON(array('status' => 1));
	}

	/**
	 * @Route("/incrementContactViewCount")
	 */
	public function incrementContactViewCount(Request $request) {
		$session = $request->getSession();
		$em = $this->getDoctrine()->getManager();
		$profileId = $session->get('profileId');
		$profiles = $em->getRepository('classesclassBundle:profiles')->findOneBy(array('profileId' => $profileId));
		$profiles->enrollmentContactViewedTimes++;
		$em->flush();
		return $this->toJSON(array('status' => 1));
	}
    /**
     * @Route("/contributions/AutoIncrease", name="contributionsAutoIncrease")
     */
    public function contributionsAutoIncreaseAction(Request $request)
    {
        $autoIncreaseSave = $this->get("session")->get("autoIncreaseSave");
        if ($autoIncreaseSave == null)
        {
            $autoIncreaseSave = array();
            $autoIncreaseSave['mode'] = "";
            $autoIncreaseSave['deferrals'] = [];
        }
        $savedDeferrals = $autoIncreaseSave['deferrals'];
        $autoIncrease = $this->get("session")->get("autoIncrease");
        $savedDeferralsHash = array();
        foreach ($savedDeferrals as $deferral)
        {
            $savedDeferralsHash[$deferral['source']] = $deferral;
        }
        foreach ($autoIncrease['data'] as $key => &$data)
        {
            if (isset($savedDeferralsHash[$data['source']]))
            {
                $deferralEndingKey = "pct";
                if ($autoIncreaseSave['mode'] == "dollar")
                {
                    $deferralEndingKey = "amount";
                }
                $data['autoincrease'.$deferralEndingKey] = (float)$savedDeferralsHash[$data['source']]['value'];
                $data[$autoIncreaseSave['mode']] = true;
                $data['autoincreasestart'] = $savedDeferralsHash[$data['source']]['date'];
            }
        }
        $this->get("session")->set("autoIncreaseProcessed",$autoIncrease);
        $params['autoIncrease'] =$autoIncrease;
        $params['autoIncreaseSave'] = $autoIncreaseSave;
        $params['common'] = $this->get("session")->get("common");
        $em = $this->getDoctrine()->getManager();
        $databaseContributions = $em->getRepository('classesclassBundle:profilesContributions')->findOneBy(array("profileid" => $this->get("session")->get("pid") ));
        $sessionContributions = $this->get("session")->get("contributions");
        $contributions['database'] = $databaseContributions;
        $contributions['session'] = $sessionContributions;
        $params['contributions'] = $contributions;
        return $this->render('SpeAppBundle:Template:contributions/autoincrease/index.html.twig',$params);
    }
    /**
     * @Route("/contributions/AutoIncreaseSaved", name="contributionsAutoIncreaseSaved")
     */
    public function contributionsAutoIncreaseSavedAction(Request $request)
    {
        $autoIncrease = $request->request->get("autoIncrease");
        foreach($autoIncrease['deferrals'] as &$deferral)
        {
            foreach ($this->get("session")->get("autoIncreaseProcessed")['data'] as $deferral2)
            {
                if ($deferral['source'] == $deferral2['source'])
                {
                    $deferral['rkp'] = $deferral2;
                }
            }
        }
        $this->get("session")->set("autoIncreaseSave",$autoIncrease);
        $this->updateProfile("autoIncrease");
        return new Response("");
    }
    /**
     * @Route("/addProfileTransaction", name="addProfileTransaction")
     */
    public function addProfileTransactionAction(Request $request)
    {
        $this->get("ParticipantsTransactionsService")->add($request->request->all());
        return new Response("saved");
    }
    
    /**
	 * @Route("/saveTrustedContact")
	 */
	public function saveTrustedContactAction(Request $request) {
		
		$session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
		$profileId = $session->get('profileId');
		$profiles = $em->getRepository('classesclassBundle:profiles')->findOneBy(array('profileId' => $profileId));
        $profiles->trustedContactStatus = $request->get("trustedContactOptIn") ? 1 : 0;
        $session->set("trustedContactOptIn", $request->get("trustedContactOptIn"));
        $response = $this->get('spe.app.rkp')->postTrustedContact();
        $data = json_decode($response, true);
        if (!$data['success'])
        {
            $profiles->trustedContactStatus = 2;
        }
        $em->flush();
        return $this->toJSON($data);
	}
}
