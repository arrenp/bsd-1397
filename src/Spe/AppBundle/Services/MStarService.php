<?php
namespace Spe\AppBundle\Services;

use classes\classBundle\Entity\participants;
use classes\classBundle\Entity\profiles;
use classes\classBundle\Entity\profilesInvestments;
use classes\classBundle\Entity\profilesRiskProfile;
use Doctrine\ORM\EntityManager;
use Spe\AppBundle\Templating\Helper\Functions;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
class MStarService
{
    public function __construct(EntityManager $entityManager, Session $session, Functions $functions){
        $this->em = $entityManager;
        $this->session = $session;
        $this->functions = $functions;
        $this->testing = $session->get('testing') ? 1 : 0;
    }

    public function getParticipantData(){
        $mstarData = $this->session->get('morningstar');
        $mstarData['participant'] = array(
            'FirstName' => 'Tanuj',
            'LastName' => 'Dave',
            'Gender' => 'Male',
            'Email' => 'tanuj.dave@gmail.com',
            'MinimumContriRateForFullMatch' => 1,
            'RMSessionID' => uniqid(),
            'RetireAge' => 65,
            'State' => 'AL',
            'ContributionLimits' => 1000,
            'Balance' => 5000,
            'AnnualSalary' => 18000,
            'PostTaxSavingRate' => 2,
            'PreTaxSavingRate' => 3,
            'RothTaxSavingRate' => 4,
            'CatchUpContribution' => array(
                'LTContri' => array('EnableContri' => true, 'ContriDollar' => 1000, 'Length' => 43),
                'RothLTContri' => array('EnableContri' => true, 'ContriDollar' => 1000, 'Length' => 32),
                'RothY3Contri' => array('EnableContri' => true, 'ContriDollar' => 1000, 'Length' => 2),
                'RothY50Contri' => array('EnableContri' => true, 'ContriDollar' => 1000, 'Length' => 23),
                'Y3Contri' => array('EnableContri' => true, 'ContriDollar' => 1000, 'Length' => 2),
                'Y50Contri' => array('EnableContri' => true, 'ContriDollar' => 1000, 'Length' => 2),
            ),
            'agreementURL' => ''
        );
        $this->session->set('morningstar', $mstarData);
        return $mstarData['participant'];
    }

    public function getStrategyData(){
        $mstarData = $this->session->get('morningstar');
        $mstarData['strategy'] = array(
            'AdvisoryAgreementText' => 'This is agreement text',
            'AnnualGoalAmount' => 10000,
            'ExpectedAnnualIncome' => 500000,
            'ProposedAssetMix' => array('test1' => 3, 'test2' => 4, 'test3' => 34, 'test4' => 6, 'test5' => 53, 'test6' => 23),
            'SavingsRate' => 30,
            'RiskLevel' => 2,
            'RiskIndex' => 3,
            'RiskDesc' => 'This is test description'
        );
        $this->session->set('updateStrategy', 1);
        $this->session->set('morningstar', $mstarData);
        return $mstarData['strategy'];
    }

    public function addMStarProfileInfo($profile){
        $em = $this->em;
        $mstar = $this->session->get('morningstar');

        if($profile){
            $participant = $profile->getParticipant();

            $participant->setFirstName(isset($mstar['FirstName']) ? $mstar['FirstName'] : null);
            $participant->setLastName(isset($mstar['LastName']) ? $mstar['LastName'] : null);
            $participant->setState(isset($mstar['State']) ? $mstar['State'] : null);
            $participant->setEmail(isset($mstar['Email']) ? $mstar['Email'] : null);
            $em->persist($participant);

            $profile->setCurrentBalance(isset($mstar['Balance']) ? $mstar['Balance'] : null);
            $profile->setAnnualSalary(isset($mstar['AnnualSalary']) ? $mstar['AnnualSalary'] : null);
            $profile->setRetireAge(isset($mstar['RetireAge']) ? $mstar['RetireAge'] : null);
            $profile->setPreTaxSavingRate(isset($mstar['PreTaxSavingRate']) ? $mstar['PreTaxSavingRate'] : null);
            $profile->setPostTaxSavingRate(isset($mstar['PostTaxSavingRate']) ? $mstar['PostTaxSavingRate'] : null);
            $profile->setRothTaxSavingRate(isset($mstar['RothTaxSavingRate']) ? $mstar['RothTaxSavingRate'] : null);
            $profile->setMStarContribution(isset($mstar['CatchUpContribution']) ? json_encode($mstar['CatchUpContribution']) : null);
            $em->persist($profile);
            $em->flush();
        }
    }

    public function addMStarInvestments($profile){
        $em = $this->em;
        $proposedAssetMix = $this->session->get('morningstar')['strategy']['ProposedAssetMix'];

        if($profile && $proposedAssetMix){
            $profilesInvestments = $em->getRepository('classesclassBundle:profilesInvestments')->findBy(array('profileid' => $profile->getId()));
            if($profilesInvestments){
                foreach($profilesInvestments as $row){
                    $em->remove($row);
                }
            }
            foreach($proposedAssetMix as $fname => $percent){
                $profilesInvestments = new profilesInvestments();
                $profilesInvestments->setUserid($this->session->get('plan')['userid']);
                $profilesInvestments->setPlanid($this->session->get('plan')['id']);
                $profilesInvestments->setProfile($profile);
                $profilesInvestments->setFname(trim($fname));
                $profilesInvestments->setPercent(trim($percent));
                $em->persist($profilesInvestments);
            }
            $em->flush();
        }
    }

    private function addMStarRiskProfile($profile){
        $session = $this->session;
        $em = $this->em;

        if($profile && isset($session->get('morningstar')['strategy']['RiskLevel'])){

            $profilesRiskProfile = $em->getRepository('classesclassBundle:profilesRiskProfile')->findOneBy(array('profileid' => $profile->getId()));
            if(!$profilesRiskProfile){
                $profilesRiskProfile = new profilesRiskProfile();
            }

            $profilesRiskProfile->setUserid($session->get('plan')['userid']);
            $profilesRiskProfile->setPlanid($session->get('plan')['id']);
            $profilesRiskProfile->setProfile($profile);
            $profilesRiskProfile->setPosition(isset($session->get('morningstar')['strategy']['RiskIndex']) ? $session->get('morningstar')['strategy']['RiskIndex'] : null);
            $profilesRiskProfile->setName(isset($session->get('morningstar')['strategy']['RiskLevel']) ? $session->get('morningstar')['strategy']['RiskLevel'] : null);
            $profilesRiskProfile->setDescription(isset($session->get('morningstar')['strategy']['RiskDesc']) ? $session->get('morningstar')['strategy']['RiskDesc'] : null);
            $em->persist($profilesRiskProfile);
            $em->flush();
        }
    }

    public function updateStrategy(){
        $session = $this->session;
        $session->set('mStarStatus', json_encode(array('success' => 1, 'message' => 'Testing Success Message')));
        return array('success' => 1, 'message' => 'Testing Success Message');
    }

    public function updateProfile(){
        $session = $this->session;
        $em = $this->em;
        $profileError = $isError = false;
        $status = array(
            'profileStatus',
            'mStarStatus'
        );

        $profileId = $session->get('profileId') ?  $session->get('profileId') : uniqid('SPE_');
        $data = array(
            'clientCalling' => $session->get('client_calling'),
            'userId' => isset($session->get('plan')['userid']) ? $session->get('plan')['userid'] : null,
            'planId' => isset($session->get('rkp')['plan_id']) ? $session->get('rkp')['plan_id'] : null,
            'planName' => isset($session->get('rkp')['plan_data']->planName) ? $session->get('rkp')['plan_data']->planName : null,
            'firstName' => isset($session->get('morningstar')['participant']['FirstName']) ? $session->get('morningstar')['participant']['FirstName'] : null,
            'lastName' => isset($session->get('morningstar')['participant']['LastName']) ? $session->get('morningstar')['participant']['LastName'] : null,
            'balance' => isset($session->get('morningstar')['participant']['Balance']) ? $session->get('morningstar')['participant']['Balance'] : null,
            'address' => isset($session->get('common')['address']) ? $session->get('common')['address'] : null,
            'city' => isset($session->get('common')['city']) ? $session->get('common')['city'] : null,
            'state' => isset($session->get('morningstar')['participant']['State']) ? $session->get('morningstar')['participant']['State'] : null,
            'email' => isset($session->get('morningstar')['participant']['Email']) ? $session->get('morningstar')['participant']['Email'] : null,
            'availability' => isset($session->get('common')['availability']) ? $session->get('common')['availability'] : null,
            'phone' => isset($session->get('common')['phone']) ? $session->get('common')['phone'] : null,
            'annualSalary' => isset($session->get('morningstar')['strategy']['ExpectedAnnualIncome']) ? $session->get('morningstar')['strategy']['ExpectedAnnualIncome'] : null,
            'retireAge' => isset($session->get('morningstar')['participant']['RetireAge']) ? $session->get('morningstar')['participant']['RetireAge'] : null,
            'preTaxSavingRate' => isset($session->get('morningstar')['participant']['PreTaxSavingRate']) ? $session->get('morningstar')['participant']['PreTaxSavingRate'] : null,
            'postTaxSavingRate' => isset($session->get('morningstar')['participant']['PostTaxSavingRate']) ? $session->get('morningstar')['participant']['PostTaxSavingRate'] : null,
            'rothTaxSavingRate' => isset($session->get('morningstar')['participant']['RothTaxSavingRate']) ? $session->get('morningstar')['participant']['RothTaxSavingRate'] : null,
            'catchUpContribution' => isset($session->get('morningstar')['participant']['CatchUpContribution']) ? json_encode(array_filter($session->get('morningstar')['participant']['CatchUpContribution'])) : null,
            'riskLevel' => isset($session->get('morningstar')['strategy']['RiskLevel']) ? $session->get('morningstar')['strategy']['RiskLevel'] : null,
            'riskIndex' => isset($session->get('morningstar')['strategy']['RiskIndex']) ? $session->get('morningstar')['strategy']['RiskIndex'] : null,
            'riskDesc' => isset($session->get('morningstar')['strategy']['RiskDesc']) ? $session->get('morningstar')['strategy']['RiskDesc'] : null,
            'proposedAssetMix' => isset($session->get('morningstar')['strategy']['ProposedAssetMix']) ? $session->get('morningstar')['strategy']['ProposedAssetMix'] : null,
            'mStarStatus' => $session->get('mStarStatus') ? $session->get('mStarStatus') : null,
            'reportDate' => new \DateTime(),
            'profileStatus' => 1
        );

        try{
            if($session->get('profileId') && $profileId){
                $profiles = $em->getRepository('classesclassBundle:profiles')->findOneBy(array('profileId' => $profileId));
                $participants = $em->getRepository('classesclassBundle:participants')->findOneBy(array('id' => $profiles->getParticipant()));
            }else{
                $profiles = new profiles();
                $participants = new participants();
            }
            $participants->setFirstName($data['firstName']);
            $participants->setLastName($data['lastName']);
            $participants->setEmail($data['email']);
            $participants->setLegalName($data['firstName'] . ' ' . $data['lastName']);
            $participants->setAddress($data['address']);
            $participants->setCity($data['city']);
            $participants->setState($data['state']);
            $participants->setPhone($data['phone']);
            $participants->setTesting($this->testing);
            $em->persist($participants);
            $em->flush();

            $profiles->setParticipant($participants);
            $profiles->setUserid($data['userId']);
            $profiles->setPlanid($data['planId']);
            $profiles->setPlanName($data['planName']);
            $profiles->setAvailability($data['availability']);
            $profiles->setClientCalling($data['clientCalling']);
            $profiles->setCurrentBalance($data['balance']);
            $profiles->setReportDate($data['reportDate']);
            $profiles->setProfilestatus($data['profileStatus']);
            $profiles->setProfileId($profileId);
            $profiles->setAnnualSalary($data['annualSalary']);
            $profiles->setRetireAge($data['retireAge']);
            $profiles->setPreTaxSavingRate($data['preTaxSavingRate']);
            $profiles->setPostTaxSavingRate($data['postTaxSavingRate']);
            $profiles->setRothTaxSavingRate($data['rothTaxSavingRate']);
            $profiles->setMStarContribution($data['catchUpContribution']);
            $profiles->setMStarStatus($data['mStarStatus']);
            $request = Request::createFromGlobals();
            $profiles->sessionData = json_encode($session->all());
            $profiles->userAgent = $request->headers->get('User-Agent');
            $em->persist($profiles);
            $em->flush();

            if($profiles){
                $this->addMStarRiskProfile($profiles);
                $this->addMStarInvestments($profiles);
            }
        }catch (Exception $e){
            $session->set('profileStatus', json_encode(array('success' => 0, 'message' => 'There was an error saving the Investor Profile.')));
            $profileError = true;
        }

        if($this->hasError($status)){
            $isError = true;
            if(isset($session->get('plan')['errorNotificationEmail']) && $session->get('plan')['errorNotificationEmail']){
                $this->functions->sendProfileErrorEmail($status, $data);
            }
        }

        if(!$profileError){
            $resp = array('success' => 1, 'message' => 'Profile ID: '.$profileId, 'confirmation' => $profileId);
        }else{
            $resp = array('success' => 0, 'message' => 'Error Saving Profile');
        }
        return $resp;
    }

    private function hasError($status){
        $session = $this->session;
        foreach($status as $d){
            $s = json_decode($session->get($d));
            if(isset($s->success) && !$s->success){
                return true;
            }
        }
        return false;
    }

    public function mStarQuit($profile){
        $em = $this->em;
        if($profile){
            $profile->setMstarQuit(1);
            $em->persist($profile);
            $em->flush();
        }
    }
}