<?php
namespace Spe\AppBundle\Services;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;

class EmailService
{
    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig){
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    public function sendMail($to, $from, $subject, $content, $cc = null, $bcc = null, $attachments = null) {
        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($from)
            ->setSender($from)
            ->setTo($to)
            ->setCc($cc)
            ->setBcc($bcc)
            ->setBody($this->twig->loadTemplate('SpeAppBundle:Template:email.html.twig')->render(array('content' => $content)), "text/html");
        return $this->mailer->send($message);
    }
}