<?php
namespace Spe\AppBundle\Services;

use classes\classBundle\Entity\profilesBeneficiaries;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Session\Session;
use Spe\AppBundle\Templating\Helper\Functions;
use classes\classBundle\Entity\profiles;
use classes\classBundle\Entity\participants;
use classes\classBundle\Entity\profilesRetirementNeeds;
use classes\classBundle\Entity\profilesRiskProfile;
use classes\classBundle\Entity\profilesInvestments;
use classes\classBundle\Entity\profilesContributions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use classes\classBundle\Entity\profilesAutoIncrease;

class ProfileService
{
    public function __construct(EntityManager $entityManager, Session $session, Functions $functions,ContainerInterface $container){
        $this->em = $entityManager;
        $this->session = $session;
        $this->functions = $functions;
        $this->testing = $session->get('testing') ? 1 : 0;
        $this->container = $container;
    }

    public function createProfile(){
        $em = $this->em;
        $session = $this->session;
        $plan = $session->get('plan');
        $rkp = $session->get('rkp')['plan_data'];
        $flexPlan =  $session->get('flexPlan');
        $planChange = $em->getRepository('classesclassBundle:plans')->findOneBy(array('id' => $plan['id']));
        $participantUniqid = isset($session->get('rkp')['participant_uid']) && $session->get('rkp')['participant_uid'] ? $session->get('rkp')['participant_uid'] : null;
        $filterData = array(
            'userId' => $plan['userid'],
            'clientCalling' => $session->get('client_calling'),
            'planId' => $plan['id'],
            'planName' => isset($rkp->planName) ? $rkp->planName : $plan['name'],
            'firstName' => isset($rkp->firstName) ? $rkp->firstName : '',
            'lastName' => isset($rkp->lastName) ? $rkp->lastName : '',
            'maritalStatus' => isset($rkp->maritalStatus) ? $rkp->maritalStatus : '',
            'address' => isset($rkp->address) ? $rkp->address : '',
            'city' => isset($rkp->city) ? $rkp->city : '',
            'state' => isset($rkp->state) ? $rkp->state : '',
            'region' => isset($rkp->region) ? $rkp->region : '',
            'postalCode' => isset($rkp->postalCode) ? $rkp->postalCode : '',
            'dateOfBirth' => isset($rkp->dateOfBirth) ? $rkp->dateOfBirth : '',
            'asOfDate' => isset($rkp->asOfDate) ? $rkp->asOfDate : '',
            'email' => isset($rkp->email) ? $rkp->email : '',
            'ssn' => isset($rkp->partSSN) ? $rkp->partSSN : ''
        );

        $profileId = $session->get('profileId') ?  $session->get('profileId') : uniqid('SPE_');
        $gender = $filterData['firstName'] ? $this->functions->findParticipantGender($filterData['firstName']) : null;

        $participants = null;
        if($participantUniqid){
            $participants = $em->getRepository('classesclassBundle:participants')->findOneBy(array('uniqid' => $participantUniqid));
        }
        if(!$participants){
            $participants = new participants();
        }
        $participants->setFirstName($filterData['firstName'] ? strtoupper($filterData['firstName']) : '');
        $participants->setLastName($filterData['lastName'] ? strtoupper($filterData['lastName']) : '');
        $participants->setAddress($filterData['address'] ? $filterData['address'] : '');
        $participants->setMaritalStatus($filterData['maritalStatus'] ? $filterData['maritalStatus'] : '');
        $participants->setBirthDate($filterData['dateOfBirth'] ? $filterData['dateOfBirth'] : '');
        $participants->setEmploymentDate($filterData['asOfDate'] ? $filterData['asOfDate'] : '');
        $participants->setGender($gender ? $gender : null);
        $participants->setEmail($filterData['email'] ? $filterData['email'] : '');
        $participants->setEmployeeId($filterData['ssn'] ? $filterData['ssn'] : '');
        $participants->setCity($filterData['city']);
        $participants->setState($filterData['state']);
        $participants->setZip($filterData['postalCode']);
        $participants->setConnection($session->get('rkp')['rkp']);
        $participants->setUniqid($participantUniqid);
        $participants->setTesting($this->testing);
        if ($plan['type'] == "smartenroll")
        $smartEnroll = 1;
        else
        $smartEnroll = 0;
        $participants->setSmartEnroll($smartEnroll);
        $em->persist($participants);
        $em->flush();

        $profile = new profiles();
        $profile->setParticipant($participants);
        $profile->setUserid($filterData['userId']);
        $profile->setPlanid($filterData['planId']);
        $profile->setClientCalling($filterData['clientCalling']);
        $profile->setPlanName($filterData['planName']);
        //$profile->setRkdData(json_encode($data));
        //$profile->setUniqid($uniqid);
        $profile->setProfilestatus(0);
        $profile->setProfileId($profileId);
        $profile->setReportDate(null);
        $profile->setFlexPlan($flexPlan);
        $request = Request::createFromGlobals();
        $profile->sessionData = json_encode($session->all());
        $profile->userAgent = $request->headers->get('User-Agent');  
        $profile->language = $this->container->get('translator')->getLocale();
        $profile->smartEnrollPartId = isset($session->get("REQUEST")["smartenrollpartid"]) ? $session->get("REQUEST")["smartenrollpartid"] : null;
        $em->persist($profile);

        if ($session->get("smart401k") !== null &&  $session->get("smart401k") == 0)
        $planChange->adviceStatus = 2;
        
        $em->flush();

        $session->set('pid', $profile->getId());
        $session->set('profileId', $profileId);
        $session->set("participantid",$participants->getId());
        
        
    }
    
    public function addRetirementNeeds($profile){
        $em = $this->em;
        $session = $this->session;

        if($profile && $session->get('retirement_needs')['retireNeedsStatus'] && $session->get('retirement_needs')['RN_CurrentYearlyIncome']){
            $profilesRetirementNeeds = $em->getRepository('classesclassBundle:profilesRetirementNeeds')->findOneBy(array('profileid' => $profile->getId()));
            if(!$profilesRetirementNeeds){
                $profilesRetirementNeeds = new profilesRetirementNeeds();
            }
            $assetTypes = '';
            if(isset($session->get('retirement_needs')['RN_AssetTypes'])){
                foreach($session->get('retirement_needs')['RN_AssetTypes'] as $as){
                    $assetTypes .= $as['value'].'^';
                }
                $assetTypes = trim($assetTypes, '^');
            }
            $profilesRetirementNeeds->setUserid(isset($session->get('plan')['userid']) ? $session->get('plan')['userid'] : null);
            $profilesRetirementNeeds->setPlanid(isset($session->get('rkp')['plan_id']) ? $session->get('rkp')['plan_id'] : null);
            $profilesRetirementNeeds->setProfile($profile);
            $profilesRetirementNeeds->setCurrentYearlyIncome($session->get('currentYearlyIncome'));
            $profilesRetirementNeeds->setEstimatedRetirementIncome($session->get('retirement_needs')['RN_EstimatedRetirementIncome']);
            $profilesRetirementNeeds->setEstimatedSocialSecurityIncome($session->get('retirement_needs')['RN_EstimatedSocialSecurityIncome']);
            $profilesRetirementNeeds->setReplacementIncome($session->get('retirement_needs')['RN_ReplacementIncome']);
            $profilesRetirementNeeds->setYearsBeforeRetirement($session->get('retirement_needs')['RN_NumberOfYearsBeforeRetirement']);
            $profilesRetirementNeeds->setRetirementAge($session->get('retirement_needs')['RN_RetirementAge']);
            $profilesRetirementNeeds->setInflationAdjustedReplacementIncome($session->get('retirement_needs')['RN_InflationAdjustedReplacementIncome']);
            $profilesRetirementNeeds->setCurrentPlanBalance($session->get('common')['planBalance']);
            $profilesRetirementNeeds->setOtherAssets($session->get('retirement_needs')['RN_OtherAssets']);
            $profilesRetirementNeeds->setTotalAssets($session->get('retirement_needs')['RN_TotalAssets']);
            $profilesRetirementNeeds->setAssetTypes($assetTypes);
            $profilesRetirementNeeds->setYearsLiveInRetirement($session->get('retirement_needs')['RN_YearsYouWillLiveInRetirement']);
            $profilesRetirementNeeds->setLifeExpectancy($session->get('retirement_needs')['RN_LifeExpectancy']);
            $profilesRetirementNeeds->setAnnualIncomeNeededInRetirement($session->get('retirement_needs')['RN_AnnualIncomeNeededInRetirement']);
            $profilesRetirementNeeds->setEstimatedSavingsAtRetirement($session->get('retirement_needs')['RN_EstimatedSavingsatRetirement']);
            $profilesRetirementNeeds->setRecommendedMonthlyPlanContribution($session->get('retirement_needs')['RN_RecommendedMonthlyPlanContribution']);
            $profilesRetirementNeeds->setPercentageOfIncomeFromPlan($session->get('retirement_needs')['RN_IncomePct']);
            $profilesRetirementNeeds->setParticipantsId($profile->participant->getId());
            $em->persist($profilesRetirementNeeds);
            $em->flush();
        }
    }

    public function addRiskProfile($profile){
        $em = $this->em;
        $session = $this->session;
        $risk_profile = $session->get('risk_profile');

        if($profile && $risk_profile['riskProfileStatus'] && $risk_profile['RP_xml']){
            $profilesRiskProfile = $em->getRepository('classesclassBundle:profilesRiskProfile')->findOneBy(array('profileid' => $profile->getId()));
            if(!$profilesRiskProfile){
                $profilesRiskProfile = new profilesRiskProfile();
            }
            $profilesRiskProfile->setUserid($session->get('plan')['userid']);
            $profilesRiskProfile->setPlanid($session->get('plan')['id']);
            $profilesRiskProfile->setProfile($profile);
            $profilesRiskProfile->setXml($risk_profile['RP_xml']);
            $profilesRiskProfile->setAns($risk_profile['RP_Points']);
            $profilesRiskProfile->setPosition($risk_profile['RP_InvestorType']);
            $profilesRiskProfile->setName($risk_profile['RP_label']);
            $profilesRiskProfile->setDescription($risk_profile['RP_desc']);
            $profilesRiskProfile->setParticipantsId($profile->participant->getId());
            $em->persist($profilesRiskProfile);
            $em->flush();
        }
    }

    public function addInvestments($profile){
        $em = $this->em;
        $session = $this->session;
        $investments = $session->get('investments');

        if($profile && ($investments['investmentsStatus'] || $this->session->get("investmentsGraphComplete",0) || $session->get('module')['ATArchitect'])){
            $profilesInvestments = $em->getRepository('classesclassBundle:profilesInvestments')->findBy(array('profileid' => $profile->getId()));
            if($profilesInvestments){
                foreach($profilesInvestments as $row):
                    $em->remove($row);
                endforeach;
            }

            $records = $this->getInvestments();
            $investments['investmentDetails'] = $records;
            if ($this->session->get("investmentsGraphComplete",0) == 0)
            {
                $session->set('investments', $investments);
            }
            
            $plansPortfolios = $session->get("plan_portfolios");
            $plansPortfoliosNameHash = array();
            foreach ($plansPortfolios as $portfolio)
            $plansPortfoliosNameHash[$portfolio['name']] = $portfolio;

            foreach($records as $i):
                $profilesInvestments = new profilesInvestments();
                $profilesInvestments->setUserid($session->get('plan')['userid']);
                $profilesInvestments->setPlanid($session->get('plan')['id']);
                $profilesInvestments->setProfile($profile);
                $profilesInvestments->setType(trim($i['type']));
                $profilesInvestments->setPname(trim($i['pname']));
                $profilesInvestments->setFname(trim($i['fname']));
                $profilesInvestments->setPercent(trim($i['percent']));
                if (isset($plansPortfoliosNameHash[trim($i['pname'])]))
                $profilesInvestments->customid = $plansPortfoliosNameHash[trim($i['pname'])]['customid'];
                

                if($session->get('common')['data_source'] == strpos($session->get('common')['data_source'], 'omni')) {
                    $profilesInvestments->setFundFactLink(trim($i['fundFactLink']));
                    $profilesInvestments->setProspectusLink(trim($i['prospectusLink']));
                }
                $profilesInvestments->setParticipantsId($profile->participant->getId());
                if (isset($i['rkp'][12][0]))
                {
                    $profilesInvestments->fundGroupValueId = $i['rkp'][12][0];
                }    
                $em->persist($profilesInvestments);
            endforeach;

            $em->flush();
        }
    }

    // Build Investments Array for database
    private function getInvestments() {
        $session = $this->session;
        $investments = $session->get('investments');
        $rkp = $session->get('rkp')['plan_data'];

        $investmentsType = $investments['I_SelectedInvestmentOption'];
        $investmentsDetails = $investments['I_SelectedInvestmentDetails'];
        $invests = array();
        if ($this->session->get("investmentsGraphComplete",0))
        {
            $existingScores = $this->session->get("graphScores");
            foreach ($existingScores['groups'] as $group)
            {
                foreach ($group['funds'] as $fund)
                {
                    if ($fund['percent'] > 0)
                    {
                        $invests[] = array("type" => "RISK","fname" => $fund['name'],"percent" => $fund['percent']);
                    }
                }
            }
            return $invests;
        }
        if ($investmentsType && $investmentsDetails) {
            if ($investmentsType == 'RISK') {
                // Turn this back on for IPMs
                //$portfolios = isset($rkp->IPMs) ? $rkp->IPMs : $rkp->portfolios;
                $portfolios = $rkp->portfolios;
                $port_id = $investmentsDetails;
                foreach ($portfolios as $portfolio):
                    if ($portfolio[0] == $port_id):
                        if($portfolio[2]){
                            foreach ($portfolio[2] as $port) :
                                $invests[] = array('type' => $investmentsType,'pname' => $portfolio[1],'fname' => $port[3],'percent' => $port[2]);
                            endforeach;
                        }else{
                            $invests[] = array('type' => $investmentsType,'pname' => $portfolio[1],'fname' => $portfolio[1],'percent' => 100);
                        }
                    endif;
                endforeach;
            }elseif ($investmentsType == 'MODEL') {
                $models = $rkp->models;
                $model_id = $investmentsDetails;
                foreach ($models as $model):
                    if ($model[0] == $model_id):
                        $invests[] = array('type' => $investmentsType,'pname' => $model[1],'fname' => $model[1],'percent' => null);
                    endif;
                endforeach;
            }else{
                if ($investmentsType == 'TARGET')
                    $funds = $rkp->targetDateFunds;
                elseif ($investmentsType == 'DEFAULT' && $session->get('plan')['defaultInvestment'])
                    $funds = array($session->get('defaultInvestmentFund'));
                else
                    $funds = $rkp->funds;

                $selected_funds = explode("|", $investmentsDetails);
                $fundFactLink = $session->get('fund_fact_link');

                $investData = array();
                foreach ($selected_funds as $sfunds):
                    $fund = explode("^", $sfunds);
                    $idx = $this->functions->findP($fund[0], $funds);
                    $investData = array('type' => $investmentsType,'pname' => null,'fname' => $funds[$idx][2],'percent' => $fund[1],"rkp" => $funds[$idx]);
                    if($session->get('common')['data_source'] == strpos($session->get('common')['data_source'], 'omni')) {
                        $link = isset($funds[$idx][5]) && $funds[$idx][5] ? $funds[$idx][5] : $fundFactLink . $funds[$idx][0];
                        $investData['fundFactLink'] = isset($session->get('common')['isFundLink']) && $session->get('common')['isFundLink'] ? $link : '';
                        $investData['prospectusLink'] = isset($funds[$idx][6]) ? $funds[$idx][6] : '';
                    }
                    $invests[] = $investData;
                endforeach;
            }
        }

        $session->set('investments', $investments);
        return $invests;
    }

    public function addContributions($profile){
        $em = $this->em;
        $session = $this->session;
        $contributionsSession = $session->get('contributions');
        if($profile && ($session->get('contributions')['contributionsStatus'] || $session->get('ACInitialize')['WsPlan']['AllowBlueprint'] === 'true')){
            $pre = $session->get('common')['pre'];
            $roth = $session->get('common')['roth'];
            $post = $session->get('common')['post'];
            $mode = $session->get('contributions')['contribution_mode'] == 'DEFERPCT' ? 'PCT' : ($session->get('contributions')['contribution_mode'] == 'DEFERDLR' ? 'DLR' : null);
            $cmode = $session->get('contributions')['catchup_mode'] == 'DEFERPCT' ? 'PCT' : ($session->get('contributions')['catchup_mode'] == 'DEFERDLR' ? 'DLR' : null);

            $preContributionsCurrent = isset($session->get('common')['preCurrent']) ? $session->get('common')['preCurrent'] : null;
            $rothContributionsCurrent = isset($session->get('common')['rothCurrent']) ? $session->get('common')['rothCurrent'] : null;
            $postContributionsCurrent = isset($session->get('common')['postCurrent']) ? $session->get('common')['postCurrent'] : null;

            $prePct = $session->get('contributions')['C_PreTaxContributionPct'];
            $preVal = $session->get('contributions')['C_PreTaxContributionValue'];

            $rothPct = $session->get('contributions')['C_RothContributionPct'];
            $rothVal = $session->get('contributions')['C_RothContributionValue'];

            $postPct = $session->get('contributions')['C_PostTaxContributionPct'];
            $postVal = $session->get('contributions')['C_PostTaxContributionValue'];

            $precontributions = 'Not Applicable';
            $rothContributions = 'Not Applicable';
            $postContributions = 'Not Applicable';

            if($mode == 'PCT'){
                $precontributions = $pre ? $prePct: null;
                $rothContributions = $roth ? $rothPct : null;
                $postContributions = $post ? $postPct : null;
            }elseif ($mode == 'DLR'){
                $precontributions = $pre ? $preVal: null;
                $rothContributions = $roth ? $rothVal : null;
                $postContributions = $post ? $postVal : null;
            }

            if ($session->get('ACInitialize')['WsPlan']['AllowBlueprint'] === 'true') { // AT
                $repository = $em->getRepository('classesclassBundle:ATWsAccounts');
                $wsAccount = $repository->findOneBy(array('profileid' => $profile->id, 'accountType' => 'Plan'));
                $precontributions = $wsAccount->preTaxPct * 100.0;
                $rothContributions = $wsAccount->rothPct * 100.0;
                $postContributions = $post ? $postPct : null;
                $mode = 'PCT';
            }

            $catchupContribution = ($mode == 'PCT' || $mode == 'DLR') ? $session->get('contributions')['C_CatchUpContributionValue'] : null;
            $catchupContribution = $session->get('contributions')['updateCatchUp'] ? $catchupContribution : null;

            $profilesContributions = $em->getRepository('classesclassBundle:profilesContributions')->findOneBy(array('profileid' => $profile->getId()));
            if(!$profilesContributions){
                $profilesContributions = new profilesContributions();
            }
            $currentYearlyIncome = null;
            if (isset($session->get('retirement_needs')['RN_CurrentYearlyIncome'])) {
                $currentYearlyIncome = $session->get('retirement_needs')['RN_CurrentYearlyIncome'];
            }
            if ($session->get('currentYearlyIncome')) {
                $currentYearlyIncome = $session->get('currentYearlyIncome');
            }
            if (!$currentYearlyIncome) {
                $repo = $this->em->getRepository('classesclassBundle:ATBlueprintPersonalInformation');
                $info = $repo->findOneBy(array('profileid' => $profile->getId()));
                $currentYearlyIncome = $info->annualCompensation;
            }
                                
            $profilesContributions->setUserid(isset($session->get('plan')['userid']) ? $session->get('plan')['userid'] : null);
            $profilesContributions->setPlanid(isset($session->get('rkp')['plan_id']) ? $session->get('rkp')['plan_id'] : null);
            $profilesContributions->setProfile($profile);
            $profilesContributions->setCurrentYearlyIncome($currentYearlyIncome);
            $profilesContributions->setPaidFrequency(isset($session->get('contributions')['C_GetPaid']) ? $session->get('contributions')['C_GetPaid'] : null);
            $profilesContributions->setTarget(isset($session->get('retirement_needs')['RN_RecommendedMonthlyPlanContribution']) ? $session->get('retirement_needs')['RN_RecommendedMonthlyPlanContribution'] : null);
            $profilesContributions->setProjected(isset($session->get('contributions')['C_MonthlyContributionTotal']) ? $session->get('contributions')['C_MonthlyContributionTotal'] : null);
            $profilesContributions->setProjectedEmployer(isset($session->get('contributions')['C_MonthlyContributionTotalEmployer']) ? $session->get('contributions')['C_MonthlyContributionTotalEmployer'] : null);
            $profilesContributions->setMode($mode);
            $profilesContributions->setCmode($cmode);
            $profilesContributions->setPre($precontributions);
            $profilesContributions->setRoth($rothContributions);
            $profilesContributions->setPost($postContributions);
            $profilesContributions->setCatchup($catchupContribution);
            $profilesContributions->setPreCurrent($preContributionsCurrent);
            $profilesContributions->setRothCurrent($rothContributionsCurrent);
            $profilesContributions->setPostCurrent($postContributionsCurrent);
            $profilesContributions->setIsAca($session->get('common')['ACAon']);
            $profilesContributions->setIsAcaOptout($session->get('contributions')['C_ACAoptOut']);
            $this->calculateContributions($profilesContributions);
            $profilesContributions->setParticipantsId($profile->participant->getId());
            $em->persist($profilesContributions);
            $em->flush();
            $contributionsSession['C_MonthlyContributionPercent'] = $prePct + $rothPct + $postPct;
            $contributionsSession['C_MatchingPercent'] = floor($contributionsSession['C_MonthlyContributionTotalEmployer']/$contributionsSession['C_MonthlyContributionTotal'] * $contributionsSession['C_MonthlyContributionPercent']);
            
            $this->session->set("contributions",$contributionsSession);
        }
    }
    function calculateContributions(&$contributions)
    {
        if ($contributions != null)
        {
            $daysInYear = 365.242199;
            $multipler = 1;
            switch ($contributions->paidFrequency)
            {
                case 'Bi-weekly':
                $multipler = $daysInYear/14;
                break;
                case 'Weekly':
                $multipler = $daysInYear/7;
                break;            
                case 'Monthly':
                $multipler = 12;
                break; 
                case 'Semi-monthly':
                $multipler = 24;
                break;    
                case 'Quarterly':
                $multipler = 4;
                break;
            }       
            $contributions->deferralPercent = 0;
            $contributions->deferralDollar = 0; 
            $contributions->rothPercent = 0; 
            $contributions->prePercent = 0;
            $contributions->preDollar = 0;
            $contributions->rothDollar = 0;
            $contributions->annualSalary = $contributions->currentYearlyIncome;
            if ($contributions->mode == "PCT")
            {
                $contributions->deferralPercent = $contributions->pre + $contributions->roth + $contributions->post;
                $contributions->prePercent = $contributions->pre;
                $contributions->rothPercent = $contributions->roth;
                if ($contributions->currentYearlyIncome != 0)
                {
                    $contributions->deferralDollar =  ($contributions->deferralPercent / 100) * $contributions->currentYearlyIncome;
                    $contributions->preDollar  = ($contributions->prePercent / 100) * $contributions->currentYearlyIncome;
                    $contributions->rothDollar  = ($contributions->rothPercent / 100) * $contributions->currentYearlyIncome;
                }
            }
            if ($contributions->mode == "DLR")
            {
                $contributions->deferralDollar = $contributions->pre + $contributions->roth + $contributions->post;
                $contributions->preDollar = $contributions->pre;
                $contributions->rothDollar = $contributions->roth;
                if ($contributions->currentYearlyIncome != 0)
                {
                    $contributions->deferralPercent = ($contributions->deferralDollar/$contributions->currentYearlyIncome) * 100;
                    $contributions->rothPercent = ($contributions->rothDollar/$contributions->currentYearlyIncome) * 100;
                    $contributions->prePercent = ($contributions->preDollar/$contributions->currentYearlyIncome) * 100;
                }
                $contributions->deferralDollar = $contributions->deferralDollar * $multipler;
                $contributions->deferralPercent = $contributions->deferralPercent * $multipler;
                $contributions->rothPercent = $contributions->rothPercent * $multipler;
                $contributions->rothDollar = $contributions->rothDollar * $multipler;
                $contributions->prePercent = $contributions->prePercent * $multipler;
                $contributions->preDollar = $contributions->preDollar * $multipler;
            }
        }
        else
        {
            $contributions->deferralPercent = 0;
            $contributions->deferralDollar = 0; 
            $contributions->annualSalary = 0; 
            $contributions->rothPercent = 0; 
            $contributions->prePercent = 0;
            $contributions->preDollar = 0;
            $contributions->rothDollar = 0;
        } 
    }
    public function addBeneficiary($profile){
        $em = $this->em;
        $session = $this->session;
        if ($session->get('common')['data_source'] != strpos($session->get('common')['data_source'], 'omni') && $session->get('common')['data_source'] != "relius" && $session->get('beneficiary') == null && $session->get('partBeneficiaries') != null)
        {
            $session->set("beneficiary",$session->get('partBeneficiaries'));
        }
        $deleteBeneficiary = $em->getRepository('classesclassBundle:profilesBeneficiaries')->findBy(array('profileid' => $profile->getId()));
        if($profile &&  $session->get('common')['data_source'] != strpos($session->get('common')['data_source'], 'omni') && $session->get('common')['data_source'] != "relius" && $session->get('beneficiary')  ) {
            if($deleteBeneficiary){
                foreach($deleteBeneficiary as $row):
                    $em->remove($row);
                endforeach;
            }
            foreach($session->get('beneficiary') as $b){
                $this->fixBeneficiary($b);
                $beneficiary = new profilesBeneficiaries();
                $beneficiary->setUserid(isset($session->get('plan')['userid']) ? $session->get('plan')['userid'] : null);
                $beneficiary->setPlanid(isset($session->get('rkp')['plan_id']) ? $session->get('rkp')['plan_id'] : null);
                $beneficiary->setProfile($profile);
                $beneficiary->setType(isset($b['type']) ? $b['type'] : "");
                $beneficiary->setRelationship(isset($b['relationship']) ? $b['relationship'] : "");
                $beneficiary->setPercent(isset($b['pct']) ? $b['pct'] : 0);
                $beneficiary->setFirstName(isset($b['fname']) ? $b['fname'] : "");
                $beneficiary->setLastName(isset($b['lname']) ? $b['lname'] : "");
                $beneficiary->setGender(isset($b['gender']) ? $b['gender'] :"");
                $beneficiary->setMaritalStatus(isset($b['marital_status']) ? $b['marital_status'] : "");
                $beneficiary->setSsn(isset($b['ssn']) ? $b['ssn'] : "");
                $beneficiary->setParticipantsId($profile->participant->getId());
                $beneficiary->setState(isset($b['BeneficiaryStateAddress']) ? $b['BeneficiaryStateAddress'] : "");
                $beneficiary->setZip(isset($b['BeneficiaryZipAddress']) ? $b['BeneficiaryZipAddress'] : "");
                $beneficiary->setDob(isset($b['BeneficiaryBirthDate']) ? $b['BeneficiaryBirthDate'] : "");
                $beneficiary->setAddress1(isset($b['BeneficiaryStreet1Address']) ? $b['BeneficiaryStreet1Address'] : "");
                $beneficiary->setAddress2(isset($b['BeneficiaryStreet2Address']) ? $b['BeneficiaryStreet2Address'] : "");
                $beneficiary->setCity(isset($b['BeneficiaryCityAddress']) ? $b['BeneficiaryCityAddress'] : "");
                $beneficiary->setPhone(isset($b['phone']) ? $b['phone'] : "");
                $beneficiary->setTrustEstateCharityOther(isset($b['trustEstateCharityOtherValue']) ? $b['trustEstateCharityOtherValue'] : 0);
                $beneficiary->setForeignaddress(isset($b['foreignaddressValue']) ? $b['foreignaddressValue'] : 0);
                $beneficiary->setCountry(isset($b['country']) ? $b['country'] : "");
                $beneficiary->setMiddleInitial(isset($b['middleInitial']) ? strtoupper($b['middleInitial']) : "");
                $em->persist($beneficiary);
            }
            $em->flush();
        } elseif($profile && $session->get('partBeneficiaries')){
            if($deleteBeneficiary){
                foreach($deleteBeneficiary as $row):
                    $em->remove($row);
                endforeach;
            }
            foreach($session->get('partBeneficiaries') as $b){
                $this->fixBeneficiary($b);
                $beneficiary = new profilesBeneficiaries();
                $beneficiary->setUserid(isset($session->get('plan')['userid']) ? $session->get('plan')['userid'] : null);
                $beneficiary->setPlanid(isset($session->get('rkp')['plan_id']) ? $session->get('rkp')['plan_id'] : null);
                $beneficiary->setProfile($profile);
                if($session->get('common')['data_source'] == strpos($session->get('common')['data_source'], 'omni')){
                    $beneficiary->setType(isset($b['BeneficiaryType']) ? ($b['BeneficiaryType'] == '1' ? 'Individual' : ($b['BeneficiaryType'] == '2' ? 'Organization' : 'Trust')) : "");
                    $beneficiary->setLevel(isset($b['BeneficiaryLevel']) ? ($b['BeneficiaryLevel'] == '1' ? 'Primary' : ($b['BeneficiaryLevel'] == '2' ? 'Secondary' : null)) : "");
                    $beneficiary->setRelationship(isset($b['BeneficiaryRelationship']) ? $this->getBeneficiaryRelationship($b['BeneficiaryRelationship']) : "");
                }else{
                    $beneficiary->setType(isset($b['BeneficiaryTypeCode']) ? ($b['BeneficiaryTypeCode'] == 'P' ? 'Primary' : 'Contingent') : "");
                    $beneficiary->setRelationship(isset($b['BeneficiaryIsSpouseCode']) ? ($b['BeneficiaryIsSpouseCode'] == 'Y' ? 'Yes' : 'No') : "");
                }
                $beneficiary->setPercent(isset($b['BeneficiaryPercent']) ? $b['BeneficiaryPercent'] : "");
                $beneficiary->setCity(isset($b['BeneficiaryCityAddress']) ? $b['BeneficiaryCityAddress'] : "");
                $beneficiary->setState(isset($b['BeneficiaryStateAddress']) ? $b['BeneficiaryStateAddress'] : "");
                $beneficiary->setZip(isset($b['BeneficiaryZipAddress']) ? $b['BeneficiaryZipAddress'] : "");
                $beneficiary->setFirstName(isset($b['BeneficiaryName']) ? $b['BeneficiaryName'] : "");
                $beneficiary->setSsn(isset($b['BeneficiaryTaxID']) ? $b['BeneficiaryTaxID'] : "");
                $beneficiary->setDob(isset($b['BeneficiaryBirthDate']) ? $b['BeneficiaryBirthDate'] : "");
                if(isset($b['BeneficiaryGender']) && $b['BeneficiaryGender']) {
                    $beneficiary->setGender($b['BeneficiaryGender']);
                }
                $beneficiary->setAddress1(isset($b['BeneficiaryStreet1Address']) ? $b['BeneficiaryStreet1Address'] : "");
                $beneficiary->setAddress2(isset($b['BeneficiaryStreet2Address']) ? $b['BeneficiaryStreet2Address'] : "");
                $beneficiary->setSpousalWaiverForm(isset($session->get('common')['spousalWaiverFormStatus']) && $session->get('common')['spousalWaiverFormStatus'] && $b['BeneficiaryLevel'] == '1' ? $session->get('common')['spousalWaiverFormStatus'] : 0);               
                $beneficiary->setParticipantsId($profile->participant->getId());

                $em->persist($beneficiary);
                $em->flush();
            }
        }
    }
    
    private function fixBeneficiary(&$b)
    {
        if (!is_array($b))
        {
            $b = (array)$b;
        }
        foreach ($b as $key => $value)
        {
            if ($value == null)
            {
                unset($b[$key]);
            }
        }        
    }

    private function getBeneficiaryRelationship($code){
        switch($code){
            case '0':
                return 'Spouse';
            break;
            case '1':
                return 'Aunt';
            break;
            case '2':
                return 'Business Partner';
            break;
            case '3':
                return 'Daughter';
            break;
            case '4':
                return 'Father';
            break;
            case '5':
                return 'Friend';
            break;
            case '6':
                return 'Father In Law';
            break;
            case '7':
                return 'Grandchild';
            break;
            case '8':
                return 'Grandfather';
            break;
            case '9':
                return 'Grandmother';
            break;
            case 'A':
                return 'Mother';
            break;
            case 'B':
                return 'Mother In Law';
            break;
            case 'C':
                return 'Niece Nephew';
            break;
            case 'D':
                return 'Step Daughter';
            break;
            case 'E':
                return 'Step Father';
            break;
            case 'F':
                return 'Step Mother';
            break;
            case 'G':
                return 'Step Son';
            break;
            case 'H':
                return 'Sister Brother';
            break;
            case 'I':
                return 'Son';
            break;
            case 'J':
                return 'Uncle';
            break;
            case 'K':
                return 'Charity';
            break;
            case 'L':
                return 'Church';
            break;
            case 'M':
                return 'Creditor';
            break;
            case 'N':
                return 'Employer';
            break;
            case 'O':
                return 'Estate';
            break;
            case 'P':
                return 'School';
            break;
            case 'Q':
                return 'Trust';
            break;
            case 'R':
                return 'Other';
            break;
            case 'S':
                return 'Originator';
            break;
            case 'T':
                return 'Domestic Partner';
            break;
            default:
                return null;
            break;
        }
    }

    public function updateProfile($saveDate = 1){
        $em = $this->em;
        $session = $this->session;
        $profileError = $isError = false;
        $status = array(
            'investmentsConfirmationStatus',
            'realignmentConfirmationStatus',
            'contributionsConfirmationStatus',
            'catchupContributionConfirmationStatus',
            'ATTransactStatus',
            'beneficiaryConfirmationStatus',
            'profileConfirmationStatus'
        );

        $profileId = $session->get('profileId') ?  $session->get('profileId') : uniqid('SPE_');
        $data = array(
            'userId' => isset($session->get('plan')['userid']) ? $session->get('plan')['userid'] : null,
            'planId' => isset($session->get('rkp')['plan_id']) ? $session->get('rkp')['plan_id'] : null,
            'planName' => isset($session->get('rkp')['plan_data']->planName) ? $session->get('rkp')['plan_data']->planName : null,
            'phone' => isset($session->get('common')['phone']) ? $session->get('common')['phone'] : null ,
            'availability' => isset($session->get('common')['availability']) ? $session->get('common')['availability'] : null ,
            'profilestatus' => 1,
            'profileId' => $profileId,
            'firstName' => $session->get('common')['firstName'] ? strtoupper($session->get('common')['firstName']) : null,
            'lastName' => $session->get('common')['lastName'] ? strtoupper($session->get('common')['lastName']) : null,
            'dateOfBirth' => $session->get('common')['dateOfBirth'] ? $session->get('common')['dateOfBirth'] : null,
            'dateOfHire' => $session->get('common')['dateOfHire'] ? $session->get('common')['dateOfHire'] : null,
            'marketingOptIn' => $session->get('common')['marketing_opt_in'] ? 1 : 0,
            'email' => $session->get('common')['email'] ? $session->get('common')['email'] : null,
            'ssn' => isset($session->get('common')['ssn']) && $session->get('common')['ssn'] ? $session->get('common')['ssn']: null,
            'reportDate' => date("Y-m-d H:i:s"),
            'legalName' => $session->get('common')['firstName'] ? $session->get('common')['firstName'] : null . ' ' . $session->get('common')['lastName'] ? $session->get('common')['lastName'] : null,
            'planBalance' => isset($session->get('common')['planBalance']) ? $session->get('common')['planBalance'] : null,
            'address' => isset($session->get('common')['address']) && $session->get('common')['address'] ? $session->get('common')['address'] : null,
            'city' => isset($session->get('common')['city']) ? $session->get('common')['city'] : null,
            'state' => isset($session->get('common')['state']) ? $session->get('common')['state'] : null,
            'zip' => isset($session->get('common')['postalCode']) ? $session->get('common')['postalCode'] : null,
            'maritalStatus' => isset($session->get('common')['maritalStatus']) && $session->get('common')['maritalStatus'] ? $session->get('common')['maritalStatus'] : null,
            'gender' => isset($session->get('common')['gender']) ? $session->get('common')['gender'] : null,
            'asOfDate' => isset($session->get('common')['asOfDate']) && $session->get('common')['asOfDate'] ? $session->get('common')['asOfDate'] : null,
            'investmentsConfirmationStatus' => $session->get('investmentsConfirmationStatus'),
            'realignmentConfirmationStatus' => $session->get('realignmentConfirmationStatus'),
            'contributionsConfirmationStatus' => $session->get('contributionsConfirmationStatus'),
            'catchupContributionConfirmationStatus' => $session->get('catchupContributionConfirmationStatus'),
            'ATTransactStatus' => $session->get('ATTransactStatus'),
            'beneficiaryConfirmationStatus' => $session->get('beneficiaryConfirmationStatus'),
            'ACAOptOutConfirmationStatus' => $session->get('ACAOptOutConfirmationStatus'),
            'enrollmentConfirmationStatus' => $session->get('enrollmentConfirmationStatus'),
        );

        $data['smart401kStatus'] = '';
        if($session->get('smart401k_completed')){
            $data['smart401kStatus'] = $this->functions->postSmart401k();
        }

        $data['ACARequestResponse'] = $session->get('ACARequestResponse');

        try{
            $session->set('profileConfirmationStatus', null);
            if($session->get('profileId') && $profileId){
                $profiles = $em->getRepository('classesclassBundle:profiles')->findOneBy(array('profileId' => $profileId));
                $participants = $em->getRepository('classesclassBundle:participants')->findOneBy(array('id' => $profiles->getParticipant()));
            }else{
                $participantUniqid = isset($session->get('rkp')['participant_uid']) && $session->get('rkp')['participant_uid'] ? $session->get('rkp')['participant_uid'] : null;
                $participants = null;
                if($participantUniqid){
                    $participants = $em->getRepository('classesclassBundle:participants')->findOneBy(array('uniqid' => $participantUniqid));
                }
                if(!$participants){
                    $participants = new participants();
                }
                $profiles = new profiles();
            }

            $gender = $data['gender'] ? $data['gender'] : ($session->get('common')['firstName'] ? $this->functions->findParticipantGender($session->get('common')['firstName']) : null);

            $participants->setFirstName($data['firstName']);
            $participants->setLastName($data['lastName']);
            $participants->setBirthDate($data['dateOfBirth']);
            $participants->setHireDate($data['dateOfHire']);
            $participants->setMarketingOptIn($data['marketingOptIn']);
            $participants->setGender($gender ? $gender : null);
            $participants->setEmail($data['email']);
            $participants->setLegalName(trim($data['legalName']) ? $data['legalName'] : null);
            $participants->setAddress($data['address']);
            $participants->setMaritalStatus($data['maritalStatus']);
            $participants->setEmploymentDate($data['asOfDate']);
            $participants->setEmployeeId($data['ssn']);
            $participants->setCity($data['city']);
            $participants->setState($data['state']);
            $participants->setZip($data['zip']);
            $participants->setPhone($data['phone']);
            $participants->setTesting($this->testing);
            $em->persist($participants);
            $em->flush();

            $profiles->setParticipant($participants);
            $profiles->setUserid($data['userId']);
            $profiles->setPlanid($data['planId']);
            $profiles->setPlanName($data['planName']);
            $profiles->setAvailability($data['availability']);
            $profiles->setProfilestatus($data['profilestatus']);
            $profiles->setClientCalling($session->get('client_calling'));
            $profiles->setCurrentBalance($data['planBalance']);
            $profiles->setAvailability($data['availability']);
            $profiles->setInvestmentsStatus($data['investmentsConfirmationStatus']);
            $profiles->setRealignmentStatus($data['realignmentConfirmationStatus']);
            $profiles->setContributionsStatus($data['contributionsConfirmationStatus']);
            $profiles->setCatchupContributionStatus($data['catchupContributionConfirmationStatus']);
            $profiles->setATTransactStatus($data['ATTransactStatus']);
            $profiles->setBeneficiaryStatus($data['beneficiaryConfirmationStatus']);
            $profiles->setSmart401kStatus($data['smart401kStatus']);
            $profiles->setACAOptOutStatus($data['ACARequestResponse']);
            $profiles->setEnrollmentStatus($data['enrollmentConfirmationStatus']);
            if ($saveDate)
            {
                $profiles->setReportDate(new \DateTime());
            }
            $profiles->setProfilestatus($data['profilestatus']);
            $profiles->setProfileId($profileId);
            $investments = $this->getInvestments();
            if (count($investments) > 0)
            $profiles->setInvestmentType($investments[0]['type']);
            $profiles->setECOMMModal($session->get("common")['edelivery']);
            $request = Request::createFromGlobals();
            $profiles->sessionData = json_encode($session->all());
            $profiles->userAgent = $request->headers->get('User-Agent');
            $profiles->smartEnrollPartId = isset($session->get("REQUEST")["smartenrollpartid"]) ? $session->get("REQUEST")["smartenrollpartid"] : null;
            $em->persist($profiles);
            $em->flush();

            if($profiles){
                $this->addRetirementNeeds($profiles);
                $this->addRiskProfile($profiles);
                $this->addInvestments($profiles);
                $this->addContributions($profiles);
                $this->addBeneficiary($profiles);
                $this->addAutoIncrease($profiles);
            }
        }catch (Exception $e){
            $session->set('profileConfirmationStatus', json_encode(array('success' => 0, 'message' => 'There was an error saving the Investor Profile.')));
            $profileError = true;
        }
        $session->set('profileId', $profileId);

        if($this->hasError($status)){
            $isError = true;
            if(isset($session->get('plan')['errorNotificationEmail']) && $session->get('plan')['errorNotificationEmail']){
                $this->functions->sendProfileErrorEmail($status, $data);
            }
        }

        if(!$profileError){
            $resp = array('success' => 2, 'message' => 'Profile ID: ', 'confirmation' => $profileId);
        }else{
            $resp = array('success' => 0, 'message' => 'Error Saving Profile');
        }
        return json_encode($resp);
    }

    private function hasError($status){
        $session = $this->session;
        foreach($status as $d){
            $s = json_decode($session->get($d));
            if(isset($s->success) && !$s->success){
                return true;
            }
        }
        return false;
    }
    
    public function addAutoIncrease($profile) {
        $repository = $this->em->getRepository('classesclassBundle:profilesAutoIncrease');
        $profilesAutoIncreaseRows = $repository->findBy(array('profileid' => $profile->getId()));
        foreach($profilesAutoIncreaseRows as $row){
            $this->em->remove($row);
        }
        
        if ($this->session->has("autoIncreaseSave")) {
            $autoIncreaseSave = $this->session->get("autoIncreaseSave");
            foreach ($autoIncreaseSave['deferrals'] as $deferral) {
                $profilesAutoIncrease = new profilesAutoIncrease();
                $profilesAutoIncrease->userid = $this->session->get('plan')['userid'];
                $profilesAutoIncrease->planid = $this->session->get('plan')['id'];
                $profilesAutoIncrease->profile = $profile;
                $profilesAutoIncrease->source = $deferral['source'];
                $profilesAutoIncrease->amount = $deferral['value'];
                $profilesAutoIncrease->mode = $autoIncreaseSave['mode'];
                if (!empty($deferral['date'])) {
                    $profilesAutoIncrease->startDate = new \DateTime($deferral['date']);
                }
                if (!empty($this->session->get("autoIncrease")['frequency'])) {
                    $profilesAutoIncrease->frequency = $this->session->get("autoIncrease")['frequency'];
                }
                if (!empty($this->session->get("autoIncrease")['enddate'])) {
                    $profilesAutoIncrease->endDate = new \DateTime($this->session->get("autoIncrease")['enddate']);
                }
                if (!empty($this->session->get("autoIncrease")['occurrences'])) {
                    $profilesAutoIncrease->occurrences = $this->session->get("autoIncrease")['occurrences'];
                }
                
                $this->em->persist($profilesAutoIncrease);
            }
        }
        $this->em->flush();
    }

}
