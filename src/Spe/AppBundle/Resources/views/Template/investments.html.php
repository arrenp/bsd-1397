<?php
$plan = $app->getSession()->get('plan');
$module = $app->getSession()->get('module');
$rkp = $app->getSession()->get('rkp')['plan_data'];
$portfolios = isset($rkp->portfolios) ? $rkp->portfolios : null;
$targetDateFunds = isset($rkp->targetDateFunds) ? $rkp->targetDateFunds : null;
$funds = isset($rkp->funds) ? $rkp->funds : null;
$models = isset($rkp->models) ? $rkp->models : null;
$request = $app->getSession()->get('REQUEST');
$isManagedAccount = (isset($DIFund[8]) && $DIFund[8] == 1);
$ACInitialize = $app->getSession()->get('ACInitialize');
foreach ($funds as &$fund)
{
    $fund[2] = html_entity_decode($fund[2]);
    
}

//var_dump($funds);
$IPMs = isset($rkp->IPMs) ? $rkp->IPMs : null;
$partner_id = $app->getSession()->get('rkp')['partner_id'];
$source = $app->getSession()->get('rkp')['rkp'];
$flexPlan =  $app->getSession()->get('flexPlan') ? 1 : 0;

$isFundLink = $plan['fundLink'];

$customFundLink = $app->getSession()->get('fund_fact_link');

$retirement_needs = $app->getSession()->get('retirement_needs');
$risk_profile = $app->getSession()->get('risk_profile');
$investments = $app->getSession()->get('investments');

$RN_NumberOfYearsBeforeRetirement = $retirement_needs['RN_NumberOfYearsBeforeRetirement'];
$investmentType = $investments['I_SelectedInvestmentOption'];
$investment = $investments['I_SelectedInvestmentDetails'];

$risk_title = array(
    'default' => $view['translator']->trans('risk_title_default', array(), 'investments'),
    'AXA' => $view['translator']->trans('risk_title_axa', array(), 'investments'),
    'LNCLN' => $view['translator']->trans('risk_title_lncln', array(), 'investments'),
    'Envoy' => $view['translator']->trans('risk_title_envoy', array(), 'investments'),
    'RBF' => $view['translator']->trans('risk_title_rbf', array(), 'investments')
);

$risk_desc = array(
    'default' => $view['translator']->trans('risk_desc_default', array(), 'investments'),
    'AXA' => $flexPlan ? $view['translator']->trans('risk_desc_axa_flex', array(), 'investments') : $view['translator']->trans('risk_desc_axa', array(), 'investments'),
    'LNCLN' => $view['translator']->trans('risk_desc_lncln', array(), 'investments'),
    'Envoy' => $view['translator']->trans('risk_desc_envoy', array(), 'investments'),
    'RBF' => $view['translator']->trans('risk_desc_rbf', array(), 'investments')
);

// Target date content
if ($clientCalling == 'AXA') { // this needs to be based on client_id
    $target_title = $view['translator']->trans('target_axa_title', array(), 'investments');
    $target_description = $view['translator']->trans('target_axa_desc', array(), 'investments');
} else {
    $target_title = $view['translator']->trans('target_title', array(), 'investments');
    $target_description = $view['translator']->trans('target_desc', array(), 'investments');
}
if($models){
$model_title = $view['translator']->trans('model_title', array(), 'investments');
$model_desc = $view['translator']->trans('model_desc', array(), 'investments');
}

$contentKey = $clientCalling;
if($clientCalling == 'MOO') $contentKey = 'default';
if($risk_type == 'RBF' && $clientCalling != 'AXA') $contentKey = 'RBF';

/*START retire section 1 */
$audio = null;
if($current_section == 1 ) {

    // Default audio file list
    $landing_audio = $i_audios[0];
    $risk_audio = $i_audios[5];
    $target_audio = $i_audios[6];
    $custom_audio = $i_audios[7];
    $ACA_audio = $i_audios[11];
    $model_audio = $i_audios[21];
	$menu_audio = $i_audios[24];

    // Audio customizations
    if($isACA) $landing_audio = $i_audios[10];
    if($clientCalling == 'MOO') $landing_audio = $i_audios[8];
	if($isManagedAccount) $landing_audio = $i_audios[23];
    if($clientCalling == 'AXA') {
        if($IRScode == '401k' || $IRScode == '403b') {
            $risk_audio = $i_audios[12];
            $target_audio = $i_audios[13];
        }
        if($IRScode == '457') {
            $risk_audio = $i_audios[14];
            $target_audio = $i_audios[15];
            $custom_audio = $i_audios[16];
        }
    }
    if($clientCalling == 'LNCLN') $risk_audio = $i_audios[18];

    // Create audio playlist
    $audio =  $landing_audio."|".$risk_audio."|".$target_audio."|".$custom_audio."|".$ACA_audio."|".$model_audio."|".$menu_audio;

    $realign = $app->getSession()->get('I_Realign');
    $types = array("DEFAULT", "RISK", "TARGET", "CUSTOM", "ADVICE");

    // investment choice
    if($risk_profile['RP_InvestorType'] >= 0){
        $riskVal = 0;
        if ($module['investmentsGraph'])
        $riskVal = "investmentsGraph";
        $riskInvestment = '';
        if($risk_profile['recommendedPortfolioName']) {
            foreach($portfolios as $portfolio){
                if($portfolio[1] == $risk_profile['recommendedPortfolioName']){
                    $riskVal = $portfolio[0];
                    $riskInvestment = $portfolio[1];
                    break;
                }
            }
            $IPMswitch = 0;
            if($IPMswitch && $IPMs) {
                foreach($IPMs as $IPM){
                    if($IPM[1] == $risk_profile['recommendedPortfolioName']){
                        $riskInvestment = $IPM[1];
                        $riskVal = $IPM[0];
                        break;
                    }
                }
            }
        }
    }
    else{
        $riskVal = '';
        $riskInvestment = $flexPlan ?
            $view['translator']->trans('please_complete_retirement_needs_and_contributions_flex', array(), 'investments') :
            $view['translator']->trans('please_complete_risk_profile', array(), 'investments');
    }


    if($RN_NumberOfYearsBeforeRetirement){
        $retireyear = (int)date('Y') + (int)$RN_NumberOfYearsBeforeRetirement;
        $targetDate = array(); // fund names
        $targetVal  = array(); // fund IDs
        // Use the target date funds to determine the closest to retirement year
        $closest = null;
        foreach($targetDateFunds as $item){
            if( $closest == null || abs($retireyear - $closest) > abs($item[0] - $retireyear) )  {
                $closest = $item[0];
            }
        }
        // find the closet target date funds
        foreach($targetDateFunds as $item){
            if ($item[0] == $closest){
                $targetDate[] = $item[2]; // add fund name to targetDate array
                if(isset($item[3])) // not sure what this is yet.
                    $targetVal[] = $item[1]."^100^".$item[3];
                else
                    $targetVal[] = $item[1]."^100"; // add fund IDs to targetVal array
            }
        }
        if(!isset($targetDate[1])){ // if only one TDMF collapse arrays back into strings.
            $targetDate = @$targetDate[0];
            $targetVal = @$targetVal[0];
        }
    } else {
        $targetDate = $view['translator']->trans('please_complete_retirement_needs', array(), 'investments');
        $targetVal = '';
    }

    $defaultInvestment = str_replace(" ","",$module['investmentSelectorDefault']);
    $riskOn = $module['investmentSelectorRiskBasedPortfolio'] && $module['riskBasedQuestionnaire']  == 1 ? 1: 0;
    $targetOn = $module['investmentSelectorTargetMaturityFund'] == 1 ? !$module['investmentsGraph']: 0;
    $customOn = $module['investmentSelectorCustomChoice'] == 1 ? 1: 0;

    // RISK
    if($riskOn == 1) {
        if (!$module['investmentsGraph'])
        {
        $investHTML['RiskBasedPortfolio'] =
            "<div class='investmentType'>
                <div class='name'>
                    <input type='radio' name='investment' label='".$risk_title[$contentKey]."' id='investment1' value='RISK' ".($defaultInvestment == 'RiskBasedPortfolio' ? 'checked' : null)." />
                    <label for='investment1'>".$risk_title[$contentKey]."</label>
                    <a class='audioPanel playAudio' rel='1' href='#' aria-label='Play audio'><span class='audio-icon'></span><span class='audio-state audio-play'></span></a>
                </div>
                <div class='desc' >".$risk_desc[$contentKey]."</div>
                <div class='suggestion'>" . $riskInvestment . "</div>
            </div>";
        }
        else
        {
        $investHTML['RiskBasedPortfolio'] =
            "<div class='investmentType'>
                <div class='name'>
                    <input type='radio' name='investment' label='".$risk_title[$contentKey]."' id='investment1' value='RISK' ".($defaultInvestment == 'RiskBasedPortfolio' ? 'checked' : null)." />
                    <label for='investment1'>".$view['translator']->trans('asset_allocation_model', array(), 'investments')."</label>
                    <a class='audioPanel playAudio' rel='1' href='#' aria-label='Play audio'><span class='audio-icon'></span><span class='audio-state audio-play'></span></a>
                </div>
                <div class='desc' >".$view['translator']->trans('investor_profile_score', array(), 'investments')." = ".$app->getSession()->get('risk_profile')['RP_Score']."</div>
                <input type ='hidden' id ='investmentsGraphExists' />
            </div>";
        }
    }

    // TARGET
    if($targetOn) {
        if (is_array($targetDate)) {
            // check if there are multiple TDMFs and if so display both in div.suggestion
            $targetHtml = "";
            foreach ($targetDate as $target)
            $targetHtml = $targetHtml.$target."<br/>";
            $investHTML['TargetMaturityFund'] =
                "<div class='investmentType'>
                    <div class='name'>
                        <input type='radio' name='investment' label='".$target_title."' id='investment2' value='TARGET' ".($defaultInvestment == 'TargetMaturityFund' ? 'checked' : null)." />
                        <label for='investment2'>".$target_title."</label>
                        <a class='audioPanel playAudio' href='#' rel='2' aria-label='Play audio'><span class='audio-icon'></span><span class='audio-state audio-play'></span></a>
                    </div>
                    <div class='desc' >".$target_description."</div>
                    <div class='suggestion'>" . $targetHtml. "</div>
                </div>";

        } else {
            $investHTML['TargetMaturityFund'] =
                "<div class='investmentType'>
                    <div class='name'>
                        <input type='radio' name='investment' label='".$target_title."' id='investment2' value='TARGET' ".($defaultInvestment == 'TargetMaturityFund' ? 'checked' : null)." />
                        <label for='investment2'>".$target_title."</label>
                        <a class='audioPanel playAudio' rel='2' href='#' aria-label='Play audio'><span class='audio-icon'></span><span class='audio-state audio-play'></span></a>
                    </div>
                    <div class='desc' >".$target_description."</div>
                    <div class='suggestion'>" . $targetDate . "</div>
                </div>";
        }
    }

    //CUSTOM
    if($customOn) {
        $custom_title = array(
            'default' => $view['translator']->trans('custom_choice', array(), 'investments'),
            'Envoy' => $view['translator']->trans('custom_title_envoy', array(), 'investments'),
        );
        $custom_desc = array(
            'default' => $view['translator']->trans('custom_desc_default', array(), 'investments'),
            'Envoy' => $view['translator']->trans('custom_desc_envoy', array(), 'investments'),
        );
        $content = $clientCalling;

        $investHTML['CustomChoice'] =
            "<div class='investmentType'>
            <div class='name'>
                <input type='radio' name='investment' label='Custom Choice' id='investment3' value='CUSTOM' ".($defaultInvestment == 'CustomChoice' ? 'checked' : null)." />
                <label for='investment3'>".$view['translator']->trans('custom_choice', array(), 'investments')."</label>
                <a class='audioPanel playAudio' href='#' rel='3' aria-label='Play audio'><span class='audio-icon'></span><span class='audio-state audio-play'></span></a>
            </div>
            <div class='desc' >".$view['translator']->trans('custom_desc', array(), 'investments')."</div>
        </div>";
    }

    // sort array
    if(isset($defaultInvestment) && $defaultInvestment != '') {
        $top = $investHTML[$defaultInvestment];
        unset($investHTML[$defaultInvestment]);
        array_unshift($investHTML, $top);
    }
    ?>

    <div class="title"><?php echo $view['translator']->trans('investments', array(), 'investments') ?></div>
    <div class="details investments" style="<?php echo $isACA || $isDIF ? 'padding-top:0 !important;' : null;?>" >
        <?php
        //ACA DEFAULT OPTION
        if($isACA) {
            $ACAFund = $app->getSession()->get('rkp')['plan_data']->rkpExtras->ACAFund;

            echo "<div class='investmentType' style='display:block'>
                        <div class='name'>
                            <input type='radio' name='investment' id='investment4' value='DEFAULT' />
                            <label for='investment4'>".$view['translator']->trans('default_investment', array(), 'investments')."</label>
                            <a class='audioPanel playAudio' rel='4' href='#' aria-label='Play audio'><span class='audio-icon'></span><span class='audio-state audio-play setstate'></span></a>
                        </div>
                        <div class='desc'>".$view['translator']->trans('default_investment_displayed_desc', array(), 'investments')."</div>
                        <div class='suggestion'>".$ACAFund[2]."</div>
                    </div>";
        }
        ?>

        <?php
        //MOO DEFAULT INVESTMENT OPTION
        if($isDIF){
            echo "<div class='investmentType' style='display:block'>
                        <div class='name'>
                            <input type='radio' name='investment' id='investment4' value='DEFAULT' />
                            <label for='investment4'>".$view['translator']->trans('default_investment', array(), 'investments') . ($isManagedAccount ? ': Managed Account' : '')."</label>
                            <span class='audioPanel playAudio playVideo' data-video='".$DIFund[6]."' data-text='".$view['translator']->trans('click_here_to_more_about_default_investment', array(), 'investments')."' data-link='".$DIFund[7]."' style='visibility:".(isset($DIFund[6]) && $DIFund[6] ? 'visible':'hidden').";white-space:nowrap;width:25px;margin-left: 5px;' ><span class='video-icon'></span> ".$view['translator']->trans('play_video', array(), 'investments')."</span>
                            ".($isManagedAccount ? "<a class='audioPanel playAudio' href='#' rel='6' aria-label='Play audio'><span class='audio-icon'></span><span class='audio-state audio-play'></span></a>":'')."
							".($isManagedAccount && $DIFund[5] ? "<span class='popupLink highlight' data-link='".$DIFund[5]."'><i class='fa fa-file-text-o'></i></span>":'')."
                        </div>
                        <div class='desc' >".($isManagedAccount ? $DIFund[9] : $view['translator']->trans('default_investment_displayed_desc', array(), 'investments'))."</div>
                        <div class='suggestion'>".$DIFund[2]."</div>
                    </div>";
        }
        ?>

        <?php
        //INVESTMENT MODELS
        if($models){
            echo "<div class='investmentType' style='display:block'>
                        <div class='name'>
                            <input type='radio' name='investment' id='investment4' value='MODEL' />
                            <label for='investment5'>".$model_title."</label>
                            <a class='audioPanel playAudio' rel='5' href='#' aria-label='Play audio'><span class='audio-icon'></span><span class='audio-state audio-play'></span></a>
                        </div>
                        <div class='desc' >".$model_desc."</div>
                        <div class='suggestion'></div>
                    </div>";
        }
        ?>

        <?php
            if ($ACInitialize['WsPlan']['AllowArchitect'] == "true") {
            print  "<div class='investmentType' style='background-color:lightgrey'>
                    <div class='name'>
                        <input type='radio' name='investment' label='Custom Choice' id='investment0' value='ATARCHITECT' ".($defaultInvestment == 'CustomChoice' ? 'checked' : null)." />
                        <label for='investment0'>"."ATArchitect"."</label>
                    </div>
                    <div class='desc' >".$view['translator']->trans('back_to_atarchitect', array(), 'investments')."</div>
                    <br>
                </div><br>";
            }
        foreach($investHTML as $data){
            print $data;
        }
        ?>
        <input type="hidden" id="investSelectType" value="<?php echo $view->escape($investmentType);?>" />

    </div>
    <!-- Navigation -->
    <div class="actions">
        <div class="iAction iATArchitect" style="display:none">
            <a class="actionBtn right" href="#" aria-label="Proceed"><span class="center action-button"><?php echo $view['translator']->trans('proceed', array(), 'global') ?></span></a>
        </div>
        <?php if($customOn) :?>
            <div class="iAction iSelect" style="display:none">
                <a class="actionBtn right" href="#" aria-label="Proceed"><span class="center action-button"><?php echo $view['translator']->trans('proceed', array(), 'global') ?></span></a>
            </div>
        <?php endif; ?>
        <?php if($targetOn && !is_array($targetDate)) : // check if there are multiple TMFDs and if not proceed normally ?>
            <div class="iAction iProceed s" style="display:none">
                <a class="actionBtn right" href="#" aria-label="Proceed" data-val="<?php echo $view->escape($targetVal); ?>"><span class="center action-button"><?php echo $view['translator']->trans('proceed', array(), 'global') ?></span></a>
            </div>
        <?php endif; ?>
        <?php if($targetOn && is_array($targetDate)): // check if there are mutiple TMDFs and if so, send to new screen ?>
            <div class="iAction iProceed m" style="display:none">
                <a class="actionBtn right" href="#" aria-label="Proceed"><span class="center action-button"><?php echo $view['translator']->trans('proceed', array(), 'global') ?></span></a>
            </div>
        <?php endif; ?>
        <?php if($riskOn) :?>
            <div class="iAction iRisk" style="display:none">
                <a class="actionBtn right" href="#" aria-label="Proceed" data-val="<?php echo $view->escape($riskVal); ?>"><span class="center action-button"><?php echo $view['translator']->trans('proceed', array(), 'global') ?></span></a>
            </div>
        <?php endif; ?>
        <?php if($isACA) :?>
            <div class="iAction iDefault" style="display:none">
                <a class="actionBtn right" href="#" aria-label="Proceed" data-val="<?php echo $view->escape($ACAFund[1]); ?>"><span class="center action-button"><?php echo $view['translator']->trans('proceed', array(), 'global') ?></span></a>
            </div>
        <?php endif; ?>
        <?php if($isDIF) :?>
            <div class="iAction iDefault" style="display:none">
                <a class="actionBtn right" href="#" aria-label="Proceed" data-val="<?php echo $view->escape($DIFund[1]); ?>"><span class="center action-button"><?php echo $view['translator']->trans('proceed', array(), 'global') ?></span></a>
            </div>
        <?php endif; ?>
        <?php if($models) :?>
            <div class="iAction iModel" style="display:none">
                <a class="actionBtn right" href="#" aria-label="Proceed"><span class="center action-button"><?php echo $view['translator']->trans('proceed', array(), 'global') ?></span></a>
            </div>
        <?php endif; ?>
        <div class="clear"></div>
    </div>

    <?php if($isACA || $isDIF): ?>
        <style>.investmentType {padding: 0 5px !important;}</style>
    <?php endif; ?>

    <?php if(!$rkp->planAllowsElections): ?>
        <!-- Warning Modal -->
        <div class="modal" id="investmentsWarning" aria-modal="true">
            <span class="title"><?php echo $view['translator']->trans('warning', array(), 'global') ?></span>
            <span class="message"><?php echo $view['translator']->trans('investments_warning_message', array(), 'investments') ?></span>
            <span class="btn-accept"><?php echo $view['translator']->trans('accept', array(), 'global') ?></span>
        </div>
    <?php endif; ?>
<?php }
/* END section 1 */

/* START investment section 2 */
if($current_section == 2 ) {
    if($investmentType == 'RISK'){
        $audio = $i_audios[1];
        if ($clientCalling == "AXA" && ($IRScode == "401k" || $IRScode == "457")) $audio = $i_audios[17];
        $title = $risk_title[$contentKey];
        if ($partner_id == 'ENVOY_RELIUS_PROD') { $title = $view['translator']->trans('account_managed_for_you', array(), 'investments'); }
        $portCount = count($portfolios);
        if ($IPMs) {
            $IPMcount = count($IPMs);
        }
        $type = 1;
    }
    if($investmentType == 'TARGET'){
        $audio = $i_audios[9];
        $title = $target_title;
        $funds = $targetDateFunds;
        $fundCount = count($funds);
        $page = 1;
        $type = 2;
        if($RN_NumberOfYearsBeforeRetirement){
            $retireyear = (int)date('Y') + (int)$RN_NumberOfYearsBeforeRetirement;
            $targetDate = array(); // fund names
            $targetVal  = array(); // fund IDs
            // Use the target date funds to determine the closest to retirement year
            $closest = null;
            foreach($targetDateFunds as $item){
                if( $closest == null || abs($retireyear - $closest) > abs($item[0] - $retireyear) )  {
                    $closest = $item[0];
                }
            }
            // find the closet target date funds
            foreach($targetDateFunds as $item){
                //if($closest == null || abs($retireyear - $closest) >= abs($item[0] - $retireyear)){
                if ($item[0] == $closest){
                    $targetDate[] = $item[2]; // add fund name to targetDate array
                    if(isset($item[3])) // not sure what this is yet.
                        $targetVal[] = $item[1]."^100^".$item[3];
                    else
                        $targetVal[] = $item[1]."^100"; // add fund IDs to targetVal array
                }
            }
            if(!$targetDate[1]){ // if only one TDMF collapse arrays back into strings.
                $targetDate = $targetDate[0];
                $targetVal = $targetVal[0];
            }
        } else {
            $targetDate = $view['translator']->trans('please_complete_retirement_needs', array(), 'investments');
            $targetVal = '';
        }
    }
    if($investmentType == 'CUSTOM' || $investmentType == 'ADVICE') {
        if(isset($smart401k)) {
            $audio = $i_audios[3];
            $title = $view['translator']->trans('recommended_fund_allocation', array(), 'investments');
        }
        else {
            $audio = $source == strpos($source, 'omni') ? $i_audios[19] : $i_audios[2];
            $title = $view['translator']->trans('custom_choice', array(), 'investments');
            if ($partner_id == 'ENVOY_RELIUS_PROD') { $title = $view['translator']->trans('you_choose_fund_self_directed', array(), 'investments'); }
        }
        $fundCount = count($funds);
        $type = 3;
    }
    if($investmentType == 'MODEL'){
        $audio = $i_audios[22];
        $title = $model_title;
        $type = 5;
    }
    ?>

    <div class="title">
    <?php
        if ($partner_id == 'ENVOY_RELIUS_PROD' ) {
            echo $view['translator']->trans('investment_choices', array(), 'investments') .' '. $title;
        } else {
            echo $view['translator']->trans('investment_selector', array(), 'investments') .' '. $title;
        }
    ?>
    </div>

    <div class="details investments">
    <?php if(isset($smart401k)): ?>
        <div class="smart401k_allocation_note"><?php echo $view['translator']->trans('smart401k_allocation_note', array(), 'investments') ?></div>
        <!-- Smart401k Allocation Note Modal -->
        <div class="modal" id="smart401kAllocationNote" aria-modal="true">
            <span class="title"><?php echo $view['translator']->trans('smart401_allocation_title', array(), 'investments') ?></span>
            <span class="message"><?php echo $view['translator']->trans('smart401_allocation_message', array(), 'investments') ?></span>
            <span class="btn-ok"><?php echo $view['translator']->trans('ok', array(), 'global') ?></span>
            <span class="btn-cancel"><?php echo $view['translator']->trans('cancel', array(), 'global') ?></span>
        </div>
    <?php endif; ?>
    <!-- start block left-->
        
    
    <div class="<?php if($source == strpos($source, 'omni')){ echo 'center'; } elseif(isset($smart401k)){ echo 'block left smart401k'; } else{ echo 'block left'; } ?>" style="<?php if($source == strpos($source, 'omni')){ echo 'overflow-x:hidden; max-height:230px'; } ?>">
        <?php
        //Risk based
        $chk_flag = 0;
        $suggested_portfolio_name = $view['translator']->trans('please_complete_risk_profile', array(), 'investments');
        if ($investmentType == "RISK") {

            //IPMs
            $IPMswitch = 0;
            if ( $IPMs && $IPMswitch) { ?>

                <?php for ($i = 0; $i < $IPMcount ; $i++) :
                    if($IPMs[$i][1] == $risk_profile['recommendedPortfolioName']) {
                        $checked = 'checked';
                        $suggested_portfolio_name = $IPMs[$i][1];
                        $chk_flag = 1;
                    }
                    else {
                        $checked = '';
                    }
                    ?>
                    <div class="fund">
                        <div class="fundTitle">
                            <input type="radio" name="portfolios" label="<?php echo $view->escape($title); ?>" class="portRadio"  data-id="<?php echo $view->escape($IPMs[$i][0]) ?>" <?php echo $checked; ?> />
                            <!-- <a href="javascript:togglePortFunds(<?php /* echo $i ?>)" aria-label="Portfolio Funds"><?php echo $IPMs[$i][1] */ ?></a> -->
                            <span><?php echo $view->escape($IPMs[$i][1]) ?></span>
                        </div>
                        <div class="clear"></div>
                        <input type="hidden" id="portId<?php echo $i ?>" value="<?php echo $view->escape($IPMs[$i][0]) ?>" />
                        <div style="display:none" class="portFunds" id="portFund<?php echo $i ?>">
                            <?php /*  for ($j = 0; $j < count($IPMs[$i][2]); $j++) : ?>
                                <div class="fundRow">
                                    <div class="name"><?php echo $IPMs[$i][2][$j][1] ?></div>
                                </div>
                                <div class="clear"></div>
                            <?php endfor; */ ?>
                        </div>
                    </div>
                <?php endfor; ?>

            <?php } else {

                //autoPortfolios
                for ($i = 0; $i < $portCount; $i++) :
                    //portfolio array: id, name, [fundId, fundName, fundPct]
                    if($portfolios[$i][0] == $investment) {
                        $checked = 'checked';
                        $suggested_portfolio_name = $portfolios[$i][1];
                        $chk_flag = 1;
                    }
                    else {
                        $checked = '';
                    }
                    ?>
                    <div class="fund">
                        <div class="fundTitle">
                            <input type="radio" name="portfolios" label="<?php echo $view->escape($title); ?>" class="portRadio" data-id="<?php echo $view->escape($portfolios[$i][0]) ?>" <?php echo $checked; ?> />
                            <?php if ( ($clientCalling != 'AXA' ) || ( $clientCalling == 'AXA' && $IRScode == '403b' ) ) { ?> <a style="color:#333;font-size:14px;" class="portTitle" href="#" aria-label="Portfolio title" data-id="<?php echo $i ?>"> <?php } ?>
                                <?php echo $view->escape($portfolios[$i][1]) ?>
                                <?php if ( ($clientCalling != 'AXA' ) || ( $clientCalling == 'AXA' && $IRScode == '403b' ) ) { ?> </a> <?php } ?>
                        </div>
                        <div class="clear"></div>
                        <input type="hidden" id="portId<?php echo $i ?>" value="<?php echo $view->escape($portfolios[$i][0]) ?>" />
                        <div style="display:none" class="portFunds" id="portFund<?php echo $i ?>">
                            <?php for ($j = 0; $j < count($portfolios[$i][2]); $j++) : ?>
                                <div class="fundRow">
                                    <div class="pct"><?php echo $view->escape($portfolios[$i][2][$j][2]) ?> %</div>
                                    <div class="name"><?php echo $view->escape($portfolios[$i][2][$j][3]) ?></div>
                                </div>
                                <div class="clear"></div>
                            <?php endfor; ?>
                        </div>
                    </div>
                <?php endfor; ?>

            <?php } ?>

        <?php }

        elseif ($investmentType == "MODEL") {
            //Models
            foreach($models as $key => $model): ?>
                <div class="fund">
                    <div class="fundTitle">
                        <label for="modelRadio"><input type="radio" name="model" id="modelRadio" label="<?php echo $view->escape($title); ?>" class="modelRadio" data-id="<?php echo $view->escape($model[0]) ?>" /> <?php echo $view->escape($model[1]) ?></label>
                    </div>
                </div>
            <?php
            endforeach; ?>
        <?php }

        // TARGET
        elseif ($investmentType == 'TARGET') { ?>

            <!-- In case MOO decides to go back to fully allocating to only one TDMF
            <div class="fundrow">
                <input name="targetDateFundChooser" aria-label="Target date fund chooser" type="radio" onclick="setTargetDateFund(this.value)" value="<?php echo $targetVal[0]; ?>" /> <?php echo $targetDate[0]; ?><br />
                <input name="targetDateFundChooser" aria-label="Target date fund chooser" type="radio" onclick="setTargetDateFund(this.value)" value="<?php echo $targetVal[1]; ?>" /> <?php echo $targetDate[1]; ?>
            </div>
            -->

            <?php
            $fundCount = count($targetDate);
            $total_pct = 0;
            for ($i = 0; $i < $fundCount; $i++) :

                $targetValArray = explode("^",$targetVal[$i]);
                $fpct = $targetValArray[1];
                $fundId = $targetValArray[0]; ?>

                <div class="fund">
                    <div class="fundRow">
                        <div class="pct"><input type="text" size="2" id="fundPct<?php echo $i; ?>" label="<?php echo $view->escape($title); ?>"> %</div>
                        <div class="name"><?php echo $view->escape($targetDate[$i]); ?></div>
                        <input type="hidden" id="fundId<?php echo $i; ?>"  value="<?php echo $view->escape($fundId); ?>" />
                    </div>
                    <div class="clear"></div>
                </div>

            <?php endfor; ?>

            <div class="clear"></div>

        <?php }
        else {

            //Custom  or Advice
            $total_pct = 0;
            ?>
            
            <table class="table table-striped fund">
                <thead>
                    <tr>
                        <?php if($source == strpos($source, 'omni') || $source == 'guardian') :
                            $asset_class = false;
                            $prospectus = false;                        
                            $performance_url = false;

                                                        
                            $risk_level = 0;
							$redemption_fee = FALSE;
                            for ($i = 0; $i < count($funds); $i++) {
                                if (trim($funds[$i][8])) {
                                    $asset_class = true;
                                }
                                if (trim($funds[$i][6])) {
                                        $prospectus = true;
                                }                                

                                if (isset($funds[$i][11]) && trim($funds[$i][11])) {
                                        $performance_url = true;
                                }
                                if(isset($funds[$i][10]) && !is_null($funds[$i][10])) {
                                        $redemption_fee = TRUE;
                                }
                                $risk_level += (int) $funds[$i][7];
                            }
                        endif; ?>
                        <?php if(!in_array($source, array('alerus','guardian', 'educate',strpos($source, 'omni')))) : ?>
                        <th><?php echo $view['translator']->trans('pending', array(), 'global') ?></th>
                        <?php endif; ?>
                        <?php if(!in_array($source, array('educate'))) : ?>
						<th><?php echo $view['translator']->trans('current_percent', array(), 'global') ?></th>
                        <?php endif; ?>
						<th><?php if(isset($smart401k)) echo $view['translator']->trans('recommended', array(), 'global'); else echo $view['translator']->trans('percent_new', array(), 'global'); ?></th>
                        <th><?php echo $view['translator']->trans('fund_name', array(), 'investments') ?></th>
                     
                        <?php if(substr($source, 0, 4) === 'omni' || $source == 'guardian') : ?>
							<?php if($risk_level): ?>
							<th><?php echo $view['translator']->trans('risk_level', array(), 'investments') ?></th>
							<?php endif; ?>
							<?php if($asset_class): ?>
							<th><?php echo $view['translator']->trans('asset_class', array(), 'investments') ?></th>
							<?php endif; ?>
							<?php if($prospectus): ?>
							<th class="text-center"><?php echo $view['translator']->trans('prospectus', array(), 'investments') ?></th>
							<?php endif; ?>                                                         
							<?php if($performance_url): ?>
							<th><?php echo $view['translator']->trans('performance_url', array(), 'investments') ?></th>
							<?php endif; ?>
							<?php if($isFundLink): ?>
							<th class="text-center"><?php echo $view['translator']->trans('fund_fact', array(), 'investments') ?></th>
							<?php endif; ?>
							<?php if($redemption_fee): ?>
								<th class="text-center"><?php echo $view['translator']->trans('redemption_fee_message', array(), 'investments') ?></th>
							<?php endif; ?>
                        <?php endif; ?>
                                                        <?php if (isset($rkp->rkpExtras->additionalFundHeader)) : ?>
                                                        <th><?php echo $view['translator']->trans($rkp->rkpExtras->additionalFundHeader, array(), 'investments') ?></th>
                                                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>

                <?php
                $fundCount = count($funds);
                $adviceCompare = 
                function ($a, $b)
                {
                    return $a['advicePercent'] <= $b['advicePercent'];
                };
                if (isset($smart401k))
                {                    
                    $zeroFunds = [];
                    for ($i = 0; $i < $fundCount; $i++)
                    {
                        $percent= (int)$view['functions']->getAdviceAllocationPct($funds[$i][strtolower($source) == 'relius' ? 1:0]);
                        $funds[$i]['advicePercent'] = $percent;
                        if ($percent == 0)
                        {
                            $zeroFunds[] =  $funds[$i];
                            unset($funds[$i]);
                        }
                        
                    }
                        
                    usort($funds,$adviceCompare);
                    foreach ($zeroFunds as $fund)
                    {
                        $funds[] = $fund;
                    }
                }
                $funds = array_values($funds);
                for ($i = 0; $i < $fundCount; $i++) :
                    //funds array: id, name, balance, pct
                    if(isset($smart401k)){
                        $fpct = $view['functions']->getAdviceAllocationPct($funds[$i][strtolower($source) == 'relius' ? 1:0]);
                    }else {
                        $fpct = $funds[$i][3];
                    }

                    if($isFundLink){
                        $link = $funds[$i][5] ? $funds[$i][5] : $customFundLink.$funds[$i][$source == 'relius' ? 1:0];
                    }
                    ?>
                    <tr>
                        <?php if(!in_array($source, array('alerus','guardian', 'educate',strpos($source, 'omni')))) : ?>
                        <td><div class="box"><?php echo (int) $view->escape($funds[$i][4]) ?></div></td>
                        <?php endif; ?>
                        <?php if(!in_array($source, array('educate'))) : ?>
                        <td><input type="text" size="2" value="<?php echo (int) $view->escape($funds[$i][3]) ?>" disabled="true"> %</td>
                        <?php endif; ?>
                        <?php if(!isset($funds[$i][9])) : ?>
                        <td><input type="text" <?php if (isset($smart401k)) { ?>class="smart401kFund"<?php } ?> size="2" id="fundPct<?php echo $i ?>" dval="<?php echo $view->escape($fpct); ?>" label="<?php echo $view->escape($title); ?>" value="<?php echo $view->escape((substr($source, 0, 4) === 'omni') ? '0' : $fpct); ?>" /> % </td>
                        <?php else: ?>
                        <td><input type="text" size="2" class="allowed <?php if (isset($smart401k)) { ?>smart401kFund<?php } ?>" id="fundPct<?php echo $i ?>" data="<?php echo $view->escape($funds[$i][9]); ?>" dval="<?php echo $view->escape($fpct); ?>" label="<?php echo $view->escape($title); ?>" value="<?php echo $view->escape((substr($source, 0, 4) === 'omni') ? '0' : number_format($fpct, $funds[$i][9])); ?>" /> % </td>
                        <?php endif; ?>
                        <td>
                            <?php if($isFundLink): ?>
                                <span class="popupLink highlight" data-link="<?php echo $view->escape($link); ?>"><?php echo $view->escape($funds[$i][2]) ?></span>
                            <?php else: ?>
                                <?php echo $view->escape($funds[$i][2]) ?>
                            <?php endif; ?>
                        </td>
                        <?php if($source == strpos($source, 'omni') || $source == 'guardian') : ?>
                        <?php if($risk_level): ?>
                        <?php 
                        $investorScore = (int)$funds[$i][7];
                        $investorScore = $investorScore/100;
                        $investorScore = $investorScore - 1;
                        ?>
                        <td><?php echo $view->escape($view['functions']->getInvestorName($investorScore)) ?></td>
                        <?php endif; ?>
                        <?php if($asset_class): ?>
                        <td><?php echo $view->escape($funds[$i][8]) ?></td>
                        <?php endif; ?>
                        <?php if($prospectus): ?>
                        <td class="text-center"><span class="popupLink highlight" data-link="<?php echo $view->escape($funds[$i][6]); ?>"><i class="fa fa-file-text-o"></i></span></td>
                        <?php endif; ?>
                        <?php if($performance_url): ?>
                        <td class="text-center"><span class="popupLink highlight" data-link="<?php echo $view->escape($funds[$i][11]); ?>"><i class="fa fa-file-text-o"></i></span></td>
                        <?php endif; ?>
                        <?php if($isFundLink): ?>
                        <td class="text-center"><span class="popupLink highlight" data-link="<?php echo $view->escape($link); ?>"><i class="fa fa-file-text-o"></i></span></td>
                        <?php endif; ?>
                        <?php endif; ?>

                        <?php if(isset($funds[$i][10]) && !is_null($funds[$i][10])): ?>
							<td class="text-center">
								<span class="highlight redemption-click" style="cursor: pointer;"><i class="fa fa-usd" style="font-size:20px;"></i></span>
								<div class="modal redemption-modal" aria-modal="true">
									<span class="title"><?php echo $view->escape($funds[$i][2]); ?></span>
									<span class="message"><?php echo $view->escape($funds[$i][10]); ?></span>
									<span class="btn-ok"><?php echo $view['translator']->trans('ok', array(), 'global') ?></span>
								</div>
							</td>
						<?php else: ?>
                                                <?php if (isset($rkp->rkpExtras->additionalFundHeader) && isset($funds[$i][12][0])) : ?>
                                                <td><?php echo $funds[$i][12][1]; ?></th>
                                                <?php else : ?>
                                                <td class="text-center"></td>
                                                <?php endif; ?>
                        <?php endif; ?>


                    </tr>
                    <?php if(isset($funds[$i][10]) && !is_null($funds[$i][10])): ?>
                        <input type="hidden" id="fundRedemption<?php echo $i; ?>" value="<?php echo $view->escape($funds[$i][10]); ?>" />
                    <?php  endif; ?>                    
                    <input type="hidden" id="fundName<?php echo $i ?>"  value="<?php echo $view->escape($funds[$i][2]) ?>" />
                    <input type="hidden" id="fundId<?php echo $i ?>"  value="<?php echo $view->escape($funds[$i][1]) ?>" />
                    <input type="hidden" id="fundTicker<?php echo $i ?>"  value="<?php echo $view->escape($funds[$i][0]) ?>" />

                    <?php $total_pct = $total_pct + $funds[$i][3]; ?>
                <?php endfor; ?>

                </tbody>
            </table>
        <?php } ?>
    </div>  <!-- end block left -->

    <!-- start block right -->
    <?php if ($investmentType == "RISK") { ?>
        <div class="<?php if($source == strpos($source, 'omni')){ echo 'center omniBlock'; } else{ echo 'block right'; } ?>">
            <div class="desc2"><?php echo $flexPlan ? $view['translator']->trans('risk_desc_flex', array(), 'investments') : $view['translator']->trans('risk_desc', array(), 'investments') ?></div>
            <div class="portfolio-name"><?php echo $view->escape($suggested_portfolio_name); ?></div>
        </div>
    <?php } elseif ($investmentType == "MODEL") { ?>
        <div class="<?php if($source == strpos($source, 'omni')){ echo 'center omniBlock'; } else{ echo 'block right'; } ?>">
            <div class="desc2"><?php echo $view->escape($model_desc) ?></div>
            <div class="portfolio-name"></div>
        </div>
    <?php } elseif ($investmentType == 'TARGET') { ?>
        <div class=" <?php if($source == strpos($source, 'omni')){ echo 'center omniBlock'; } else{ echo 'block right'; } ?>">
            <div class="head"><?php echo $view['translator']->trans('target_head', array(), 'investments') ?></div>
            <div class="desc"><?php echo $view['translator']->trans('investment_selection_desc', array(), 'investments') ?></div>
            <div class="head"><?php echo $view['translator']->trans('your_total_must_equal', array(), 'investments') ?></div>
        </div>
    <?php } elseif(!isset($smart401k))  { ?>
        <div class="<?php if($source == strpos($source, 'omni')){ echo 'center omniBlock'; } else{ echo 'block right'; } ?>">
            <div class="head"><?php echo $view['translator']->trans('other_head', array(), 'investments') ?></div>
            <div class="desc" style="text-align:left;"><?php echo $view['translator']->trans('investment_selection_desc', array(), 'investments') ?></div>
            <div class="head" style ="padding-bottom:40px" ><?php echo $view['translator']->trans('your_total_must_equal', array(), 'investments') ?></div>
        </div>
    <?php } ?>
    <!-- end block right -->
    <div class="clear"></div>
    <input type="hidden" name="editRecommended" id="editRecommended" value="0" />
    <input type="hidden" id="investSelection" value="<?php echo $view->escape($investment); ?>" />

    </div>  <!-- end Details -->

    <!-- Navigation -->
    <div class="actions">
        <?php if ($investmentType == "CUSTOM" || $investmentType == 'ADVICE' || $investmentType == 'TARGET') : ?>
            <div class="pad-bottom-20" style = "float:right;">
                <a class="actionBtn right iCalculate" href="#" aria-label="Calculate" id="calculate" data-count="<?php echo $fundCount; ?>"><span class="center action-button"><?php echo $view['translator']->trans('calculate', array(), 'global') ?></span></a>
                <a class="actionBtn right iReset" href="#" aria-label="Reset" style="margin-right:15px;" data-count="<?php echo $fundCount; ?>"><span class="center action-button"><?php echo $view['translator']->trans('reset', array(), 'global') ?></span></a>
                <div class="right total"><?php echo $view['translator']->trans('total', array(), 'investments') ?> <input  type="text" size="3" aria-label="Investment total" id="investTotal" value="<?php echo $view->escape($total_pct); ?>" disabled /> %</div>
                <div class="clearfix"></div>
            </div>
        <?php endif; ?>
        <?php if(!isset($smart401k)): ?>
         <div  style = "float:left">   <a class="actionBtn left iNav" data-page="1" href="#" aria-label="Back" data-type="<?php echo $view->escape($type); ?>" ><span class="center action-button" ><?php echo $view['translator']->trans('back', array(), 'global') ?></span></a></div>
        <?php endif; ?>
            <a class="actionBtn right iNav <?php if(!$chk_flag) echo 'hide' ?>" href="#" id="next" aria-label="Proceed" data-page="3" data-type="<?php echo $view->escape($type); ?>"><span class="center action-button"><?php echo $view['translator']->trans('proceed', array(), 'global') ?></span></a>
        <div class="clearfix"></div>
    </div>
<?php } ?>
<!-- END section 2 -->

<!--  START Investments section 3 -->
<?php if($current_section == 3 ) {
//$audio = $i_audios[4];
$realignOptions = array("FUTURE", "CURRENT_FUTURE");
$realign = $investments['I_Realign'];
$planBalance = $rkp->planBalance;
if($investmentType == 'RISK'){
    $portCount = count($portfolios);
    if($IPMs){
        $IPMcount = count($IPMs);
    }
    $type = 1;
    $page = 2;
    $title = $risk_title[$contentKey];
}
elseif($investmentType == 'TARGET'){
    $funds = $targetDateFunds;
    $fundCount = count($funds);
    if($RN_NumberOfYearsBeforeRetirement){
        $retireyear = (int)date('Y') + (int)$RN_NumberOfYearsBeforeRetirement;
        $targetDateFunds = $targetDateFunds;
        $targetDate = array(); // fund names
        $targetVal  = array(); // fund IDs
        // Use the target date funds to determine the closest to retirement year
        $closest = null;
        foreach($targetDateFunds as $item){
            if( $closest == null || abs($retireyear - $closest) > abs($item[0] - $retireyear) )  {
                $closest = $item[0];
            }
        }
        // find the closet target date funds
        foreach($targetDateFunds as $item){
            //if($closest == null || abs($retireyear - $closest) >= abs($item[0] - $retireyear)){
            if ($item[0] == $closest){
                $targetDate[] = $item[2]; // add fund name to targetDate array
                if(isset($item[3])) // not sure what this is yet.
                    $targetVal[] = $item[1]."^100^".$item[3];
                else
                    $targetVal[] = $item[1]."^100"; // add fund IDs to targetVal array
            }
        }
        if(!isset($targetDate[1])){ // if only one TDMF collapse arrays back into strings.
            $targetDate = $targetDate[0];
            $targetVal = $targetVal[0];
        }
    } else {
        $targetDate = $view['translator']->trans('please_complete_retirement_needs', array(), 'investments');
        $targetVal = '';
    }
    $page = 1;
    if (is_array($targetDate)) {
        $page = 2;
    }
    $type = 2;
    $title = $target_title;
}
elseif($investmentType == 'CUSTOM' || $investmentType == 'ADVICE'){
    $editRecommended = $app->getSession()->get('editRecommended');
    $fundCount = count($funds);
    $type = 3;
    $page = 2;
    if(isset($smart401k) && !$editRecommended)
        $title = $view['translator']->trans('advice', array(), 'investments');
    else
        $title = $view['translator']->trans('custom_choice', array(), 'investments');
    if(isset($smart401k)){
        $page = 1;
    }
}
elseif($investmentType == 'DEFAULT'){
    if($isDIF) {
        $funds = array($DIFund);
    }
    $type = 4;
    $page = 1;
    $title = $view['translator']->trans('default_investment', array(), 'investments');
}
elseif($investmentType == 'MODEL'){
    $type = 5;
    $page = 2;
    $title = $model_title;
}

if($planBalance > 0 && $rkp->showRealignment) {
    $titleH = $view['translator']->trans('realignment_option', array(), 'investments');
    if ( $partner_id == 'ENVOY_RELIUS_PROD' ) {
        $titleH = $view['translator']->trans('realignment_option_current_balance', array(), 'investments');
    }
    else
        $audio = $source == strpos($source, 'omni') ? $i_audios[20] : $i_audios[4];
}else{
    $audio = '';      //suppression of RealignVO
    $titleH = '';
}

?>
<div class="title">
    <?php if ( $partner_id == 'ENVOY_RELIUS_PROD' ) {
        echo $view['translator']->trans('investment_choices', array(), 'investments').' '.$titleH;
    } else {
        echo $view['translator']->trans('investment_selector', array(), 'investments').' '.$titleH;
    } ?>
</div>

<div class="details investments realignment">
    <?php if ( $partner_id == 'ENVOY_RELIUS_PROD' ) { ?>
    <div><br/><div>
            <?php } else { ?>
                <div><?php echo $view['translator']->trans('investment_selections', array(), 'investments') ?><br/> <?php echo $title; ?></div>
            <?php } ?>
            <!-- start block left-->

                <div class="block <?php if($source === strpos($source, 'omni')){ echo 'center'; } else{ echo 'left'; } ?>">

                <?php
                //Risk based
                if ($investmentType == "RISK") { ?>

                    <!-- IPMs -->
                    <?php $IPMswitch = 0; ?>
                    <?php if ($IPMs && $IPMswitch) { ?>

                        <div class="fund">
                            <?php foreach($IPMs as $IPM) : ?>
                                <?php if($IPM[0] == $investment ) : ?>
                                    <div class="fundTitleSelected"><?php echo $IPM[1]; ?></div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>

                        <!-- portfolios -->
                    <?php } else { ?>

                        <div class="fund">
                            <?php foreach($portfolios as $portfolio) : ?>
                                <?php if($portfolio[0] == $investment ) : ?>
                                    <div class="fundTitleSelected"><?php echo $view->escape($portfolio[1]); ?></div>
                                    <?php foreach ($portfolio[2] as $port) : ?>
                                        <div class="fundRow">
                                            <div class="pct"><?php echo $view->escape($port[2]) ?> %</div>
                                            <div class="name"><?php echo $view->escape($port[3]) ?></div>
                                        </div>
                                        <div class="clear"></div>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    <?php } ?>
                <?php }
                //Model
                elseif ($investmentType == "MODEL") { ?>
                   <div class="fund">
                        <?php foreach($models as $model) : ?>
                            <?php if($model[0] == $investment ) : ?>
                                <div class="fundTitleSelected"><?php echo $view->escape($model[1]); ?></div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                   </div>
                <?php }
                //Default, Custom, Advice or Target
                else {
                    $selectedfunds = explode("|", $investment);  ?>
                    <table class="table table-striped fund">
                        <thead>
                        <tr>
                            <th><?php if(isset($smart401k)) echo $view['translator']->trans('recommended', array(), 'global'); else echo $view['translator']->trans('percent', array(), 'global'); ?></th>
                            <th><?php if ($isManagedAccount && $investmentType == 'DEFAULT') echo $view['translator']->trans('Managed Account', array(), 'investments'); else echo $view['translator']->trans('fund_name', array(), 'investments'); ?></th>
							<?php if($source == strpos($source, 'omni') || ($source == 'guardian')) : ?>
                                <?php
                                    $prospectus = false;
                                    $acknowledged = isset($rkp->rkpExtras->requireprospectusacknowledge) && $rkp->rkpExtras->requireprospectusacknowledge == "yes";
                                    $funds = $rkp->funds;
                                    for ($i = 0; $i < count($selectedfunds); $i++) {
                                        $id_pct = explode("^", $selectedfunds[$i]);
                                        $indx = $view['functions']->findP($id_pct[0], $funds);
                                        if (trim($funds[$indx][6])) {
                                            $prospectus = true;
                                            break;
                                        }
                                    }
                                ?>
                                <?php if($prospectus) { ?>
                                <th class="text-center"><?php echo $view['translator']->trans('prospectus', array(), 'investments') ?></th>
                                <?php } ?>
                                <?php if($acknowledged) { ?>
                                <th class="text-center"><?php echo $view['translator']->trans('acknowledged', array(), 'investments') ?></th>
                                <?php } ?>
                            <?php endif; ?>
                            <?php if (isset($rkp->rkpExtras->additionalFundHeader)): ?>
                                <th><?php echo $view['translator']->trans($rkp->rkpExtras->additionalFundHeader, array(), 'investments') ?></th>
                            <?php endif ?>
                        </tr>
                        </thead>
                        <tbody>
                            <?php for ($i = 0; $i < count($selectedfunds); $i++) :
                                $id_pct = explode("^", $selectedfunds[$i]);
                                $indx = $view['functions']->findP($id_pct[0], $funds);
                                $selectedFund = $funds[$indx];
                                ?>
                            <tr>
                                <td><?php echo $view->escape($id_pct[1]) ?> %</td>
                                <td>
                                    <?php
                                        $link = null;
                                        if($isFundLink){
                                            foreach ($rkp->funds as $fund) {
                                                if ($fund[2] == $targetDate) {
                                                    $link = $fund[5];
                                                    break;
                                                }
                                            }
                                            if($link == null) {
                                                if ($investmentType === "TARGET") {
                                                    // Search for link in original fund array by fundnum and funddesc
                                                    foreach ($rkp->funds as $eachFund) {
                                                        if (array_slice($eachFund, 1, 2) === array_slice($selectedFund, 1, 2)) {
                                                            $link = $customFundLink.$eachFund[$source == 'relius' ? 1:0];
                                                            break;
                                                        }
                                                    }
                                                }
                                                else {
                                                    $link = $customFundLink.$funds[$indx][$source == 'relius' ? 1:0];
                                                }
                                            }
                                        }
                                    ?>
                                    <?php if($link): ?>
                                        <span class="popupLink highlight" data-link="<?php echo $view->escape($link) ?>">
											<?php 
										   if ($isManagedAccount && $investmentType == 'DEFAULT')
										   echo $view->escape($DIFund[2]);
										   else
										   echo $view->escape($funds[$indx][2])
										   ?>
										</span>
                                    <?php else: ?>
                                       <?php 
                                       if ($isManagedAccount && $investmentType == 'DEFAULT')
                                       echo $view->escape($DIFund[2]);
                                       else
                                       echo $view->escape($funds[$indx][2] )
                                       ?>
                                    <?php endif; ?>
                                </td>
                                <?php if($source == strpos($source, 'omni') || $source == 'guardian') : ?>
                                <?php if($prospectus) { ?>
                                <td class="text-center">
                                    <?php if(trim($funds[$indx][6])) { ?>
                                    <span class="popupLink highlight" data-link="<?php echo $view->escape($funds[$indx][6]); ?>"><i class="fa fa-file-text-o"></i></span>
                                    <?php } ?>
                                </td>
                                <?php } ?>
                                <?php if($acknowledged) { ?>
                                <td class="text-center"><input type="checkbox" class="acknowledged" aria-label="Acknowledged" /></td>
                                <?php } ?>
                                <?php endif; ?>
                                <?php if (isset($rkp->rkpExtras->additionalFundHeader) && isset($funds[$indx][12][0])) : ?>
                                <td><?php echo $funds[$indx][12][1]; ?></td>
                                <?php else: ?>
                                <td></td>
                                <?php endif ?>
                            </tr>
                            <?php endfor; ?>
                        </tbody>
                    </table>

                <?php } ?>
                <div class="clear"></div>
            </div>  <!-- end block left -->

            <?php if($planBalance > 0 && $rkp->showRealignment): ?>
                <!-- start block right-->
                <div class="block right">
                    <div class="head"><?php echo $view['translator']->trans('apply_my_investment_selections_to', array(), 'investments') ?></div>
                    <div class="choice">
                        <input type="radio" name="realign" id="realign1"  value="FUTURE" <?php echo $realign == 'FUTURE' ? 'checked' : null; ?>  />
                        <label for="realign1"><?php echo $view['translator']->trans('future_contributions_only', array(), 'investments') ?></label>
                    </div>
                    <div class="choice">
                        <input style="" type="radio" name="realign" id="realign2" value="CURRENT_FUTURE" <?php echo $realign == 'CURRENT_FUTURE' ? 'checked' : null; ?> />
                        <label for="realign2"><?php echo $view['translator']->trans('my_current_balance_future_contributions', array(), 'investments') ?></label>
                    </div>
                </div>  <!-- end block right -->
            <?php endif; ?>

            <input type="hidden" id="investSelection" value="<?php echo $view->escape($investment); ?>" />
            <input type="hidden" id="realignment" value="<?php echo $view->escape($realign); ?>" />

        </div>  <!-- end Details -->
        <div class="clear"></div>

        <!-- Navigation -->
        <div class="actions">
            <a class="actionBtn left iNav" aria-label="Back" href="#" data-page="<?php echo $view->escape($page); ?>" data-type="<?php echo $view->escape($type); ?>"><span class="center action-button"><?php echo $view['translator']->trans('back', array(), 'global') ?></span></a>
            <a class="actionBtn right iSave" aria-label="Save" href="#"><span class="center action-button"><?php echo $view['translator']->trans('save', array(), 'global') ?></span></a>
            <div class="clear"></div>
        </div>

<?php } ?>
<!-- END section 3 -->

<?php if($audio): ?>
    <div class="audio-area"><?php echo $view->escape($audio);?></div>
<?php endif; ?>

<?php if(isset($request['server']) && $request['server'] == 1): ?>
    <script>ips.push('<?php echo $_SERVER["SERVER_ADDR"]; ?>')</script>
<?php endif; ?>

<?php if (!$investmentType): ?>
<script>
localStorage.removeItem("investmentProfile");
</script>
<?php endif; ?>