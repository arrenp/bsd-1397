<div class="details profile">
    <div class="profileBox report" <?php if ($profile['_extras']['ajax']): ?>style ="width:100%"<?php endif ?>>
        <?php if($profile): ?>
            <div class="profileHeader">
                <div class="logo left">
                    <?php if($profile['providerLogoImage']): ?>
                        <img src="<?php echo $profile['providerLogoImage'] ?>" alt="Plan logo">
                    <?php endif; ?>
                </div>
                <div class="headtitle right"><?php echo $view['translator']->trans('your_investor_profile', array(), 'my_profile') ?><br>
                    <div class="date"><?php echo $view['translator']->trans('date_generated', array(), 'my_profile') ?> <?php echo $view['translator']->trans($profile['dayOfWeek'],array(),'global'); ?>, <?php echo $view['translator']->trans($profile['month'],array(),'alert'); ?> <?php echo $profile['reportDate'] ? date("j, Y, g:i A", strtotime($profile['reportDate'])) : '' ?> PST</div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>

            <!-- Start Personal -->
            <?php
            if ($profile['fromAdmin'] || $plan->profileNotificationShowEmail) {
                $firstName = $profile['firstName'] ? $view['functions']->speDecrypt($profile['firstName']) : '';
                $lastName = $profile['lastName'] ? $view['functions']->speDecrypt($profile['lastName']) : '';
                $legalName = trim($firstName . $lastName) != '' ? $firstName . ' ' . $lastName : '';
                $email = $view['functions']->speDecrypt($profile['preferredEmail']) ? $view['functions']->speDecrypt($profile['preferredEmail']) : ($view['functions']->speDecrypt($profile['email']) ? $view['functions']->speDecrypt($profile['email']) : '');
                if ($profile['sessionData']->module->ATBlueprint) {
                    $email = $profile['sessionData']->common->ATemail;
                }
            }
            ?>
            <div class="reportBox">
                <div class="row">
                    <div class="col-xs-6 col-sm-6">
                        <div class="item"><b><?php echo $view['translator']->trans('plan_name', array(), 'my_profile') ?></b> <?php echo $view->escape($profile['planName']) ?></div>
                                            <?php if($profile['fromAdmin'] || $plan->profileNotificationShowEmail) { ?>
                                                <div class="item"><b><?php echo $view['translator']->trans('participant_name', array(), 'my_profile') ?></b> <?php echo $view->escape($legalName) ?></div>
                                                <?php if($profile['fromAdmin']) { ?>
                                                    <div class="item"><b><?php echo $view['translator']->trans('email', array(), 'my_profile') ?>:</b> <?php echo $view->escape($email) ?></div>
                                                <?php } ?>
                                            <?php } ?>
                                            <div class="item"><b><?php echo $view['translator']->trans('Plan ID', array(), 'my_profile') ?>:</b> <?php echo $view->escape($plan->planid) ?></div>
                    </div>
                    <?php if($profile['IRSCode'] == '403b' && $profile['email'] && $profile['phone'] && $profile['availability']): ?>
                        <div class="col-xs-6 col-sm-3">
                            <div class="item"><b><?php echo $view['translator']->trans('telephone', array(), 'my_profile') ?> </b><?php echo $view->escape($profile['phone']) ?></div>
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <div class="item"><b><?php echo $view['translator']->trans('a_good_time_to_reach_me', array(), 'my_profile') ?>: </b><br><?php echo $view->escape($profile['availability']) ?></div>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="clear"></div>
            </div>
            <!-- End Personal -->

            <div class="divider"></div>

            <!-- Start Balance Contributions -->

            <?php
            if($profile['contributions']['mode'] == 'PCT'){

                if (isset($profile['contributions']['pre']))
                    $contributions = number_format($profile['contributions']['pre'], 2)."%";
                else
                    $contributions = 'Not Applicable';

                if (isset($profile['contributions']['roth']))
                    $rothContributions = number_format($profile['contributions']['roth'],2)."%";
                else
                    $rothContributions = 'Not Applicable';

                if (isset($profile['contributions']['post']))
                    $postContributions = number_format($profile['contributions']['post'],2)."%";
                else
                    $postContributions = 'Not Applicable';
            }elseif($profile['contributions']['mode'] == 'DLR'){
                if (isset($profile['contributions']['pre']))
                    $contributions = "$".number_format($profile['contributions']['pre'], 2);
                else
                    $contributions = 'Not Applicable';

                if (isset($profile['contributions']['roth']))
                    $rothContributions = "$".number_format($profile['contributions']['roth'], 2);
                else
                    $rothContributions = 'Not Applicable';


                if (isset($profile['contributions']['post']))
                    $postContributions = "$".number_format($profile['contributions']['post'], 2);
                else
                    $postContributions = 'Not Applicable';

            }else{
                $contributions = $rothContributions = $postContributions = 'Not Applicable';
            }

            if($profile['contributions']['cmode'] == 'PCT'){
                $catchupContribution = number_format($profile['contributions']['catchup'])."%";
            }elseif($profile['contributions']['cmode'] == 'DLR'){
                $catchupContribution = "$".number_format($profile['contributions']['catchup']);
            }else{
                $catchupContribution = null;
            }

            $source = strpos($profile['connectionType'], 'retrev') !== false ? 'retrev' : $profile['connectionType'];
            ?>

            <div id="contributions" class="reportBox">
                <div class="boxTitle"><?php echo $view['translator']->trans('plan_balance_and_contributions', array(), 'my_profile') ?></div>
                <div class="clear"></div>
                <!-- Monthly Salary -->
                <div class="profile-row">
                    <div class="left"><?php echo $view['translator']->trans('current_yearly_income', array(), 'my_profile') ?>:</div>
                    <div class="right"> <?php echo '$'.number_format($profile['currentYearlyIncome'] ? $profile['currentYearlyIncome'] : 0);  ?></div>
                    <div class="clear"></div>
                </div>
                <!-- Current Plan Balance -->
                <div class="profile-row">
                    <div class="left"><?php echo $view['translator']->trans('current_plan_balance', array(), 'my_profile') ?>:</div>
                    <div class="right"><?php echo  '$'.number_format((double)$profile['currentBalance']); ?></div>
                    <div class="clear"></div>
                </div>
                <!-- Pre-Tax Contributions: -->
                <?php if($contributions != 'Not Applicable') { ?>
                <div class="profile-row">
                    <div class="left"><?php echo $view['translator']->trans('pre_tax_pre_paycheck', array(), 'my_profile') ?><?php if($profile['contributions']['isAca']){ echo $profile['contributions']['isAcaOptout'] ? " - ".$view['translator']->trans('aca_opt_out', array(), 'my_profile') : " - ".$view['translator']->trans('default', array(), 'my_profile'); } ?>:</div>
                    <div class="right"> <?php echo $contributions ?></div>
                    <div class="clear"></div>
                </div>
                <?php } ?>
                <!-- roth-Tax Contributions: -->
                <?php if($rothContributions != 'Not Applicable') { ?>
                <div class="profile-row">
                    <div class="left"><?php echo $view['translator']->trans('after_tax_roth_per_paycheck', array(), 'my_profile') ?><?php if($profile['contributions']['isAca']){ echo $profile['contributions']['isAcaOptout'] ? " - ".$view['translator']->trans('aca_opt_out', array(), 'my_profile') : " - ".$view['translator']->trans('default', array(), 'my_profile'); } ?>:</div>
                    <div class="right"><?php echo $rothContributions ?></div>
                    <div class="clear"></div>
                </div>
                <?php } ?>

                <?php if($postContributions != 'Not Applicable'): ?>
                    <!-- post-Tax Contributions: -->
                    <div class="profile-row">
                        <div class="left"><?php echo $view['translator']->trans('post_tax_per_paycheck', array(), 'my_profile') ?><?php if($profile['contributions']['isAca']){ echo $profile['contributions']['isAcaOptout'] ? " - ".$view['translator']->trans('aca_opt_out', array(), 'my_profile') : " - ".$view['translator']->trans('default', array(), 'my_profile'); } ?></div>
                        <div class="right"><?php echo $postContributions ?></div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                <?php endif; ?>

                <!-- Catch Up Contributions: -->
                <?php if ($catchupContribution) { ?>
                    <div class="profile-row">
                        <div class="left"><?php echo $view['translator']->trans('catch_up_contributions', array(), 'my_profile') ?>:</div>
                        <div class="right"><?php echo $catchupContribution ?></div>
                        <div class="clear"></div>
                    </div>
                <?php } ?>

                <!-- Your Total Monthly Contribution: -->
                <div class="clear"></div>
                <div class="profile-row">
                    <div class="left"><?php echo $view['translator']->trans('your_estimated_monthly_contribution', array(), 'my_profile') ?>: <img class="hint" alt="hint" src="<?php echo $view['assets']->getUrl('spe/images/global/hint.png') ?>" title="<?php echo $view['translator']->trans('contribution_hint', array(), 'my_profile') ?>"/></div>
                    <div class="right"><?php echo "$".number_format($profile['contributions']['projected'],2); ?></div>
                    <div class="clear"></div>
                </div>

                <?php if ($profile['matchingContributions'] !== 'no' && $profile['contributions']['projectedEmployer'] > 0) { ?>
                    <!-- Your Employer's Monthly Contribution: -->
                    <div class="clear"></div>
                    <div class="profile-row">
                        <div class="left"><?php echo $view['translator']->trans('your_employers_estimated_monthly_contribution', array(), 'my_profile') ?>: <img class="hint" alt="hint" src="<?php echo $view['assets']->getUrl('spe/images/global/hint.png') ?>" title="<?php echo $view['translator']->trans('contribution_hint', array(), 'my_profile') ?>"/></div>
                        <div class="right"><?php echo "$".number_format($profile['contributions']['projectedEmployer'],2); ?></div>
                        <div class="clear"></div>
                    </div>
                <?php } ?>
            </div>
            <!-- End Balance Contributions -->
            <?php if (!empty($profile['auto_increase'])){ ?>
                <!-- Start Auto-Escalate -->
                <div class="divider"></div>
                <div id="contributions" class="reportBox">
                    <div class="boxTitle"><?php echo $view['translator']->trans('auto_escalate', array(), 'my_profile') ?></div>
                    <div class="clear"></div>
                    <?php
                    $hasStartDate = count(array_filter(array_column($profile['auto_increase'], 'startDate'))) > 0;
                    $hasFrequency = count(array_filter(array_column($profile['auto_increase'], 'frequency'))) > 0;
                    $hasOccurrences = count(array_filter(array_column($profile['auto_increase'], 'occurrences'))) > 0;
                    $hasEndDate = count(array_filter(array_column($profile['auto_increase'], 'endDate'))) > 0;
                    $sourceMap = ['A' => $view['translator']->trans('pretax', array(), 'widget'), 'B' => $view['translator']->trans('roth', array(), 'widget')];
                    ?>
                    <table class="table">
                        <thead>
                        <tr class="rowDark">
                            <th><?php echo $view['translator']->trans('amount', array(), 'my_profile') ?></th>
                            <th><?php echo $view['translator']->trans('source', array(), 'my_profile') ?></th>
                            <?php if ($hasStartDate) { ?>
                                <th><?php echo $view['translator']->trans('start_date', array(), 'my_profile') ?></th>
                            <?php } ?>
                            <?php if ($hasFrequency) { ?>
                                <th><?php echo $view['translator']->trans('frequency', array(), 'my_profile') ?></th>
                            <?php } ?>
                            <?php if ($hasOccurrences) { ?>
                                <th><?php echo $view['translator']->trans('occurrences', array(), 'my_profile') ?></th>
                            <?php } ?>
                            <?php if ($hasEndDate) { ?>
                                <th><?php echo $view['translator']->trans('end_date', array(), 'my_profile') ?></th>
                            <?php } ?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($profile['auto_increase'] as $autoIncrease) { ?>
                            <tr class="rowLight">
                                <td><?php echo ($autoIncrease['mode'] == 'dollar' ? '$' : '') . $autoIncrease['amount'] . ($autoIncrease['mode'] == 'percent' ? '%' : ''); ?></td>
                                <td><?php echo isset($sourceMap[$autoIncrease['source']]) ? $sourceMap[$autoIncrease['source']] : $autoIncrease['source']; ?></td>
                                <?php if ($hasStartDate) { ?>
                                    <td><?php echo isset($autoIncrease['startDate']) ? date('m/d/Y',strtotime($autoIncrease['startDate'])) : ''; ?></td>
                                <?php } ?>
                                <?php if ($hasFrequency) { ?>
                                    <td><?php echo $autoIncrease['frequency']; ?></td>
                                <?php } ?>
                                <?php if ($hasOccurrences) { ?>
                                    <td><?php echo $autoIncrease['occurrences']; ?></td>
                                <?php } ?>
                                <?php if ($hasEndDate) { ?>
                                    <td><?php echo isset($autoIncrease['endDate']) ? date('m/d/Y',strtotime($autoIncrease['endDate'])) : ''; ?></td>
                                <?php } ?>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- End Auto-Escalate -->
            <?php } ?>
            <div class="divider"></div>
            <!-- Start Investments -->
            <div id="investments" class="reportBox">
                <div class="boxTitle"><?php echo $view['translator']->trans('investment_selections', array(), 'my_profile') ?></div>
                <div class="clear"></div>

                <?php
                $investmentLabels = array('DEFAULT' => $view['translator']->trans('default_investment', array(), 'investments'), 'RISK' => $view['translator']->trans('risk_title_default', array(), 'investments'), 'MODEL' => $view['translator']->trans('model_title', array(), 'investments'), 'CUSTOM' => $view['translator']->trans('custom_choice', array(), 'investments'), 'TARGET' => $view['translator']->trans('target_title', array(), 'investments'), 'ADVICE' => $view['translator']->trans('advice_fund', array(), 'investments'));

                if($profile['clientCalling'] == "Envoy") {
                    $investmentLabels['RISK'] = $view['translator']->trans('account_managed_for_you', array(), 'investments');
                    $investmentLabels['CUSTOM'] = $view['translator']->trans('custom_title_envoy', array(), 'investments');
                }
                elseif($profile['clientCalling'] == "AXA") {
                    $investmentLabels['RISK'] = $view['translator']->trans('risk_title_axa', array(), 'investments');
                    $investmentLabels['TARGET'] = $view['translator']->trans('target_axa_title', array(), 'investments');
                }
                elseif($profile['clientCalling'] == "LNCLN") {
                    $investmentLabels['RISK'] = $view['translator']->trans('risk_title_lncln', array(), 'investments');
                }
                elseif($profile['riskType'] == 'RBF') {
                    $investmentLabels['RISK'] = $view['translator']->trans('risk_title_rbf', array(), 'investments');
                }

                $fundFact = $prospectus = false;
                foreach($profile['investments'] as $investment){
                    $fundFact += isset($investment['fundFactLink']) && $investment['fundFactLink'] ? 1 : 0;
                    $prospectus += isset($investment['prospectusLink']) && $investment['prospectusLink'] ? 1 : 0;
                }
                if ($profile['ATArchitect'] == "")
                {
                if(count($profile['investments'])): ?>
                    <!-- Investment Name-->
                    <div class="rowDark">
                        <?php if($source == strpos($source, 'omni')): ?>
                            <div class="row">
                                <div class="col-xs-7"><?php echo $investmentLabels[$profile['investments'][0]['type']] ?></div>
                                <?php if($fundFact): ?>
                                    <div class="col-xs-2 text-center"><?php echo $view['translator']->trans('fund_fact', array(), 'investments') ?></div>
                                <?php endif; ?>
                                <?php if($prospectus): ?>
                                    <div class="col-xs-2 text-center"><?php echo $view['translator']->trans('prospectus', array(), 'investments') ?></div>
                                <?php endif; ?>
                            </div>
                        <?php elseif (isset($profile['sessionData']->investmentsGraphData)): ?>
                        
                        <?php else: ?>
                            <div class="left"><?php echo $investmentLabels[$profile['investments'][0]['type']] ?></div>
                            <?php if($profile['investments'][0]['type'] <> 'MODEL'): ?>
                                <div class="right"><?php echo $profile['investments'][0]['pname'] ?></div>
                            <?php endif; ?>
                        <?php endif; ?>
                        <div class="clear"></div>
                    </div>
                    <!-- Investment Details -->

                    <!-- Will this need to be edited to accommodate single IPM vs. underlying portfolio funds? -->
                    <div class="rowLight">
                        <?php if (!($source == strpos($source, 'omni') || isset($profile['sessionData']->investmentsGraphData))): ?>
                        <table class ="table">
                            <thead>
                                <tr>
                                    <th><?php echo $view['translator']->trans('fund_name', array(), 'investments') ?></th>
                                    <th><?php echo $view['translator']->trans('percent', array(), 'global') ?></th>
                                    <?php if (isset($profile['sessionData']->rkp->plan_data->rkpExtras->additionalFundHeader)): ?>
                                    <th><?php echo $view['translator']->trans($profile['sessionData']->rkp->plan_data->rkpExtras->additionalFundHeader, array(), 'investments') ?></th>
                                    <?php endif; ?>
                                </tr>
                            </thead>
                        </tbody>
                        <?php $headerAdded = true; ?>
                        <?php endif; ?>
                        <?php foreach ($profile['investments'] as $f) : ?>
                            <?php if($source == strpos($source, 'omni')): ?>
                                <div class="fundRow row">
                                    <div class="pct col-xs-1"><?php echo number_format($f['percent']) ?>%</div>
                                    <div class="name col-xs-6"><?php echo $f['fname'] ?></div>
                                    <div class="fundFactLink col-xs-2"><?php echo ($f['fundFactLink'] ? "<span class='popupLink' data-link='" . $f['fundFactLink'] . "'><i class='fa fa-file-text-o'></i></span>" : "") ?></div>
                                    <div class="prospectusLink col-xs-2"><?php echo ($f['prospectusLink'] ? "<span class='popupLink' data-link='" . $f['prospectusLink'] . "'><i class='fa fa-file-text-o'></i></span>" : "") ?></div>
                                </div>
                            <?php elseif (isset($profile['sessionData']->investmentsGraphData)): ?>
                          <div  id ="graphReport"></div>
                            <?php else: ?>
                            <tr>
                                <td><?php echo $f['fname'] ?></td>
                                <?php if ($f['fundsGroupsValue'] != null): ?>
                                <td><?php echo number_format($f['percent']) ?></td>
                                <td><?php echo $f['fundsGroupsValue'] ?></td>
                                <?php else: ?>
                                <td colspan = "2"><?php echo number_format($f['percent']) ?></td>
                                <?php endif; ?>
                            </tr>
                            <?php endif; ?>
                            <div class="clear"></div>
                        <?php endforeach; ?>
                        <?php if (isset($headerAdded)): ?>
                        </tbody></table>
                        <?php endif; ?>
                    </div>
                <?php else: ?>
                    <div class="rowDark">
                        <div class="info"><?php echo $view['translator']->trans('no_investment_selections', array(), 'my_profile') ?></div>
                    </div>
                <?php endif; ?>
                <div class="info"><?php echo $view['translator']->trans('investment_info', array(), 'my_profile') ?><?php if ($profile['clientCalling'] == "AXA" && $profile['IRSCode'] =="403b") echo ' '.$view['translator']->trans('at', array(), 'my_profile').' <a href="https://www.axa-equitable.com" aria-label="Axa equitable" target="_blank">https://www.axa-equitable.com</a>'; ?>.</div>
                <div class="clear"></div>
            </div>
            <!-- End Investments  -->
                <?php } else { echo $profile['ATArchitect']; }?>


            <?php echo $profile['ATBlueprint']; ?>

            <?php if (trim($profile['ATBlueprint']) == "" && trim($profile['ATArchitect']) == "") : ?>

            <div class="divider"></div>

            <?php if((int) $profile['flexPlan'] != 1 && $profile['riskBasedQuestionnaire'] && !$profile['sessionData']->module->ATArchitect): ?>
                <!-- Start Risk  -->
                <div id="riskProfile" class="reportBox">
                    <div class="boxTitle"><?php if (!$profile['investmentsGraph']) {echo $view['translator']->trans('risk_profile', array(), 'my_profile'); } else { echo $view['translator']->trans('ADP_risk_profile', array(), 'navigation'); } ?></div>
                    <div class="clear"></div>
                    <?php
                    if (!$profile['investmentsGraph'])
                    {
                        $risk_profile_incomplete_label = "risk_profile_incomplete";
                        $please_complete_risk_profile_label = "please_complete_risk_profile";
                    }
                    else
                    {
                        $risk_profile_incomplete_label = "personal_investor_profile_incomplete";
                        $please_complete_risk_profile_label = "please_complete_personal_investor_profile";
                    }
                    ?>
                    <?php if (isset($profile['sessionData']->module->investmentsGraph) && $profile['sessionData']->module->investmentsGraph):   ?>
                    <div class="rowDark"><?php echo $view['translator']->trans('investor_profile_score', array(), 'investments'); ?> = <?php echo $profile['sessionData']->risk_profile->RP_Score ?></div>
                    <?php else: ?> 
                    <!-- Investor Type-->
                    <div class="rowDark">
                        <div style="float:left"><?php echo $view['translator']->trans('investor_type', array(), 'my_profile') ?>:</div>
                        <div style="float:right"><?php echo $profile['risk_profile'] ? $profile['risk_profile']['name'] : $view['translator']->trans($risk_profile_incomplete_label, array(), 'alert') ?></div>
                        <div class="clear"></div>
                    </div>
                    <?php endif; ?>

                    <!-- Investor Details -->
                    <?php if($profile['risk_profile']['qa']) { ?>
                        <?php if ($profile['clientCalling'] == 'AXA') { ?>
                            <?php if($profile['risk_profile']['xml'] == 'riskProfileAXA2013'): ?>
                                <div class="rowLight">
                                    <div style="float:left;width:360px"><?php echo $profile['risk_profile']['description'] ?></div>
                                    <div style="float:right"><img align="right" alt="risk profile" src="/images/profile/axa2013/<?php echo $profile['risk_profile']['position'] ?>.png"/></div>
                                    <div class="clear"></div>
                                </div>
                            <?php else: ?>
                                <div class="rowLight">
                                    <div style="float:left;width:360px"><?php echo $profile['risk_profile']['description'] ?></div>
                                    <div style="float:right"><img align="right" alt="risk profile" src="/images/profile/axa/<?php echo $profile['risk_profile']['position'] ?>.png"/></div>
                                    <div class="clear"></div>
                                </div>
                                <img src="/images/profile/axa/legend.png" alt="Legend" />
                            <?php endif; ?>
                        <?php } else { ?>
                            <?php if($profile['risk_profile']['xml'] == 'riskProfileLNCLN7'): ?>
                                <?php
                                $answers = explode('|', $profile['risk_profile']['ans']);
                                $th_score = isset($answers[6]) ? $answers[6] : '';
                                ?>
                                <div class="rowLight">
                                    <div style="float:left;width:360px"><?php echo $profile['risk_profile']['description'] ?></div>
                                    <div style="float:right"><img align="right" alt="risk profile" src="/images/profile/lncln7/<?php echo $profile['risk_profile']['position'].'_'.$th_score; ?>.png"/></div>
                                    <div class="clear"></div>
                                </div>
                            <?php else: ?>
                                <?php if (!isset($profile['sessionData']->module->investmentsGraph)  || !$profile['sessionData']->module->investmentsGraph): ?>
                                <div class="rowLight">
                                    <div style="float:left;width:360px"><?php echo $profile['risk_profile']['description'] ?></div>
                                    <div style="float:right"><img align="right" alt="risk profile" src="/images/profilebar_<?php echo $profile['risk_profile']['position'] ?>.gif" /></div>
                                    <div class="clear"></div>
                                </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php } ?>
                    <?php } ?>

                    <!-- Detailed Report -->
                    <?php if(isset($profile['risk_profile']['qa'])) : ?>
                        <div class="info"><?php if (!$profile['investmentsGraph']) { echo $view['translator']->trans('to_view_your_risk_profile_answers', array(), 'my_profile'); } else { echo $view['translator']->trans('to_view_your_personal_investor_profile_answers', array(), 'my_profile');  } ?></div>
                        <div class='info'>
                            <select name='RPQuestionSet' class='question' style='width:400px;'>
                                <option value=""><?php echo $view['translator']->trans('select_question', array(), 'my_profile') ?></option>
                                <?php foreach($profile['risk_profile']['qa'] as $question => $answer): ?>
                                    <option value="<?php echo $answer ?>"><?php echo $question ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="info answer"></div>
                    <?php else: ?>
                        <div class="info"><?php echo $view['translator']->trans($please_complete_risk_profile_label, array(), 'alert') ?></div>
                    <?php endif; ?>
                    <div class="clear"></div>

                </div>
                <!-- End Risk  -->

                <div class="divider"></div>
            <?php endif; ?>
            <?php if(!$profile['sessionData']->module->ATArchitect) : ?>
            <!-- Start Retire -->
            <div id="retireNeeds" class="reportBox">
                <div class="boxTitle"><?php echo $view['translator']->trans('retirement_needs', array(), 'my_profile') ?></div>
                <div class="clear"></div>
                <div class="rowDark">
                    <div style="float:left"><?php echo $view['translator']->trans('recommended_monthly_plan_contribution', array(), 'my_profile') ?>:</div>
                    <div style="float:right"><?php echo isset($profile['retirement_needs']['qa']) && $profile['retirement_needs']['qa'] ? "$".number_format($profile['retirement_needs']['recommendedMonthlyPlanContribution']) : "Retirement Needs incomplete."; ?></div>
                    <div class="clear"></div>
                </div>

                <!-- Detailed Report -->
                <?php if(isset($profile['retirement_needs']['qa'])) : ?>
                    <div class="info"><?php echo $view['translator']->trans('to_view_your_retirement_needs_answers', array(), 'my_profile') ?></div>
                    <div class='info'>
                        <select name='RNQuestionSet' class='question' style='width:400px;'>
                            <option value=""><?php echo $view['translator']->trans('select_question', array(), 'my_profile') ?></option>
                            <?php foreach($profile['retirement_needs']['qa'] as $question => $answer): ?>
                                <option value="<?php echo $answer ?>"><?php echo $question ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="info answer"></div>
                <?php else: ?>
                    <div class="info"><?php echo $view['translator']->trans('please_complete_retirement_needs', array(), 'my_profile') ?></div>
                <?php endif; ?>
                <div class="clear"></div>
                <?php if ($profile['trustedContactStatus'] !== null) { ?>
                <div class="info"><?php echo $view['translator']->trans('trusted_contact', array(), 'global') ?>: <?php if ($profile['trustedContactStatus'] == 1) echo ucfirst(strtolower($view['translator']->trans('yes', array(), 'global'))); else if (!$profile['trustedContactStatus']) echo ucfirst(strtolower($view['translator']->trans('no', array(), 'global'))); else  echo $view['translator']->trans('update_error', array(), 'alert')  ?></div>
                <?php } ?>

            </div>
            <?php endif; ?>
            <!-- End Retire -->
            <?php endif; ?>
            

            <!-- Start Beneficiary -->
            <?php if(count($profile['beneficiaries'])){ ?>
                <div class="divider"></div>
                <?php
                $spousalWaiverForm = false;
                foreach($profile['beneficiaries'] as $beneficiary){
                    $spousalWaiverForm += isset($beneficiary['spousalWaiverForm']) && $beneficiary['spousalWaiverForm'] ? 1 : 0;
                }
                ?>
                <div id="beneficiary" class="reportBox">
                    <div class="boxTitle"><?php echo $view['translator']->trans('beneficiary', array(), 'beneficiary') ?></div>
                    <div class="clear"></div>
                    <table style="width:100%;text-indent: 10px;">
                        <tr style="font-weight:bold;">
                            <?php if($source == 'relius' && in_array($profile['beneficiaries'][0]['relationship'], array('Yes','No'))): ?>
                                <td><?php echo $view['translator']->trans('name', array(), 'beneficiary') ?></td>
                                <td><?php echo $view['translator']->trans('dob', array(), 'beneficiary') ?></td>
                                <td><?php echo $view['translator']->trans('type', array(), 'beneficiary') ?></td>
                                <td><?php echo $view['translator']->trans('spouse', array(), 'beneficiary') ?></td>
                                <td><?php echo $view['translator']->trans('pct', array(), 'beneficiary') ?></td>
                            <?php else: ?>
                                <td><?php echo $view['translator']->trans('beneficiary_type', array(), 'beneficiary') ?></td>
                                <td><?php echo $view['translator']->trans('beneficiary_relationship', array(), 'beneficiary') ?></td>
                                <td><?php echo $view['translator']->trans('beneficiary_name', array(), 'beneficiary') ?></td>
                                <td><?php echo $view['translator']->trans('beneficiary_percent', array(), 'beneficiary') ?></td>
                                <?php if($source == strpos($source, 'omni') && $spousalWaiverForm): ?>
                                    <td style="text-align:center;"><?php echo $view['translator']->trans('spousal_wavier_form', array(), 'beneficiary') ?></td>
                                <?php endif; ?>
                            <?php endif; ?>
                        </tr>
                        <?php if($source == 'relius' && in_array($profile['beneficiaries'][0]['relationship'], array('Yes','No'))): ?>
                            <?php foreach($profile['beneficiaries'] as $k => $b): ?>
                                <tr class="<?php echo $k%2 ? 'rowDark' : 'rowLight';; ?>" style="line-height:30px;" >
                                    <td>
                                        <?php if($profile['fromAdmin']){
                                            echo $b['firstName'] ? $view->escape($view['functions']->speDecrypt($b['firstName'])) : '';
                                        } else {
                                            echo $b['firstName']  ? explode(" ", trim($view->escape($view['functions']->speDecrypt($b['firstName']))))[0] : '';
                                        }; ?>
                                    </td>
                                    <td><?php echo $b['dob'] ?  date('m/d/Y', strtotime($view['functions']->speDecrypt($b['dob']))) : ''; ?></td></td>
                                    <td><?php echo $view->escape($b['type']); ?></td>
                                    <td><?php echo $view->escape($b['relationship']); ?></td>
                                    <td><?php echo $b['percent']; ?>%</td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <?php foreach($profile['beneficiaries'] as $k => $b): ?>
                                <tr class="<?php echo $k%2 ? 'rowDark' : 'rowLight';; ?>" style="line-height:30px;" >
                                    <td><?php echo $view->escape($b['type']); ?></td>
                                    <td><?php echo $view->escape($b['relationship']); ?></td>
                                    <td><?php if($profile['fromAdmin']){
                                            echo $b['firstName'] ? $view->escape($view['functions']->speDecrypt($b['firstName'])) . " " : '';
                                            echo $b['lastName'] ? $view->escape($view['functions']->speDecrypt($b['lastName'])) : '';
                                        } else {
                                            echo $b['firstName']  ? explode(" ", trim($view->escape($view['functions']->speDecrypt($b['firstName']))))[0] : '';
                                        }; ?>
                                    </td>
                                    <td><?php echo $b['percent']; ?>%</td>
                                    <?php if($source == strpos($source, 'omni') && $spousalWaiverForm): ?>
                                        <td class="text-center">
                                            <?php if($b['level'] == 'Primary' && $b['relationship'] != 'Spouse'): ?>
                                                <span class='popupSpousalWavierForm'><i class='fa fa-file-text-o'></i></span>
                                            <?php endif; ?>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </table>
                </div>
                
            <?php } ?>
            <!-- End Beneficiary -->

            <?php $row = 0; ?>
			<?php $showType = $source !== 'omniamrts'; ?>
            <!-- Start Confirmation  -->
            <?php if ($profile['_extras']['showConfirmationCodes']) : ?>
            <div class="divider"></div>
            <div id="confirmation" class="reportBox">
                <div class="boxTitle"><?php echo $view['translator']->trans('confirmation_codes', array(), 'my_profile') ?></div>
                <?php if($source <> 'educate'): ?>

                    <!-- Investment Selections Update-->
                    <div class="<?php echo (($row++)%2 == 0)?'rowDark':'rowLight' ?>">
                        <div class="col-sm-6"><?php echo in_array($source, array('retrev', 'alerus')) || !isset(json_decode($profile['investmentsStatus'])->success) ? null : 'Investment '; ?><?php echo $view['translator']->trans('selections_update', array(), 'my_profile') ?>:</div>
                        <div class="col-sm-6 text-right"><?php echo in_array($source, array('retrev', 'alerus')) ? $view['functions']->getConfirmationStatus('Selections', $profile['investmentsStatus'], true, $showType) : $view['functions']->getConfirmationStatus(isset(json_decode($profile['investmentsStatus'])->success) ? 'Investment':'Selections', $profile['investmentsStatus'], ($source != 'omniamrts'), $showType); ?></div>
                        <div class="clear"></div>
                    </div>

                    <?php if(!in_array($source, array('retrev', 'alerus', 'omniinspty'))) : ?>

                        <!-- Contribution Amount Update -->
                        <div class="<?php echo (($row++)%2 == 0)?'rowDark':'rowLight' ?>">
                            <div class="col-sm-6"><?php echo $view['translator']->trans('contribution_amount_update', array(), 'my_profile') ?>:</div>
                            <div class="col-sm-6 text-right"> <?php echo ($source == 'omniamrts') ? $view['functions']->getConfirmationStatus(isset(json_decode($profile['investmentsStatus'])->success) ? 'Investment':'Selections', $profile['investmentsStatus'], FALSE, $showType) : $view['functions']->getConfirmationStatus('Contribution', $profile['contributionsStatus'], true, $showType); ?> </div>
                            <div class="clear"></div>
                        </div>

                        <?php if ($profile['catchupContributionStatus']) { ?>
                            <!-- Contribution Catch Up Amount Update -->
                            <div class="<?php echo (($row++)%2 == 0)?'rowDark':'rowLight' ?>">
                                <div class="col-sm-6"><?php echo $view['translator']->trans('contribution_catch_up_update', array(), 'my_profile') ?>:</div>
                                <div class="col-sm-6 text-right"> <?php echo ($source == 'omniamrts') ? $view['functions']->getConfirmationStatus(isset(json_decode($profile['investmentsStatus'])->success) ? 'Investment':'Selections', $profile['investmentsStatus'], FALSE, $showType) : $view['functions']->getConfirmationStatus('Catch Up', $profile['catchupContributionStatus'], true, $showType); ?> </div>
                                <div class="clear"></div>
                            </div>
                        <?php } ?>

                        <!-- Realignment Update -->
                        <?php if ($source != 'omniamrts'): ?>
                            <div class="<?php echo (($row++)%2 == 0)?'rowDark':'rowLight' ?>">
                                <div class="col-sm-6"><?php echo $view['translator']->trans('realignment_update', array(), 'my_profile') ?>:</div>
                                <div class="col-sm-6 text-right"><?php echo $view['functions']->getConfirmationStatus('Realignment', $profile['realignmentStatus'], true, $showType); ?></div>
                                <div class="clear"></div>
                            </div>
                        <?php endif; ?>

                        <?php if ($profile['enrollmentStatus']) { ?>
                            <!-- Enrollment Update -->
                            <div class="<?php echo (($row++)%2 == 0)?'rowDark':'rowLight' ?>">
                                <div class="col-sm-6"><?php echo $view['translator']->trans('enrollment_update', array(), 'my_profile') ?>:</div>
                                <div class="col-sm-6 text-right"><?php echo ($source == 'omniamrts') ? $view['functions']->getConfirmationStatus(isset(json_decode($profile['investmentsStatus'])->success) ? 'Investment':'Selections', $profile['investmentsStatus'], FALSE, $showType) : $view['functions']->getConfirmationStatus('Enrollment', $profile['enrollmentStatus'], true, $showType); ?></div>
                                <div class="clear"></div>
                            </div>
                        <?php } ?>
                        <?php if ($profile['beneficiaryStatus']) { ?>
                            <!-- Enrollment Update -->
                            <div class="<?php echo (($row++)%2 == 0)?'rowDark':'rowLight' ?>">
                                <div class="col-sm-6"><?php echo $view['translator']->trans('beneficiary_update', array(), 'my_profile') ?>:</div>
                                <div class="col-sm-6 text-right"><?php if (isset(json_decode($profile['beneficiaryStatus'])->confirmation)) echo json_decode($profile['beneficiaryStatus'])->confirmation ?></div>
                                <div class="clear"></div>
                            </div>
                        <?php } ?>
                        <?php if ($profile['callMe'] != 0 || $profile['callUs'] != null || $profile['emailMe'] != null) { ?>
                            <!-- Enrollment Update -->
                            <div class="<?php echo (($row++)%2 == 0)?'rowDark':'rowLight' ?>">
                                <div class="col-sm-6"><?php echo $view['translator']->trans('confirmation_rollover_contact_request', array(), 'my_profile') ?>:</div>
                                <div class="col-sm-6 text-right">
                                    Selection
                                    <?php if ($profile['callMe'] != null){ ?>
                                    <?php echo $view['translator']->trans('confirmation_rollover_callme', array(), 'my_profile') ?>
                                    <?php } ?>
                                    <?php if ($profile['emailMe'] != null){ ?>
                                    <?php echo $view['translator']->trans('confirmation_rollover_emailme', array(), 'my_profile') ?>
                                    <?php } ?>
                                    <?php if ($profile['callUs'] != 0){ ?>
                                    <?php echo $view['translator']->trans('confirmation_rollover_callus', array(), 'my_profile') ?>
                                    <?php } ?>
                                </div>
                                <div class="clear"></div>
                            </div>
                        <?php } ?>
                    <?php endif; ?>
                <?php endif; ?>

                <!-- Investor Profile-->
                <div class="<?php echo (($row++)%2 == 0)?'rowDark':'rowLight' ?>">
                    <div class="col-sm-6"><?php echo $view['translator']->trans('investor_profile', array(), 'my_profile') ?>:</div>
                    <div class="col-sm-6 text-right"><?php echo $profile['profileId']; ?></div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
            <?php endif ?>
            <!-- End Confirmation  -->

            <!-- Start Important Message -->
            <?php if($profile['investorProfileDisclosure']){ ?>
                <div class="divider"></div>
                <div id="important_message" class="reportBox">
                    <div class="boxTitle"><?php echo $view['translator']->trans('important_message', array(), 'my_profile') ?></div>
                    <div class="clear"></div>
                    <div class="info"><?php echo $profile['investorProfileDisclosure']; ?></div>
                    <div class="clear"></div>
                </div>
            <?php } ?>

            <!-- Start Important Documents -->
            <?php if(count($profile['planDocuments'])){ ?>
                <div class="divider"></div>
                <div class="reportBox">
                    <div class="boxTitle"><?php echo $view['translator']->trans('important_documents', array(), 'my_profile') ?></div>
                    <div class="clear"></div>
                    <?php foreach($profile['planDocuments'] as $title => $link) : ?>
                        <div class="info highlight"><a target="_blank" aria-label="Plan document" href="<?php echo $link; ?>"><?php echo $title; ?></a></div>
                    <?php endforeach; ?>
                    <div class="clear"></div>
                </div>
            <?php } ?>
        <?php else: ?>
            <h2><?php echo $view['translator']->trans('investor_profile_is_not_available_for_given_profile', array(), 'my_profile') ?></h2>
        <?php endif; ?>
    </div>
</div>

<!-- Spousal Wavier Form Model -->
<div class="modal fade" id="spousalWavierModal" aria-modal="true" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <p><?php echo $view['translator']->trans('under_the_plan_your_spouse_primary', array(), 'beneficiary') ?></p>
                        <?php if(isset($profile['spousalWaiverUrl']) && $profile['spousalWaiverUrl']): ?>
                            <p><a href="<?php echo $profile['spousalWaiverUrl'] ?>" aria-label="Spousal waiver" target="_blank"><?php echo $profile['spousalWaiverUrl'] ?></a></p>
                        <?php endif; ?>
                        <p><?php echo $view['translator']->trans('the_form_must_signed_by_your_spouse', array(), 'beneficiary') ?></p>
                    </div>
                    <div class="col-sm-12 address-block">
                        <p>Insperity Retirement Services</p>
                        <p>P.O. Box 1988</p>
                        <p>Kingwood, TX 77347-1988</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default dismiss" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><?php echo $view['translator']->trans('ok', array(), 'global') ?></span></button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function alternateClasses() {
        $('.profile-row').each( function (i, item) {
            if (i % 2 == 0) {
                $(this).attr('class', 'rowDark');
            } else {
                $(this).attr('class', 'rowLight');
            }
        });
    }

    $(function(){
        alternateClasses();
        $('.question').on('change', function(){
            $(this).parent().parent().find('.answer').text($(this).find('option:selected').val());
        });

        $('#investments').on('click', '.popupLink', function() {
            window.open($(this).data('link'),'_blank','width=800,height=600,top=200,left=200,scrollbars=1,location=0')
        });

        $('#beneficiary').on('click', '.popupSpousalWavierForm', function () {
            $('#spousalWavierModal').modal('show');
        });
    })
</script>
<?php if (isset($profile['sessionData']->investmentsGraphData)): ?>
<script>
$.post( "/profile/graphReport", { id:"<?php echo $profile['pid']; ?>" })
  .done(function( data ) {
    $("#graphReport").html(data);
  });    
</script>
<?php endif; ?>
