<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $view['translator']->trans('smartplan_enterprise', array(), 'my_profile') ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="<?php echo $view['assets']->getUrl('favicon.ico') ?>" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link type="text/css" rel="stylesheet" href="<?php echo $view['assets']->getUrl('spe/css/bootstrap.min.css') ?>" media="all" >
    <link type="text/css" rel="stylesheet" href="<?php echo $view['assets']->getUrl('spe/css/main.css?v='.rand(0,1000)) ?>" media="all" >
    <script src="<?php echo $view['assets']->getUrl('spe/js/vendor/jquery/jquery.min.js') ?>" type="text/javascript"></script>

    <!-- Google Chart -->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load('visualization', '1.0', {'packages': ['corechart']});
    </script>

<?php
$investmentArray = "";
foreach($profile['investments'] as $in){
    $investmentArray .= "['".$in['fname']."', ".$in['percent']."],";
}
?>
    <script type="text/javascript">
        $(function(){
            google.setOnLoadCallback(drawChart);
            function drawChart() {
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Asset Mix');
                data.addColumn('number', 'Percentage');
                data.addRows([
                    <?php echo rtrim($investmentArray, ","); ?>
                ]);
                var options = {
                    backgroundColor: '#F2F1F1',
                    pieSliceBorderColor: 'none',
                    pieSliceTextStyle: {fontSize:11},
                    legend: {alignment: 'center'},
                    chartArea: {width:'100%',height:'100%'},
                    colors: ['#ff9900', '#99cc00', '#993333', '#003366', '#6699cc'],
                    pieHole: 0.4
                };
                var chart = new google.visualization.PieChart(document.getElementById('asset-mix-chart-wrapper'));
                chart.draw(data, options);
            }
        })
    </script>
</head>

<body class="report">

<div class="details profile">
<div class="profileBox report">
<?php if($profile): ?>
    <div class="profileHeader">
        <div class="logo left">
            <?php if($profile['providerLogoImage']): ?>
                <img src="<?php echo $profile['providerLogoImage'] ?>" alt="Plan logo">
            <?php endif; ?>
        </div>
        <div class="headtitle right"><?php echo $view['translator']->trans('your_investor_profile', array(), 'my_profile') ?><br>
            <div class="date"><?php echo $view['translator']->trans('date_generated', array(), 'my_profile') ?> <?php echo $profile['reportDate'] ? date("F j, Y, g:i a", strtotime($profile['reportDate'])) : '' ?></div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>

    <!-- Start Personal -->
    <?php
    $firstName = $profile['firstName'] ? $view['functions']->speDecrypt($profile['firstName']) : '';
    $lastName = $profile['lastName'] ? $view['functions']->speDecrypt($profile['lastName']) : '';
    $legalName = trim($firstName . $lastName) != '' ? $firstName . ' ' . $lastName : '';
    ?>
    <div class="reportBox">
        <div class="row">
            <div class="col-xs-6 col-sm-6">
                <div class="item"><b><?php echo $view['translator']->trans('plan_name', array(), 'my_profile') ?> </b><?php echo $profile['planName'] ?></div>
                <div class="item"><b><?php echo $view['translator']->trans('participant_name', array(), 'my_profile') ?></b> <?php echo $legalName ?></div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <!-- End Personal -->

    <div class="divider"></div>

    <!-- Start Balance Contributions -->
    <div id="contributions" class="reportBox">
        <div class="boxTitle">STRATEGY OVERVIEW</div>
        <div class="clear"></div>
        <div class="rowDark">
            <div class="left">Estimated Annual Retirement Income:</div>
            <div class="right">$ <?php echo @number_format($profile['annualSalary'], 2) ?></div>
            <div class="clear"></div>
        </div>
        <div class="rowLight">
            <div class="left">Retirement Age:</div>
            <div class="right"><?php echo $profile['retireAge'] ?></div>
            <div class="clear"></div>
        </div>
        <div class="rowDark">
            <div class="left">Savings Rate:</div>
            <div class="right"><?php echo ($profile['preTaxSavingRate'] +  $profile['postTaxSavingRate'] + $profile['rothTaxSavingRate'])?>%</div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="info">Don't forget! You must contact your plan provider to update your Savings Rate.</div>
    <!-- End Balance Contributions -->

    <div class="divider"></div>

    <!-- Start Risk  -->
    <div id="riskProfile" class="reportBox assetMix">
        <div class="boxTitle">ASSET MIX & RISK LEVEL</div>
        <div class="clear"></div>

        <div class="rowLight">

            <?php if($profile['risk_profile']): ?>
                <div class="col-sm-6">
                    <div class="risk-level-wrapper">
                        <h3>Associated Risk Level</h3>
                        <div class="riskLevel">
                            <table class="table-bordered table">
                                <tr>
                                    <td class="<?php echo $profile['risk_profile']['position'] == 1 ? 'fill' : '' ?>"></td>
                                    <td class="<?php echo $profile['risk_profile']['position'] == 2 ? 'fill' : '' ?>"></td>
                                    <td class="<?php echo $profile['risk_profile']['position'] == 3 ? 'fill' : '' ?>"></td>
                                    <td class="<?php echo $profile['risk_profile']['position'] == 4 ? 'fill' : '' ?>"></td>
                                    <td class="<?php echo $profile['risk_profile']['position'] == 5 ? 'fill' : '' ?>"></td>
                                </tr>
                            </table>
                            <div class="capsWrapper">
                                <div class="capLeft text-left">Conservative</div><div class="capRight text-right">Aggressive</div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="chart-wrap">
                        <div id="asset-mix-chart-wrapper"></div>
                    </div>
                </div>
                <div class="clear"></div>
            <?php else: ?>
                Asset Mix & Risk Level is not available.
            <?php endif; ?>
        </div>
        <br>
    </div>

    <div class="divider"></div>

    <?php
        $row = 0;
        $mStarStatus = json_decode($profile['mStarStatus']);
    ?>
    <!-- Start Confirmation  -->
    <div id="confirmation" class="reportBox">
        <div class="boxTitle">Confirmation Codes</div>
        <!-- Strategy Update-->
        <?php if (is_object($mStarStatus) && $profile['mStarStatus']) { ?>
            <div class="<?php echo (($row++) % 2 == 0) ? 'rowDark' : 'rowLight' ?>">
                <div class="col-sm-6">Strategy Update:</div>
                <div class="col-sm-6 text-right">
                    <?php
                        if($mStarStatus->success){
                            echo "<div class='statusSuccess'> Strategy Update Successful. <br/>Confirmation: ".$mStarStatus->message ."</div>";
                        }else{
                            echo "<div class='statusError'> Strategy Update Error. <br/>Error: ".$mStarStatus->message."</div>";
                        }
                    ?>
                </div>
                <div class="clear"></div>
            </div>
        <?php } ?>

        <!-- Investor Profile-->
        <div class="<?php echo (($row++) % 2 == 0) ? 'rowDark' : 'rowLight' ?>">
            <div class="col-sm-6">Investor Profile:</div>
            <div class="col-sm-6 text-right"><?php echo $profile['profileId']; ?></div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
    <!-- End Confirmation  -->

    <!-- Start Important Message -->
    <?php if($profile['investorProfileDisclosure']){ ?>
        <div class="divider"></div>
        <div id="important_message" class="reportBox">
            <div class="boxTitle"><?php echo $view['translator']->trans('important_message', array(), 'my_profile') ?></div>
            <div class="clear"></div>
            <div class="info"><?php echo $profile['investorProfileDisclosure']; ?></div>
            <div class="clear"></div>
        </div>
    <?php } ?>
    <!-- End Important Message  -->

<?php else: ?>
    <h2><?php echo $view['translator']->trans('investor_profile_is_not_available_for_given_profile', array(), 'my_profile') ?></h2>
<?php endif; ?>
</div>
</div>

</body>
</html>