<?php

namespace Shared\TranslationBundle\Command;

use classes\classBundle\Entity\AccountsTranslations;
use classes\classBundle\Entity\DefaultTranslations;
use classes\classBundle\Entity\TranslationSources;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Finder\Finder;

class ImportTranslationCommand extends ContainerAwareCommand {
    
    protected function configure()
    {
        $this
            ->setName('app:import-translation')
            ->setDescription('Imports translation files into the database.')
            ->setHelp('This command allows you to import xlf translation files into the database')
            ->addOption('all', null, InputOption::VALUE_NONE, "Import all xlf files from the translations folder")
            ->addOption('ignore-duplicates', null, InputOption::VALUE_NONE, "Ignore duplicate domain/source/locale/account translations")
            ->addOption('exclude-dir', null, InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, "Array of directories to skip")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($input->getOption("all")) {
            $this->importAll($output, $input->getOption("ignore-duplicates"), $input->getOption("exclude-dir"));
        }
    }
    
    protected function importAll(OutputInterface $output, $ignoreDuplicates = false, $excludeDir = array()) {
        
        $repository = $this->getContainer()->get("doctrine")->getRepository("classesclassBundle:accounts");
        
        $query = $this->getContainer()->get('doctrine')->getManager()->createQuery("
            SELECT a
            FROM classesclassBundle:accounts a
            WHERE a.languagePrefix != '' AND a.translationId IS NULL
        ");
        $accounts = $query->getResult();
        $accountIdMap = array();
        foreach ($accounts as $account) {
            $accountIdMap[strtolower($account->languagePrefix)] = $account->id;
        }
        
        $finder = new Finder();
        $kernel = $this->getContainer()->get("kernel");
        $translationsPath = $kernel->locateResource('@SpeAppBundle/Resources/translations');
        
        $toInsert = array();
        $duplicates = array();
        $files = $finder->files()->in($translationsPath)->exclude($excludeDir)->name('/[^\.]+\.[^\.]+\.xlf/i');
        
        /* @var $file \Symfony\Component\Finder\SplFileInfo */
        foreach ($files as $file) {
            $filename = $file->getFilename();
            $matches = array();
            preg_match('/^(.+)\.([^\.]+)\.([^\.]+)$/i', $filename, $matches);
            
            list(,$domain,$locale,$ext) = $matches;
            list($prefix) = explode("_", $locale);
            $oldLocale = $locale;
             
            $account = 'default';
            if (isset($accountIdMap[$prefix])) {
                $locale = substr($locale, strlen($prefix.'_'));
                $account = $accountIdMap[$prefix];
            }
            
            $crawler = new Crawler($file->getContents());
            
            foreach ($crawler->filter("trans-unit") as $node) {
                $transUnitCrawler = new Crawler($node);
                $source = trim($transUnitCrawler->filter("source")->text());
                $target = trim($transUnitCrawler->filter("target")->text());
                 
                if (
                    isset($toInsert["$account|$domain|$locale"][$source]) &&
                    $target != $toInsert["$account|$domain|$locale"][$source]['target'] &&
                    $file->getRelativePathname() != $toInsert["$account|$domain|$locale"][$source]['filename']
                ) {
                    $duplicates[] = array($account,$domain,$oldLocale,$source);
                }
                
                $toInsert["$account|$domain|$locale"][$source] = array(
                    'account' => $account,
                    'domain' => $domain,
                    'source' => $source,
                    'locale' => $locale,
                    'target' => $target,
                    'filename' => $file->getRelativePathname()
                );
            }
        }
        
        $count = 0;
        foreach ($toInsert as $group) {
            foreach ($group as $key => $val) {
                $count++;
            }
        } 
        $em = $this->getContainer()->get('doctrine')->getManager();
        $repository = $em->getRepository("classesclassBundle:languages");
        $languages = $repository->findAll();
        $languageMap = array();
        foreach ($languages as $language) {
            $languageMap[$language->name] = $language->id; 
        }
        
        if (!$ignoreDuplicates) {
            if (count($duplicates) > 0) {
                $dupeTable = new Table($output);
                $dupeTable->setStyle("borderless");
                $dupeTable->setHeaders(array("Account","Domain","Locale","Source"));
                $dupeTable->setRows($duplicates);
                $output->writeln("The following translations have duplicate keys with different translations:");
                $dupeTable->render();
                return;
            }
        }
        
        $sourcesRepo = $this->getContainer()->get("doctrine")->getRepository("classesclassBundle:TranslationSources");
        $defaultTranslationRepo = $this->getContainer()->get("doctrine")->getRepository("classesclassBundle:DefaultTranslations");
        $accountsTranslationRepo = $this->getContainer()->get("doctrine")->getRepository("classesclassBundle:AccountsTranslations");
        
        $progress = new ProgressBar($output, $count);
        $progress->start();
        $results = array();

        foreach ($toInsert as $group) {
            foreach ($group as $val) {
                try {
                    $sources = $sourcesRepo->findBy(array('sourceKey'=>$val['source'], 'domain'=>$val['domain']));
                    $found = false;
                    foreach ($sources as $source) {
                        if ($source->sourceKey === $val['source'] && $source->domain === $val['domain']) {
                            $found = true;
                            break;
                        }
                    }
                    if (!$found) {
                        $source = new TranslationSources;
                        $source->sourceKey = $val['source'];
                        $source->domain = $val['domain'];
                        $em->persist($source);
                        $em->flush();
                    }
                    
                    if ($val['account'] === 'default') {
                        $translation = $defaultTranslationRepo->findOneBy(array('sourceId' => $source->id, 'languageId' => $languageMap[$val['locale']]));
                        if ($translation == null) {
                            $translation = new DefaultTranslations;
                            $translation->translationSource = $source;
                            $translation->languageId = $languageMap[$val['locale']];
                            $em->persist($translation);
                        }
                        $translation->content = $val['target'];
                        $results[] = array($val['filename'],$val['account'],$val['domain'],$val['source'],$val['locale'],"Success");
                    } 
                    else {
                        if (
                            isset($toInsert["default|{$val['domain']}|{$val['locale']}"][$val['source']]) && 
                            $toInsert["default|{$val['domain']}|{$val['locale']}"][$val['source']]['target'] != $val['target']
                        ) { 
                            $translation = $accountsTranslationRepo->findOneBy(array(
                                'sourceId' => $source->id,
                                'languageId' => $languageMap[$val['locale']],
                                'accountId' => $val['account']
                            ));
                            if ($translation == null) {
                                $translation = new AccountsTranslations;
                                $translation->translationSource = $source;
                                $translation->languageId = $languageMap[$val['locale']];
                                $translation->accountId = $val['account'];
                                $em->persist($translation);
                            }
                            $translation->content = $val['target'];
                            $results[] = array($val['filename'],$val['account'],$val['domain'],$val['source'],$val['locale'],"Success");
                        }
                    }
                }
                catch (Exception $ex) {
                    $results[] = array($val['filename'],$val['account'],$val['domain'],$val['source'],$val['locale'],"Failed");
                }
                $progress->advance();
            }
        }
        $em->flush();
        $progress->finish();
        $resultsTable = new Table($output);
        $resultsTable->setStyle("borderless");
        $resultsTable->setHeaders(array("File","Account","Domain","Source","Locale","Status"));
        $resultsTable->setRows($results);
        $resultsTable->render();
    }
}
