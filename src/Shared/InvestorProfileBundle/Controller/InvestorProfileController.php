<?php

namespace Shared\InvestorProfileBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

abstract class InvestorProfileController extends Controller
{
    protected $tablename;
    protected $findBy;
    protected $twigParameters;
    protected $planid;
    protected $userid;

    public function indexAction() {

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsConfiguration');
        $ourplan = $repository->findOneBy($this->findBy);
        $headercolor = "#ffffff";
        if ($ourplan != null) {
            $headercolor = $ourplan->headerColor;
        }

        return $this->render('SharedInvestorProfileBundle:Default:index.html.twig', array_merge($this->twigParameters,
            array(
                'headerColor' => $headercolor)
        ));
    }

    public function investmentsScoreIndexAction() {

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsScore');
        $investmentsScores = $repository->findBy($this->findBy, array('min' => 'ASC'));

        $qb = $this->getDoctrine()->getEntityManager()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsConfigurationFundsGroupValuesColors')->createQueryBuilder('p');
        $groupsValuesColors = $qb->select('p.scoreMinId')
            ->distinct(true)
            ->where($this->getWhereClause())
            ->setParameter('findby', $this->getParameterClause())
            ->getQuery()
            ->getArrayResult();

        $scoreIdWithAssign = array();
        foreach ($groupsValuesColors as $row) {
            array_push($scoreIdWithAssign, $row['scoreMinId']);
        }

        return $this->render('SharedInvestorProfileBundle:Default:investmentsScoreIndex.html.twig', array_merge($this->twigParameters,
            array(
                'investmentsScores' => $investmentsScores,
                'scoreIdWithAssign' => $scoreIdWithAssign)
        ));

    }

    public function investmentsScoreEditAction($id, $previd) {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsScore');
        $investmentsScore = $repository->find($id);

        $prevmin = $repository->find($previd);
        return $this->render("SharedInvestorProfileBundle:Default:investmentsScoreEdit.html.twig", array(
            "action" => "edit",
            "investmentsScore" => $investmentsScore,
            "prevmin" => $prevmin
        ));
    }

    public function investmentsScoreDeleteAction($id, $previd) {

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsScore');
        $investmentsScore = $repository->find($id);

        $previous = $repository->find($previd);
        return $this->render("SharedInvestorProfileBundle:Default:investmentsScoreDelete.html.twig", array(
            "action" => "edit",
            "investmentsScore" => $investmentsScore,
            'previd' => $previous
        ));
    }

    public function investmentsScoreDeletedAction(Request $request) {

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsScore');
        $em = $this->getDoctrine()->getManager();

        $id = $request->get("id");
        $previd = $request->get("previd");

        $investmentsScore = $repository->find($id);
        $em->remove($investmentsScore);
        $prevScore = $repository->find($previd);

        if ($prevScore->min == 0) {
            $em->remove($prevScore);
        }

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsConfigurationFundsGroupValuesColors');
        $items = $repository->findBy(array('scoreMinId' => $previd));
        foreach ($items as $row) {
            $em->remove($row);
        }

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsLinkYearsToRetirementAndScore');
        $items = $repository->findBy(array_merge($this->findBy, array('InvestmentsScoreId' => $previd)));
        foreach ($items as $row) {
            $em->remove($row);
        }

        $em->flush();
        return new Response("success");
    }

    public function investmentsScoreAddAction() {

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsScore');
        $result = $repository->findBy($this->findBy, array('min' => 'DESC'));

        $maxvalue = -1;
        if ($result != null) {
            $maxvalue = $result[0]->min;
        }

        return $this->render("SharedInvestorProfileBundle:Default:investmentsScoreEdit.html.twig" , array(
            "action" => "add",
            "maxvalue" => $maxvalue
        ));
    }

    private function _addzero($repository, $arr, &$func) {
        $em = $this->getDoctrine()->getManager();

        if (array_key_exists('planid', $arr)) {
            $ifexists = $repository->findOneBy(array('planid' => $arr['planid'], 'min' => 0 ));
        } else {
            $ifexists = $repository->findOneBy(array('userid' => $arr['userid'], 'min' => 0 ));
        }

        if ($ifexists == null) {
            $addzero = $func;
            foreach ($arr as $key => $value) {
                $addzero->$key = $value;
            }
            $em->persist($addzero);
            $em->flush();
        }

    }

    public function investmentsScoreSavedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsScore');
        $arr = array_merge($this->findBy, array('userid' => $this->userid, 'min' => 0));

        $whichEntity = $this->getInvestmentsScoreEntity();

        if ($request->get('min') == 0) {
            $this->_addzero($repository, $arr, new $whichEntity);

        }

        if ($request->get("action") == "add") {
            $investmentsScore = new $whichEntity;
            $em->persist($investmentsScore);
        } else {
            $investmentsScore = $repository->find($request->get("id"));
        }

        if ($investmentsScore != null) {
            $investmentsScore->planid = $this->planid;
            $investmentsScore->userid = $this->userid;
            $investmentsScore->min = $request->get("max");
            $em->flush();
        }

        return new Response("success");
    }

    public function investmentsYearsToRetirementDeletedAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsYearsToRetirement');
        $em = $this->getDoctrine()->getManager();

        $id = $request->get("id");
        $previd = $request->get("previd");
        $investmentsYearsToRetirement = $repository->find($id);
        $em->remove($investmentsYearsToRetirement);

        $prevYear = $repository->find($previd);
        if ($prevYear->min == 0) {
            $em->remove($prevYear);
        }

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsLinkYearsToRetirementAndScore');
        $items = $repository->findBy(array_merge($this->findBy, array('InvestmentsYearsToRetirementId' => $id)));
        foreach ($items as $row) {
            $em->remove($row);
        }

        $em->flush();
        return new Response("success");
    }

    public function investmentsYearsToRetirementIndexAction() {

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsYearsToRetirement');
        $investmentsYears = $repository->findBy($this->findBy, array('min' => 'ASC'));

        return $this->render('SharedInvestorProfileBundle:Default:investmentsYearsToRetirementIndex.html.twig', array_merge($this->twigParameters,
            array(
                'investmentsYears' => $investmentsYears)
        ));

    }

    public function investmentsYearsToRetirementAddAction() {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsYearsToRetirement');
        $result = $repository->findBy($this->findBy, array('min' => 'DESC'));

        $maxvalue = -1;
        if ($result != null) {
            $maxvalue = $result[0]->min;
        }

        return $this->render("SharedInvestorProfileBundle:Default:investmentsYearsToRetirementEdit.html.twig" , array(
            "action" => "add",
            "maxvalue" => $maxvalue
        ));
    }

    public function investmentsYearsToRetirementSavedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsYearsToRetirement');
        $arr = array_merge($this->findBy, array('userid' => $this->userid, 'min' => 0));

        $whichEntity = $this->getInvestmentsYearsToRetirementEntity();

        if ($request->get("min") == 0) {
            $this->_addzero($repository, $arr, new $whichEntity);
        }


        if ($request->get("action") == "add") {
            $investmentsYearsToRetirement = new $whichEntity;
            $em->persist($investmentsYearsToRetirement);
        }
        else {
            $investmentsYearsToRetirement = $repository->find($request->get("id"));
        }

        if ($investmentsYearsToRetirement != null) {
            $investmentsYearsToRetirement->planid = $this->planid;
            $investmentsYearsToRetirement->userid = $this->userid;
            $investmentsYearsToRetirement->min = $request->get("max");

            $em->flush();
        }
        return new Response("success");
    }

    public function investmentsYearsToRetirementEditAction($id, $previd) {

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsYearsToRetirement');
        $investmentsYearsToRetirement = $repository->find($id);

        $prevmin = $repository->find($previd);

        return $this->render("SharedInvestorProfileBundle:Default:investmentsYearsToRetirementEdit.html.twig", array(
            "action" => "edit",
            "investmentsYearsToRetirement" => $investmentsYearsToRetirement,
            "prevmin" => $prevmin
        ));
    }

    public function investmentsYearsToRetirementDeleteAction($id, $previd) {

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsYearsToRetirement');
        $investmentsYearsToRetirement = $repository->find($id);

        $previous = $repository->find($previd);
        return $this->render("SharedInvestorProfileBundle:Default:investmentsYearsToRetirementDelete.html.twig", array(
            "action" => "edit",
            "investmentsYearsToRetirement" => $investmentsYearsToRetirement,
            'previd' => $previous
        ));
    }


    public function investmentsYearsToRetirementScoreIndexAction() {
        $whichEntity = $this->getInvestmentsConfigurationColorsEntity();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsScore');
        $investmentsScores = $repository->findBy($this->findBy, array('min' => 'ASC'));

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsYearsToRetirement');
        $investmentsYears = $repository->findBy($this->findBy, array('min' => 'ASC'));

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsLinkYearsToRetirementAndScore');
        $investmentsScoresYears = $repository->findBy($this->findBy);

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsConfigurationColors');
        $configurationColors = $repository->findOneBy($this->findBy);

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . strtolower($this->tablename) . 'Modules');
        $modules = $repository->findOneBy($this->findBy);
        $enabled = false;
        if ($modules->investmentsGraph == 1) {
            $enabled = true;
        }

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsConfiguration');
        $ourplan = $repository->findOneBy($this->findBy);


        if ($configurationColors == null) {
            $em = $this->getDoctrine()->getManager();
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:DefaultInvestmentsConfigurationColors');
            $defaultColor = $repository->findOneBy(array('userid' => $this->userid));
            $newCC = new $whichEntity;
            $newCC->planid = $this->planid;
            $newCC->userid = $this->userid;
            $newCC->leftColor = "gray";
            $newCC->middleColor = "black";
            $newCC->rightColor = "green";
            if ($defaultColor != null) {
                $newCC->leftColor = $defaultColor->leftColor;
                $newCC->middleColor = $defaultColor->middleColor;
                $newCC->rightColor = $defaultColor->rightColor;
            }
            $em->persist($newCC);
            $em->flush();
            $configurationColors = $newCC;
        }

        $investmentsScoresYearsArray = array();
        foreach ($investmentsScoresYears as $val) {
            $investmentsScoresYearsArray[$val->InvestmentsScoreId][$val->InvestmentsYearsToRetirementId] = $val;
        }

        $headercolor = "#ffffff";
        if ($ourplan != null) {
            $headercolor = $ourplan->headerColor;
        }

        return $this->render("SharedInvestorProfileBundle:Default:investmentsYearsToRetirementScoreIndex.html.twig", array(
            'investmentsScores' => $investmentsScores,
            'investmentsYears' => $investmentsYears,
            'investmentsScoresYearsArray' => $investmentsScoresYearsArray,
            'headerColor' => $headercolor,
            'configurationColors' => $configurationColors,
            'enabled' => $enabled
        ));
    }

    private function _buildPortfolioMap() {
        return array('Conservative' => 0, 'Moderate Conservative' => 1, 'Moderate' => 2, 'Moderate Aggressive' => 3, 'Aggressive' => 4);
    }

    public function investmentsYearsToRetirementScoreSavedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . strtolower($this->tablename) . 'Portfolios');
        $portfolios = $repository->findBy($this->findBy);
        foreach ($portfolios as $portfolio) {
            $em->remove($portfolio);
        }

        $portfolioDefinitionMap = $this->_buildPortfolioMap();
        $scoresMap = $this->_generateScoresMap();

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsLinkYearsToRetirementAndScore');
        $whichEntity = $this->getInvestmentsLinkYearsToRetirementAndScoreEntity();
        $whichPortfolioEntity = $this->getPortfolioEntity();


        $data = $request->request->get('investmentsScoresYears');

        foreach ($data as $investmentsScoreId => $investmentsScoresYearsTypes) {
            $i = 0;
            foreach ($investmentsScoresYearsTypes as $investmentsYearsToRetirementId => $type) {
                if (!empty($type)) {
                    $investmentsScoresYears = $repository->findOneBy(array_merge($this->findBy, array(
                        'InvestmentsScoreId' => $investmentsScoreId,
                        'InvestmentsYearsToRetirementId' => $investmentsYearsToRetirementId
                    )));
                    if ($investmentsScoresYears == null) {
                        $investmentsScoresYears = new $whichEntity;
                        $investmentsScoresYears->planid = $this->planid;
                    }
                    $investmentsScoresYears->userid = $this->userid;
                    $investmentsScoresYears->InvestmentsScoreId = $investmentsScoreId;
                    $investmentsScoresYears->InvestmentsYearsToRetirementId = $investmentsYearsToRetirementId;
                    $investmentsScoresYears->type = $type;
                    $em->persist($investmentsScoresYears);

                    $portfolio = new $whichPortfolioEntity();
                    $portfolio->userid = $this->userid;
                    $portfolio->planid = $this->planid;
                    $portfolio->name = $type;
                    $portfolio->profile = $portfolioDefinitionMap[$type];
                    $portfolio->minscore = $scoresMap[$investmentsScoreId]->low;
                    $portfolio->maxscore = $scoresMap[$investmentsScoreId]->high;
                    $portfolio->thMinscore = $i;
                    $portfolio->thMaxscore = $i;
                    $em->persist($portfolio);

                }
                $i++;
            }
        }
        $em->flush();

        return new Response("success");
    }

    public function saveRiskRewardBarAction (Request $request) {
        $em = $this->getDoctrine()->getManager();
        $whichEntity = $this->getInvestmentsConfigurationColorsEntity();

        $leftColor = $request->request->get("leftColor");
        $rightColor = $request->request->get("rightColor");
        $middleColor = $request->request->get("middleColor");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsConfigurationColors');
        $colorConfig = $repository->findOneBy($this->findBy);
        $newCC = new $whichEntity;

        if ($colorConfig != null) {
            $em->remove($colorConfig);
        }

        $newCC->planid = $this->planid;
        $newCC->userid = $this->userid;
        $newCC->leftColor = $leftColor;
        $newCC->rightColor = $rightColor;
        $newCC->middleColor = $middleColor;
        $em->persist($newCC);

        $em->flush();

        return new Response("success");
    }

    public function saveHeaderColorAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $headerColor = $request->request->get("headerColor");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsConfiguration');
        $theplan = $repository->findOneBy($this->findBy);
        $whichEntity = $this->getInvestmentsConfigurationEntity();

        if ($theplan != null)  {
            $em->remove($theplan);
        }

        $newplan = new $whichEntity;
        $newplan->planid = $this->planid;
        $newplan->userid = $this->userid;
        $newplan->headerColor = $headerColor;
        $em->persist($newplan);

        $em->flush();

        return new Response("success");
    }

    public function investmentsScoreAssignColorAction(Request $request) {
        $scoreMinId = $request->get('id');

        $repository = $this->getDoctrine()->getRepository($this->getinvestmentsScoreAssignColorActionRepository());
        $groupid = $repository->findOneBy($this->getinvestmentsScoreAssignColorActionFindBy())->fundGroupid;

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsConfigurationFundsGroupValuesColors');
        $items = $repository->findBy(array_merge($this->findBy, array('scoreMinId' => $scoreMinId)));

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:FundsGroupsValues');
        $arrayOfGroupValues = $repository->findBy(array('groupid' => $groupid, 'deleted' => 0));

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsConfigurationColors');
        $configurationColors = $repository->findOneBy($this->findBy);

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsConfigurationDefaultFundsGroupValuesColors');
        $defaultColors = $repository->findBy($this->findBy);
        
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsScore');
        $scoreMin = $repository->findOneBy( array('id' => $scoreMinId));

        

        $totalpercent = 0;
        $objecthash = array();
        foreach ($items as $item) {
            $objecthash[$item->fundGroupValueId] = $item;
            $totalpercent += $item->min;
        }

        $defaultColorsMap = array();

        foreach ($defaultColors as $row) {
            $defaultColorsMap[$row->fundGroupValueId] = $row;
            if(array_key_exists($row->fundGroupValueId, $objecthash)) {
                if ($objecthash[$row->fundGroupValueId]->isCustom == 0) {
                    $objecthash[$row->fundGroupValueId]->color = $row->color;
                    $objecthash[$row->fundGroupValueId]->hoverColor = $row->hoverColor;
                }
            }
        }

        return $this->render('SharedInvestorProfileBundle:Default:piechart.html.twig', array('defaultColorsMap' => $defaultColorsMap,
            'arrayOfGroupValues' => $arrayOfGroupValues, 'scoreMinId' => $scoreMinId, 'totalPercent' => $totalpercent, 'objectHash' => $objecthash, 'configurationColors' => $configurationColors,"scoreMin" => $scoreMin));
    }

    public function investmentsScoreAssignColorAllAction(Request $request) {

        $repository = $this->getDoctrine()->getRepository($this->getinvestmentsScoreAssignColorActionRepository());
        $groupid = $repository->findOneBy($this->getinvestmentsScoreAssignColorActionFindBy())->fundGroupid;

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:FundsGroupsValues');
        $arrayOfGroupValues = $repository->findBy(array('groupid' => $groupid, 'deleted' => 0));

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsConfigurationDefaultFundsGroupValuesColors');
        $defaultColors = $repository->findBy($this->findBy);

        $totalpercent = 0;
        $objecthash = array();
        foreach ($defaultColors as $item) {
            $objecthash[$item->fundGroupValueId] = $item;
            $totalpercent += $item->min;
        }

        return $this->render('SharedInvestorProfileBundle:Default:piechartdefaults.html.twig', array(
            'arrayOfGroupValues' => $arrayOfGroupValues, 'totalPercent' => $totalpercent, 'objectHash' => $objecthash));
    }


    public function investmentsScoreAssignColorSavedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $scoreMinId = $request->request->get('id');
        $totalpercent = 0;

        $defaultColorRepository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsConfigurationDefaultFundsGroupValuesColors');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsConfigurationFundsGroupValuesColors');
        foreach($request->request->all() as $key => $value) {
            if (strpos($key, 'percent-id-') !== false) {
                if (strpos($key, 'percent-id-undeclared') !== false) {
                    $newconfig = $this->getInvestmentsConfigurationFundsGroupValuesColors();

                    $newconfig->planid = $this->planid;
                    $myColor = $request->request->get(str_replace("percent-id-", "display-cp-", "$key"));
                    $myHoverColor = $request->request->get(str_replace("percent-id-", "hover-cp-", "$key"));
                    $fundGroupValueId = str_replace("percent-id-undeclared-", "", "$key");

                    $existing = $repository->findBy(array_merge($this->findBy, array('fundGroupValueId' => $fundGroupValueId, 'scoreMinId' => $scoreMinId)));
                    if ($existing == null) { // check for double submit
                        $newconfig->userid = $this->userid;
                        $newconfig->min = $value;
                        $newconfig->color = $myColor;
                        $newconfig->hoverColor = $myHoverColor;
                        $newconfig->fundGroupValueId = $fundGroupValueId;
                        $newconfig->scoreMinId = $scoreMinId;
                        $newconfig->isCustom = 0;
                        $defaultColors = $defaultColorRepository->findOneBy(array_merge($this->findBy, array('fundGroupValueId' => $fundGroupValueId)));
                        if ($defaultColors->color != $newconfig->color || $newconfig->hoverColor != $defaultColors->hoverColor) {
                            $newconfig->isCustom = 1;
                        }

                        $em->persist($newconfig);
                    }
                } else {
                    $colorid = str_replace("percent-id-", "", "$key");
                    $myColor = $request->request->get(str_replace("percent-id-", "display-cp-", "$key"));
                    $myHoverColor = $request->request->get(str_replace("percent-id-", "hover-cp-", "$key"));
                    $item = $repository->findOneBy(array('id' => $colorid));
                    $item->color = $myColor;
                    $item->hoverColor = $myHoverColor;
                    $item->min = $value;
                    $defaultColors = $defaultColorRepository->findOneBy(array_merge($this->findBy, array('fundGroupValueId' => $item->fundGroupValueId)));
                    if ($defaultColors->color != $item->color || $item->hoverColor != $defaultColors->hoverColor) {
                        $item->isCustom = 1;
                    }
                    $em->persist($item);
                }
                $totalpercent += $value;
            }
        }
        
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsScore');
        $scoreMin = $repository->findOneBy( array('id' => $request->request->get("id")));
        $label = $request->request->get("label",null);
        if ($label == "")
        {
            $label = null;
        }
        $scoreMin->label = $label;

        if ($totalpercent == 100) {
            $em->flush();
            return new Response('success');
        }
        
        

        return new Response('fail');
    }

    public function investmentsScoreAssignColorSavedAllAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsConfigurationDefaultFundsGroupValuesColors');
        foreach($request->request->all() as $key => $value) {
            if (strpos($key, 'display-cp-') !== false) {
                if (strpos($key, 'display-cp-undeclared') !== false) {
                    $newconfig = $this->getInvestmentsConfigurationDefaultFundsGroupValuesColors();

                    $newconfig->planid = $this->planid;
                    $myColor = $request->request->get($key);
                    $myHoverColor = $request->request->get(str_replace("display-cp-", "hover-cp-", "$key"));
                    $fundGroupValueId = str_replace("display-cp-undeclared-", "", "$key");

                    $existing = $repository->findBy(array_merge($this->findBy, array('fundGroupValueId' => $fundGroupValueId)));
                    if ($existing == null) { // check for double submit
                        $newconfig->userid = $this->userid;
                        $newconfig->color = $myColor;
                        $newconfig->hoverColor = $myHoverColor;
                        $newconfig->fundGroupValueId = $fundGroupValueId;
                        $em->persist($newconfig);
                    }
                } else {
                    $colorid = str_replace("display-cp-", "", "$key");
                    $myColor = $request->request->get($key);
                    $myHoverColor = $request->request->get(str_replace("display-cp-", "hover-cp-", "$key"));
                    $item = $repository->findOneBy(array('id' => $colorid));
                    $item->color = $myColor;
                    $item->hoverColor = $myHoverColor;
                    $em->persist($item);
                }
            }
        }

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsConfigurationFundsGroupValuesColors');
        $colors = $repository->findBy($this->findBy);
        foreach ($colors as $row) {
            $row->isCustom = 0;
            $em->persist($row);
        }
        $em->flush();
        return new Response('success');

    }

    public function investmentsOptionsIndexAction() {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . strtolower($this->tablename) . 'Modules');
        $investmentsGraph = $repository->findOneBy($this->findBy)->investmentsGraph;

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsConfigurationColors');
        $configurationColors = $repository->findOneBy($this->findBy);

        return $this->render('SharedInvestorProfileBundle:Default:options.html.twig', array('investmentsGraph' => $investmentsGraph, 'configurationColors' => $configurationColors));
    }

    private function _generateScoresMap() {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsScore');
        $scores = $repository->findBy($this->findBy);
        $scoresMap = array();

        for ($i = 1; $i < count($scores); $i++) {
            $scoresMap[$scores[$i-1]->id]->low = $scores[$i-1]->min;
            if ($scoresMap[$scores[$i-1]->id]->low != 0) {
                $scoresMap[$scores[$i-1]->id]->low++;
            }
            $scoresMap[$scores[$i-1]->id]->high = $scores[$i]->min;
        }

        return $scoresMap;
    }

    public function saveOptionsAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . strtolower($this->tablename) . 'Modules');
        $module = $repository->findOneBy($this->findBy);

        $module->investmentsGraph = $request->request->get('graph-enabled');
        if ($module->investmentsGraph == 0) {
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . strtolower($this->tablename) . 'Portfolios');
            $portfolios = $repository->findBy($this->findBy);
            foreach ($portfolios as $portfolio) {
                $em->remove($portfolio);
            }
        } else {
            $portfolioDefinitionMap = $this->_buildPortfolioMap();
            $scoresMap = $this->_generateScoresMap();

            $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'InvestmentsLinkYearsToRetirementAndScore');
            $whichPortfolioEntity = $this->getPortfolioEntity();
            $associations = $repository->findBy($this->findBy);

            $associationMap = array();
            foreach ($associations as $association) {
                if (!isset($associationMap[$association->InvestmentsScoreId])) {
                    $associationMap[$association->InvestmentsScoreId] = array();
                }
                $associationMap[$association->InvestmentsScoreId][] = $association;
            }
            foreach($associationMap as $key => $value) {
                $i = 0;
                foreach ($value as $association) {
                    $portfolio = new $whichPortfolioEntity();
                    $portfolio->userid = $this->userid;
                    $portfolio->planid = $this->planid;
                    $portfolio->name = $association->type;
                    $portfolio->profile = $portfolioDefinitionMap[$association->type];
                    $portfolio->minscore = $scoresMap[$association->InvestmentsScoreId]->low;
                    $portfolio->maxscore = $scoresMap[$association->InvestmentsScoreId]->high;
                    $portfolio->thMinscore = $i;
                    $portfolio->thMaxscore = $i;
                    $i++;
                    $em->persist($portfolio);
                }
            }
        }
        $em->flush();

        return new Response('success');
    }

    public abstract function getInvestmentsScoreEntity();
    public abstract function getInvestmentsConfigurationEntity();
    public abstract function getInvestmentsYearsToRetirementEntity();
    public abstract function getInvestmentsConfigurationColorsEntity();
    public abstract function getInvestmentsLinkYearsToRetirementAndScoreEntity();
    public abstract function getInvestmentsConfigurationFundsGroupValuesColors();
    public abstract function getInvestmentsConfigurationDefaultFundsGroupValuesColors();

    public abstract function getinvestmentsScoreAssignColorActionRepository();
    public abstract function getinvestmentsScoreAssignColorActionFindBy();

    public abstract function getWhereClause();
    public abstract function getParameterClause();

    public abstract function getPortfolioEntity();

}
