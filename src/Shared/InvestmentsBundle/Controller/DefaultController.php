<?php

namespace Shared\InvestmentsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SharedInvestmentsBundle:Default:index.html.twig', array('name' => $name));
    }
}
