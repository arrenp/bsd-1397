<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();

$collection->add('shared_library_homepage', new Route('/hello/{name}', array(
    '_controller' => 'SharedLibraryBundle:Default:index',
)));

return $collection;
