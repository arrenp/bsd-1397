<?php

namespace Shared\OutreachBundle\Classes;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Shared\RecordkeepersBundle\Classes\recordKeeper;
use Shared\General\GeneralMethods;

class outreach
{
	public $em;
	public $userid;//current userid
	public $adviserid;//current adviserid if adviser
	public $templates;


	public function templates($type)
	{
		$methods = new GeneralMethods($this->em);
		$repository = $this->em->getRepository('classesclassBundle:accounts');
		$account = $repository->findOneBy(array("id" => $this->userid));
		$integrated = $methods->isIntegrated($account);
		if ($integrated)
		$integrated = 1;//integrated
		else
		$integrated = 2;//non integrated

		
		$em = $this->em->getManager();
		$query = $em->createQuery(
		"SELECT p
		FROM classesclassBundle:outreachCategories p
		WHERE p.integrated = 0
		OR p.integrated = :integrated OR p.accounts LIKE '%%".$this->userid."%%'
		ORDER BY p.orderid ASC"
		)->setParameter('integrated',$integrated);
		$categories = $query->getResult();;
		foreach ($categories as $key => $category)
		{
			$filter = 0;
			$findbycategory = array('categoryid' => $category->id,"userid" => $this->userid,"adviserid" => $this->adviserid);
			if ($type == "plans")
			{
				$repository = $this->em->getRepository('classesclassBundle:outreachCategoriesDeny');
				$searchedCategory = $repository->findBy($findbycategory );

				if (count($searchedCategory) > 0)
				{
					$filter = 1;
					unset($categories[$key]);
				}
			}
			if ($type == "settings")
			{
			$repository = $this->em->getRepository('classesclassBundle:outreachCategoriesDeny');
			$searchedCategory = $repository->findBy($findbycategory );
			$category->checked = "checked";
			if (count($searchedCategory) > 0)
			{
			$category->checked = "";
			}


			}
			if (!$filter)
			{
				$em = $this->em->getManager();
				$query = $em->createQuery(
				"SELECT p
				FROM classesclassBundle:outreachTemplates p
				WHERE ( p.integrated = 0
				OR p.integrated = :integrated OR p.accounts LIKE '%%".$this->userid."%%' ) AND p.categoryid = ".$category->id."
				ORDER BY p.name ASC"
				)->setParameter('integrated',$integrated);
				$templates = $query->getResult();;
				if (count($templates) > 0)
				$categories[$key]->templates = $templates;
			}
		}

		foreach ($categories as $keyCategory => $category)
		{
			if (isset($category->templates))
			foreach ($category->templates as  $keyTemplate => $template)
			{

				$findbytemplate = array('templateid' => $template->id,"userid" => $this->userid,"adviserid" => $this->adviserid);


				if ($type == "plans")
				{
					$repository = $this->em->getRepository('classesclassBundle:outreachTemplatesDeny');
					$searchedTemplate = $repository->findBy($findbytemplate);
					if (count($searchedTemplate) > 0)
					{
						unset($categories[$keyCategory]->templates[$keyTemplate]);
					}
				}
				if ($type == "settings")
				{
					$repository = $this->em->getRepository('classesclassBundle:outreachTemplatesDeny');
					$searchedTemplate = $repository->findBy($findbytemplate);
					$categories[$keyCategory]->templates[$keyTemplate]->checked = "checked";
					if (count($searchedTemplate) > 0)
					{
						$categories[$keyCategory]->templates[$keyTemplate]->checked = "";
					}
				}
			}
		}

	return $categories;

	}

	public function alltemplates()
	{
		$repository = $this->em->getRepository('classesclassBundle:outreachCategories');
		$categories = $repository->findBy(array(),array("orderid"=>"ASC"));

		foreach ($categories as $category)
		{
			$repository = $this->em->getRepository('classesclassBundle:outreachTemplates');
			$templates= $repository->findBy(array("categoryid" => $category->id),array('name' => 'ASC'));
			$category->templates = $templates;

		}

		return $categories;

	}
        public function allnewtemplates()
        {
 		$repository = $this->em->getRepository('classesclassBundle:outreachNewCategories');
		$categories = $repository->findBy(array(),array("name"=>"ASC"));

		foreach ($categories as $category)
		{
			$repository = $this->em->getRepository('classesclassBundle:outreachNewTemplates');
			$templates= $repository->findBy(array("categoryid" => $category->id),array('name' => 'ASC'));
			$category->templates = $templates;
		}
		return $categories;           
        }
        public function sponsorConnectTemplates()
        {
            $repository = $this->em->getRepository('classesclassBundle:outreachNewCategories');
            $category = $repository->findOneBy(array("name" =>"SmartConnect"),array("name"=>"ASC"));
            $repository = $this->em->getRepository('classesclassBundle:outreachNewTemplates');
            $templates= $repository->findBy(array("categoryid" => $category->id),array('name' => 'ASC'));
            
            return $templates;  
        }

   	public function __construct($em,$userid = 0,$adviserid = 0)
	{
		$this->em = $em;
		$this->userid = $userid;
		$this->adviserid = $adviserid;
	}

}
