<?php

namespace Shared\GeneralBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
set_time_limit(0);

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SharedGeneralBundle:Default:index.html.twig', array('name' => $name));
    }

    public function saveAndUpdateAction($type, Request $request) {
        $entityPlansMapping = array('Funds' => "plansFunds", 'Documents' => "plansDocuments", 'Messages' => 'PlansMessages');
        $entityDefaultsMapping = array('Funds' => "defaultFunds", 'Documents' => "defaultDocuments", 'Messages' => 'DefaultMessages');
        $object = "classes\\classBundle\\Entity\\plans{$type}";

        $planidsfromjs = $request->request->all();
        $session = $this->get('session');
        $userid = $session->get('userid');
        $em = $this->getDoctrine()->getManager();

        $sql = "delete from classesclassBundle:{$entityPlansMapping[$type]} p where p.userid = {$userid}";
        if(empty(count($planidsfromjs))) {
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
            $plans = $repository->findBy(array('userid' => $userid, 'deleted' => 0));
        } else {
            $sql .= " and (p.planid =";
            $planstemp = array_keys($planidsfromjs);
            $plans = array();
            foreach ($planstemp as $plantemp) {
                $tempObj = new \stdClass();
                $tempObj->id = $plantemp;
                $plans[] = $tempObj;
                $sql .= " {$plantemp} or p.planid =";
            }
            $sql = substr($sql, 0, -14) . ")";
        }

        $q = $em->createQuery($sql);
        $numDeleted = $q->execute();

        $repository = $this->getDoctrine()->getRepository("classesclassBundle:{$entityDefaultsMapping[$type]}");
        $defaultTable = $repository->findBy(array('userid' => $userid));


        $session->set("saveAndUpdateCurrent", 0);
        $session->set("saveAndUpdateTotal", count($plans));
        $session->save();
        $i = 1;
        $planindex = 0;
        foreach ($plans as $plan) {
            $session->set("saveAndUpdateCurrent", ++$planindex);
            $session->save();
            foreach ($defaultTable as $defaultRow) {
                $newObject = new $object;
                foreach(get_object_vars($defaultRow) as $column => $value) {
                    $newObject->$column = $value;
                }
                $newObject->planid = $plan->id;

                $em->persist($newObject);
                if ($i++ % 500 == 0) {
                    $em->flush();
                }
            }
        }
        $em->flush();

        return new Response($numDeleted);
    }


    public function checkProgressAction(Request $request) {
        $session = $request->getSession();
        $current = $session->get("saveAndUpdateCurrent");
        $total = $session->get("saveAndUpdateTotal");
        $session->save();
        return new JsonResponse(compact("current", "total"));
    }

    public function selectPlansIndexAction($type) {
        $session = $this->get('session');
        $userid = $session->get('userid');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $plans = $repository->findBy(array('userid' => $userid, 'deleted' => 0), array('id' => 'DESC'));

        return $this->render('SharedGeneralBundle:Default:defaultselectplans.html.twig', array('pagename' => 'settings', 'plans' => $plans, 'type' => $type));
    }
}
