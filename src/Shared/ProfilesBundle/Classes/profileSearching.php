<?php
namespace Shared\ProfilesBundle\Classes;


class profileSearching
{
	public $profiles;
	public $lastid;
	public $decryptionKey;
	public $sectionDescriptions;
	public $sectionNames;
	public $sectionTypes;
	public $sectionDatabases;
	public $conn;

	public function addSearchField($description,$name,$type,$database)
	{
		$this->sectionDescriptions[] = $description;
		$this->sectionNames[] = $database."_".$name;
		$this->sectionTypes[] = $type;
		$this->sectionDatabases[] = $database;
	}

	public function __construct($this)
	{
		$this->profiles = array();
		$this->sectionDescriptions = array();
		$this->sectionNames = array();
		$this->sectionTypes = array();
		$this->sectionDatabases = array();
		$this->lastid = 0;
		$this->decryptionKey = '5E6D217336';
		$this->conn = $this->get('doctrine.dbal.default_connection');
	}




}

?>