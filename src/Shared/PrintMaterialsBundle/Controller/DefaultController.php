<?php

namespace Shared\PrintMaterialsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SharedPrintMaterialsBundle:Default:index.html.twig', array('name' => $name));
    }
}
