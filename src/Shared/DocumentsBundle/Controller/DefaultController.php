<?php

namespace Shared\DocumentsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SharedDocumentsBundle:Default:index.html.twig', array('name' => $name));
    }
}
