<?php

namespace Shared\vadsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SharedvadsBundle:Default:index.html.twig', array('name' => $name));
    }
}
