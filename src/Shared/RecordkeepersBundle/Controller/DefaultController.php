<?php

namespace Shared\RecordkeepersBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SharedRecordkeepersBundle:Default:index.html.twig', array('name' => $name));
    }
}
