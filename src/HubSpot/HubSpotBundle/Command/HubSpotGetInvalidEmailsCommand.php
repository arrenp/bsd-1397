<?php

namespace HubSpot\HubSpotBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use HubSpot\HubSpotBundle\Services\HubSpotContactsService;
use classes\classBundle\Entity\launchToolRecipients;
use Doctrine\Common\Collections\Criteria;

class HubSpotGetInvalidEmailsCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('app:hubSpotGetInvalidEmails')
            ->setDescription('Displays a list of emails that failed to import to hubspot')
            ->setHelp('run it');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $hubSpotService = $this->getContainer()->get('hub_spot.contacts');

        $qb = $this->getContainer()->get('doctrine')->getEntityManager()->getRepository('classesclassBundle:launchToolRecipients')->createQueryBuilder('ltr')
            ->select('DISTINCT ltr.email')
            ->leftJoin('classesclassBundle:plans', 'p', \Doctrine\ORM\Query\Expr\Join::WITH, 'ltr.planid = p.id')
            ->where('p.deleted = 0 and p.participantDetails = 1')
            ->getQuery();

        $emails = $qb->getResult();

        $arr = array();
        $emailMap = array();
        foreach ($emails as $item) {
            if (!filter_var($item['email'], FILTER_VALIDATE_EMAIL)) {
                continue;
            }
            $domainTestVar = strtolower('domaintest' . strrchr( $item['email'] , '@' ));
            $emailMap[$domainTestVar][] = $item['email'];
            array_push($arr, $domainTestVar);
        }

        $domaintest = array_unique($arr);

        foreach($domaintest as $item) {
            $properties = [];
            $properties['email'] = $item;
            $hubSpotService->addToBatch($item, $properties);
        }
        $hubSpotService->flushBatch();
        $invalidEmails = $hubSpotService->getInvalidEmails();

        $output->writeln("Invalid Emails are:");
        foreach ($invalidEmails as $email) {
            foreach($emailMap[$email] as $item) {
                $output->writeln($item);
            }
        }
    }
}