<?php

namespace HubSpot\HubSpotBundle\Command;

use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use HubSpot\HubSpotBundle\Services\HubSpotContactsService;
use Doctrine\ORM\EntityManager;
use classes\classBundle\Entity\sponsorConnectRequests;
use classes\classBundle\Entity\plans;
use classes\classBundle\Entity\accounts;
use classes\classBundle\MappingTypes\EncryptedType;

class SponsorConnectCommand extends ContainerAwareCommand
{

    /**
     * @var HubSpotContactsService
     */
    private $hubSpotService;

    protected function configure()
    {
        $this
            ->setName('app:sponsorconnect')
            ->setDescription('Use this command to migrate sponsors to HubSpot')
            ->setHelp('Use this command to migrate sponsors to HubSpot');
    }

    protected function getField($key, $array)
    {
        return trim(array_key_exists($key, $array) ? $array[$key] : '');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->hubSpotService = $this->getContainer()->get('hub_spot.contacts');

        $sql = "
        select
            IFNULL(AES_DECRYPT(scr.toEmailAddress, UNHEX('".EncryptedType::KEY."')),'') as toEmailAddress,
            scr.fromEmailAddress,
            scr.fromFirstName,
            scr.fromLastName,
            IFNULL(AES_DECRYPT(scr.toFirstName, UNHEX('".EncryptedType::KEY."')),'') as toFirstName,
            IFNULL(AES_DECRYPT(scr.toLastName, UNHEX('".EncryptedType::KEY."')),'') as toLastName,
            p.providerLogoImage,
            p.planid,
            a.partnerid,
            lu.urlPass
        from
          sponsorConnectRequests scr
        left join
          launchUsers lu on lu.email = scr.toEmailAddress and lu.partnerid = scr.userid and lu.smartplanid = scr.planid
        left join
          plans p on p.id = scr.planid
        left join
          accounts a on a.id = scr.userid
        ";
        $entityManager = $this->getContainer()->get('doctrine')->getManager();
        $connection  = $entityManager->getConnection();
        $contacts = $connection->query($sql);
        while ($contact = $contacts->fetch()) {
            if (array_key_exists('toEmailAddress', $contact)) {
                $email = trim($contact['toEmailAddress']);
                $properties = [];
                $properties['provider_email'] = $this->getField('fromEmailAddress', $contact);
                $properties['provider_first'] = $this->getField('fromFirstName', $contact);
                $properties['provider_last'] = $this->getField('fromLastName', $contact);
                $properties['provider_logo'] = $this->getField('providerLogoImage', $contact);
                $properties['partner_id'] = $this->getField('partnerid', $contact);
                $properties['plan_id'] = $this->getField('planid', $contact);
                $properties['firstname'] = $this->getField('toFirstName', $contact);
                $properties['lastname'] = $this->getField('toLastName', $contact);
                $properties['smart_connect_url'] = 'https://smartconnect.smartplanenterprise.com/?uc=' . urlencode($this->getField('urlPass', $contact));
                $properties['role'] = 'Plan Sponsor';
                
                $this->hubSpotService->addToBatch($email, $properties);
            }
        }

        $this->hubSpotService->flushBatch();

    }
}