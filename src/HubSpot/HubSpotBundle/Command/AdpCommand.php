<?php

/**
 * please note that this job only works for ADP Educate plans
 * all plans are assumed to be smartenroll
 */

namespace HubSpot\HubSpotBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use HubSpot\HubSpotBundle\Services\HubSpotContactsService;
use classes\classBundle\Entity\launchToolRecipients;
use Doctrine\Common\Collections\Criteria;

class AdpCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:adpmigration')
            ->setDescription('Migrates ADP enrollees to HubSpot.')
            ->setHelp('Use this command to migrate ADP enrollees to HubSpot')
            ->addOption('accountId', null, InputOption::VALUE_REQUIRED, 'Account id of enrollees');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {   
        $this->getContainer()->get("HubSpotDataMigrationService")->processAccount($input->getOption("accountId"));
    }
}