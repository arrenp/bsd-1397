<?php

namespace HubSpot\HubSpotBundle\Services;

class HubSpotContactListsService extends HubSpotBaseService {

    public function getDynamicContactLists() {
        $response = $this->doCall("/contacts/v1/lists/dynamic", "GET", array());
        return $response !== false ? $response : [];
    }

    public function getContactsInAList($listId, $query) {
        $response = $this->doCall("/contacts/v1/lists/{$listId}/contacts/all", "GET", null, $query);
        return $response !== false ? $response : [];
    }

    /**
     * Example:
     * updateContactListCriteria(5, array(
     *   array('operator' => 'NEQ', 'property' => "email", 'value' => "ntest@test.com"),
     *   array('operator' => 'NEQ', 'property' => "foo", 'value' => "ntest@test.com"),
     *   array('operator' => 'NEQ', 'property' => "bar", 'value' => "ntest@test.com"),
     *   array('operator' => 'NEQ', 'property' => "baz", 'value' => "ntest@test.com", "operator" => "or"),
     *   array('operator' => 'NEQ', 'property' => "man", 'value' => "ntest@test.com"),
     * ));
     * 
     * Note: 'operator' is 'and' by default; 'and' has a higher precedence than 'or'
     */
    public function updateContactListCriteria($listId, $filters) {
        $filters = $this->buildFilters($filters);
        $response = $this->doCall("/contacts/v1/lists/{$listId}", "POST", $filters);
        return $response !== false ? $response : [];
    }
    
    public function getGroupById(array $listIds = array()) {
        $response = $this->doCall("/contacts/v1/lists/batch", "GET", null, ['listId' => $listIds]);
        return $response !== false ? $response : [];
    }

    private function buildFilters($filters) {
        $returnFilter = array();
        $outer = 0;
        foreach ($filters as $filter) {
            if (isset($filter['conjunction']) && $filter['conjunction'] === 'or') {
                $outer++;
            }
            $returnFilter[$outer][] = $filter;
        }
        $returnFilter = array_values($returnFilter);
        return array("filters" => $returnFilter);
    }
    
}
