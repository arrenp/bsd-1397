<?php

namespace HubSpot\HubSpotBundle\Services;

class HubSpotEmailService extends HubSpotBaseService {
    
    public function getEmailEvents($appId = null, $campaignId = null, $recipient = null, $eventType = null, $startTimestamp = null, $endTimestamp = null, $limit = 1000)
    {
        $query = array();
        if ($appId) {
            $query['appId'] = $appId;
        }
        if ($campaignId) {
            $query['campaignId'] = $campaignId;
        }
        if ($recipient) {
            $query['recipient'] = $recipient;
        }
        if ($eventType) {
            $query['eventType'] = $eventType;
        }
        if ($startTimestamp) {
            $query['startTimestamp'] = $startTimestamp;
        }
        if ($endTimestamp) {
            $query['endTimestamp'] = $endTimestamp;
        }
        $query['limit'] = $limit;

        $events = [];
        do {
            $response = $this->doCall("/email/public/v1/events", "GET", null, $query);
            if (!$response) {
                return false;
            }
            $events = array_merge($events, $response['events']);
            $query['offset'] = $response['offset'];
        }
        while($response['hasMore']);
        return $events;
    }
    
    public function getEmailEventTypes() {
        return ["SENT", "DROPPED", "PROCESSED", "DELIVERED", "BOUNCE", "OPEN", "CLICK", "PRINT", "STATUSCHANGE"];
    }

}