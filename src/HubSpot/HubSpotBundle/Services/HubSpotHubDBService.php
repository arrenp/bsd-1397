<?php

namespace HubSpot\HubSpotBundle\Services;

class HubSpotHubDBService extends HubSpotBaseService {
	
	/**
	 * @param int $portalId Portal ID
	 * @return array|bool
	 */
	public function getAllTables($portalId) {
		$response = $this->doCall("/hubdb/api/v1/tables", "GET", null, array('portalId' => $portalId));  
        return $response;
	}
	
	/**
	 * @param int $portalId Portal ID
	 * @param int $tableId Table ID
	 * @return array|bool
	 */
	public function getTableById($portalId, $tableId) {
		$response = $this->doCall("/hubdb/api/v1/tables/$tableId", "GET", null, array('portalId' => $portalId));  
        return $response;
	}
	
	/**
	 * @param string $name The name of the table
	 * @param array $data Table data
	 * @return array|bool An array representing the created table or false on failure
	 */
	public function createTable($name, $data = array()) {
		$response = $this->doCall("/hubdb/api/v1/tables", "POST", array_merge(array('name' => $name), $data));  
        return $response;
	}
	
	/**
	 * NOTE: It doesn't seem possible to use this endpoint to create column definitions if there are no existing column definitions.
	 * Column definitions should be created along with the table using the createTable method 
	 * @param id $tableId Table ID
	 * @param array $data Table data
	 * @return array|bool An array representing the updated table or false on failure
	 */
	public function updateTable($tableId, $data = array()) {
		$response = $this->doCall("/hubdb/api/v1/tables/$tableId", "PUT", $data);  
        return $response;
	}
	
	/**
	 * @param int $tableId Table ID
	 * @return bool
	 */
	public function deleteTable($tableId) {
		$response = $this->doCall("/hubdb/api/v1/tables/$tableId", "DELETE");  
        return $response !== false;
	}
	
	/**
	 * @param int $portalId Portal ID
	 * @param int $tableId Table ID
	 * @param array $options Options and filters
	 * @return array|bool
	 */
	public function getTableRows($portalId, $tableId, $options = array()) {
		$response = $this->doCall("/hubdb/api/v1/tables/$tableId/rows", "GET", null, array_merge(array('portalId' => $portalId), $options));  
        return $response;
	}
	
	/**
	 * @param int $tableId Table ID
	 * @param array $data Row data
	 * @return array|bool An array representing the created row or false on failure
	 */
	public function addTableRow($tableId, $data = array()) {
		$response = $this->doCall("/hubdb/api/v1/tables/$tableId/rows", "POST", $data);  
        return $response;
	}
	
	/**
	 * @param int $tableId Table ID
	 * @param int $rowId Row ID
	 * @param array $data Row data
	 * @return array|bool An array representing the updated row or false on failure
	 */
	public function updateTableRow($tableId, $rowId, $data) {
		$response = $this->doCall("/hubdb/api/v1/tables/$tableId/rows/$rowId", "PUT", $data);  
        return $response;
	}
	
	/**
	 * @param int $tableId Table ID
	 * @param int $rowId Row ID
	 * @return bool
	 */
	public function deleteTableRow($tableId, $rowId) {
		$response = $this->doCall("/hubdb/api/v1/tables/$tableId/rows/$rowId", "DELETE");  
        return $response !== false;
	}
	
	/**
	 * @param int $tableId Table ID
	 * @param int $rowId Row ID
	 * @param int $cellId Cell ID
	 * @param array $data Cell data
	 * @return array|bool An array representing the updated cell or false on failure
	 */
	public function updateTableCell($tableId, $rowId, $cellId, $data) {
		$response = $this->doCall("/hubdb/api/v1/tables/$tableId/rows/$rowId/cells/$cellId", "PUT", $data);  
        return $response;
	}
	
	/**
	 * @param int $tableId Table ID
	 * @param int $rowId Row ID
	 * @param int $cellId Cell ID
	 * @return bool
	 */
	public function deleteTableCell($tableId, $rowId, $cellId) {
		$response = $this->doCall("/hubdb/api/v1/tables/$tableId/rows/$rowId/cells/$cellId", "DELETE");  
        return $response !== false;
	}
	
	
	/**
	 * @param int $tableId Table ID
	 * @param string $filePath Path to CSV file.
	 * @param array $columnMappings Array of associative arrays containing CSV/HubDB column mapping. Format: [['source' => 1, 'target' => 1], ...]
	 * @param bool $resetTable If set to true, replace all data in the table.
	 * @param int $skipRows Number of header rows to skip.
	 * @param array $options Additional optional parameters. 
	 * @return array|bool Result data or false on failure 
	 */
	public function importCsvFile($tableId, $filePath, array $columnMappings, $resetTable = false, $skipRows = 1, array $options = array()) {
		$file = new \CURLFile($filePath, "text/csv");
		$data = array(
			'file' => $file,
			'config' => json_encode(array_merge(array(
				'columnMappings' => $columnMappings,
				'resetTable' => $resetTable,
				'skipRows' => $skipRows,
				'format' => "csv"
			), $options))
		);
		$response = $this->doCall("/hubdb/api/v1/tables/$tableId/import", "POST", $data, array(), false);  
		return $response;
	}
}
