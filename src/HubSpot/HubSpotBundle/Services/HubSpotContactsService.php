<?php

namespace HubSpot\HubSpotBundle\Services;

class HubSpotContactsService extends HubSpotBaseService {

    private $batchSize = 100;
    private $batchQueue = array();
    
    const BATCH_ENQUEUED = 1;
    const BATCH_FLUSHED = 2;
    const BATCH_ERROR = 3;

    protected function buildProperties($arr) {
        $properties = array();
        foreach ($arr as $propName => $propValue) {
            $properties['properties'][] = array(
                'property' => $propName,
                'value' => $propValue
            );
        }
        return $properties;
    }
    
    /**
     * @param array $data Contact info as associated array
     * @return mixed The ID of the created contact or false on failure
     */
    public function createContact($data) {
        $data = $this->buildProperties($data);
        $response = $this->doCall("/contacts/v1/contact", "POST", $data);
        return $response !== false ? $response['vid'] : false;
    }
    
    /**
     * @param int $vid Contact ID
     * @param array $data Contact info as associated array
     * @return bool
     */
    public function updateContact($vid, $data) {
        $data = $this->buildProperties($data);
        $response = $this->doCall("/contacts/v1/contact/vid/$vid/profile", "POST", $data);
        return $response !== false ? true : false;
    }
    
    /**
     * @param string $email E-mail address of the contact
     * @param array $data Contact info as associated array
     * @return bool
     */
    public function updateContactByEmail($email, $data) {
        $data = $this->buildProperties($data);
        $response = $this->doCall("/contacts/v1/contact/email/$email/profile", "POST", $data);
        return $response !== false ? true : false;
    }
    
    /**
     * @param string $email
     * @param array $data Contact info as associated array
     * @return mixed Array of format ['vid'=>, 'isNew'=>] or false on failure
     */
    public function replaceContact($email, $data) {
        $data = $this->buildProperties($data);
        $response = $this->doCall("/contacts/v1/contact/createOrUpdate/email/$email", "POST", $data);
        return $response !== false ? $response : false;
    }
    
    /**
     * @param array $data ['email@example.com' => ['firstname'=>, 'lastname'=>]]
     * @return bool
     */
    public function replaceGroup($data) {
        array_walk_recursive($data, function(&$string) {
            $string = trim($string, ' ' . chr(194) . chr(160));
            $string = iconv(mb_detect_encoding($string, mb_detect_order(), true), 'UTF-8//IGNORE', $string);
        });
        $postData = array();
        foreach ($data as $email => $properties) {
            if (!empty($email)) {
                $postData[] = array_merge(
                    $this->buildProperties($properties),
                    array('email' => $email)
                );
            }
        }
        $response = $this->doCall("/contacts/v1/contact/batch/", "POST", $postData);
        return $response !== false ? true : false;
    }
    
    /**
     * 
     * @param int $vid Contact ID
     * @return bool
     */
    public function deleteContact($vid) {
        $response = $this->doCall("/contacts/v1/contact/vid/$vid", "DELETE");
        return $response !== false ? true : false;
    }
    
    /**
     * @param array $options Optional arguments for HubSpot API
     * @return array
     */
    public function getAllContacts($options = array()) {
        $response = $this->doCall('/contacts/v1/lists/all/contacts/all', "GET", null, $options);
        return $response !== false ? $response : array();
    }
    
    /**
     * @param array $options Optional arguments for HubSpot API
     * @return array
     */
    public function getRecentlyUpdatedContacts($options = array()) {
        $response = $this->doCall("/contacts/v1/lists/recently_updated/contacts/recent", "GET", null, $options);
        return $response !== false ? $response : array();
    }
    
    /**
     * @param array $options Optional arguments for HubSpot API
     * @return array
     */
    public function getRecentlyCreatedContacts($options = array()) {
        $response = $this->doCall("/contacts/v1/lists/all/contacts/recent", "GET", null, $options);
        return $response !== false ? $response : array();
    }
    
    /**
     * @param int $vid Contact ID
     * @param array $options Optional arguments for HubSpot API
     * @return array
     */
    public function getContactById($vid, $options = array()) {
        $response = $this->doCall("/contacts/v1/contact/vid/$vid/profile", "GET", null, $options);  
        return $response !== false ? $response : array();
    }
    
    /**
     * @param array $vids Array of Contact IDs to retrieve
     * @param array $options Optional arguments for HubSpot API
     * @return array
     */
    public function getGroupById($vids, $options = array()) {
        $response = $this->doCall("/contacts/v1/contact/vids/batch", "GET", null, array_merge(array('vid'=>$vids), $options));
        return $response !== false ? $response : array();
    }
    
    /**
     * @param string $email E-mail address of the contact
     * @param array $options Optional arguments for HubSpot API
     * @return array
     */
    public function getContactByEmail($email, $options = array()) {
        $response = $this->doCall("/contacts/v1/contact/email/$email/profile", "GET", null, $options);
        return $response !== false ? $response : array();
    }
    
    /**
     * @param array $emails Array of e-mail addresses
     * @param array $options Optional arguments for HubSpot API
     * @return array
     */
    public function getGroupByEmail($emails, $options = array()) {
        $response = $this->doCall("/contacts/v1/contact/emails/batch", "GET", null, array_merge(array('email'=>$emails), $options));
        return $response !== false ? $response : array();
    }
    
    /**
     * @param string $token Contact user token
     * @param array $options Optional arguments for HubSpot API
     * @return array
     */
    public function getContactByUserToken($token, $options = array()) {
        $response = $this->doCall("/contacts/v1/contact/utk/$token/profile", "GET", null, $options);
        return $response !== false ? $response : array();
    }
    
    /**
     * @param array $tokens Array of contact user tokens
     * @param array $options Optional arguments for HubSpot API
     * @return array
     */
    public function getGroupByUserToken($tokens, $options = array()) {
        $response = $this->doCall("/contacts/v1/contact/utks/batch", "GET", array(), array_merge(array('utk'=>$tokens), $options));
        return $response !== false ? $response : array();
    }
    
    /**
     * @param string $term Search string
     * @param array $options Optional arguments for HubSpot API
     * @return array
     */
    public function searchContacts($term, $options = array()) {
        $response = $this->doCall("/contacts/v1/search/query", "GET", array(), array_merge(array('q'=>$term), $options));
        return $response !== false ? $response : array();
    }
    
    /**
     * @param int $toVid Contact ID of the contact to merge into
     * @param int $fromVid Contact ID of the contact to merge from
     * @return bool
     */
    public function mergeContacts($toVid, $fromVid) {
        $data = array('vidToMerge' => $fromVid);
        $response = $this->doCall("/contacts/v1/contact/merge-vids/$toVid/", "POST", $data);
        return $response !== false ? true : false;
    }

    public function writeBatch()
    {
        $result = $this->replaceGroup($this->batchQueue);
        $this->batchQueue = array();
        return $result;
    }

    public function addToBatch($email, $properties) {
        $this->batchQueue[$email] = $properties;
        if (count($this->batchQueue) >= $this->batchSize) {
            $result = $this->writeBatch();
            return $result ? self::BATCH_FLUSHED : self::BATCH_ERROR;
        }
        return self::BATCH_ENQUEUED;
    }

    public function flushBatch() {
        $result = null;
        if (count($this->batchQueue) > 0) {
            $result = $this->writeBatch();
            return $result ? self::BATCH_FLUSHED : self::BATCH_ERROR;
        }
        return $result;
    }
    
    public function __destruct()
    {
        $this->flushBatch();
    }

}
