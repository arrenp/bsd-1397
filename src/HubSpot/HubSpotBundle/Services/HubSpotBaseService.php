<?php

namespace HubSpot\HubSpotBundle\Services;

class HubSpotBaseService {
    
    private $baseDomain;
    
    private $apiKey;
    
    private $lastError;

    private $invalidEmails;
    
    private $appId;

    private $gm;
    
    public function __construct($baseDomain, $apiKey, $appId, $generalmethods) {
        $this->baseDomain = $baseDomain;
        $this->apiKey = $apiKey;
        $this->invalidEmails = array();
        $this->appId = $appId;
        $this->gm = $generalmethods;
    }
    
    protected function doCall($endpoint, $method = "GET", $data = null, $query = array(), $postAsJson = true) {

        $ch = curl_init();
        $query['hapikey'] = $this->apiKey;
        $queryString = $this->buildQueryString($query);
        $url = 'https://' . $this->baseDomain . $endpoint . '?' . $queryString;

		if (in_array($method, array("POST", "PUT")) && $postAsJson) {
			$data = !empty($data) ? json_encode($data) : '{}';
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data)));
		}

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        switch ($method) {
            case "POST": 
                curl_setopt($ch, CURLOPT_POST, true); 
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            break;
            case "PUT":
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            break;
            case "DELETE":
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            break;
            default: // "GET"
                curl_setopt($ch, CURLOPT_HTTPGET, true); 
            break;
        }
        
        $response = curl_exec($ch);

        if ($response === false) {
            $this->lastError = curl_error($ch);
            $this->gm->postToSlack("error", "HubSpot Base Service response === false: ", curl_error($ch), "default");
            return false;
        }
        
        $response = json_decode($response, true);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($response != null) {
            if (isset($response['status']) && $response['status'] === 'error' && isset($response['invalidEmails'])) {
                $this->invalidEmails = array_merge($this->invalidEmails, $response['invalidEmails']);
            }
        }
        
        if ($httpCode < 200 || $httpCode >= 300) {
            // Anything that is not 2xx is an error
            $this->lastError = !empty($response['message']) ? $response['message'] : 'Unknown Error';
            $this->gm->postToSlack("error", "HubSpot Base Service HTTP Response != 2XX: ", !empty($response['message']) ? $response['message'] : 'Unknown Error', "default");
            $this->gm->postToSlack("error", "HubSpot Base Service HTTP Response", print_r($response, true), "code");
            return false;
        }
        curl_close($ch);
        return $response;
    }
    
    protected function buildQueryString($arr = array()) {
        $query = array();
        foreach ($arr as $name=>$outerVal) {
            if (is_array($outerVal)) {
                foreach ($outerVal as $innerVal) {
                    $query[] = urlencode($name) . '=' . urlencode($innerVal);
                }
            }
            else {
                $query[] = urlencode($name) . '=' . urlencode($outerVal);
            }
        }
        return implode("&", $query);
    }
    
    public function getLastError() {
        return $this->lastError;
    }

    public function getInvalidEmails() {
        return $this->invalidEmails;
    }
    
    public function clearInvalidEmails() {
        $this->invalidEmails = array();
    }
    
    public function getAppId() {
        return $this->appId;
    }
    
}
