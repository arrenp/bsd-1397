<?php

namespace HubSpot\HubSpotBundle\Services;

class HubSpotLayoutsService extends HubSpotBaseService {
	
	/**
	 * @param array $options Options and filters
	 * @return array|bool
	 */
	public function listLayouts(array $options = array()) {
		$response = $this->doCall("/content/api/v2/layouts", "GET", null, $options);
		return $response;
	}
	
	/**
	 * @param int $layoutId Layout ID
	 * @return array|bool
	 */
	public function getLayoutById($layoutId) {
		$response = $this->doCall("/content/api/v2/layouts/$layoutId");
		return $response;
	}
	
	/**
	 * @param int $layoutId Layout ID
	 * @return array|bool
	 */
	public function getAutoSaveBuffer($layoutId) {
		$response = $this->doCall("/content/api/v2/layouts/$layoutId/buffer");
		return $response;
	}
	
	/**
	 * @param int $layoutId Layout ID
	 * @return bool
	 */
	public function hasBufferedChanges($layoutId) {
		$response = $this->doCall("/content/api/v2/layouts/$layoutId/has-buffered-changes");
		return $response !== false ? $response['has_changes'] : false;
	}
	
	/**
	 * @param int $layoutId Layout ID
	 * @return array|bool
	 */
	public function listPreviousVersions($layoutId) {
		$response = $this->doCall("/content/api/v2/layouts/$layoutId/versions");
		return $response;
	}
	
	/**
	 * @param int $layoutId Layout ID
	 * @param int $versionId Version ID
	 * @return array|bool
	 */
	public function getPreviousVersion($layoutId, $versionId) {
		$response = $this->doCall("/content/api/v2/layouts/$layoutId/versions/$versionId");
		return $response;
	}
	
}
