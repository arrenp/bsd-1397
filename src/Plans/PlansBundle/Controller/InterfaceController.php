<?php

namespace Plans\PlansBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use classes\classBundle\Entity\plans;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\General\GeneralMethods;
use Permissions\RolesBundle\Classes\rolesPermissions;
class InterfaceController extends Controller
{
    public function indexAction()
    {
    	$general = new GeneralMethods($this);
		$session = new adminsession($this);
		$permissions = new rolesPermissions($this);
		$writeable = $permissions->writeable("PlansInterface");	
		$session->set("section","Plans");
		$session->set("currentpage","PlansInterface");
		$userid = $session->userid;
		$planid = $session->planid;
		$repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
		$interface = $repository->findOneBy(array('id' => $planid));
		$interfaceColor = $interface->interfaceColor;
		$interfaceLayout = $interface->interfaceLayout;

		$colors = array();
		$general->addSTDClassToArray($colors,6);
		$colors[0]->value = "blue";
		$colors[0]->colors = array("003366","005599","3399EE","99CCEE","CCDDEE");
		$colors[1]->value = "green";
		$colors[1]->colors = array("004400","006600","008800","99CC99","DDEEDD");
		$colors[2]->value = "gold";
		$colors[2]->colors = array("776644","998866","ccbb99","ccccbb","eeeeee");
		$colors[3]->value = "red";
		$colors[3]->colors = array("660000","990000","cc0000","ccbbbb","eeeeee");
		$colors[4]->value = "burgundy";
		$colors[4]->colors = array("550000","770000","990000","bbaaaa","eeeeee");
		$colors[5]->value = "charcoal";
		$colors[5]->colors = array("333333","666666","999999","cccccc","eeeeee");

		for ($i = 0; $i < count($colors); $i++)
		{
		$colors[$i]->title = $colors[$i]->value;
		$colors[$i]->title[0] = strtoupper($colors[$i]->title[0]);

		if ($colors[$i]->value == $interfaceColor)
		$colors[$i]->checked = "checked";
		else
		$colors[$i]->checked = "";
		}

		$layouts = array();
		$general->addSTDClassToArray($layouts,2);
		$layouts[1]->value = "top";
		$layouts[0]->value = "side";

		$layouts[1]->title = "Navigation at Top";
		$layouts[0]->title = "Navigation at Left";
		
		$layouts[1]->image = "spe-nav-top.svg";
		$layouts[0]->image = "spe-nav-side.svg";
		

		for ($i = 0; $i < count($layouts); $i++)
		if ($layouts[$i]->value == $interfaceLayout)
		$layouts[$i]->checked = "checked";
		else
		$layouts[$i]->checked = "";


    	return $this->render('PlansPlansBundle:Interface:index.html.twig',array('userid' => $userid,'planid' => $planid,"colors" => $colors, "layouts" => $layouts, "writeable" => $writeable));
    }




}
