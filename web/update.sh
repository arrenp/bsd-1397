#! /bin/bash
git pull && \
php ../app/console cache:clear --env=prod --no-debug && \
for arg; do
  case $arg in
    --dev)
      php ../app/console cache:clear
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
    :)
      echo "No Migration"
      ;;
  esac
done
chown -R apache:apache ../