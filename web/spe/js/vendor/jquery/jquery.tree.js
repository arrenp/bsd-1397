(function( $ ) {
    var pluginOptions = {
        onClass : 'plusimageapply',
        offClass: 'minusimageapply',
        selected : 'selectedimage'
    };

    $.fn.tree = function(options) {
        var config = $.extend(pluginOptions, options);
        this.each(function(){
            var $node = $(this);
                $node.addClass(config.offClass);
                $node.children().addClass(config.selected);

                $node.each(function(){
                    var $branch = $(this);
                        $branch.click(function(e){
                             if(this == e.target){
                                 var $leaf = $branch.children();
                                 if($leaf.is('.'+config.onClass)){
                                     $leaf.slideDown().removeClass(config.onClass).addClass(config.offClass)
                                         .parent().removeClass(config.onClass).addClass(config.offClass);
                                 }else{
                                     $leaf.slideUp().removeClass(config.offClass).addClass(config.onClass)
                                         .parent().removeClass(config.offClass).addClass(config.onClass);
                                 }
                             }
                        });
                });
        });
        return this;
    };
}( jQuery ));