require.config({
    urlArgs: "bust=" + (new Date()).getTime(),
    waitSeconds: 10,
    paths: {
        'jquery': './vendor/jquery/jquery.min',
        'underscore': './vendor/utils/underscore.min',
        'underscore.string': './vendor/utils/underscore.string.min',
        'bootstrap': './vendor/utils/bootstrap.min',
        'bootbox': './vendor/utils/bootbox.min',
        'pubsub': './vendor/jquery/pubsub',
        'emitter': './vendor/jquery/emitter',
        'jwplayer': './vendor/utils/jwplayer.min',
        'playerHtml5': './vendor/utils/jwplayer.html5.min',
        'jquery.ui': './vendor/jquery/jquery.ui.min',
        'jquery.touch': './vendor/jquery/jquery.ui.touch-punch.min',
        'jquery.mousewheel': './vendor/jquery/jquery.mousewheel.min',
        'jquery.customscroll': './vendor/jquery/jquery.mCustomScrollbar.min',
        'jquery.tree': './vendor/jquery/jquery.tree',
        'jquery.maskedinput' : './vendor/jquery/jquery.maskedinput.min',
        'jquery.maskssn' : './vendor/jquery/jquery.maskssn',
        'autoNumeric-min' : './vendor/jquery/autoNumeric-min',
        'jquery.ui.datepicker.validation' : './vendor/utils/jquery.ui.datepicker.validation',
        'jquery.validate' : './vendor/jquery/jquery.validate.min',
        'moment' : './vendor/utils/moment.min',

        'config' : './spe/config',
        'controller' : './spe/controller',
        'service' : './spe/service',
        'view' : './spe/view',

        'polyFill' : './vendor/utils/polyfill',
        'json' : './vendor/utils/json2'
    },
    shim: {
        'underscore': { exports: '_'},
        'underscore.string':{ exports: 's'},
        'pubsub':{deps: ['jquery']},
        'emitter':{deps: ['jquery']},
        'jwplayer': { exports: 'jwplayer', deps: ['jquery']},
        'bootstrap':{deps: ['jquery'] },
        'bootbox':{deps: ['bootstrap']},
        'jquery.ui':{deps: ['jquery']},
        'jquery.tree':{deps: ['jquery']},
        'jquery.maskedinput' : {deps :['jquery']},
        'jquery.maskssn' : {deps :['jquery']},
        'jquery.mousewheel' : {deps :['jquery']},
        'jquery.customscroll' : {deps :['jquery']},
        'autoNumeric-min' : {deps :['jquery']},
        'jquery.touch' : {deps :['jquery.ui']},
        'jquery.ui.datepicker.validation' : {deps :['jquery.ui']},
    },
    deps: ['bootstrap','bootbox','polyFill', 'emitter', 'json', 'jquery.tree', 'jquery.maskedinput', 'jquery.maskssn', 'moment', 'jquery.validate', 'underscore.string', 'jquery.touch', 'autoNumeric-min', 'jquery.ui.datepicker.validation']
});

require(['./spe/app'], function(app) {
    app.initialize();
});
