define([
    'jquery',
    'jquery.ui',
    'spe/service/common',
    'spe/service/RetirementNeedService'
], function($, ui, common, retirementNeedService){

    var hasBind = false;

    var RetirementNeedView = {

        tpl : ['retirement-needs-page-1','retirement-needs-page-2','retirement-needs-page-3','retirement-needs-page-4','retirement-needs-page-5','retirement-needs-page-6','retirement-needs-page-7'],

        actions : ['init','page1','page2','page3','page4','page5', 'page6', 'save'],

        tplEl : function(index){
            return $('#'+ this.tpl[index ? (index-1) : 0]);
        },

        cacheElements : function(){
			this.input = $('input.currency');
		},

		initNumeric: function() {
			this.input.autoNumeric('init',{lZero: 'deny', mDec: 0, aPad: false, vMin: 0});
		},

        bindEvents : function(){
            var section = $('body');
            section.on('click', '.yearsBeforeRetirement', retirementNeedService.inflationAdjustedReplacementIncome.bind(this));
            section.on('click', '.yearsInRetire', retirementNeedService.setYearsInRetire.bind(this));
            section.on('change keyup', '#retirement_age', retirementNeedService.inflationAdjustedReplacementIncome.bind(this));
            section.on('keyup', '.totalAssetEducate', retirementNeedService.calculateTotalAssetEducate.bind(this));
            section.on('keyup', '.totalAssets', retirementNeedService.calculateTotalAsset.bind(this));
            section.on('change keyup', '#life_expectancy', retirementNeedService.setYearsInRetire.bind(this));
            section.on('click', '.slide', retirementNeedService.slide.bind(this));
            section.on('click', '.proceed', this.navAction.bind(this));
            section.on('click', '.back', this.navAction.bind(this));
        },

        render : function(section, data, index){
            var tpl = _.template(this.tplEl(index).html())( { model : data });
            $(section).addClass('text').html(tpl);
            retirementNeedService.selectAssetTypes();
            if(index == 7){retirementNeedService.initializeIncomePlanSlider()}
            if(!hasBind){
                this.bindEvents();
                hasBind = true;
            }
			this.cacheElements();
			this.initNumeric();
			if(index == 4 && $('input[name=ybr]:checked').val() !== null) {
                $("input[name=ybr][value=" + $('input[name=ybr]:checked').val() + "]").click();
            }

        },

        navAction : function(e){
            var self = $(e.currentTarget),
                step = self.attr('step'),
                dir = self.attr('dir'),
                action = this.actions[dir == 'next' ? step - 1 : step];
            $.publish('spe:retirementNeeds:'+ action, { current: step, dir: dir });
        }
    };

    return{
        render : RetirementNeedView.render.bind(RetirementNeedView),
        bindEvents : RetirementNeedView.bindEvents.bind(RetirementNeedView)
    }

});
