define(['jquery', 'underscore', 'spe/model/Section'], function($, _, Section){

    var hasBind = false;

    var MyProfileView  =  {

        getTemplate : function(name){
            return $("#my-profile-"+ name).html();
        },

        initialize: function(){
            if(!hasBind){
                this.bindEvents();
                hasBind = true;
            }
        },

        bindEvents : function(){
            this.$container = $('body');
            this.$container.on('change', '#RPQuestionSet',this.callAction.bind(this, 'showAnswer'));
            this.$container.on('change', '#RNQuestionSet',this.callAction.bind(this, 'showAnswer'));
            this.$container.on('click', '.back',this.callAction.bind(this, 'back'));
            this.$container.on('click', '.backBeneficiary',this.callAction.bind(this, 'backBeneficiary'));
            this.$container.on('click', '.cancelProfile',this.callAction.bind(this, 'cancel'));
            this.$container.on('click', '.saveProfile',this.callAction.bind(this, 'save'));
            this.$container.on('click', '.proceedTransactionSubmit', this.callAction.bind(this, 'proceedTransaction'));
            this.$container.on('click', '.transactionCancelSubmit', this.callAction.bind(this, 'cancelTransaction'));
            this.$container.on('click', '.confirmTransaction', this.callAction.bind(this, 'confirmTransaction'));
            this.$container.on('click', '.processUpdates', this.callAction.bind(this, 'processUpdates'));
            this.$container.on('click', '.printProfile', this.callAction.bind(this, 'printProfile'));
            this.$container.on('click', '.emailProfile', this.callAction.bind(this, 'emailProfile'));
            this.$container.on('click', '.popupSpousalWavierForm', function(){$('#spousalWavierModal').removeClass('ben-modal').addClass('pro-modal').modal('show');});
            this.$container.on('click', '.continueToSaveProfile', this.callAction.bind(this, 'emailAndSaveProfile'));
			this.$container.on('click', '.saveRolloverOption', this.callAction.bind(this, 'saveRolloverOption'));
			this.$container.on('click', '.saveRolloverOption.proceed', this.callAction.bind(this, 'index'));
            this.$container.on('click', '.saveTrustedContact', this.callAction.bind(this, 'saveTrustedContact'));
        },

        callAction: function(actionName, e){
            $.publish("spe:myProfile:"+ actionName, { do : actionName, target: e.target });
        },

        render : function(templateName, model){
            this.$section = $('.content-area');
            var tpl = _.template(this.getTemplate(templateName))({ model : model });
            this.$section.html(tpl).addClass('text');
        }

    };

    MyProfileView.initialize();

    return {
        render: MyProfileView.render.bind(MyProfileView)
    };
});