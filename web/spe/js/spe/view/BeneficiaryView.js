define(['jquery','pubsub','spe/service/common','spe/model/Section','spe/view/PlayerView','spe/service/data', 'spe/service/BeneficiaryService'], function($, pubsub, common, Section, playerView, data, beneficiaryService){
    "use strict";

    var hasModalBind = false;
    var used_p = 0, used_s = 0, used_c = 0, rem_p = 0, rem_s = 0, rem_c = 0, max_p = 0, max_s = 0, max_c = 0;
    var BeneficiaryView = {

        el : ['beneficiary-tpl', 'beneficiary-relius-tpl'],

        $el : function(type){
            return $('#' + this.el[type]);
        },

        $modelEl : function(type){
            return $('#add-' + this.el[type]);
        },

        cacheElements : function(){
            this.addBeneficiary = $('.add-beneficiary');
            this.beneficiaryList = $('.beneficiary');
            this.addBeneficiaryModal = $('#addBeneficiaryModal');
            this.spousalWavierConfirmationModal = $('#spousalWavierConfirmationModal');
            this.spousalWavierModal = $('#spousalWavierModal');
            this.updateBeneficiary = this.addBeneficiaryModal.find('#updateBeneficiary');
            this.allocationPct = this.addBeneficiaryModal.find('#bPercent');
            this.isSpouse = this.addBeneficiaryModal.find('#bIsSpouse');
            this.bType = this.addBeneficiaryModal.find('#bType');
            this.bOmniLevel = this.addBeneficiaryModal.find('#bOmniLevel');
            this.bZip = this.addBeneficiaryModal.find('#bZip');
            this.dob = this.addBeneficiaryModal.find('#bDob');
            this.ssnPublic = this.addBeneficiaryModal.find('#bssnPublic');            
            this.ssn = this.addBeneficiaryModal.find('#bSSN');
            this.phone = this.addBeneficiaryModal.find('#bPhone');
            this.edit = this.beneficiaryList.find('.b-edit');
            this.delete = this.beneficiaryList.find('.b-delete');
            this.back = this.beneficiaryList.find('.back');
            this.proceed = this.beneficiaryList.find('.proceed');
            this.swProceed = this.spousalWavierConfirmationModal.find('.proceed');
            this.swBack = this.spousalWavierConfirmationModal.find('.back');
            this.bRelationship = this.addBeneficiaryModal.find('#bRelationship');
            this.middleInitial = this.addBeneficiaryModal.find('#bmiddleInitial');
        },

        bindEvents : function(){
            this.addBeneficiary.on('click', this.openAddBeneficiary.bind(this));
            this.edit.on('click', this.editBeneficiary.bind(this));
            this.delete.on('click', this.deleteBeneficiary.bind(this));
            //$('body').on('click', '.b-delete', this.deleteBeneficiary.bind(this));
            this.back.on('click', this.backAction.bind(this));
            this.proceed.on('click', this.proceedAction.bind(this));
        },

        bindModalEvents : function(){
            this.ssnPublic.on('keyup', this.ssnPublic, this.typeMask.bind(this));            
            this.allocationPct.on('click keyup change blur', this.calculatePct.bind(this));
            this.bType.on('change', this.changeBeneficiaryType.bind(this));
            this.bOmniLevel.on('change', this.changeOmniLevel.bind(this));
            this.updateBeneficiary.on('click', this.save.bind(this));
            this.isSpouse.on('change', this.hasSpouse.bind(this));
            this.bRelationship.on('change', this.hasOmniSpouse.bind(this));
            this.swBack.on('click', this.closeSwConfirmationModal.bind(this));
            this.swProceed.on('click', this.proceedSwConfirmationModal.bind(this));
            this.spousalWavierModal.on('click', '.proceed', this.proceedSwForm.bind(this));
            this.spousalWavierModal.on('click', '.back', this.backSwForm.bind(this));
        },
        
        typeMask : function(){
            
            var ssnMasked = $('input:hidden[name="bssnPublic"]').val();
            this.ssn.val(ssnMasked);

        },        

        render : function(type, data){
            //if(type){
            //    _.each(data, function(d, k){
            //        if(d.BeneficiaryBirthDate){
            //            data[k].BeneficiaryBirthDate = beneficiaryService.formatDate(d.BeneficiaryBirthDate, 'MM-DD-YYYY');
            //        }
            //    });
            //}
            var tpl = _.template(this.$el(type).html())( { model : data });
            $('.content-area').addClass('text').html(tpl);
            this.cacheElements();
            this.bindEvents();
        },

        hasOmniSpouse : function () {
            this.calculatePct();
            if(this.bRelationship.find('option:selected').val() == '0'){
                this.bOmniLevel.val('1').attr('disabled','disabled');
                this.allocationPct.val('100').attr('disabled','disabled');
                this.addBeneficiaryModal.find('.pRem').text('0');
                this.addBeneficiaryModal.find('.sRem').text(rem_s);
                return false;
            }
            this.bOmniLevel.val('').removeAttr('disabled');
            this.allocationPct.val('').removeAttr('disabled');
            this.addBeneficiaryModal.find('.pRem').text(rem_p);
            this.addBeneficiaryModal.find('.sRem').text(rem_s);
        },

        hasSpouse : function(e){
            var self = $(e.currentTarget);
            this.calculatePct();
            if(self.is(':checked')){
                this.bType.val('P').attr('disabled','disabled');
                this.allocationPct.val('100').attr('disabled','disabled');
                this.addBeneficiaryModal.find('.pRem').text('0');
                this.addBeneficiaryModal.find('.cRem').text('100');
                return false;
            }
            this.bType.val('').removeAttr('disabled');
            this.allocationPct.val('').removeAttr('disabled');
            this.addBeneficiaryModal.find('.pRem').text('100');
            this.addBeneficiaryModal.find('.cRem').text('100');
        },

        proceedSwForm : function () {
            Section.setData('common', {'spousalWaiverFormStatus' : true});
            this.spousalWavierModal.modal('hide');
        },

        backSwForm : function () {
            this.spousalWavierConfirmationModal.modal('show');
            this.spousalWavierModal.modal('hide');
        },

        proceedSwConfirmationModal : function () {
            var haveSpouse = $('input[name=haveSpouse]:checked').val();
            if(haveSpouse == 'yes'){
                this.spousalWavierConfirmationModal.modal('hide');
                this.spousalWavierModal.removeClass('pro-modal').addClass('ben-modal').modal('show');
            }else if(haveSpouse == 'no'){
                this.spousalWavierConfirmationModal.modal('hide');
                Section.setData('common', {'spousalWaiverFormStatus' : false});
            }
        },

        closeSwConfirmationModal : function () {
            this.spousalWavierConfirmationModal.modal('hide');
        },

        backAction : function(){
            $('body').trigger("spe:router:reload");
        },

        reload : function(type){
            var bType = type == 'relius' ? 1 : 0,
                mType = type == 'relius' ? 'partBeneficiaries' : 'beneficiary',
                model = Section.getData(mType);

            this.render(bType, model);
            this.addBeneficiaryModal.modal('hide');
        },

        renderModal : function(type, uid){
            var model = { 'states' : data.getStateList(),'countries':data.getCountryList() };
            model = uid ? _.extend(model, beneficiaryService.filterById(type, uid)) : model;
            if(type){
                model = _.extend(model, this.initPct());
                if(model.BeneficiaryBirthDate){
                    //model.BeneficiaryBirthDate = beneficiaryService.formatDate(model.BeneficiaryBirthDate, 'MM-DD-YYYY');
                }
            }
            var tpl = _.template(this.$modelEl(type).html())({ model : model});
            this.addBeneficiaryModal.find('.modal-dialog').html(tpl);
            this.initModal();
        },

        openAddBeneficiary : function(e){
            var self = $(e.currentTarget),
                type = self.attr('type') == 'relius' ? 1 : 0;
            this.renderModal(type);
            this.addBeneficiaryModal.modal('show');
            this.initPct();
        },

        editBeneficiary : function(e){
            var self = $(e.currentTarget),
                type = self.data('type') == 'relius' ? 1 : 0,
                uid = self.data('uid');
            this.renderModal(type, uid);
            this.addBeneficiaryModal.modal('show');
            this.initPct();
        },

        changeBeneficiaryType : function(){
            this.addBeneficiaryModal.find('.pRem').text(max_p);
            this.addBeneficiaryModal.find('.cRem').text(max_c);
            var max = (this.bType.find('option:selected').val() == 'P') ? max_p : (this.bType.find('option:selected').val() == 'C' ? max_c : 100);
            this.allocationPct.attr('max',max);
            this.calculatePct();
        },

        changeOmniLevel : function(){
            this.addBeneficiaryModal.find('.pRem').text(max_p);
            this.addBeneficiaryModal.find('.sRem').text(max_s);
            var max = (this.bOmniLevel.find('option:selected').val() == '1') ? max_p : (this.bOmniLevel.find('option:selected').val() == '2' ? max_s : 100);
            this.allocationPct.attr('max',max);
            this.calculatePct();
        },

        calculatePct : function(){
            var type = beneficiaryService.isOmni() ? this.bOmniLevel.find('option:selected').val() : this.bType.find('option:selected').val(),
                pct = this.allocationPct.val() ? parseInt(this.allocationPct.val()) : 0;

            if(beneficiaryService.isOmni()){
                if (type == '1') {
                    rem_p = max_p - parseFloat(pct);
                    this.addBeneficiaryModal.find('.pRem').text(rem_p);
                } else if (type == '2') {
                    rem_s = max_s - parseFloat(pct);
                    this.addBeneficiaryModal.find('.sRem').text(rem_s);
                } else {
                    this.addBeneficiaryModal.find('.pRem').text(max_p);
                    this.addBeneficiaryModal.find('.sRem').text(max_s);
                }
            }else {
                if (type == 'P') {
                    rem_p = max_p - parseFloat(pct);
                    this.addBeneficiaryModal.find('.pRem').text(rem_p);
                } else if (type == 'C') {
                    rem_c = max_c - parseFloat(pct);
                    this.addBeneficiaryModal.find('.cRem').text(rem_c);
                } else {
                    this.addBeneficiaryModal.find('.pRem').text(max_p);
                    this.addBeneficiaryModal.find('.cRem').text(max_c);
                }
            }
        },

        initModal : function(){
            if(!hasModalBind){
                this.bindEvents();
                hasModalBind = true;
            }
            this.cacheElements();
            this.bindModalEvents();
            this.initWidget();
            this.ssnPublic.unmask().maskSSN('999-99-9999', {maskedChar:'X', maskedCharsLength:9});
            beneficiaryService.initService();
        },

        initPct : function(){
            var model = Section.getData('partBeneficiaries') ? Section.getData('partBeneficiaries') : [];
                used_p = 0, used_s = 0, used_c = 0, rem_p = 0, rem_s = 0, rem_c = 0, max_p = 0, max_s = 0, max_c = 0;

            if(!_.isEmpty(model)){
                _.each(model, function(data, key){

                    if(beneficiaryService.isOmni()){
                        if(data.BeneficiaryLevel == '1'){
                            used_p += parseFloat(data.BeneficiaryPercent);
                        }
                        if(data.BeneficiaryLevel == '2'){
                            used_s += parseFloat(data.BeneficiaryPercent);
                        }
                    }else{
                        if(data.BeneficiaryTypeCode == 'P'){
                            used_p += parseFloat(data.BeneficiaryPercent);
                        }
                        if(data.BeneficiaryTypeCode == 'C'){
                            used_c += parseFloat(data.BeneficiaryPercent);
                        }
                    }
                })
            }

            rem_p = 100 - used_p;
            rem_s = 100 - used_s;
            rem_c = 100 - used_c;
            if(beneficiaryService.isOmni()) {
                max_p = parseFloat(rem_p + parseFloat(!beneficiaryService.isNew() && this.bOmniLevel && this.bOmniLevel.find('option:selected').val() == '1' ? this.allocationPct.val() : 0));
                max_s = parseFloat(rem_s + parseFloat(!beneficiaryService.isNew() && this.bOmniLevel && this.bOmniLevel.find('option:selected').val() == '2' ? this.allocationPct.val() : 0));
            }else{
                max_p = parseFloat(rem_p + parseFloat(!beneficiaryService.isNew() && this.bType && this.bType.find('option:selected').val() == 'P' ? this.allocationPct.val() : 0));
                max_c = parseFloat(rem_c + parseFloat(!beneficiaryService.isNew() && this.bType && this.bType.find('option:selected').val() == 'C' ? this.allocationPct.val() : 0));
            }

            return {
                rem_p : rem_p ? rem_p : 0,
                max_p : max_p ? max_p : 0,
                rem_s : rem_s ? rem_s : 0,
                max_s : max_s ? max_s : 0,
                rem_c : rem_c ? rem_c : 0,
                max_c : max_c ? max_c : 0
            }
        },

        initWidget : function(){
            this.dob.datepicker({dateFormat: 'mm/dd/yy',changeMonth: true,changeYear: true,maxDate: 'today', minDate: new Date(1900, 0, 1),yearRange: "-130:+0"});
            this.bZip.mask('?99999-9999');
            this.dob.mask('99/99/9999', {placeholder:"MM/DD/YYYY"});
            this.ssn.mask('999-99-9999');
            this.phone.mask('999-999-9999');
            this.middleInitial.mask('a');
            // Fix DatePicker force focus
            var enforceModalFocusFn = $.fn.modal.Constructor.prototype.enforceFocus;
            $.fn.modal.Constructor.prototype.enforceFocus = function() {};
            this.addBeneficiaryModal.on('hidden', function() {
                $.fn.modal.Constructor.prototype.enforceFocus = enforceModalFocusFn;
            });
            this.addBeneficiaryModal.modal({ backdrop : false });
        },

        save : function(e){
            var self = $(e.currentTarget),
                type = self.data('type'),
                model = Section.getData('common');

            if(!beneficiaryService.validate(type, max_p, max_s, max_c)){
                return false;
            }

            if(beneficiaryService.isOmni() && model.spousalWaiverOn && this.bOmniLevel.find('option:selected').val() == '1' && this.bRelationship.find('option:selected').val() !== '0' && model.spousalWaiverFormStatus == null){
                this.spousalWavierConfirmationModal.modal('show');
                return false;
            }

            if(!beneficiaryService.isNew()){
                beneficiaryService.update(type);
            }else{
                beneficiaryService.add(type);
            }
            this.reload(type);
        },

        deleteBeneficiary : function(e){
            var self = $(e.currentTarget),
                type = self.data('type'),
                uid = self.data('uid'),
                deleteBackup = self.data('delete'),
                translation = Section.getData('translation'),
                that = this;

            common.showConfirmMsg(translation.are_you_sure, function(result){
                if(!result){
                    return false;
                }
                beneficiaryService.delete(type, uid, deleteBackup);
                Section.setData('common', {'spousalWaiverFormStatus' : null});
                that.reload(type);
            });

        },

        proceedAction : function(e){
            var self = $(e.currentTarget),
                type = self.data('type');
            if(beneficiaryService.process(type)){
                $('body').trigger("spe:router:reload");
            }
        }
    };

    return {
        render : BeneficiaryView.render.bind(BeneficiaryView)
    }

});