define(['jquery', 'underscore', 'spe/model/Section', 'spe/view/PlayerView'], function($, _, Section, playerView){

    var MorningStarView  =  {

        getTemplate : function(name){
            return $("#morning-star-"+ name).html();
        },

        initialize: function(){
            this.cacheElement();
            this.bindEvents();
        },

        cacheElement: function(){
            this.$container = $('body');
            this.$section = $('.content-area');
        },

        bindEvents : function(){
            this.$container.on('click','.exitMStar',this.callAction.bind(this,'exitMorningStar'));
            this.$container.on('click','.mStarPath',this.callAction.bind(this,'privacyPolicy'));
            this.$container.on('click','.actionEdit',this.editProfile.bind(this));
            this.$container.on('click','.actionSave',this.callAction.bind(this,'saveProfile'));
            this.$container.on('click','.actionCancel',this.cancelProfile.bind(this));
            this.$container.on('click','.actionReturn',this.returnProfile.bind(this));
            this.$container.on('click','.actionProceed',this.callAction.bind(this,'proceed'));
            this.$container.on('click','.backConfirmInfo',this.confirmInfo.bind(this));
            this.$container.on('click','.importantMessageModal', this.importantMessage.bind(this));
            this.$container.on('click','.completeMethodologyModal', this.completeMethodology.bind(this));
            this.$container.on('click','.proceedAssetMix', this.reviewDetail.bind(this));
            this.$container.on('click','.actionCancelReviewDetail', this.cancelReviewDetail.bind(this));
            this.$container.on('click','.actionSaveFinish', this.callAction.bind(this,'updateReviewDetail'));
            this.$container.on('click','.actionAgree', this.agreeTerms.bind(this));
            this.$container.on('click','.actionProceedUpdate', this.callAction.bind(this,'sendFinish'));
            this.$container.on('click','.emailProfile', this.callAction.bind(this,'emailProfile'));
            this.$container.on('click','.printProfile', this.callAction.bind(this,'printProfile'));
            $(window).on('resize', this.drawChart);
        },

        callAction: function(actionName, e){
            $.publish("spe:morningStar:"+ actionName, { do : actionName, target: e.target });
        },

        cancelReviewDetail : function(){
            var translation = Section.getData('translation');
            if(confirm(translation.if_you_choose_to_cancel_the_choices_you_have_made)) {
                _gaq.push(['_trackEvent', 'MStar-Profile', 'Cancel']);
                this.$container.trigger("spe:router:redirect", 'confirmInfo');
                this.handleExit({morningStar: true});
            }
            return false;
        },

        reviewDetail: function(){
            this.$container.trigger("spe:router:redirect", 'reviewDetails');
            this.handleExit({morningStar: true});
        },

        agreeTerms : function(){
            Section.setData('common', {'mStarTermsAgree' : 1});
            this.reviewDetail();
        },

        importantMessage : function(){
            var impMessageModal = bootbox.dialog({
                size: 'medium',
                closeButton: false,
                title: $('#importantMessageModal .title').text(),
                message: $('#importantMessageModal .body').html(),
                buttons: {
                    ok: {
                        label: 'OK',
                        callback: function() {
                            impMessageModal.modal('hide');
                        }
                    }
                }
            });
            _gaq.push(['_trackEvent', 'Morningstar Strategy Overview Disclosure', 'Popup', 'OK']);
        },

        completeMethodology : function(){
            var compMethodlogyModal = bootbox.dialog({
                size: 'large',
                closeButton: true,
                className: 'complete-methodology-modal',
                title: $('#completeMethodologyModal .title').text(),
                message: $('#completeMethodologyModal .body').html()
            });
        },

        confirmInfo : function(){
            this.$container.trigger("spe:router:redirect", 'confirmInfo');
        },

        returnProfile : function(){
            this.handleExit({morningStar: true, initial : true});
        },

        handleExit : function(data){
            $.publish("spe:morningStar:exit", data);
        },

        editProfile: function(){
            var $form = this.$section.find(".profileInfo");
                $form.find('.text').hide();
                $form.find('.input').show();
            this.$section.find('.actionEdit, .actionProceed').hide();
            this.$section.find('.actionSave, .actionCancel').show();
        },

        cancelProfile: function(){
            var $form = this.$section.find(".profileInfo");
            $form.find('.input').hide();
            $form.find('.text').show();
            this.$section.find('.actionSave, .actionCancel').hide();
            this.$section.find('.actionEdit, .actionProceed').show();
        },

        render : function(templateName, modelData){
            var tpl = _.template(this.getTemplate(templateName || 'index'))({ model : modelData });
            this.$section.html(tpl).addClass('text');
        },

        initConfirmInfoView : function(){
            var translation = Section.getData('translation'),
                rPanel = this.$section.find('.rightBlock'),
                model = Section.getData('common').morningstar,
                mStarContribution = this.$section.find('.mstar_contribution'),
                firstName = model.FirstName ? model.FirstName : '',
                lastName = model.LastName ? model.LastName : '';

            if(mStarContribution.find('tr').length > 4){
                this.$section.find('.leftInfoBlock').append(rPanel);
            }else{
                this.$section.find('.leftInfoBlock').addClass('large');
            }
            $('#footer .emp_name').text(translation.employee.toUpperCase() + ': ' + firstName + ' ' + lastName);
        },

        drawChart : function(){
            var chartData = Section.getData('morningstar').strategy ?
                Section.getData('morningstar').strategy.ProposedAssetMix : null,
                translation = Section.getData('translation');

            if(_.isEmpty(chartData) || !$('#asset-mix-chart-wrapper').length){
               return;
            }
            var data = new google.visualization.DataTable();
            data.addColumn('string', translation.asset_mix);
            data.addColumn('number', translation.percentage);
            data.addRows($.map(chartData, function(percent, fname) { return [[fname, percent]]; }));

            var options = {
                backgroundColor: '#EFEFEF',
                pieSliceBorderColor: 'none',
                pieSliceTextStyle: {fontSize:11},
                legend: {alignment: 'center'},
                width: '100%',
                height: '100%',
                chartArea: {left:0,width:'100%',height:'100%'},
                colors: ['#ff9900', '#99cc00', '#993333', '#003366', '#6699cc'],
                pieHole: 0.4
            };
            var chart = new google.visualization.PieChart(document.getElementById('asset-mix-chart-wrapper'));
                chart.draw(data, options);
        },

        validateConfirmInfo : function(){
            var model = Section.getData('common'),
                translation = Section.getData('translation');

            $("#profile_info_form").validate({
                errorClass: "error",
                validClass: "success",
                ignore: ".ignore",
                ignoreTitle: true,
                onfocusout: function(element) { sn = 1; $(element).valid(); },
                errorPlacement: function(error, element) {
                    if(sn){
                        sn = 0;
                        element.tooltip('destroy').tooltip({'title': error.text()});
                    }
                },
                unhighlight: function(element){
                    $(element).removeClass("error");
                    $(element).tooltip('destroy');
                },
                rules: {
                    FirstName: "required",
                    LastName: "required",
                    Gender: "required",
                    State: "required",
                    Email: {required: true, maxlength: 80, email: true},
                    AnnualSalary: {required: true, number: true, min: 4000, max: parseFloat(model.limit_402)},
                    Balance: {required: true, number: true, range: [0, 99999999]},
                    RetireAge: {required: true, number: true, range: [65, 115]},
                    'contribution[Y50Contri][ContriDollar]': {required: true, number: true, max: parseFloat(model.limit_50y)},
                    'contribution[Y50Contri][Length]': {required: true, number: true, range: [0, 50]},
                    'contribution[RothY50Contri][ContriDollar]': {required: true, number: true, max: parseFloat(model.limit_50y)},
                    'contribution[RothY50Contri][Length]': {required: true, number: true, range: [0, 50]},
                    'contribution[LTContri][ContriDollar]': {required: true, number: true, max: parseFloat(model.longTermEm)},
                    'contribution[LTContri][Length]': {required: true, number: true, range: [0, 50]},
                    'contribution[RothLTContri][ContriDollar]': {required: true, number: true, max: parseFloat(model.longTermEm)},
                    'contribution[RothLTContri][Length]': {required: true, number: true, range: [0, 50]},
                    'contribution[Y3Contri][ContriDollar]': {required: true, number: true, max: parseFloat(model.lastThreeYears)},
                    'contribution[Y3Contri][Length]': {required: true, number: true, range: [0, 3]},
                    'contribution[RothY3Contri][ContriDollar]': {required: true, number: true, max: parseFloat(model.lastThreeYears)},
                    'contribution[RothY3Contri][Length]': {required: true, number: true, range: [0, 3]}
                },
                messages: {
                    FirstName: translation.please_specify_your_first_name,
                    LastName: translation.please_specify_your_last_name,
                    Gender: translation.please_specify_your_gender,
                    State: translation.please_select_your_state_of_residence,
                    Email: {required: translation.please_specify_your_email_address,email: translation.please_check_your_full_email_address},
                    AnnualSalary: {required: translation.please_specify_your_annual_salary,number:translation.please_check_that_you_entered_valid_salary,min: translation.please_confirm_that_your_salary_amount_correctly,max: translation.the_pretax_and_roth_contribution_amount_less_then_equal+model.limit_402+" "+translation.annually_excluding_catchup_contributions+ model.limit_402 +"."},
                    Balance: {required: translation.please_specify_your_account_balance, range: translation.please_enter_balance_between_0_to_99999999},
                    RetireAge: {required: translation.please_specify_your_retirement_age, range: translation.retirement_age_must_be_greater_than_your_current_age},
                    'contribution[Y50Contri][ContriDollar]': {required: translation.please_specify_your_annual_50_years_of_age, max: translation.the_50_years_of_age_catch_up_contribution_amount_must_be_less+ model.limit_50y +"."},
                    'contribution[Y50Contri][Length]': {required: translation.please_specify_number_of_years_of_your_50_years_of_age, range: translation.please_enter_value_between_0_to_50_for_the_number_of_years},
                    'contribution[RothY50Contri][ContriDollar]': {required: translation.please_specify_your_annual_50_years_of_age_roth_catchup, max: translation.the_50_years_of_age_roth_catch_up_contribution_amount_must_be_less + model.limit_50y +"."},
                    'contribution[RothY50Contri][Length]': {required: translation.please_specify_number_of_years_of_50_years_of_age, range: translation.please_enter_value_between_0_and_50},
                    'contribution[LTContri][ContriDollar]': {required: translation.please_specify_your_annual_catchup, max: translation.the_long_term_employee_catch_up_contribution + model.longTermEm +"."},
                    'contribution[LTContri][Length]': {required: translation.please_specify_number_of_years_of_your_long_term_employee, range: translation.please_enter_value_between_0_and_50_for_the_number_years},
                    'contribution[RothLTContri][ContriDollar]': {required: translation.please_specify_your_annual_long_term_employee, max: translation.the_long_term_employee_roth_catchup_contribution_amount_must_less + model.longTermEm +"."},
                    'contribution[RothLTContri][Length]': {required: translation.please_specify_number_of_years_of_your_long_term_employee_roth, range: translation.please_enter_value_between_0_and_50_for_the_number_of_your_long_term_roth},
                    'contribution[Y3Contri][ContriDollar]': {required: translation.please_specify_your_annual_last_3_years_of_employment, max: translation.the_last_3_years_of_employment_catch_up_contribution_amount + model.lastThreeYears +" " + translation.and_its_percentage_must_be_less_then_salary},
                    'contribution[Y3Contri][Length]': {required: translation.please_specify_number_of_years_of_your_last_3_years_of_employment, range: translation.please_enter_value_between_0_and_3_for_number_of_years},
                    'contribution[RothY3Contri][ContriDollar]': {required: translation.please_specify_your_annual_last_3_years_of_employment_roth, max: translation.the_last_3_years_of_employment_roth_catchup_contribution_amount + model.lastThreeYears +" " + translation.and_its_percentage_must_be_less_than_of_salary},
                    'contribution[RothY3Contri][Length]': {required: translation.please_specify_number_of_years_of_your_last_3_years_of_employment_roth_catch_up, range: translation.please_enter_value_between_0_and_3_for_the_number_of_years}
                }
            });

        }

    };

    MorningStarView.initialize();

    return {
        render: MorningStarView.render.bind(MorningStarView),
        initConfirmInfoView : MorningStarView.initConfirmInfoView.bind(MorningStarView),
        validateConfirmInfo : MorningStarView.validateConfirmInfo,
        handleExit : MorningStarView.handleExit,
        drawChart : MorningStarView.drawChart
    };
});