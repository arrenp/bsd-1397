define(['jquery', 'spe/view/PlayerView', 'spe/model/Section'], function($, playerView, Section){
    "use strict";

    var showPlayer = function(){
          var model = Section.getData('playlist').planBasics;
              playerView.render(".section-area", model);
        },
        start = function(){
            showPlayer();
        };

    return {
        start: start
    }
});