define(['jquery','moment','underscore','./SpeController','spe/service/common','spe/view/RetirementNeedView','spe/model/Section', 'service/data', 'spe/view/PlayerView'], function($, moment, _, SPE, common, retirementNeedView, Section, data, playerView){
    "use strict";

    var RetirementNeed = new SPE.Controller({

            name : 'retirementNeeds',
            CurrentSocialSecurityOnly : 1,
            EnhancedSocialSecurityOnly : 2,
            PensionOnly : 3,
            EnhancedSocialSecurityAndPension :4,

            loadPage : function(index){
                var model = Section.getData('retirement_needs'),
                    common = Section.getData('common'),
                    idx = index ? index : 1;
                    model.data_source = common.data_source;
                    model.planBalance = common.planBalance;
                    model.age = !isNaN(moment().diff(common.dateOfBirth, 'years')) ? moment().diff(common.dateOfBirth, 'years') : 0;

                retirementNeedView.render('.content-area', model, idx);
                var audio = idx > 4 ? common.RN_Audio[idx] : common.RN_Audio[idx-1];
                if(idx == 4){
                    $('#retirement_age').trigger('change');
                }
                playerView.play(audio, 'audio');
                $('body').trigger('spe:disclosure:hide');
                if(idx == 7){
                    $('body').trigger('spe:disclosure:show', { title: 'targetMonthlyContribution'});
                }
            },

            setMessage : function(msg){
                this.errMessage += msg + '<br>';
            },

            hasError : function(msg){
                if(common.hasError(msg)){
                    this.errMessage = '';
                    return true;
                }
                return false;
            },

            actions : {

                init : function(e, d){
                    var index = d ? d.current : false;
                    this.loadPage(index);
                },

                page1 : function(e, d){
                    if(d.dir == 'next'){
                        var q1 = common.dollar2float($('#q1').val());
                        this.errMessage = '';
                        if (!common.validator(q1)) {
                            this.setMessage(this.translation.please_enter_yearly_income);
                        }
                        if(Section.getData('module').maximumAnnualIncome > 0 && q1 > Section.getData('module').maximumAnnualIncome) {
                            this.setMessage(this.translation.enter_a_lower_salary + Section.getData('module').maximumAnnualIncome);
                        }
                        if(this.hasError(this.errMessage)){
                            return false;
                        }
                        var sectionData = Section.getData('retirement_needs');
                        var socialSecurityMultiplier = sectionData.socialSecurityMultiplier;
                        if(typeof sectionData.RN_CurrentYearlyIncome !== typeof undefined && sectionData.RN_CurrentYearlyIncome != q1){
                            _.extend(sectionData, {
                                'RN_EstimatedRetirementIncome' : Math.round(q1 * (sectionData.replacementIncomePercent / 100)),
                                'RN_EstimatedSocialSecurityIncome' : q1 * socialSecurityMultiplier > Section.getData('serverConfig').estimatedSocialSecurityIncomeMax ? Section.getData('serverConfig').estimatedSocialSecurityIncomeMax : Math.round(q1 * socialSecurityMultiplier)
                            });
                        }
                        if(typeof sectionData.RN_EstimatedRetirementIncome === 'undefined' || sectionData.RN_EstimatedRetirementIncome == null){
                            _.extend(sectionData, {
                                'RN_CurrentYearlyIncome' : q1,
                                'RN_EstimatedRetirementIncome' : Math.round(q1 * (sectionData.replacementIncomePercent / 100)),
                                'RN_EstimatedSocialSecurityIncome' : q1 * socialSecurityMultiplier > Section.getData('serverConfig').estimatedSocialSecurityIncomeMax ? Section.getData('serverConfig').estimatedSocialSecurityIncomeMax : Math.round(q1 * socialSecurityMultiplier)
                            });
                        }else{
                            _.extend(sectionData, {
                                'RN_CurrentYearlyIncome' : q1
                            });
                        }
                        
                        switch (sectionData.retirementNeedsFlow) {
                            case this.PensionOnly:
                            case this.EnhancedSocialSecurityAndPension:
                                _.extend(sectionData, {
                                    'RN_EstimatedPensionIncome' : sectionData.RN_EstimatedPensionIncome ? sectionData.RN_EstimatedPensionIncome : 0
                                });
                            break;
                        }
                        
                        Section.setData('retirement_needs', sectionData);
                        _gaq.push(['_trackEvent', 'Retirement Needs', d.dir.toUpperCase(), 'Current Yearly Income', parseInt(q1)]);
                    }
                    this.loadPage(d.current);
                },

                page2 : function(e, d){
                    
                    if(d.dir == 'next'){
                        var q2 = common.dollar2float($('#q2').val()),
                            q3 = 0,
                            q3p = 0;

                        var retirementNeedsFlow = Section.getData('retirement_needs').retirementNeedsFlow;
                        if (retirementNeedsFlow == this.CurrentSocialSecurityOnly) {
                            q3 = $('#q3').val() ? common.dollar2float($('#q3').val()) : $('#q3').val();
                        } else if (retirementNeedsFlow == this.EnhancedSocialSecurityOnly) {
                            q3 = $('#q3').val() ? common.dollar2float($('#q3').val()) : $('#q3').val();
                        } else if (retirementNeedsFlow == this.PensionOnly) {
                            q3p = $('#q3p').val() ? common.dollar2float($('#q3p').val()) : $('#q3p').val();
                        } else if (retirementNeedsFlow == this.EnhancedSocialSecurityAndPension) {
                            q3 = $('#q3').val() ? common.dollar2float($('#q3').val()) : $('#q3').val();
                            q3p = $('#q3p').val() ? common.dollar2float($('#q3p').val()) : $('#q3p').val();
                        }
                        
                        if (!common.validator(q2)){
                            this.setMessage(this.translation.please_enter_estimated_retirement_income);
                        }
                        if (retirementNeedsFlow != this.PensionOnly) {
                            if (isNaN(parseFloat(q3)) ||  q3 === ""){
                                this.setMessage(this.translation.please_enter_estimated_social_security_income);
                            }else if(q3 > Section.getData('serverConfig').estimatedSocialSecurityIncomeMax){
                                this.setMessage(this.translation.please_enter_upto_estimated_social_security_income.replace('24,636',Section.getData('serverConfig').estimatedSocialSecurityIncomeMax));
                            }
                        }
                        if ((retirementNeedsFlow == this.PensionOnly) || (retirementNeedsFlow == this.EnhancedSocialSecurityAndPension)) {
                            if (isNaN(parseFloat(q3p)) ||  q3p === ""){
                                this.setMessage(this.translation.please_enter_estimated_pension_income);
                            }
                        }
                        if(this.hasError(this.errMessage)){
                            return false;
                        }

                        var sectionData = {
                            'RN_EstimatedRetirementIncome' : q2,
                            'RN_EstimatedSocialSecurityIncome' : q3,
                            'RN_EstimatedPensionIncome' : q3p,
                            'RN_ReplacementIncome' : q2 - q3 - q3p
                        };
                        Section.setData('retirement_needs', sectionData);
                        _gaq.push(['_trackEvent', 'Retirement Needs', d.dir.toUpperCase(), 'Estimated Retirement Income', parseInt(q2)]);
                        _gaq.push(['_trackEvent', 'Retirement Needs', d.dir.toUpperCase(), 'Estimated Social Security Income', parseInt(q3)]);
                    }
                    this.loadPage(d.current);
                },

                page3 : function(e, d){
                    if(d.dir == 'next'){
                        var q4 = common.dollar2float($('#q4').val());
                        if (!common.validator(q4)){
                            this.setMessage(this.translation.please_enter_replacement_income)
                        }
                        if(this.hasError(this.errMessage)){
                            return false;
                        }

                        var sectionData = {
                            'RN_ReplacementIncome' : q4
                        };
                        Section.setData('retirement_needs', sectionData);
                        _gaq.push(['_trackEvent', 'Retirement Needs', d.dir.toUpperCase() , 'Replacement Income', parseInt(q4)]);
                    }
                    this.loadPage(d.current);
                },

                page4 : function(e, d){
                    if(d.dir == 'next'){

                        var q5 = $('#q5').val(),
                            q5a = common.dollar2float($('#q5a').val()),
                            retirementAge = Section.getData('retirement_needs').RN_RetirementAge;

                        if(Section.getData('common').retirementNeedsVersion == 'by age'){
                            retirementAge = $('#retirement_age').val();
                            if(!common.validator(q5)){
                                this.setMessage(this.translation.please_enter_the_age_of_retirement);
                            }
                        }else{
                            if(!common.validator(q5)){
                                this.setMessage(this.translation.please_select_number_year);
                            }
                        }

                        if(!common.validator(q5a)){
                            this.setMessage(this.translation.please_enter_inflation_income);
                        }
                        if(this.hasError(this.errMessage)){
                            return false;
                        }

                        var sectionData = {
                            'RN_NumberOfYearsBeforeRetirement' : q5,
                            'RN_InflationAdjustedReplacementIncome' : q5a,
                            'RN_RetirementAge' : retirementAge
                        };
                        Section.setData('retirement_needs', sectionData);
                        _gaq.push(['_trackEvent', 'Retirement Needs', d.dir.toUpperCase() , 'Inflation-Adjusted Replacement Income', parseInt(q5a)]);
                        _gaq.push(['_trackEvent', 'Retirement Needs', d.dir.toUpperCase() , 'Years Before Retirement', parseInt(q5)]);
                    }
                    this.loadPage(d.current);
                },

                page5 : function(e, d){
                    if(d.dir == 'next'){
                        var otherAssets = common.dollar2float($('#q6b').val()),
                            totalAssets = $('#totalAssets').val(),
                            assetTypes = $('.assetTypeForm').serializeArray(),
                            planBalance = common.dollar2float($('#q6a').val()),
                            commonData = Section.getData('common');

                        var sectionData = {
                                'RN_OtherAssets' : otherAssets,
                                'RN_TotalAssets' : totalAssets,
                                'RN_AssetTypes' : assetTypes
                            };

                        if(Section.getData('common').data_source){
                            _.extend(sectionData, {
                                'planBalance' : planBalance
                            });
                        }
                        Section.setData('retirement_needs', sectionData);

                        _.extend(commonData, {'planBalance' : planBalance});
                        Section.setData('common', commonData);

                        _gaq.push(['_trackEvent', 'Retirement Needs', d.dir.toUpperCase(), 'Other Assets', parseInt(otherAssets)]);
                        _gaq.push(['_trackEvent', 'Retirement Needs', d.dir.toUpperCase(), 'Current Plan Balance', parseInt(planBalance)]);
                        _gaq.push(['_trackEvent', 'Retirement Needs', d.dir.toUpperCase(), 'Asset Types - '+assetTypes]);
                        _gaq.push(['_trackEvent', 'Retirement Needs', d.dir.toUpperCase(), 'Total Assets', parseInt(totalAssets)]);
                    }

                    this.loadPage(d.current);
                },

                page6 : function(e, d){
                    if(d.dir == 'next'){
                        var q7 = parseInt($('#q7').val()),
                            lifeExpectancy = Section.getData('retirement_needs').RN_LifeExpectancy;

                        if(Section.getData('common').retirementNeedsVersion == 'by age'){
                            var lifeExpectancy = parseInt($('#life_expectancy').val());
                            var retirementAge = parseInt(Section.getData('retirement_needs').RN_RetirementAge);
                            q7 = lifeExpectancy - retirementAge;
                            if (!common.validator(q7) || isNaN(lifeExpectancy) || lifeExpectancy < retirementAge) {
                                this.setMessage(this.translation.please_enter_the_age_you_anticipate_no_longer_requiring);
                            }
                        }else{
                            if (!common.validator(q7)) {
                                this.setMessage(this.translation.please_enter_number_years_retirement);
                            }
                        }
                        if(this.hasError(this.errMessage)){
                            return false;
                        }
                        var sectionData = Section.getData('retirement_needs');
                        _.extend(sectionData, { 'RN_YearsYouWillLiveInRetirement' : q7 , 'RN_LifeExpectancy' : lifeExpectancy});
                        if(common.isSet(sectionData.RN_InflationAdjustedReplacementIncome)){
                            _.extend(sectionData, { 'RN_AnnualIncomeNeededInRetirement' : parseInt(sectionData.RN_InflationAdjustedReplacementIncome) });
                        }
                        Section.setData('retirement_needs', sectionData);
                        _gaq.push(['_trackEvent', 'Retirement Needs', d.dir.toUpperCase(), 'Number of Years', parseInt(q7)]);
                    }
                    this.loadPage(d.current);
                },

                save : function(e, d){
                    var self = this;
                    if(d.dir == 'save'){
                        var annualIncome = parseInt(Math.round($('#annualIncome').val())),
                            estimatedSavings =  parseInt(Math.round($('#estimatedSavings').val())),
                            monthlyContribution =  parseInt(Math.round($('#monthlyContribution').val())),
                            pctValue = parseInt($('#pctValue').val()),
                            retireNeedsStatus = 2;

                        var sectionData = {
                            'RN_AnnualIncomeNeededInRetirement' : annualIncome,
                            'RN_EstimatedSavingsatRetirement' : estimatedSavings,
                            'RN_RecommendedMonthlyPlanContribution' : monthlyContribution,
                            'RN_IncomePct' : pctValue,
                            'retireNeedsStatus' : retireNeedsStatus
                        };
                        Section.setData('retirement_needs', sectionData);

                        var pData = {
                            'retirement_needs' : Section.getData('retirement_needs'),
                            'common' : Section.getData('common')
                        };

                        data.saveRetirementNeeds(pData).then(function(data){

                            if(parseInt(Section.getData('common').flexPlan)) {
                                $('body').trigger('spe:router:redirect', { to: 'contributions', complete : self.name });
                            }
                            else if (!parseInt(Section.getData('module').riskBasedQuestionnaire)){
                                $('body').trigger('spe:router:redirect', { to: 'investments', complete : self.name });
                            }                                                        
                            else{
                                $('body').trigger('spe:router:redirect', { to: 'riskProfile', complete : self.name });
                            }
                            _gaq.push(['_trackEvent', 'Retirement Needs', 'Save']);
                        });
                    }

                    if(d.dir == 'back'){
                        this.loadPage(d.current);
                    }
                }
            },

            start : function(){
                this.playVideos();
            }

        });

    return {
        start: RetirementNeed.start.bind(RetirementNeed)
    }
});