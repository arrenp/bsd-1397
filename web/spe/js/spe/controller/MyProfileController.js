define(['jquery', 'underscore', 'spe/service/common', './SpeController', 'service/data', 'spe/view/MyProfileView', 'spe/model/Section', 'spe/service/MyProfileService', './PersonalController', './BeneficiaryController', 'spe/view/PlayerView'], function($, _, common, SPE, data, MyProfileView, Section, myProfileService, personal, beneficiary, playerView){
    "use strict";

    var MyProfile = new SPE.Controller({

        name: 'myProfile',

        actions : {

            index : function(){
                var myProfile = Section.getData('my_profiles'),
                    common = Section.getData('common'),
                    updateProfileProceed = myProfile.updateProfileProceed || 0,
					retirementNeeds = Section.getData('retirement_needs'),
					translations = Section.getData('translations');
                var model = Section.getData('common');
                if(updateProfileProceed){
					var found = retirementNeeds.RN_AssetTypes && retirementNeeds.RN_AssetTypes.some(function(e) {
						return e.value === translations.retirement_needs.asset_type_option_4 ||
							e.value === translations.retirement_needs.asset_type_option_8;
					});
					if (myProfile.rolloverWorkflow && !myProfile.rolloverCompleted && found) {
						if (common.rolloverVideo) 
						{
							playerView.render('.content-area', _.extend(JSON.parse(localStorage.getItem('mediaPath')), { 'playlist' : [common.rolloverVideo], 'callback' : this.actions.rollover.bind(this)}), 'video');
						}
						else 
						{
							this.actions.rollover.apply(this);
						}
						return;
					}
					var obj = {};
					if(myProfile.endVideoStatus){
						this.actions.thankYou.apply(this);
						obj = {'endVideoStatus' : 0, 'updateProfileProceed' : 0, 'updateProfileEmail' : 0, 'updateProfile' : 0};
					}else{
						common.thankYouVideo ? playerView.render('.content-area', _.extend(JSON.parse(localStorage.getItem('mediaPath')), { 'playlist' : [common.thankYouVideo], 'callback' : this.actions.thankYou.bind(this)}), 'video') : '';
						obj = {'endVideoStatus' : 1};
					}
					Section.setData('my_profiles', obj);
                }else if(myProfile.updateProfileEmail){

                    this.actions.sendProfile.apply(this);
                }else if(myProfile.updateProfile){
					
                    this.actions.update.apply(this);
                }else if(common.data_source == 'expertplan' && !common.updateEPProfile && !common.updateProfileBeneficiary){

                    personal.start(2);
                }else if(Section.getData('partBeneficiaries') && (common.beneficiaries || common.data_source == 'relius') && !common.updateProfileBeneficiary){   // relius new & omni with beneficiaries on
                    if (common.data_source == "relius" || common.data_source.match('omni'))
                    beneficiary.start("relius");    
                    else
                    beneficiary.start(common.data_source);
                }else if( ((common.data_source == 'relius' && !common.newVersion) || common.data_source != 'relius') && common.beneficiaries && !common.updateProfileBeneficiary && !common.data_source.match('omni')){  // relius old and others except omni

                    beneficiary.start();
                    Section.setData('common', {'updateEPProfile' : 0});
                }else{

                    this.actions.profile.apply(this);
                    Section.setData('common', {'updateProfileBeneficiary' : 0, 'updateEPProfile' : 0});
                }
            },

            showAnswer : function(e, args){
                var $options = $(args.target);
                $options.parent().parent().find('.answer').html($options.find('option:selected').val());
            },

            profile : function(){
                var model = myProfileService.initData();
                this.loadPage('profile', model);

                if(!model.common.planAllowsElections){
                    $('#dontForgetModel1').modal('show');
                }
                if(!model.common.planAllowsDeferrals){
                    $('#dontForgetModel2').modal('show');
                }
                playerView.play(model.common.PR_Audio, 'audio');
                $('body').trigger('spe:disclosure:show', { title: 'investorProfile'});
            },

            thankYou : function(){
                var model = Section.getData('common');
                Section.setData('my_profiles', {'endVideoStatus' : 0, 'updateProfileProceed' : 0, 'updateProfileEmail' : 0, 'updateProfile' : 0});
                
                if(model.data_source == 'expertplan'){
                    if(model.forwardUrl) {
                        var forwardUrl = decodeURIComponent(model.forwardUrl),
                            url = forwardUrl + '?vwiseSessionid=' + model.session_id + '&vwiseAction=' + (model.initialEnrollment == 'YES' ? 'newEnrollment' : 'changeRequest');
                        window.top.location.href = url;
                    }else{
                        this.actions.profile.apply(this);
                    }
                }else if(model.data_source == 'relius' && model.enrollment && model.newVersion){
                    if (model.enrollmentRedirectOn && common.isUrl(model.enrollmentRedirectAddress)) {
                        window.top.location.href = model.enrollmentRedirectAddress;
                        return;
                    }
                    if(model.enrollmentPath && common.isUrl(model.enrollmentPath)) {
                        window.top.location.href = model.enrollmentPath;
                    }else{
                        this.actions.profile.apply(this);
                    }
                }
                else if (Section.getData('my_profiles').trustedContactOptIn)
                {
                    this.loadPage('trusted-contact', model);
                }
                else{
                    this.loadPage('thank-you', model);
                }
                if (Section.getData('my_profiles').trustedContactOptIn)
                {                   
                    setTimeout(function()
                    {
                        window.top.location.href = decodeURIComponent(Section.getData('my_profiles').trustedContactRedirectUrl);
                    }, 2000);
                }
                else if(model.redirectWebsiteAddress && common.isUrl(decodeURIComponent(model.redirectWebsiteAddress))){
                    _gaq.push(['_trackPageview', 'profile/thankyou']);
                    setTimeout(function(){
                        window.top.location.href = decodeURIComponent(model.redirectWebsiteAddress);
                    }, 2000);
                }else{
                    _gaq.push(['_trackPageview', 'profile/thankyou']);
                    var self = this;

                }
                $('body').trigger('spe:disclosure:show', { title: 'thankyou'});
                var audio = (model.thankyouAudio && model.client_calling == 'LNCLN') ? model.P_Audio[14] : null;
                if(audio){
                    playerView.render('.audio-area', _.extend(JSON.parse(localStorage.getItem('mediaPath')), { 'playlist' : [audio]}), 'audio');
                }
            },

            sendProfile : function(){
                var model = Section.getData('common'),
                    audio = (model.data_source == 'expertplan') ? model.P_Audio[11] : null;

                var email = model.email;
                if (Section.getData('module').ATBlueprint) {
                    $.ajax({
                        type: "POST",
                        url: "/commonData",
                        data: {'attr': 'common'},
                        success: function(data) {
                            email = data.ATemail;
                        },
                        async: false
                    });
                }

                this.loadPage('send-email', {
                    'firstName': model.firstName,
                    'lastName': model.lastName,
                    'email': email,
                    'edelivery': model.edelivery
                });
                audio ? playerView.render('.audio-area', _.extend(JSON.parse(localStorage.getItem('mediaPath')), {'playlist': [audio]}), 'audio') : '';

            },

            update : function(){
                var model = myProfileService.transaction.getUpdateSection();
                this.loadPage('update', model);
                myProfileService.transaction.init(model);
                myProfileService.transaction.postACAOptOut().then(function(){
                    myProfileService.transaction.start();
                });
                playerView.play(model.common.PU_Audio, 'audio');
                $('body').trigger('spe:disclosure:show', { title: 'confirmation'});
            },

            back : function(){
                $('body').trigger("spe:router:reload");
            },

            backBeneficiary : function(){
                Section.setData('common', { 'updateEPProfile' : 1});
                $('body').trigger("spe:router:reload");
            },

            cancel : function(){
                common.showConfirmMsg(this.translation.my_profile_cancel, function(result){
                    if(result) {
                        $('body').trigger('spe:router:redirect','investments');
                        _gaq.push(['_trackEvent', 'Profile', 'Cancel']);
                    }
                });

            },
            
            saveTrustedContact : function(e, args) {
                var self = this;
                var translations = Section.getData('translations')
                var trustedContactOptIn = $(args.target).attr('href') === '#optIn' ? 1 : 0;
                data.saveTrustedContact({trustedContactOptIn: trustedContactOptIn }).then(function(response)
                {
                    $('#trustedContactModal').modal('hide');
                    if (response.success)
                    {
                        Section.setData('my_profiles', {trustedContactCompleted : 1, trustedContactOptIn: trustedContactOptIn, trustedContactRedirectUrl: response.redirectUrlWithToken});                      
                    }
                    else
                    {
                        Section.setData('my_profiles', {trustedContactCompleted : 1});                      
                        common.showErrMsg(self.translation.update_error + ": " +  translations.global.trusted_contact);
                    }
                    self.actions.save();
                });
            },

            save : function(){
                var model = Section.getData('common');
                var common = model;
                var myProfile = Section.getData('my_profiles');
                if(!model.omnii_email && model.ECOMMModal == '1'){
                    $('#emailTransactionModal').find('#email').val(model.email);
                    $('#emailTransactionModal').modal('show');
                    if (model.ECOMMModalChecked == 0) {
                        $("#e-delivered").prop('checked',false);     
                    }          
                    return false;
                }
                
                if (!myProfile.trustedContactCompleted && myProfile.trustedContactOn) {
                    $('#trustedContactModal').modal('show');
                    return false;
                }

                if(common.data_source.match('omni') || common.data_source.match('guardian')){
                    $("#mainNav").find('.highlight').not('#library').addClass('full-lock');
                }

                Section.setData('my_profiles', {'updateProfile' : 1});
                Section.setData('profile_transaction', {'enrollment' : 0, 'ep_modal' : 1});
                var p_data = {
                        'common' : {
                            'updateEPProfile' : model.updateEPProfile,
                            'updateBeneficiary' : model.updateBeneficiary,
                            'updateProfileBeneficiary' : model.updateProfileBeneficiary,
                            'email' : model.email,
                            'edelivery' : model.edelivery,
                            'spousalWaiverFormStatus' : model.spousalWaiverFormStatus ? 1 : 0
                        },
                        'my_profiles' : {
                            'updateProfile' : model.updateProfile
                        },
                        'partBeneficiaries' : Section.getData('partBeneficiaries'),
                        'partBeneficiariesDeleted' : Section.getData('partBeneficiariesDeleted'),
                        'beneficiary' : Section.getData('beneficiary')
                    };

                data.saveMyProfile(p_data).then(function(data){
                    if (Section.getData('my_profiles').trustedContactOptIn)
                    {
                        $("#trustedContactTransactionModal").modal("show");
                    }
                    _gaq.push(['_trackEvent', 'Profile', 'Save & Finish']);
                    $('body').trigger("spe:router:reload");
                });
            },

            emailAndSaveProfile : function () {
                var self = this,
                    model = Section.getData('common'),
                    email = $.trim($('#emailTransactionModal #email').val()),
                    extra = {};
                    extra.edelivery = $('#emailTransactionModal #e-delivered').length ? ($('#emailTransactionModal #e-delivered').is(':checked') ? 'E':'P') : '';

                if(!email || !common.isEmail(email)){
                    common.showErrMsg(this.translation.please_enter_email_address);
                    return false;
                }

                Section.setData('common', {'omnii_email' : 1, 'email' : email, 'edelivery' : extra.edelivery});
                $('#emailTransactionModal').modal('hide');
                self.actions.save();
            },

            proceedTransaction : function(){
                $('#confirmSubmitModal').modal('hide');
            },

            cancelTransaction : function(){
                $('#errorSubmitModal').modal('hide');
                Section.setData('common', {'updateProfileBeneficiary' : 1});
                $('body').trigger("spe:router:reload");
            },

            confirmTransaction : function(){
                Section.setData('profile_transaction', {'ep_modal' : 0});
                $('#updateTransactionModal').modal('hide');
            },

            processUpdates : function(){
                Section.setData('my_profiles', {'updateProfileEmail':1});
                _gaq.push(['_trackEvent', 'Profile', 'Proceed', 'Saving Transactions']);
                $('body').trigger("spe:router:reload");
            },

            printProfile : function(){
                var w = 880,
                    h = screen.height/1.5,
                    l = (screen.width/2)-(w/2),
                    t = (screen.height/2)-(h/2),
                    url = window.location.origin+'/profile/report/'+Section.getData('common').profileId,
                    win = window.open(url,'Investor_Profile','width='+ w +',height='+ h +',top='+ t +',left='+ l +',scrollbars=1, resizable=1,location=1');
                win.focus();

                setTimeout(function() {
                    win.window.print();
                }, 5000);
                _gaq.push(['_trackEvent', 'Profile', 'Print' ]);
            },

            emailProfile : function(){
                var model = Section.getData('common'),
                    name = $.trim($('.sendProfile #name').val()),
                    email = $.trim($('.sendProfile #email').val()),
                    self = this;

                if(!email || !common.isEmail(email)){
                    common.showErrMsg(this.translation.please_enter_email_address);
                    return false;
                }

                data.sendProfileEmail(email).then(function(response){
                    if ((response.hasOwnProperty("status") && response.status != 200) ||  (!response.hasOwnProperty("status") && JSON.parse(response).status != "success")){
                        common.showErrMsg(self.translation.error_the_email_was_not_sent);
                        $('body').trigger("spe:router:reload");
                    }
                    else{
                        _gaq.push(['_trackEvent', 'Profile', 'Send Email']);
                        Section.setData('my_profiles', {'updateProfileProceed' : 1});
                        $('body').trigger("spe:router:reload");                        
                    }
                });
            },
			rollover: function() {
				var common =  Section.getData('common');
				data.incrementContactViewCount();
				this.loadPage('rollover', common);
				playerView.play(Section.getData('common').rolloverAudio, 'audio');
			},
			saveRolloverOption: function(e, args) {
				$('.rolloverInput .errorMessage').text("");
				var option = {};
				if ($(args.target).attr('href') === '#emailMe') {
					var emailMe = $($(args.target).attr('href')).val();
					if (!emailMe) {
						$('#emailMe').next('.errorMessage').text("This field is required");
						return false;
					}
					option.emailMe = emailMe;
				}
				else if ($(args.target).attr('href') === '#callMe') {
					var callMe = $($(args.target).attr('href')).val();
					if (!callMe) {
						$('#callMe').next('.errorMessage').text("This field is required");
						return false;
					}
					option.callMe = callMe;
				}
				else {
					option.callUs = 1;
				}
				$('.saveRolloverOption').not(args.target).prop('disabled', true);
				$(args.target).text("SAVING...");
				data.saveRolloverOption(option).then(function(response){
					Section.setData('my_profiles', {rolloverCompleted : 1});
					$('.saveRolloverOption').not(args.target).prop('disabled', false).text("SELECT");
					$(args.target).addClass('proceed').text("PROCEED");
				});
			}
        },

        loadPage : function(templateName, model){
            $('body').trigger('spe:disclosure:hide');
            MyProfileView.render(templateName, model);
        },

        start : function(){
            this.actions.index.apply(this);
        }
		
    });

    return {
        start: MyProfile.start.bind(MyProfile)
    }
});
