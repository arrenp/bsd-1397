define(['jquery',
        './SpeController',
        'spe/controller/PersonalController',
        'spe/view/ContributionsView',
        'spe/service/contributionService',
        'spe/model/Section',
        'spe/view/PlayerView'
        ],
function($, SPE, personalController, contributionsView, contributionService, Section, playerView){
        "use strict";

    var Contributions = new SPE.Controller({
        name : 'contributions',
        start : function(){
            this.playVideos();
        },
        cacheElements : function(){
            this.saveContribution = $('#saveContribution');
            this.maximizeSavingModal = $('#maximizeSavingModal');
            this.paycheckAmountModal = $('#paycheckAmountModal');
            this.cArrangementModal = $('#cArrangementModal');
        },
        bindEvents : function(){
            this.saveContribution.on('click', this.actions.save.bind(this));
            this.maximizeSavingModal.find('.saveNext').on('click', this.actions.saveNext.bind(this));
            this.paycheckAmountModal.find('.action').on('click', contributionService.paycheckAmount.bind(this));
            this.cArrangementModal.find('.action').on('click', contributionService.contributionArrangement.bind(this));
            var self = this;
            $( "#pretaxslidercheckbox" ).click(function() 
            {
                self.validateChecked($(this),'salary_total_pct','salary_total_value','pretaxslidercheckboxLabel');
            });
            $( "#rothslidercheckbox" ).click(function() 
            {
                self.validateChecked($(this),'roth_total_pct','roth_total_value','rothslidercheckboxLabel');
            });
        },
        validateChecked : function(object,pctid,valueid,label){
            if (object.is(":checked"))
            {
                $("#" + pctid).prop('readonly', false);
                $("#" + valueid).prop('readonly', false);
                $("#" + pctid).val($("#" + pctid).prop("min"));
                $("#" + valueid).val($("#" + valueid).prop("min"));
                $("#" + label).fadeTo( "slow", 1 );
            }
            else
            {
                $("#" + pctid).prop('readonly', true);
                $("#" + valueid).prop('readonly', true);
                $("#" + pctid).val("");
                $("#" + valueid).val("");
                $("#" + label).fadeTo( "slow", .33 );
            }    
        },
        actions : {
            init: function(){
                var c_data = contributionService.initData(),
                    model = Section.getData('common'),
                    idx = '';

                if(model.ACAon){ idx = 3;}
                else if(model.planAllowsDeferrals){idx = 0;}
                else{idx = 2;}

                contributionsView.render('.content-area', c_data);
                playerView.play(model.C_Audio[idx], 'audio');

                $('body').trigger('spe:disclosure:show', { title: 'deferrals'});
                if(c_data.commonData.client_calling == 'MOO'){
                    Section.setData('common',{'pipCompleted' : 0});
                }
                this.cacheElements();
                this.bindEvents();
            },
            save : function(){
                var model = Section.getData('common');
                if (contributionService.cvalidate())
                {
                    return;
                }
                if((parseFloat(pre_tax_pct) + parseFloat(roth_tax_pct) + parseFloat(post_tax_pct)) < savingsMaximizer){
                    this.maximizeSavingModal.find('.savingMaximizer').text(savingsMaximizer);
                    this.maximizeSavingModal.find('.maxMatch').text(maxMatch.toLocaleString());
                    this.maximizeSavingModal.modal('show');
                }else{
                    model.showCatchup ? contributionService.saveCatchup() : contributionService.csave();
                }
            },
            saveNext : function(){
                var model = Section.getData('common');
                model.showCatchup ? contributionService.saveCatchup() : contributionService.csave();
            }
        }
    });

    return {
        start: Contributions.start.bind(Contributions)
    }

});
