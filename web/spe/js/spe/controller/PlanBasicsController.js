define(['jquery', './SpeController'], function($, SPE){

    var PlanBasicsController = new SPE.Controller({
            name : 'planBasics',
            start : function(){
                this.playVideos();
            }
        });

    return {
        start: PlanBasicsController.start.bind(PlanBasicsController)
    }
});