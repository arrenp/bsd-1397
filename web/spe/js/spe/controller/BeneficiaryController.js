define(['jquery','pubsub','spe/service/common','spe/view/BeneficiaryView','spe/model/Section','spe/view/PlayerView'], function($, pubsub, common, beneficiaryView, Section, playerView){
    "use strict";

    var Beneficiary = {

        start : function(type){
            var bType = type == 'relius' ? 1 : 0,
                mType = type == 'relius' ? 'partBeneficiaries' : 'beneficiary',
                data = Section.getData(mType) ? Section.getData(mType) : [],
                model = Section.getData('common');
                $('body').trigger('spe:disclosure:show', { title: 'beneficiary'});

            beneficiaryView.render(bType, data);
            if(model.data_source == 'educate' && type != 'relius'){
                playerView.play(model.P_Audio[4], 'audio');
            }
        }

    };

    return {
        start : Beneficiary.start.bind(Beneficiary)
    }

});