define(['jquery', 'underscore', 'moment', 'spe/service/common', 'spe/view/PlayerView', 'spe/view/MorningStarView', 'spe/model/Section', './PersonalController', 'spe/service/data'], function($, _, moment, common, playerView, MorningStarView, Section, personal, data){

    var MorningStarController = {

        name : "morningStar",

        init: function(){
            this.cacheElements();
            this.bindEvents();
        },

        cacheElements : function(){

            this.$container = $('body');
            this.$page = $(".content-area");
            this.$processing = $("#morningStartProcessing");
        },

        bindEvents : function(){
            var self = this;
            _.each(this.actions, function(handler, action){
                $.subscribe("spe:"+ self.name +":"+ action, self.actions[action].bind(self));
            });
        },
        actions : {

            index : function(){
                var model = Section.getData('common');
                if(model.data_source == 'educate' && !model.pipCompleted){
                    personal.start(model.pipContent);
                }else{
                    if(model.choosePathStatus){
                        MorningStarView.render('index', {});
                        playerView.play(model.mStar_Audio[0], 'audio');
                    }else{
                        Section.setData('common', {'choosePathStatus' : 1});
                        model.mstarVideo ? playerView.render('.content-area', _.extend(JSON.parse(localStorage.getItem('mediaPath')), { 'playlist' : [model.mstarVideo], 'callback' : this.actions.index.bind(this)}), 'video') : '';
                    }
                }
            },

            info: function(){
                var translation = Section.getData('translation'),
                    model = Section.getData('common'),
                    retrievingMStarModal = bootbox.dialog({
                        size: 'medium',
                        closeButton: false,
                        title: 'Retrieving data',
                        message: translation.please_hang_on_while_we_retrieve_your_data_from_morningstar + ' ...<br><div style="text-align:center;padding:20px;"><img alt="Loading..." src="/images/load-icon.gif"></div>'
                    });

                data.mStarProfileInfo().then(function(resp){
                    var response = JSON.parse(resp);
                    if(response.success){
                        retrievingMStarModal.modal('hide');
                        Section.setData('morningstar', {'participant' : response.data});
                        var infoData = response.data;
                            infoData.stateList = data.getStateList();
                        MorningStarView.render('info', infoData);
                        MorningStarView.initConfirmInfoView();
                        MorningStarView.validateConfirmInfo();
                        playerView.play(model.mStar_Audio[1], 'audio');
                    }
                });
            },

            assetMix: function(){
                var model = Section.getData('common');
                data.mStarAssetMix().then(function(resp){
                    var response = JSON.parse(resp);
                    if(response.success){
                        Section.setData('morningstar', {'strategy' : response.data});
                        Section.setData('common', {'updateStrategy' : 1});
                        MorningStarView.render('asset-mix', response.data);
                        MorningStarView.drawChart();
                        playerView.play(model.mStar_Audio[5], 'audio');
                    }
                });
            },

            reviewDetails: function(){
                var model = Section.getData('common');
                    model.morningstar = Section.getData('morningstar');
                    model.generatedDate = moment().format('dddd, MMMM D, YYYY H:m:s A');

                if(!model.mStarTermsAgree){
                    if(model.morningstar.participant.agreementURL){
                        MorningStarView.render('terms', { 'agreementURL' : model.morningstar.participant.agreementURL});
                        return false;
                    }
                }
                MorningStarView.render('review-details', model);
                MorningStarView.drawChart();
                playerView.play(model.mStar_Audio[6], 'audio');
            },

            sendFinish: function(){
                var model = Section.getData('common'),
                    audio = model.planAllowsDeferrals == 1 ? model.mStar_Audio[7] : model.mStarAudio[8],
                    morningstar = Section.getData('morningstar');

                if(model.mStarUpdate){
                    this.transaction.init();
                    MorningStarView.render('update', Section.getData('mstar_transaction'));
                    this.transaction.startTransaction();
                    playerView.play(model.audio, 'audio');
                }else if(model.sendProfile){
                    if(!model.endVideoStatus){
                        Section.setData('common', {'endVideoStatus' : 1});
                        model.mStarThankYouVideo ? playerView.render('.content-area', _.extend(JSON.parse(localStorage.getItem('mediaPath')), { 'playlist' : [model.mStarThankYouVideo], 'callback' : this.actions.sendFinish.bind(this)}), 'video') : '';
                    }else{
                        Section.setData('common', {'endVideoStatus' : 0, 'sendProfile' : 0});
                        this.actions.thankYou();
                    }
                }else{
                    MorningStarView.render('send-email', morningstar.participant);
                    playerView.play(model.mStar_Audio[9], 'audio');
                }
            },

            exitMorningStar : function(){
                var updateData = {'mstar_quit' : 1, 'morningstar' : 0};
                data.quitMStar(updateData).then(function(resp){
                    Section.setData('common', updateData);
                    MorningStarView.handleExit({morningStar: false});
                });
            },

            exit : function(e, data){
                this.$container.trigger("spe:router:reset", data);
            },

            privacyPolicy : function(){
                var self = this,
                    translation = Section.getData('translation'),
                    privacyPolicyModal = bootbox.dialog({
                        size: 'large',
                        closeButton: false,
                        className: 'privacyPolicyModal',
                        animate: false,
                        title: $('#mStarPrivacyPolicy .title').text(),
                        message: $('#mStarPrivacyPolicy .body').html(),
                        buttons: {
                            ok: {
                                label: translation.ok,
                                callback: function() {
                                    self.$container.trigger("spe:router:redirect", 'confirmInfo');
                                    MorningStarView.handleExit({morningStar: true});
                                    privacyPolicyModal.modal('hide');
                                }
                            }
                        }
                    });
            },

            saveProfile: function(){
                var profileForm = $('#profile_info_form'),
                    model = Section.getData('common'),
                    self = this;

                if(profileForm.valid()){
                    data.updateMStarProfileInfo(profileForm.serializeArray()).then(function(resp){
                        var response = JSON.parse(resp);
                        if(response.success){
                            profileForm.find('.input').hide();
                            profileForm.find('.text').show();
                            self.$page.find('.actionSave, .actionCancel').hide();
                            self.$page.find('.actionEdit, .actionProceed').show();
                            Section.setData('morningstar', {'participant' : response.data});
                            response.data.stateList = data.getStateList();

                            MorningStarView.render('info', response.data);
                            MorningStarView.initConfirmInfoView();
                            MorningStarView.validateConfirmInfo();
                            playerView.play(model.mStar_Audio[1], 'audio');
                        }
                    });
                }
            },

            proceed: function(){
                var self = this,
                    translation = Section.getData('translation'),
                    sendingMStarModal = bootbox.dialog({
                        size: 'medium',
                        closeButton: false,
                        title: 'Sending data',
                        message: translation.please_hang_on_while_we_send_your_data_to_morningstar + ' ...<br><div style="text-align:center;padding:20px;"><img alt="Loading..." src="/images/load-icon.gif"></div>'
                    });

                data.saveMStarProfile().then(function(resp){
                    var response = JSON.parse(resp);
                    if(response.success){
                        sendingMStarModal.modal('hide');
                        _gaq.push(['_trackEvent', 'MStar-ConfirmInfo', 'Proceed']);
                        self.$container.trigger("spe:router:redirect", 'assetMix');
                        MorningStarView.handleExit({morningStar: true});
                    }
                });
            },

            updateReviewDetail: function(){
                var self = this;
                data.updateReviewDetail().then(function(resp){
                    var response = JSON.parse(resp);
                    if(response.success){
                        Section.setData('common', { 'mStarUpdate' : 1});
                        _gaq.push(['_trackEvent', 'MStar-Profile', 'SaveAndFinish']);
                        self.$container.trigger("spe:router:redirect", 'sendFinish');
                        MorningStarView.handleExit({morningStar: true});
                    }
                })
            },

            emailProfile : function(){
                var translation = Section.getData('translation'),
                    name = $.trim($('.sendProfile #Name').val()),
                    email = $.trim($('.sendProfile #Email').val()),
                    self = this;

                if(!email || !common.isEmail(email)){
                    common.showErrMsg(translation.please_enter_email_address);
                    return false;
                }

                data.sendProfileEmail(email).then(function(response){
                    response = JSON.parse(response);
                    if(response.status == 'success'){
                        Section.setData('common', {'sendProfile' : 1});
                        _gaq.push(['_trackEvent', 'MStar-Profile', 'Send Email']);
                        self.$container.trigger("spe:router:redirect", 'sendFinish');
                    }else{
                        common.showErrMsg(translation.error_the_email_was_not_sent);
                    }
                });
            },

            thankYou : function(){
                var model = Section.getData('common');

                if(model.data_source == 'expertplan' && model.forwardUrl){
                    window.top.location.href = decodeURIComponent(model.redirectWebsiteAddress)+'vwiseSessionid=' + model.session_id + '&vwiseAction=' + model.initialEnrollment == 'YES' ? 'newEnrollment' : 'changeRequest';
                }else if(model.data_source == 'relius' && model.enrollment && model.newVersion){
                    window.top.location.href = model.enrollmentPath;
                }else{
                    MorningStarView.render('thank-you', null);
                }

                if(model.redirectWebsiteAddress){
                    _gaq.push(['_trackPageview', 'mStar/thankyou' ]);
                    setTimeout(function(){
                        window.top.location.href = decodeURIComponent(model.redirectWebsiteAddress);
                    }, 2000);
                }
            },

            printProfile : function(){
                var w = 880,
                    h = screen.height/1.5,
                    l = (screen.width/2)-(w/2),
                    t = (screen.height/2)-(h/2),
                    url = window.location.origin+'/profile/report/'+Section.getData('common').profileId,
                    win = window.open(url,'Investor_Profile','width='+ w +',height='+ h +',top='+ t +',left='+ l +',scrollbars=1, resizable=1,location=1');
                win.focus();

                setTimeout(function() {
                    win.window.print();
                }, 5000);
                _gaq.push(['_trackEvent', 'Profile', 'Print' ]);
            }

        },

        transaction : {

            init : function(){
                var model = Section.getData('common'),
                    updateSection = [];

                if(model.updateStrategy){
                    updateSection.push({'id' : 0, 'section' : 'Strategy'});
                }
                updateSection.push({'id' : 1, 'section' : 'Profile'});
                Section.setData('mstar_transaction', {
                    'updateSection' : updateSection,
                    'failed' : 0,
                    'complete' : 0
                });
            },

            startTransaction : function(){
                var profile = Section.getData('mstar_transaction');
                if(profile.complete < profile.updateSection.length){
                    this.updateTransaction(profile.updateSection[profile.complete]);
                }else{
                    this.finishTransaction();
                }
            },

            updateTransaction : function(section){
                var self = this;
                this.updateTransactionWidget(_.extend(section, {'type' : 'loading'}));

                data.updateMStarTransaction(section).then(function(response){
                    var result = JSON.parse(response);

                    if(section.id == 1 && result.confirmation){
                        Section.setData('common',{'profileId' : result.confirmation});
                    }
                    self.updateTransactionWidget(_.extend(section, {
                        'type' : result.success ? 'confirm' : 'error',
                        'message' : result.message
                    }));
                    Section.setData('mstar_transaction', {'complete' : parseInt(Section.getData('mstar_transaction').complete) + 1});
                    self.startTransaction();
                });
            },

            updateTransactionWidget : function(config){
                var widget = $('#transactionWidget'+config.id),
                    setting = {},
                    labels = [
                        'Strategy',
                        'Profile'
                    ];

                if(config.type == 'loading'){
                    setting = {
                        'add' : 'update',
                        'loader' : true,
                        'title' : labels[config.id] + '...',
                        'message' : ''
                    };
                }else if(config.type == 'confirm'){
                    setting = {
                        'add' : 'confirm',
                        'loader' : false,
                        'title' : labels[config.id] + ' ' + 'Updated' + '.',
                        'message' : config.message
                    };
                }else if(config.type == 'error'){
                    setting = {
                        'add' : 'error',
                        'loader' : false,
                        'title' : labels[config.id] + ' ' + 'Updated Failed' +'.',
                        'message' : 'Error: ' + config.message
                    };
                    Section.setData('mstar_transaction', { 'failed' : parseInt(Section.getData('mstar_transaction').failed) + 1});
                }

                widget.find('.loadIcon').removeClass('wait confirm error').addClass(setting.add);
                setting.loader ? widget.find('.spin').show() : widget.find('.spin').hide();
                widget.find('.updateTitle').html(setting.title);
                widget.find('.message').html(setting.message);
            },

            finishTransaction : function(){
                $('.actionProceedUpdate').show();
                Section.setData('common', {'mStarUpdate' : 0});
            }
        },

        start: function(actionName){
           this.actions[actionName || 'index'].apply(this);
        }

    };

    MorningStarController.init();

    return MorningStarController
});