define(['jquery', './SpeController', 'spe/model/Library', 'spe/view/LibraryView', 'spe/view/PlayerView'], function($, SPE, library, libraryView, playerView){

    var LibraryController = new SPE.Controller({
        name : 'library',
        start : function(){
            this.actions.index();
        },
        actions : {
            index: function(e, data){
                $('body').trigger('spe:disclosure:hide');
                if(data){ // render video
                    if(data.video) {
                        var playList = _.extend(JSON.parse(localStorage.getItem('mediaPath')), {'playlist': [data.video], type: 'normal', dir: data.dir});
                        playerView.render(".library-video", playList);
                        $('.content-area').removeClass('text');
                    }
                }else{
                    var model = library.getData();
                    libraryView.render(".content-area", model);
                }
            }
        }
    });

    return {
        start: LibraryController.start.bind(LibraryController)
    }
});