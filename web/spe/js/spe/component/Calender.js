define(['jquery', 'spe/model/Section'], function($, section){
    var trans = section.getData('translation');
    var Calender = {
        getMonthName : function(i){
            var trans = section.getData('translation'),
                month = [
                    trans.january,
                    trans.february,
                    trans.march,
                    trans.april,
                    trans.may,
                    trans.june,
                    trans.july,
                    trans.august,
                    trans.september,
                    trans.october,
                    trans.november,
                    trans.december
                ];
            return month[i];
        },
        createWidget : function(date){
            var m = '', d = '', y = '';
            if(date){
                var dateList = date.split('/');
                m = dateList[0].replace(/\b0(?=\d)/g, '');
                d = dateList[1].replace(/\b0(?=\d)/g, '');
                y = dateList[2].replace(/\b0(?=\d)/g, '');
            }

            return "<div id='calander'>" +
                        "<select class='normal' name='month'>" +
                            this.getMonth(m) +
                        "</select>" +
                        "<select class='normal' name='day'>" +
                            this.getDay(d) +
                        "</select>" +
                        "<select class='normal' name='year'>" +
                             this.getYear(y) +
                        "</select>" +
                   "</div>";
        },
        getMonth : function(month){
            var html = '<option value="">' + trans.month + '</option>';
            for(var i = 1; i <= 12; i++){
                var selected = (month == i) ? 'selected ' : '';
                html += "<option "+ selected +" value='"+ i  +"'>"+ this.getMonthName(i-1) +"</option>";
            }
            return html;
        },
        getDay : function(day){
            var html = '<option value="">' + trans.day + '</option>';
            for(var i = 1; i <= 31; i++){
                var selected = (day == i) ? 'selected ' : '';
                html += "<option "+ selected +" value='"+ i  +"'>"+ i +"</option>";
            }
            return html;
        },
        getYear : function(year){
            var html = '<option value="">' + trans.year + '</option>';
            for(var i = parseInt(new Date().getFullYear()); i >= 1900; i--){
                var selected = (year == i) ? 'selected ' : '';
                html += "<option "+ selected +" value='"+ i  +"'>"+ i +"</option>";
            }
            return html;
        },
        getWidget : function($date){
            return _.template(this.createWidget($date));
        }
    };

    return {
        getWidget : Calender.getWidget.bind(Calender)
    }

});