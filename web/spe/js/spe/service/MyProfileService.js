define(['jquery','underscore','underscore.string','moment','spe/service/common', 'spe/service/data', 'spe/model/Section','spe/service/RetirementNeedService', 'spe/service/investmentsService', 'spe/service/RiskProfileService'], function($, underscore, s, moment, common, data,Section, retirementNeedService, investmentService, RiskProfileService){
    "use strict";

    var MyProfileService = {


        initData : function(){
            var common = Section.getData('common'),
                translation = Section.getData('translation'),
                retirementNeedsData = Section.getData('retirement_needs'),
                retirementNeedQA = retirementNeedService.getAnswers(),
                investmentsData = Section.getData('investments'),
                contributionsData = Section.getData('contributions'),
                riskProfileData = Section.getData('risk_profile'),
                riskProfileQA = RiskProfileService.getAnswers(),
                preContributions = translation.not_applicable,
                rothContributions = translation.not_applicable,
                postContributions = translation.not_applicable,
                catchupContribution = translation.not_applicable,
                riskIndex = riskProfileData.RP_InvestorType,
                riskProfile = riskProfileData.RP_label,
                riskProfileDes = riskProfileData.RP_desc,
                beneficiary = Section.getData('beneficiary'),
                partBeneficiaries = Section.getData('partBeneficiaries'),
                investmentProfile = investmentService.getInvestmentProfile();

            if(contributionsData.contribution_mode == 'DEFERPCT'){
                if (Section.getData("pretaxslidercheckbox"))
                {
                    preContributions = contributionsData.C_PreTaxContributionPct + '%';
                }
                if(common.post){
                    postContributions = contributionsData.C_PostTaxContributionPct + '%';
                }
                if(common.roth){
                    if (Section.getData("rothslidercheckbox"))
                    {
                        rothContributions = contributionsData.C_RothContributionPct + '%';
                    }
                }
            }else if(contributionsData.contribution_mode == 'DEFERDLR'){
                if (Section.getData("pretaxslidercheckbox"))
                {
                    preContributions = '$' + contributionsData.C_PreTaxContributionValue;
                }
                if(common.post){
                    postContributions = '$' + contributionsData.C_PostTaxContributionValue;
                }
                if(common.roth){
                    if (Section.getData("rothslidercheckbox"))
                    {
                        rothContributions = '$' + contributionsData.C_RothContributionValue;
                    }
                }
            }

            if(contributionsData.updateCatchUp){
                if(contributionsData.catchup_mode == 'DEFERPCT'){
                    catchupContribution = contributionsData.C_CatchUpContributionValue + '%';
                }else if(contributionsData.catchup_mode == 'DEFERDLR'){
                    catchupContribution = '$' + contributionsData.C_CatchUpContributionValue;
                }
            }

            if(riskProfileData.RP_InvestorType == -1){
                riskIndex = null;
                if (!Section.getData("module").investmentsGraph)
                {
                    riskProfile = translation.risk_profile_incomplete;
                    riskProfileDes = translation.please_complete_risk_profile;
                }
                else
                {
                    riskProfile = translation.personal_investor_profile_incomplete;
                    riskProfileDes = translation.please_complete_personal_investor_profile;              
                }
            }

            return {
                'generatedDate' : moment().format('dddd, MMMM D, YYYY H:m:s A'),
                'common' : common,
                'retirementNeedsData' : retirementNeedsData,
                'investmentsData' : investmentsData,
                'investmentProfile' : investmentProfile,
                'contributionsData' : contributionsData,
                'riskProfileData' : riskProfileData,
                'preContributions' : preContributions,
                'rothContributions' : rothContributions,
                'postContributions' : postContributions,
                'catchupContribution' : catchupContribution,
                'riskIndex' : riskIndex,
                'riskProfile' : riskProfile,
                'riskProfileDes' : riskProfileDes,
                'beneficiary' : beneficiary,
                'partBeneficiaries' : partBeneficiaries,
                'retirementNeedQA' : retirementNeedQA,
                'riskProfileQA' : riskProfileQA,
                'messaging' : Section.getData('messaging'),
                'data': data
            };
        },

        transaction : {

            getUpdateSection : function(){
                var common = Section.getData('common'),
                    translation = Section.getData('translation'),
                    flexPlan = parseInt(Section.getData('common').flexPlan),
                    updateCombined = 0,
                    updatePersonal = 1,
                    updateElections = common['allowElectionChange'],
                    updateRealignment = 1,
                    updateContributions = common['allowContributionChange'],
                    updateCatchUp = 1,
                    updateBeneficiary = 1,
                    updateATTransact = Section.getData('module').ATBlueprint,
                    contributions = Section.getData('contributions'),
                    updateSection = [];

                if(common.data_source != 'educate'){

                    if(Section.getData('module').ATBlueprint) {
                        $.ajax({
                            type: "POST",
                            url: "/commonData",
                            data: {'attr': 'common'},
                            success: function(data) {
                                common = data;
                            },
                            async: false
                        });
                        $.ajax({
                            type: "POST",
                            url: "/commonData",
                            data: {'attr': 'contributions'},
                            success: function(data) {
                                contributions = data;
                            },
                            async: false
                        });
                    }

                    if(common.data_source == 'envisage'){
                        updateElections = (Section.getData('investments').I_SelectedInvestmentOption == 'RISK' && common.riskType != 'RBF') ? 0 : 1;
                        if(common.ACAon && !Section.getData('contributions').C_ACAoptOut){
                            updateContributions = 0;
                        }
                    }

                    if(_.contains(common.post_calls, 'postCombined')){
                        updateCombined = 1;
                        updateElections = 0;
                        updateRealignment = 0;
                        updateCatchUp = 0;
                        updateBeneficiary = 0;
                        updateContributions = 0;
                    }

                    if(updatePersonal && common.updatePersonal && common.data_source == 'expertplan' && _.contains(common.post_calls, 'postPersonal')){
                        updateSection.push({'id' : 0, 'section' : translation.personal});
                    }

                    if(flexPlan == 1){
                        if(updateContributions && Section.getData('contributions').updateContributions && common.planAllowsDeferrals && _.contains(common.post_calls, 'postContributions')){
                            updateSection.push({'id' : 3, 'section' : translation.contributions});
                        }
                    }
                    var electionsPassed = updateElections && common.updateElections && common.planAllowsElections && _.contains(common.post_calls, 'postElections');
                    if(electionsPassed){
                        updateSection.push({'id' : 1, 'section' : translation.elections});
                    }

                    if(updateRealignment && common.updateRealignment && common.planAllowsRealignments && _.contains(common.post_calls, 'postRealignment')){
                        updateSection.push({'id' : 2, 'section' : translation.realignment});
                    }

                    if(flexPlan != 1){
                        if(updateContributions && contributions.updateContributions && common.planAllowsDeferrals && _.contains(common.post_calls, 'postContributions')){
                            updateSection.push({'id' : 3, 'section' : translation.contributions});
                        }
                    }

                    if(updateCatchUp && contributions.updateCatchUp && common.planAllowsCatchup && _.contains(common.post_calls, 'postCatchUp')){
                        updateSection.push({'id' : 4, 'section' : translation.catch_up});
                    }

                    if(updateBeneficiary && common.updateBeneficiary && _.contains(common.post_calls, 'postBeneficiary')){
                        if(Section.getData('partBeneficiaries') || Section.getData('partBeneficiariesDeleted') || (_.contains(['envisage', 'expertplan','srt2'], common.data_source) && common.beneficiaries)){
                            updateSection.push({'id' : 5, 'section' : translation.beneficiaries});
                        }
                    }

                    if((common.data_source == 'relius'  && $.parseJSON(common.enrollment) && common.newVersion) || (common.data_source == 'srt2' && $.parseJSON(common.enrollment) ) ) {
                        updateSection.push({'id' : 6, 'section' : translation.enrollment});
                    }

                    if(updateCombined){
                        updateSection.push({'id' : 7, 'section' : translation.selections});
                    }
                    
                    if(updateATTransact) {
                        updateSection.push({'id' : 8, 'section' : translation.at_futurebuilder});
                    }
                    if(electionsPassed && Section.getData('common').adviceStatus && Section.getData('investments').I_SelectedInvestmentOption == "ADVICE") {
                        updateSection.push({'id' : 9, 'section' : translation.advice_fund});
                    }
                }
                updateSection.push({'id' : 10, 'section' : translation.profile});
                return {
                    'common' : common,
                    'profile_transaction' : Section.getData('profile_transaction'),
                    'updateSection' : updateSection
                };
            },

            init : function(model){
                Section.setData('profile_transaction', {
                    'updateSection' : model.updateSection,
                    'enrollmentInput' : model.common.enrollment,
                    'enrollment' : 0,
                    'version' : model.common.newVerson,
                    'enrollmentPath' : model.common.enrollmentPath,
                    'realignment' : model.common.updateRealignment,
                    'initialEnrollment' : model.common.initialEnrollment,
                    'failed' : 0,
                    'complete' : 0,
                    'ep_modal' : model.profile_transaction.ep_modal
                });
            },

            start : function(){
                var profile = Section.getData('profile_transaction'),
                    self = this;

                if(profile.failed){
                    Section.setData('profile_transaction', _.without(profile.updateSection, _.findWhere(profile.updateSection, {id: 6})));
                    profile = Section.getData('profile_transaction');
                    $('#update6').parent().parent().remove();
                }

                if(this.isValidTransaction(profile)){
                    self.update(profile.updateSection[profile.complete]);
                }else{
                    this.complete();
                }
            },

            postACAOptOut : function(){
                var Deferred = $.Deferred(),
                    self = this;
                if(Section.getData('common').data_source == 'envisage' && Section.getData('common').ACAon){
                    data.postACAOptOut().then(function(result){
                        self.attachDebugLog(JSON.parse(JSON.parse(result).response));
                        Deferred.resolve();
                    });
                }else{
                    Deferred.resolve();
                }
                return Deferred.promise();
            },

            update : function(updateSection){
                var self = this;
                if(Section.getData('common').data_source == 'expertplan' && Section.getData('profile_transaction').ep_modal){
                    $('#updateTransactionModal').modal('show');
                }

                this.updateTransactionWidget(_.extend(updateSection, {'type' : 'loading'}));
                data.updateTransaction(updateSection).then(function(result){
                    var multi = "confirmation" in result ? 0:1;
                    if(multi){
                        var labels = {'elections' : 'Investments', 'asset_allocation' : 'Asset Allocation Model', 'realignment' : Section.getData('translation').realignment , 'contributions' : Section.getData('translation').contributions };
                        var combine = {success : 0, message : '', confirmation : ''};
                        $.each( result, function( name, res ) {
                            var label = labels[name] ? labels[name] : s.capitalize(name);
                            var hasMessage = $.trim(res.message) != "";
                            combine.success += res.success ? 1 : (hasMessage ? 0 : 1);
                            if(hasMessage){
                                combine.message += '<li>' + label + ' Fail Message: '  + res.message + '</li>';
                            }else if(res.confirmation){
                                combine.message += '<li>' +  label + ' ' + Section.getData('translation').confirmation_id +': ' + res.confirmation + '</li>';
                            }
                            self.attachDebugLog(res);
                        });
                        combine.message = '<ol>' + combine.message + '</ol>';
                        combine.success = Object.keys(result).length == combine.success ? 1 : (combine.success == 0 ? 0 : 3);
                        result = combine;
                    }
                    if(result.success == 2){
                        Section.setData('common',{'profileId' : result.confirmation});
                    }
                    var type = self.getTypeFromResult(result);
                    self.updateTransactionWidget(_.extend(updateSection, {
                        'type' : type,
                        'mode' : multi ? 'multi' : 'single',
                        'message' : result.message,
                        'confirmation' : result.confirmation
                    }));

                    if(Section.getData('common').data_source == 'relius' && !Section.getData('profile_transaction').enrollmentInput){
                        if(result.message.search('enrollment') > 0){
                            Section.setData('profile_transaction', { 'enrollment' : 1});
                        }
                    }
                    Section.setData('profile_transaction', { 'complete' : parseInt(Section.getData('profile_transaction').complete) + 1});
                    if(!multi){
                        if(result.forwardUrl && result.forwardUrl != '') {
                            Section.setData('common', {'forwardUrl': result.forwardUrl});
                        }
                        self.attachDebugLog(result);
                    }
                    self.start();
                });
            },

            complete : function(){
                var profile = Section.getData('profile_transaction');
                Section.setData('my_profiles', {'updateProfile' : 0});

                if(profile.enrollment || profile.enrollmentInput && Section.getData('common').newVersion == 0){
                    $('#confirmSubmitModal').modal('show');
                }else if(profile.enrollmentInput && profile.failed){
                    $('#errorSubmitModal').modal('show');
                }else if(Section.getData('common').data_source.match('omni') && profile.failed){
                    $('#omniTransactionFailedModal').modal('show');
                }else if(Section.getData('common').data_source.match('guardian') && profile.failed){
					// Do not show proceed button
                }else{
                    $('.processUpdates').fadeIn();
                    if (Section.getData('module').ATBlueprint) {
                        $("#mainNav").find('.highlight').not('#library').addClass('full-lock');
                    }
                    data.sendProfile().then(function(response){ });
                }
            },

            updateTransactionWidget : function(config){
                var widget = $('#transactionWidget'+config.id),
                    setting = {},
                    translation = Section.getData('translation'),
                    labels = [
                        translation.personal,
                        translation.elections,
                        translation.realignment,
                        translation.contributions,
                        translation.catch_up,
                        translation.beneficiaries,
                        translation.enrollment,
                        translation.selections,
                        translation.at_futurebuilder,
                        translation.advice_fund,
                        translation.profile
                    ];

                if(config.type == 'loading'){
                    setting = {
                        'add' : 'update',
                        'loader' : true,
                        'title' : labels[config.id] + '...',
                        'message' : ''
                    };
                }else if(config.type == '_confirm'){
                    setting = {
                        'add' : 'confirm',
                        'loader' : false,
                        'title' : labels[config.id] + ' ' + translation.updated + '.',
                        'message' : config.message + config.confirmation
                    };
                }else if(config.type == 'confirm'){
                    if(Section.getData('profile_transaction').initialEnrollment && Section.getData('profile_transaction').initialEnrollment.length > 0){
                        setting = {
                            'add' : 'confirm',
                            'loader' : false,
                            'title' : labels[config.id],
                            'message' : labels[config.id].replace(translation.personal, translation.personal_info) + translation.received
                        };
                    }else{
                        setting = {
                            'add' : 'confirm',
                            'loader' : false,
                            'title' : labels[config.id] + ' ' + translation.updated +'.',
                            'message' : (config.message ? config.message + '<br>' : '') + (config.confirmation ? translation.confirmation_id +': ' + config.confirmation : '')
                        };
                    }
                }else if(config.type == 'error'){
                    setting = {
                        'add' : 'error',
                        'loader' : false,
                        'title' : labels[config.id] + ' ' + translation.update_failed +'.',
                        'message' : (config.mode == 'single' ? translation.error + ': ' :'') + config.message
                    };
                    Section.setData('profile_transaction', { 'failed' : parseInt(Section.getData('profile_transaction').failed) + 1});
                }else if(config.type == 'mix'){
                    setting = {
                        'add' : 'success-error',
                        'loader' : false,
                        'title' : labels[config.id] + ' ' + translation.update_failed +'.',
                        'message' : (config.message ? config.message : '')
                    };
                }

                widget.find('.loadIcon').removeClass('wait confirm error').addClass(setting.add);
                setting.loader ? widget.find('.spin').show() : widget.find('.spin').hide();
                widget.find('.updateTitle').html(setting.title);
                widget.find('.message').html(setting.message);
            },

            isValidTransaction : function(profile){
                if(profile.complete < profile.updateSection.length){
                    return true;
                }
                return false;
            },

            attachDebugLog : function(result){
                var sfToolbar = $('.sf-toolbarreset .sf-toolbar-block.rkp .sf-toolbar-info');
                if (sfToolbar.length) {
                    if (result.debugResponse)
                        sfToolbar.prepend('<fieldset><legend>RESPONSE</legend><textarea>' + result.debugResponse + '</textarea></fieldset>');
                    if (result.debugRequest)
                        sfToolbar.prepend('<fieldset><legend>REQUEST</legend><textarea>' + result.debugRequest + '</textarea></fieldset>');
                }
            },
            
            getTypeFromResult : function(result) {
                if ((result.confirmation !== null && result.confirmation.toString().indexOf('ERROR:') === 0) || !result.success) {
                    return 'error';
                }
                switch (result.success) {
                    case 2:
                        return '_confirm';
                    case 3:
                        return 'mix';
                    default:
                        return 'confirm';
                }           
            }

        }

    };

    return {
        initData : MyProfileService.initData,
        transaction :  MyProfileService.transaction
    }

});
