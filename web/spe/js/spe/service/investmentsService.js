define(['jquery','jquery.ui','spe/service/common','spe/service/data','spe/model/Section','spe/view/PlayerView'], function($, ui, common, service, Section,playerView){

    var investmentsService = {

        init : function(){
            this.setTargetDateFund('null');
            if(parseInt($('#investTotal').val()) === 100){
                $('.actionBtn.iCalculate').click();
            }

            if($('#investment4').length){
                $('#investment4').click()
            }
            else{
                $('.content-area .investments input[name=investment]:checked').click();
            }

            if($('#investmentsWarning').length){
                var bb = bootbox.dialog({
                    closeButton: true,
                    title: $('#investmentsWarning .title').text(),
                    message: $('#investmentsWarning .message').text(),
                    buttons: {
                        ok: {
                            label: $('#investmentsWarning .btn-accept').text(),
                            className: "btn-success",
                            callback: function() {
                                bb.modal('hide');
                                _gaq.push(['_trackEvent', 'Investments', 'Popup - Plan does not allow investment allocation change']);
                            }
                        }
                    }
                });
            }
        },

        //Play Audio
        playAudio : function() {
            var audios = $.trim($('.audio-area').html());
            if (audios) {
                playerView.render('.audio-area', _.extend(JSON.parse(localStorage.getItem('mediaPath')), {'playlist': audios.split('|')}), 'audio');
            }
        },

        // select investment type
        setInvestmentType : function(e){
            var val = $(e.target).val();
            $('#investSelectType').val(val);
            // swap buttons
            if(val == "DEFAULT"){
                $('.iAction').hide();
                $('.iDefault').show();
            }
            else if(val == "TARGET"){
                $('.iAction').hide();
                $('.iProceed').show();
            }
            else if(val == "RISK" ){
                $('.iAction').hide();
                $('.iRisk').show();
            }
            else if(val == "MODEL" ){
                $('.iAction').hide();
                $('.iModel').show();
            }
            else if(val == "CUSTOM"){
                $('.iAction').hide();
                $('.iSelect').show();
            }
            else if(val == "ATARCHITECT"){
                $('.iAction').hide();
                $('.iATArchitect').show();
            }
        },

        // investments navigation
        investments : function(e){
            var target = $(e.currentTarget),
                type = target.data('type'),
                page = target.data('page'),
                translation = Section.getData('translation'),
                disclosure = '';

            multiTarget = 'null';
            warning = 0;
            switch(type){
                case 1:
                    type = 'RISK';
                    disclosure = 'riskBasedReview';
                    break;
                case 2:
                    type = 'TARGET';
                    disclosure = 'targetDateReview';
                    break;
                case 3:
                    type = 'CUSTOM';
                    disclosure = 'customReview';
                    break;
                case 4:
                    type = 'DEFAULT';
                    break;
                case 5:
                    type = 'MODEL';
                    break;
                default:
                    warning = 1;
                    break;
            }
            if(warning) {
                common.showErrMsg(translation.please_select_investment_type);
            }
            else {
                $('body').trigger('spe:disclosure:hide');
                var a = $('#investSelection').val();
                var self = this;
                service.ajax({ url: '/investments', data: {page: page, type: type, a: a} }).then(function(view){
                    $(".content-area").html(view);
                    if(disclosure && page == 3) {
                        $('body').trigger('spe:disclosure:show', {title: disclosure});
                    }
                    if(page == 1){
                        $('.content-area .investments input[name=investment]:checked').click();
                    }
                    self.playAudio();
                });
            }
        },

        //risk based action
        saveRisk : function(e){
            var target = $(e.currentTarget);
            var val = target.data('val');
            var self = this,
                translation = Section.getData('translation');
            if(val){
                $('body').trigger('spe:disclosure:hide');
                service.ajax({ url: '/investments', data: {page: 2, type: 'RISK', a: val} }).then(function(view){
                    $(".content-area").html(view);
                    self.playAudio();
                    if (!$("#investmentsGraphContainer").length)
                    {
                        $('body').trigger('spe:disclosure:show', { title: 'portfolioSelector'});
                    }
                });
                _gaq.push(['_trackEvent', 'Investments', 'Proceed', 'Risk Based Portfolio']);
            }
            else{
                if(parseInt(Section.getData('common').flexPlan)) {
                    common.showErrMsg(translation.please_complete_retirement_needs_and_contributions_to_select_risk_based_portfolio_flex);
                }
                else if(Section.getData('common').data_source.indexOf('retrev') !== -1) {
                    common.showErrMsg(translation.please_complete_risk_profile_to_select_investment_model);
                }
                else{
                    if ($("#investmentsGraphExists").length)
                    {
                        common.showErrMsg(translation.please_complete_personal_investor_profile);
                    }
                    else
                    {
                        if (val === 0) {
                            common.showErrMsg(translation.please_complete_risk_profile_to_select_risk_based_portfolio);
                        } else {
                            $('#riskProfileClick').trigger('click');
                        }
                    }
                }
            }
        },

        //Model action
        saveModel : function(){
            var self = this;
            $('body').trigger('spe:disclosure:hide');
            service.ajax({ url: '/investments', data: {page: 2, type: 'MODEL'} }).then(function(view){
                $(".content-area").html(view);
                self.playAudio();
                _gaq.push(['_trackEvent', 'Investments', ' Proceed ', 'Investment Model']);
            });
        },

        setTargetDateFund : function(val){
            multiTarget = val;
        },

        //target date action
        saveTarget : function(e){
            var target = $(e.currentTarget);
            var val = target.data('val');
            var self = this,
                translation = Section.getData('translation');
            if (multiTarget !== 'null') {
                $('body').trigger('spe:disclosure:hide');
                service.ajax({ url: '/investments', data: {page: 3, type: 'TARGET', a: multiTarget} }).then(function(view){
                    $(".content-area").html(view);
                    self.playAudio();
                });
                _gaq.push(['_trackEvent', 'Investments', 'Proceed', 'Target Date Maturity Fund']);
            } else if(val == 'Array') {
                common.showErrMsg(translation.please_choose_target_date_maturity_fund);
            } else if(val){
                $('body').trigger('spe:disclosure:hide');
                service.ajax({ url: '/investments', data: {page: 3, type: 'TARGET', a: val} }).then(function(view){
                    $(".content-area").html(view);
                    $('body').trigger('spe:disclosure:show', {title: 'targetDateReview'});
                    self.playAudio();
                });
                _gaq.push(['_trackEvent', 'Investments', 'Proceed', 'Target Date Investment']);
            } else { // this doesn't seem to be firing when val = ''?
                common.showErrMsg(translation.please_complete_retirement_needs_to_select_target_date_investment);
            }
        },

        //multiple TDMF action
        chooseTarget : function(){
            var self = this;
            $('body').trigger('spe:disclosure:hide');
            service.ajax({ url: '/investments', data: {page: 2, type: 'TARGET'} }).then(function(view){
                $(".content-area").html(view);
                self.playAudio();
            });
        },

        //custom action
        saveCustom : function(){
            var self = this;
            $('body').trigger('spe:disclosure:hide');
            service.ajax({ url: '/investments', data: {page: 2, type: 'CUSTOM'} }).then(function(view){
                $(".content-area").html(view);
                self.playAudio();
                $('body').trigger('spe:disclosure:show', { title: 'customChoice'});
                _gaq.push(['_trackEvent', 'Investments', ' Proceed ', 'Custom Choice']);
            });
        },

        //ATArchitect action
        ATArchitect : function(){
            $.post( "/investments/atarchitect/1/enable", {  }).done(function( html )
            {
                $('body').trigger('spe:router:redirect','investments');
            });
        },

        //ACA Default action
        saveDefault : function(e){
            var target = $(e.currentTarget);
            var val = target.data('val');
            var self = this,
                translation = Section.getData('translation');
            if(val){
                $('body').trigger('spe:disclosure:hide');
                service.ajax({ url: '/investments', data: {page: 3, type: 'DEFAULT', a: val} }).then(function(view){
                    $(".content-area").html(view);
                    self.playAudio();
                    _gaq.push(['_trackEvent', 'Investments', 'Proceed', 'Default Investment']);
                });
            } else {
                common.showErrMsg(translation.selection_not_allowed);
            }
        },

        // select custom choice investment
        calcCustom : function(e){
            var target = $(e.currentTarget);
            var num = target.data('count');
            var total = 0.00;
            var custom = '',
                translation = Section.getData('translation');
			var html = '<table style="width:100%"><tr><th style="width:150px;">' + translation.redemption_fee_message_name + '</th><th>' + translation.redemption_fee_message_rule + '</th></tr>';
			var redemptionFee = false;

            for(i=0; i<num; i++){
                var chkValue = parseFloat($('#fundPct'+i).val());
                var id = $('#fundId'+i).val();
                var ticker = $('#fundTicker'+i).val();
                if(chkValue){
					if ($('#fundRedemption' + i).val()) {
						redemptionFee = true;
						html += '<tr style="border-top:1px solid #CCC;"><td>' + $('#fundName' + i).val() + '</td><td>' + $('#fundRedemption' + i).val() + '</td></tr>';
					}

                    custom += id+"^"+chkValue+"^"+ticker+"|";
                    total += chkValue;
                }
            }

			html += '</html>';

            //remove the last pipe
            var custom = custom.slice(0, -1);
            $('#investTotal').val(parseFloat(total));

            if(parseFloat(total) == parseFloat(100)){
				if (redemptionFee) {
					var bb = bootbox.dialog({
						closeButton: false,
						title: translation.redemption_fee_calculate_title,
						message: html,
						buttons: {
							ok: {
								label: 'ok',
								className: "btn-success",
								callback: function() {
									bb.modal('hide');
								}
							}
						}
					});
				}

                // save it
                $('#investSelection').val(custom);
                $('.actions .hide').removeClass('hide').addClass('show');
            }
            else{
                $('.actions .show').removeClass('show').addClass('hide');
                common.showErrMsg(translation.please_make_sure_selection_up_to_100);
            }
        },

        // reset custom choice
        calcReset : function(e){
            var target = $(e.currentTarget);
            var num = target.data('count');
            var total = 0;
            for(i=0; i<num; i++){
                var dval = $('#fundPct'+i).attr('dval');
                total += parseFloat(dval)
                $('#fundPct'+i).val(dval);
            }
            $('#editRecommended').val(0);
            service.updateSession({'editRecommended': 0});
            $('#investTotal').val(total);
            $('#investSelection').val('');
        },

        // Select Collection Portfolio/Model
        pickInvestment : function(e){
            var target = $(e.currentTarget);
            var id = target.data('id');
            $('#investSelection').val(id);
            $('#investTotal').val('100');
            $('.actions .hide').removeClass('hide').addClass('show');
        },

        // open / close portfolio to show funds
        togglePortFunds : function(e){
            var target = $(e.currentTarget);
            var id = target.data('id');
            $('#portFund'+id).toggle();
        },

        // Save Investments
        saveInvestments : function(){
            var translation = Section.getData('translation');
            if($(".acknowledged[type='checkbox']").length && $(".acknowledged[type='checkbox']").length != $(".acknowledged[type='checkbox']:checked").length){
                common.showErrMsg(translation.acknowledge_alert);
            }
            else if($('#realign1')[0] && $('#realign2')[0] && !$('#realign1').is(':checked') && !$('#realign2').is(':checked')) {
                common.showErrMsg(translation.please_select_realignment_option);
            }
            else{
                if($('#realign2').is(':checked')){
                    var realignment = 1;
                    var rval = $('#realign2').val();
                }
                else{
                    var realignment = 0;
                    var rval = $('#realign1').val();
                }
                $('#realignment').val(rval);
                Section.setData('common', {'updateElections' : 1, 'updateRealignment' : parseInt(realignment)});
                var data = {
                    'investments' : {'I_Realign' : rval},
                    'common' : Section.getData('common')
                };
                service.saveInvestments(data).then(function(data){
                    if(!_.isEmpty(data)){
                        var parseData = JSON.parse(data);
                        Section.setData('investments', parseData);
                        Section.setData('common', {'defaultInvestmentFund' : parseData.defaultInvestmentFund});
                    }
                    _gaq.push(['_trackEvent', 'Investments', 'Save Investments']);
                    if(parseInt(Section.getData('common').flexPlan) || parseInt(Section.getData('module').ATBlueprint)) {
                        $('body').trigger('spe:router:redirect', { to: 'myProfile', complete : 'investments' });
                    } else {
                        $('body').trigger('spe:router:redirect', { to: 'contributions', complete : 'investments' });
                    }
                });
            }
        },

        // Smart401 Allocation Modal popup
        smart401kNote : function(e){
            var target = $(e.currentTarget);
            if($('.smart401k_allocation_note').length > 0 && $("#editRecommended").val() == 0){
                if(target.val() != target.attr('dval')){
                    var bb = bootbox.dialog({
                        closeButton: false,
                        title: $('#smart401kAllocationNote .title').text(),
                        message: $('#smart401kAllocationNote .message').text(),
                        buttons: {
                            cancel: {
                                label: $('#smart401kAllocationNote .btn-cancel').text(),
                                className: "btn-default",
                                callback: function() {
                                    bb.modal('hide');
                                    _gaq.push(['_trackEvent', 'Investments', 'Popup', 'Change Recommended  - Cancel']);
                                    $('body').trigger('spe:router:redirect','investments');
                                }
                            },
                            ok: {
                                label: $('#smart401kAllocationNote .btn-ok').text(),
                                className: "btn-success",
                                callback: function() {
                                    bb.modal('hide');
                                    $("#editRecommended").val(1);
                                    service.updateSession({'editRecommended': 1});
                                    _gaq.push(['_trackEvent', 'Investments', 'Popup', 'Change Recommended  - OK']);
                                }
                            }
                        }
                    });
                }
            }
        },

		redemptionFee : function(e) {
            var target = $(e.currentTarget);
			var current = $(target).parent().find('.redemption-modal');
			var bb = bootbox.dialog({
				closeButton: false,
				title: $(current).find('.title').text(),
				message: $(current).find('.message').text(),
				buttons: {
					ok: {
						label: $(current).find('.btn-ok').text(),
						className: "btn-success",
						callback: function() {
							bb.modal('hide');
						}
					}
				}
			});
		},

        //Default Investment Model Popup
        playVideo : function(e){
            var target = $(e.currentTarget);
            var video = target.data('video');
            var text = target.data('text');
            var link = target.data('link');
            if(video){
                bootbox.dialog({
                    closeButton: true,
                    message: '<div id="popup-video"></div><div align="center"><a href="'+ link +'" target="_blank" style="color:'+ $('.content-area .details').css('color') +'">' + text + '</a></div>'
                }).on("shown.bs.modal", function() {
                    playerView.render('#popup-video', _.extend(JSON.parse(localStorage.getItem('mediaPath')), {'popup': true, 'playlist' : [video]}));
                    _gaq.push(['_trackEvent', 'Investments', 'Default Investment - Video Popup - ' + video]);
                });
            }
            return false;
        },

        getInvestmentProfile : function(){
            var common = Section.getData('common'),
                investmentsData = Section.getData('investments'),
                investmentLabels = null,
                investmentsProfile = [],
                invest = {},
                self = this,
                translation = Section.getData('translation');
                translations = Section.getData('translations');
                customChoice = false;
            if (Section.getData('investmentProfile'))
            {
                return Section.getData('investmentProfile');
            }
            invest.details = '';

            if(common.updateElections){
                investmentLabels = {'DEFAULT' : translation.default_investment, 'RISK' : translation.risk_based_portfolio, 'MODEL' : translation.investment_models, 'CUSTOM' : translation.custom_choice, 'TARGET' : translation.target_date_fund, 'ADVICE' : translation.advice_fund};
                if(common.client_calling == 'Envoy') {
                    investmentLabels.RISK = translation.account_managed_for_you;
                    investmentLabels.CUSTOM = translation.you_choose_funds_self_directed;
                }
                else if(common.client_calling == 'AXA') {
                    investmentLabels.RISK = translation.risk_based_asset_allocation_fund;
                    investmentLabels.TARGET = translation.target_date_portfolio;
                }
                else if(common.client_calling == 'LNCLN') {
                    investmentLabels.RISK = translation.combination_target_date_risk_portfolios;
                }
                else if(common.riskType == 'RBF') {
                    investmentLabels.RISK = translation.risk_based_funds;
                }

                invest.lName = investmentLabels[investmentsData.I_SelectedInvestmentOption];
                invest.rName = '';

                if(investmentsData.I_SelectedInvestmentOption == 'RISK'){
                    invest.rName = investmentsData.investmentDetails[0]['pname'];
                    _.each(investmentsData.investmentDetails, function(data, key){
                        invest.details += "<div class='fundRow'><div class='pct'>"+ data['percent'] +"%</div><div class='name'>"+ data['fname'] +"</div></div><div class='clear'></div>";
                    });
                    investmentsProfile.push(invest);
                }else if(investmentsData.I_SelectedInvestmentOption == 'MODEL'){
                    _.each(investmentsData.investmentDetails, function(data, key){
                        invest.details += "<div class='fundRow'><div class='name'>"+ data['fname'] +"</div></div><div class='clear'></div>";
                    });
                    investmentsProfile.push(invest);
                }else if(investmentsData.I_SelectedInvestmentOption == 'TARGET'){
                    _.each(investmentsData.investmentDetails, function(data, key){
                        invest.details += "<div class='fundRow'><div class='pct'>"+ data['percent'] +"%</div><div class='name'>"+ data['fname'] +"</div></div><div class='clear'></div>";
                    });
                    investmentsProfile.push(invest);
                }else if(investmentsData.I_SelectedInvestmentDetails){
                    if(common.data_source.match('omni')) {
                        var isFundLink = parseInt(common.isFundLink);
                        invest.prospectus = 0;
                        invest.fundFact = isFundLink;
                    }
                    _.each(investmentsData.investmentDetails, function(data, key){
                        
                        if(common.data_source.match('omni')) {
                            invest.prospectus += data['prospectusLink'] ? 1 : 0;
                            invest.details += "<div class='fundRow row'>" +
                                "<div class='col-xs-1'>" + data['percent'] + "%</div>" +
                                "<div class='name col-xs-6'>" + data['fname'] + "</div>" +
                                "<div class='name col-xs-1 text-center'>" + (data['fundFactLink'] ? "<span class='popupLink' data-link='" + data['fundFactLink'] +"'><i class='fa fa-file-text-o'></i></span>" : "") + "</div>" +
                                "<div class='name col-xs-1 text-center'>" + (data['prospectusLink'] ? "<span class='popupLink' data-link='" + data['prospectusLink'] +"'><i class='fa fa-file-text-o'></i></span>" : "") + "</div>" +
                                "</div><div class='clear'></div>";
                        }else{
                            customChoice = true;
                            if (invest.details == "")
                            {

                                invest.details +=
                                        "<table class ='table'><thead><tr><th>" + translations.investments.fund_name + "</th><th>" + translations.global.percent + "</th>";
                                if (Section.getData('additionalFundHeader') != "")
                                {
                                    invest.details += "<th>" + Section.getData('additionalFundHeader') + "</th>";
                                }
                                invest.details += '</tr></thead><tbody>';
                            }
                            invest.details += "<tr>";
                            invest.details += "<td>" + data['fname'] + "</td>";
                            if (data['rkp'][12] != null)
                            {
                                invest.details +=  "<td>" + data['percent'] + "</td>" + "<td>" + data['rkp'][12][1] + "</td>";
                            }
                            else
                            {
                                invest.details +=  "<td colspan='2'>" + data['percent'] + "</td>";
                            }
                            invest.details += "</tr>";
                        }

                    });
                    if (customChoice)
                    {
                        invest.details += "</body></table>";
                    }
                    investmentsProfile.push(invest);
                }
            }
            return investmentsProfile;
        },

        getMatchId : function(needle, items){
            var id = false;
            _.each(items, function(item, key){
                if(_.contains(item, needle) || item == needle || _.contains(item, parseInt(needle))){
                    id = key;
                    return id;
                }
            });
            return id;
        }

    };

    return {
        init : investmentsService.init.bind(investmentsService),
        playAudio : investmentsService.playAudio,
        setInvestmentType : investmentsService.setInvestmentType,
        investments : investmentsService.investments.bind(investmentsService),
        saveRisk : investmentsService.saveRisk.bind(investmentsService),
        saveModel : investmentsService.saveModel.bind(investmentsService),
        saveTarget : investmentsService.saveTarget.bind(investmentsService),
        chooseTarget : investmentsService.chooseTarget.bind(investmentsService),
        saveCustom : investmentsService.saveCustom.bind(investmentsService),
        ATArchitect : investmentsService.ATArchitect.bind(investmentsService),
        saveDefault : investmentsService.saveDefault.bind(investmentsService),
        calcCustom : investmentsService.calcCustom,
        calcReset : investmentsService.calcReset,
        pickInvestment : investmentsService.pickInvestment,
        togglePortFunds : investmentsService.togglePortFunds,
        saveInvestments : investmentsService.saveInvestments.bind(investmentsService),
        smart401kNote : investmentsService.smart401kNote,
        redemptionFee : investmentsService.redemptionFee,
        playVideo : investmentsService.playVideo,
        getInvestmentProfile : investmentsService.getInvestmentProfile.bind(investmentsService)
    }
});
