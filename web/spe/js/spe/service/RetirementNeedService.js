define(['jquery','underscore' ,'jquery.ui','spe/service/common','spe/model/Section','spe/view/PlayerView'], function($, underscore, ui, common, Section, playerView){
    "use strict";

    var RetirementNeedService = {

        initializeIncomePlanSlider : function(){
            var data = Section.getData('retirement_needs');
            this.calculateResult(data.RN_IncomePct + 40)
            var self = this,
                IPSlider = $("#incomePlanSlider").slider({
                    value: parseInt(data.RN_IncomePct),
                    min: 0,
                    max: 100,
                    step: 1,
                    slide: function(event, ui) {
                        $('#pctValue').val(parseInt(ui.value));
                        $('#pct').html(ui.value + "%");
                        self.calculateResult(parseInt(ui.value) + 40);
                    },
                    stop:function(event, ui){
                        _gaq.push(['_trackEvent', 'Retirement Needs', 'Value Changed', 'Percentage of income from Plan' , IPSlider.slider("option", "value")]);
                    }
                });

            $('#pctValue').val($("#incomePlanSlider").slider( "value" ));
            $('#pct').html($("#incomePlanSlider").slider( "value" ) + "%");
        },

        slide : function(e){
            var pctValue = $('#pctValue').val();
            if($(e.target).hasClass('up')){
                pctValue++;
            }else{
                pctValue--;
            }
            $("#incomePlanSlider").slider({value: pctValue});
            this.calculateResult(parseInt(pctValue) + 40);
            _gaq.push(['_trackEvent', 'Retirement Needs', 'Value Changed', 'Percentage of income from Plan' , pctValue ]);
        },

        calculateResult : function(incomePct){
            var pct = Math.round(incomePct) - 40,
                month = $('#monthlyContributionDisplay'),
                savings = $('#estimatedSavingsDisplay'),
                value_factors = (Section.getData('common').retirementNeedsVersion == 'by age') ?
                    [1.06, 2.18, 3.37, 4.46, 5.98, 7.39, 8.9, 10.49, 12.18, 13.97, 15.87, 17.88, 20.02, 22.28, 24.67, 27.21, 29.91, 32.76, 35.79, 38.99, 42.39, 46, 49.82, 53.86, 58.16, 62.71, 67.53, 72.64, 78.06, 83.8, 89.89, 96.34, 13.18, 110.43, 118.12, 126.27, 134.9, 144.06, 153.76, 164.05, 174.95, 186.51, 198.76, 221.74, 225.51, 240.1, 255.56, 271.96, 289.34, 307.76] :
                    [5.98,13.97,24.67,38.99,58.16,83.8,118.12,164.05],
                payout_factors = (Section.getData('common').retirementNeedsVersion == 'by age') ?
                    [1.00, 1.94, 2.83, 3.67, 4.47, 5.21, 5.92, 6.58, 7.21, 7.80, 8.36, 8.89, 9.38, 9.85, 10.29, 10.71, 11.11, 11.48, 11.83, 12.16, 12.47, 12.76, 13.04, 13.30, 13.55, 13.78, 14.00, 14.21, 14.41, 14.59, 14.76, 14.93, 15.08, 15.23, 15.37, 15.50, 15.62, 15.74, 15.85, 15.95, 16.05, 16.22, 16.31, 16.38, 16.46, 16.52, 16.59, 16.65, 16.71] :
                    [4.47,7.80,10.29,12.16,13.55,14.59,15.37,15.95],
                yearsInRetire = $('#yearsInRetire').val() ? parseFloat($('#yearsInRetire').val()) : 0,
                yearsBeforeRetire = $('#yearsBeforeRetire').val() ? parseFloat($('#yearsBeforeRetire').val()) : 0, // == C7
                anualIncome = $('#annualIncome').val() ? parseFloat($('#annualIncome').val()) : 0, // == C8
                totalAssets = $('#totalAssets').val() ? parseFloat($('#totalAssets').val()) : 0; // == C9
            $('#pct').html(pct+'%');

            // Cell numbers refer to the model here: https://docs.google.com/spreadsheet/ccc?key=0AnlxSfsd1fvkdEFJcVJLUUFQZWxYd1lmaUJfX0VKR1E
            var r = .06, // APR == C32
                Y = yearsBeforeRetire; // == C7

            var monthly =  anualIncome * payout_factors[(Section.getData('common').retirementNeedsVersion == 'by age') ? yearsInRetire : ((yearsInRetire / 5) - 1)]; // == C13
            monthly = monthly - totalAssets * Math.pow(1 + r,Y); // == C14
            monthly = monthly / value_factors[(Section.getData('common').retirementNeedsVersion == 'by age') ? yearsBeforeRetire : ((yearsBeforeRetire / 5) - 1) ]; // == C15
            monthly = monthly / 12; // == C16
            monthly = monthly * (pct/100); // == C17
            if (monthly < 0) {monthly = 0;}

            var c = monthly * 12, // == C17 * 12
                P = totalAssets, // == C9
                total_contributions = Math.ceil( P * Math.pow(1 + r, Y) + ( c * ( ( Math.pow(1 + r, Y + 1) - (1 + r) ) / r) ) ); // What will your total savings be when you retire?

            // Update display & hidden fields
            // Estimated Savings at Retirement display
            $('#pctValue').val(pct);
            savings.html('$ '+ common.formatCurrency(total_contributions))
            $('#estimatedSavings').val(total_contributions);
            //Recommended Monthly Plan Contribution Display
            month.html('$ '+ common.formatCurrency(monthly))
            $('#monthlyContribution').val(monthly);
            $('#annualIncomeDisplay').html('$ '+ common.formatCurrency(Section.getData('retirement_needs').RN_AnnualIncomeNeededInRetirement));
        },

        selectAssetTypes : function(){
            var data = Section.getData('retirement_needs');
            if(typeof data.RN_AssetTypes !== typeof undefined && !_.isEmpty(data.RN_AssetTypes) && $('.assetTypeForm .assetTypes').length > 0){
                var assetType = $('.assetTypeForm .assetTypes');
                _.each(assetType, function(check){
                    if(!_.isEmpty(_.findWhere(data.RN_AssetTypes, {value: $(check).val()}))){
                        $(check).prop('checked', true);
                    }
                });
            }
        },

        inflationAdjustedReplacementIncome : function(e){
            var input = $(e.target),
                years = parseInt(input.val());

            if(Section.getData('common').retirementNeedsVersion == 'by age'){
                var age = input.attr('data-age') ? parseInt(input.attr('data-age')) : 0,
                    inflation_factors = [1.03, 1.06, 1.09, 1.13, 1.16, 1.19, 1.23, 1.27, 1.3, 1.34, 1.38, 1.43, 1.47, 1.51, 1.56, 1.6, 1.65, 1.7, 1.75, 1.81, 1.86, 1.92, 1.97, 2.03, 2.09, 2.16, 2.22, 2.29, 2.36, 2.43, 2.5, 2.58, 2.65, 2.73, 2.81, 2.9, 2.99, 3.07, 3.17, 3.26, 3.36, 3.46, 3.56, 3.67, 3.78, 3.9, 4.01, 4.13, 4.26, 4.38];

                if(isNaN(years) || years < age || years <= 50){
                    $('#q5a, #q5').val('');
                    return;
                }
                var years = years - age,
                    RI = input.parents('.details').find('#RI').val(),
                    idx = years;
            }else{
                var RI = input.parents('.details').find('#RI').val(),
                    idx = (years / 5) - 1,
                    inflation_factors = [1.16, 1.34, 1.56, 1.81, 2.09, 2.43, 2.81, 3.26],
                    audio = Section.getData('common').RN_Audio;
                playerView.play(audio[4], 'audio');
            }
            $('#q5').val(years);
            $('#q5a').val(common.formatCurrency(RI * inflation_factors[idx]));
        },

        calculateTotalAssetEducate : function(e){
            var planBalance = common.dollar2float($('#q6a').val()),
                assets = common.dollar2float($('#q6b').val());

            if (isNaN(planBalance) || planBalance == '') {planBalance = 0;}
            if (isNaN(assets) || assets == '') {assets = 0;}

            var tav = parseFloat(planBalance) + parseFloat(assets);
            $('#q6c').val(tav).html("$ " + common.formatCurrency(tav));
            $('#totalAssets').val(tav);
        },

        calculateTotalAsset : function(e){
            var assets = common.dollar2float($('#q6b').val()),
                planBalance = $(e.target).data('planBalance');
            if(isNaN(assets) || assets == ''){assets = 0}
            if(isNaN(planBalance) || planBalance == ''){planBalance = 0}
            var tav = parseFloat(planBalance) + parseFloat(assets);
            $('#q6c').val(tav).html("$ " + common.formatCurrency(tav));
            $('#totalAssets').val(tav);
        },

        setYearsInRetire : function(e){
            if(Section.getData('common').retirementNeedsVersion == 'by age'){
                var current = $(e.target),
                    lifeExpectancy = parseInt(current.val()),
                    retirementAge = parseInt(current.attr('data-retirement-age'));

                if(isNaN(lifeExpectancy) || lifeExpectancy < retirementAge){
                    $('#q7').val('');
                    return;
                }
                $('#q7').val(lifeExpectancy - retirementAge);
            }else{
                $('#q7').val($(e.target).val());
            }
        },

        getAnswers : function(){

            var model = Section.getData('retirement_needs'),
                translation = Section.getData('translation');
            var returnBuilder = [
                { 'question': translation.rn_question_1, 'answer': '$' + common.formatCurrency(model.RN_CurrentYearlyIncome) },
                { 'question': translation.rn_question_2, 'answer': '$' + common.formatCurrency(model.RN_EstimatedRetirementIncome) },
            ];
            if (model.retirementNeedsFlow != 3) {
                returnBuilder.push({ 'question': translation.rn_question_3, 'answer': '$' + common.formatCurrency(model.RN_EstimatedSocialSecurityIncome) });
            }
            if (model.retirementNeedsFlow == 3 || model.retirementNeedsFlow == 4 ) {
                returnBuilder.push({ 'question': translation.rn_question_3a, 'answer': '$' + common.formatCurrency(model.RN_EstimatedPensionIncome) });
            }
            returnBuilder.push(
                { 'question': translation.rn_question_4, 'answer': '$' + common.formatCurrency(model.RN_ReplacementIncome) },
                {
                    'question': (Section.getData('common').retirementNeedsVersion == 'by age') ? translation.rn_question_5 : translation.rn_question_5a,
                    'answer': (Section.getData('common').retirementNeedsVersion == 'by age') ? model.RN_RetirementAge : model.RN_NumberOfYearsBeforeRetirement
                },
                { 'question': translation.rn_question_6, 'answer': '$' + common.formatCurrency(model.RN_InflationAdjustedReplacementIncome) },
                { 'question': translation.rn_question_7, 'answer': '$' + common.formatCurrency(model.planBalance) },
                { 'question': translation.rn_question_8, 'answer': '$' + common.formatCurrency(model.RN_OtherAssets) },
                { 'question': translation.rn_question_9, 'answer': '$' + common.formatCurrency(model.RN_TotalAssets) },
                { 'question': translation.rn_question_10, 'answer': !_.isEmpty(model.RN_AssetTypes) ? model.RN_AssetTypes.map(function(ele){return ele.value}).join(',') : ''  },
                {
                    'question': (Section.getData('common').retirementNeedsVersion == 'by age') ? translation.rn_question_11 : translation.rn_question_11a,
                    'answer': (Section.getData('common').retirementNeedsVersion == 'by age') ? model.RN_LifeExpectancy : model.RN_YearsYouWillLiveInRetirement
                },
                { 'question': translation.rn_question_12, 'answer': '$' + common.formatCurrency(model.RN_AnnualIncomeNeededInRetirement) },
                { 'question': translation.rn_question_13, 'answer': '$' + common.formatCurrency(model.RN_EstimatedSavingsatRetirement) },
                { 'question': translation.rn_question_14, 'answer': '$' + common.formatCurrency(model.RN_RecommendedMonthlyPlanContribution) },
                { 'question': translation.rn_question_15, 'answer': model.RN_IncomePct + '%' }
            );
            return returnBuilder;
        }

    };

    return{
        initializeIncomePlanSlider : RetirementNeedService.initializeIncomePlanSlider.bind(RetirementNeedService),
        slide : RetirementNeedService.slide.bind(RetirementNeedService),
        selectAssetTypes : RetirementNeedService.selectAssetTypes,
        inflationAdjustedReplacementIncome : RetirementNeedService.inflationAdjustedReplacementIncome,
        calculateTotalAssetEducate : RetirementNeedService.calculateTotalAssetEducate,
        calculateTotalAsset : RetirementNeedService.calculateTotalAsset,
        setYearsInRetire : RetirementNeedService.setYearsInRetire,
        getAnswers: RetirementNeedService.getAnswers
    }
});