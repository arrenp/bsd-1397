define(['jquery','underscore','moment','spe/service/common','spe/model/Section','spe/service/data'], function($, underscore, moment, common, Section,data){
    "use strict";

    var BeneficiaryService = {

        initService : function(){
            this.cacheElements();
        },

        cacheElements : function(){
            this.modal = $('#addBeneficiaryModal');
            this.uniqid = this.modal.find('#uniqueId');
            this.number = this.modal.find('#bNumber');
            this.name = this.modal.find('#bName');
            this.address1 = this.modal.find('#bAddress1');
            this.address2 = this.modal.find('#bAddress2');
            this.city = this.modal.find('#bCity');
            this.state = this.modal.find('#bState');
            this.zip = this.modal.find('#bZip');
            this.dob = this.modal.find('#bDob');
            this.isSpouse = this.modal.find('#bIsSpouse');
            this.percent = this.modal.find('#bPercent');
            this.firstName = this.modal.find('#bFirstName');
            this.lastName = this.modal.find('#bLastName');
            this.ssnPublic = this.modal.find('#bssnPublic');
            this.ssn = this.modal.find('#bSSN');
            this.phone = this.modal.find('#bPhone');
            this.relationship = this.modal.find('#bRelationship');
            this.gender = this.modal.find('#bGender');
            this.bType = this.modal.find('#bType');
            this.maritalStatus = this.modal.find('#bMaritalStatus');
            this.deleted = this.modal.find('#bDelete');
            this.bRelationship = this.modal.find('#bRelationship');
            this.bOmniType = this.modal.find('#bOmniType');
            this.bOmniLevel = this.modal.find('#bOmniLevel');
            this.trustEstateCharityOther = this.modal.find('#btrustEstateCharityOther');
            this.foreignaddress = this.modal.find('#bforeignaddress');
            this.country = this.modal.find('#bcountry');
            this.middleInitial = this.modal.find('#bmiddleInitial');
            if (this.ssn.val() == "")
            this.ssn.val(this.modal.find("[name=bssnPublic]").val());
            this.ssn.val(this.ssn.val().replace(/-/g, ''));
        },

        getData : function(type){
            if (type != "relius")
            {
                return {
                    'uniqid' : this.uniqid.val() ? this.uniqid.val() : this.getUniqueId(),
                    'fname' : $.trim(this.firstName.val()),
                    'lname' : $.trim(this.lastName.val()),
                    'gender' : $.trim(this.gender.find('option:selected').val()),
                    'marital_status' : $.trim(this.maritalStatus.find('option:selected').val()),
                    'ssn' : $.trim(this.ssn.val()),
                    'ssnPublic' : $.trim(this.ssnPublic.val()),
                    'relationship' : $.trim(this.relationship.find('option:selected').val()),
                    'type' : $.trim(this.bType.find('option:selected').val()),
                    'pct' : this.percent.val() && !isNaN(this.percent.val()) ? parseFloat($.trim(this.percent.val())) : '',
                    'BeneficiaryStreet1Address' : $.trim(this.address1.val()),
                    'BeneficiaryStreet2Address' : $.trim(this.address2.val()),
                    'BeneficiaryStateAddress' : this.state.find('option:selected').val(),
                    'BeneficiaryZipAddress' : $.trim(this.zip.val()).replace(/-/g, ""),
                    'BeneficiaryBirthDate' : $.trim(this.dob.val()) ? $.trim(this.dob.val()) : '',
                    'BeneficiaryCityAddress' : $.trim(this.city.val()),
                    'phone' : $.trim(this.phone.val()),
                    'trustEstateCharityOtherValue' : this.trustEstateCharityOther.prop("checked")? 1: 0,
                    'trustEstateCharityOtherChecked' : this.trustEstateCharityOther.prop("checked")? "checked" : "",
                    'foreignaddressValue' : this.foreignaddress.prop("checked")? 1: 0,
                    'foreignaddressChecked' : this.foreignaddress.prop("checked")? "checked" : "",
                    'country': this.country.find('option:selected').val(),
                    'middleInitial': this.middleInitial.val()
                }                
                
            }
            else{
                return {
                    'uniqid' : this.uniqid.val() ? this.uniqid.val() : this.getUniqueId(),
                    'BeneficiaryNumber' : $.trim(this.number.val()),
                    'BeneficiaryName' : $.trim(this.name.val()),
                    'BeneficiaryStreet1Address' : $.trim(this.address1.val()),
                    'BeneficiaryStreet2Address' : $.trim(this.address2.val()),
                    'BeneficiaryCityAddress' : $.trim(this.city.val()),
                    'BeneficiaryStateAddress' : this.state.find('option:selected').val(),
                    'BeneficiaryZipAddress' : $.trim(this.zip.val()).replace(/-/g, ""),
                    'BeneficiaryBirthDate' : $.trim(this.dob.val()) ? $.trim(this.dob.val()) : '',
                    'BeneficiaryTaxID' : $.trim(this.ssn.val()),
                    'BeneficiaryIsSpouseCode' : this.isSpouse.is(':checked') ? "Y" : "N",
                    'BeneficiaryTypeCode' : this.bType.find('option:selected').val(),
                    'BeneficiaryGender' : $.trim(this.gender.find('option:selected').val()),
                    'BeneficiaryRelationship' : this.bRelationship.find('option:selected').val(),
                    'BeneficiaryType' : this.bOmniType.find('option:selected').val(),
                    'BeneficiaryLevel' : this.bOmniLevel.find('option:selected').val(),
                    'BeneficiaryPercent' : this.percent.val() && !isNaN(this.percent.val()) ? parseFloat($.trim(this.percent.val())) : '',
                    'delete' : this.deleted.val() ? parseInt(this.deleted.val()) : 0
                }
            }
        },

        validate : function(type, max_p, max_s, max_c){
            var model = this.getData(type),
                translation = Section.getData('translation');
            if(type == 'Standard'){
                if(!model.fname){
                    common.showErrMsg(translation.please_enter_first_name);
                    return false;
                }else if(!model.lname){
                    common.showErrMsg(translation.please_enter_last_name);
                    return false;
                }else if(!model.gender){
                    common.showErrMsg(translation.please_select_gender);
                    return false;
                }else if(!model.marital_status){
                    common.showErrMsg(translation.please_select_marital_status);
                    return false;
                }else if(!common.isSsn(model.ssn) && model.ssn !== ''){
                    common.showErrMsg(translation.please_enter_valid_ssn);
                    return false;
                }else if(!model.relationship){
                    common.showErrMsg(translation.please_select_beneficiary_relationship);
                    return false;
                }else if(!model.type){
                    common.showErrMsg(translation.please_select_beneficiary_type);
                    return false;
                }else if(!model.pct || model.pct <= 0 || model.pct > 100){
                    common.showErrMsg(translation.please_enter_valid_percentage);
                    return false;
                }
            }
            if (type == "Srt"){
                if(!model.fname){
                    common.showErrMsg(translation.please_enter_first_name);
                    return false;
                }else if(!model.lname){
                    common.showErrMsg(translation.please_enter_last_name);
                    return false;
                }else  if(model.ssn !== '' && !common.isSsn(model.ssn)){
                    common.showErrMsg(translation.please_enter_valid_ssn);
                    return false;
                }else if(!model.relationship){
                    common.showErrMsg(translation.please_select_beneficiary_relationship);
                    return false;
                }else if(!model.type){
                    common.showErrMsg(translation.please_select_beneficiary_type);
                    return false;
                }else if(!model.pct || model.pct <= 0 || model.pct > 100){
                    common.showErrMsg(translation.please_enter_valid_percentage);
                    return false;
                    
                }                
                else if(!model.BeneficiaryZipAddress){
                    common.showErrMsg(translation.please_enter_valid_postal_code);
                    return false;
                }
                else if (!model.BeneficiaryBirthDate){
                    common.showErrMsg(translation.please_enter_date_of_birth);
                    return false;                    
                }                
            }
            if (type == "relius"){
                if(!model.BeneficiaryName) {
                    common.showErrMsg(translation.please_enter_name);
                    return false;
                }else if(this.isOmni() && !model.BeneficiaryStreet1Address){
                    common.showErrMsg(translation.please_enter_address1);
                    return false;
                }else if(this.isOmni() && !model.BeneficiaryCityAddress){
                    common.showErrMsg(translation.please_enter_city);
                    return false;
                }else if(this.isOmni() && !model.BeneficiaryStateAddress){
                    common.showErrMsg(translation.please_select_state);
                    return false;
                }else if((model.BeneficiaryZipAddress || this.isOmni()) && !common.isZip(model.BeneficiaryZipAddress)){
                    common.showErrMsg(translation.please_enter_valid_zip);
                    return false;
                }else if(this.isOmni() && !model.BeneficiaryBirthDate){
                    common.showErrMsg(translation.please_enter_date_of_birth);
                    return false;
                }else if(model.BeneficiaryBirthDate && !common.isValidDob(model.BeneficiaryBirthDate)){
                    common.showErrMsg(translation.please_enter_valid_dob);
                    return false;
                }else if((this.isOmni() || model.BeneficiaryTaxID) && !common.isSsn(model.BeneficiaryTaxID)){
                    common.showErrMsg(translation.please_enter_valid_ssn);
                    return false;
                }else if(this.isOmni() && model.BeneficiaryRelationship === ''){
                    common.showErrMsg(translation.please_select_beneficiary_relationship);
                    return false;
                }else if(this.isOmni() && !model.BeneficiaryType){
                    common.showErrMsg(translation.please_select_beneficiary_type);
                    return false;
                }else if(!this.isOmni() && !model.BeneficiaryTypeCode){
                    common.showErrMsg(translation.please_select_beneficiary_type);
                    return false;
                }else if(this.isOmni() && !model.BeneficiaryLevel){
                    common.showErrMsg(translation.please_select_beneficiary_level);
                    return false;
                }else if(!model.BeneficiaryPercent || model.BeneficiaryPercent <= 0 || model.BeneficiaryPercent > 100){
                    common.showErrMsg(translation.please_enter_benefit_allocation_percentage);
                    return false;
                }else if(!this.isOmni() && (model.BeneficiaryTypeCode == 'P') && (model.BeneficiaryPercent < 1 || model.BeneficiaryPercent > max_p)){
                    common.showErrMsg(translation.sum_of_pct_primary_beneficiaries_must_be_100);
                    return false;
                }else if(!this.isOmni() && (model.BeneficiaryTypeCode == 'C') && ( model.BeneficiaryPercent < 1 ||  model.BeneficiaryPercent > max_c)){
                    common.showErrMsg(translation.sum_of_pct_contingent_beneficiaries_must_be_100);
                    return false;
                }else if(this.isOmni() && (model.BeneficiaryLevel == '1') && (model.BeneficiaryPercent < 1 || model.BeneficiaryPercent > max_p)){
                    common.showErrMsg(translation.sum_of_pct_primary_beneficiaries_must_be_100);
                    return false;
                }else if(this.isOmni() && (model.BeneficiaryLevel == '2') && ( model.BeneficiaryPercent < 1 ||  model.BeneficiaryPercent > max_s)){
                    common.showErrMsg(translation.sum_of_pct_secondary_beneficiaries_must_be_100);
                    return false;
                }
            }
            return true;
        },

        isOmni : function () {
            return Section.getData('common').data_source.match('omni');
        },

        isNew : function(){
            if(this.uniqid && this.uniqid.val()){
                return false;
            }
            return true;
        },

        getUniqueId : function(){
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
        },

        getModel : function(type){
            return type == "relius" || type == 1 ?
                (Section.getData('partBeneficiaries') ? Section.getData('partBeneficiaries') : []) :
                (Section.getData('beneficiary') ? Section.getData('beneficiary') : []);
        },

        add : function(type){
            var model = this.getData(type),
                bData = this.getModel(type),
                bType = (type == 'relius') ? 'partBeneficiaries' : 'beneficiary';
            if($.type(bData) == 'object'){
                var tmp = [];
                _.each(bData, function(value, key){
                    tmp.push(value)
                });
                bData = tmp;
            }
            bData.push(model);
            localStorage.setItem(bType, JSON.stringify(bData));
            var savedData = new Object();
            savedData[bType] = bData;
            data.updateSession(savedData);
            _gaq.push(['_trackEvent', 'Beneficiary', 'Add']);
        },

        update : function(type){
            var model = this.getData(type),
                bData = this.getModel(type),
                bType = (type == 'relius') ? 'partBeneficiaries' : 'beneficiary';
            bData[this.getIndex(type == 'relius' ? 1 : 0, model.uniqid)] = model;
            localStorage.setItem(bType, JSON.stringify(bData));
            var savedData = new Object();
            savedData[bType] = bData;
            data.updateSession(savedData);
            _gaq.push(['_trackEvent', 'Beneficiary', 'Update']);
        },

        delete : function(type, uid, deleteBackup){
            var bData = this.getModel(type),
                bType = (type == 'relius') ? 'partBeneficiaries' : 'beneficiary',
                deletedBene = _.findWhere(bData, {uniqid: uid});

            if(!deleteBackup && type == 'relius'){
                var partBeneficiariesDeleted = Section.getData('partBeneficiariesDeleted') ? Section.getData('partBeneficiariesDeleted') : '';

                if(!partBeneficiariesDeleted){
                    var tmp = [];
                    tmp.push(deletedBene);
                    partBeneficiariesDeleted = tmp;
                }else{
                    if($.type(partBeneficiariesDeleted) == 'object'){
                        var tmp = [];
                        _.each(partBeneficiariesDeleted, function(value, key){
                            tmp.push(value)
                        });
                        partBeneficiariesDeleted = tmp;
                    }
                    partBeneficiariesDeleted.push(deletedBene);
                }
                localStorage.setItem('partBeneficiariesDeleted', JSON.stringify(partBeneficiariesDeleted));
            }
            bData = _.without(bData, _.findWhere(bData, {uniqid: uid}));
            localStorage.setItem(bType, JSON.stringify(bData));
            var savedData = new Object();
            savedData[bType] = bData;
            data.updateSession(savedData);
            _gaq.push(['_trackEvent', 'Beneficiary', 'Delete']);
        },

        getIndex : function(type, uid){
            var bData = this.getModel(type),
                index = -1;

            _.each(bData, function(d, idx) {
                if (d.uniqid == uid) {
                    index = idx;
                    return;
                }
            });
            return index;
        },

        filterById : function(type, uid){
            return this.getModel(type)[this.getIndex(type, uid)];
        },

        getPct : function(type){
            var bData = this.getModel(type),
                p_pct = 0, s_pct = 0, c_pct = 0, p_exist = false, c_exist = false, s_exist = false,
                _self = this;

            if(type){
                _.each(bData, function(d, i) {

                    if(_self.isOmni()){
                        if(d.BeneficiaryLevel == '1'){
                            p_pct += parseFloat(d.BeneficiaryPercent);
                            p_exist = true;
                        }else if(d.BeneficiaryLevel == '2'){
                            s_pct += parseFloat(d.BeneficiaryPercent);
                            s_exist = true;
                        }
                    }else{
                        if(d.BeneficiaryTypeCode == 'P'){
                            p_pct += parseFloat(d.BeneficiaryPercent);
                            p_exist = true;
                        }else{
                            c_pct += parseFloat(d.BeneficiaryPercent);
                            c_exist = true;
                        }
                    }
                });
            }else{
                _.each(bData, function(d, i) {
                    if(d.type == 'Primary'){
                        p_pct += parseFloat(d.pct);
                    }else{
                        s_pct += parseFloat(d.pct);
                    }
                });
            }

            return {
                'p_pct' : p_pct,
                's_pct' : s_pct,
                'c_pct' : c_pct,
                'p_exist' : p_exist,
                's_exist' : s_exist,
                'c_exist' : c_exist,
                'rem_p' : 100 - p_pct,
                'rem_s' : 100 - s_pct,
                'rem_c' : 100 - c_pct
            }
        },

        process : function(type){
            var bData = this.getModel(type),
                model = Section.getData('common'),
                pct = this.getPct(type),
                pData = {},
                translation = Section.getData('translation');

            if(type == 'relius'){
                if(parseInt(model.planRequiresBeneficiary) && _.isEmpty(bData)){
                    common.showErrMsg(translation.beneficiary_information_required);
                    return false;
                }else{
                    if(this.isOmni()){
                        if (pct.p_exist && !pct.s_exist && pct.rem_p != 0) {
                            common.showErrMsg(translation.sum_of_pct_primary_beneficiaries_must_be_100);
                            return false;
                        } else if ((pct.s_exist && (!pct.p_exist || pct.rem_s != 0)) || (pct.s_exist && pct.p_exist && (pct.rem_p != 0 || pct.rem_s != 0) )) {
                            common.showErrMsg(translation.sum_of_pct_secondary_beneficiaries_must_be_100);
                            return false;
                        } else {
                            Section.setData('common', {updateProfileBeneficiary: 1, updateBeneficiary: 1});
                            _gaq.push(['_trackEvent', 'Beneficiary', 'Proceed']);
                            return true;
                        }
                    }else {
                        if (pct.p_exist && !pct.c_exist && pct.rem_p != 0) {
                            common.showErrMsg(translation.sum_of_pct_primary_beneficiaries_must_be_100);
                            return false;
                        } else if ((pct.c_exist && (!pct.p_exist || pct.rem_c != 0)) || (pct.c_exist && pct.p_exist && (pct.rem_p != 0 || pct.rem_c != 0) )) {
                            common.showErrMsg(translation.sum_of_pct_contingent_beneficiaries_must_be_100);
                            return false;
                        } else {
                            Section.setData('common', {updateProfileBeneficiary: 1, updateBeneficiary: 1});
                            _gaq.push(['_trackEvent', 'Beneficiary', 'Proceed']);
                            return true;
                        }
                    }
                }
            }else{
                if(model.beneficiaries == 2 && _.isEmpty(bData)){
                    common.showErrMsg(translation.add_beneficiary);
                    return false;
                }else{
                    if((model.beneficiaries == 1 && !_.isEmpty(bData)) || (model.beneficiaries == 2) ){
                        if(pct.p_pct != 100){
                            common.showErrMsg(translation.primary_beneficiaries_percentages_must_equal_100);
                            return false;
                        }else if(pct.s_pct && pct.s_pct != 100){
                            common.showErrMsg(translation.secondary_beneficiaries_percentages_must_equal_100);
                            return false;
                        }else{
                            Section.setData('common', { updateProfileBeneficiary: 1, updateBeneficiary: 1 });
                            _gaq.push(['_trackEvent', 'Beneficiary', 'Proceed']);
                            return true;
                        }
                    }else{
                        Section.setData('common', { updateProfileBeneficiary: 1, updateBeneficiary: 0 });
                        _gaq.push(['_trackEvent', 'Beneficiary', 'Proceed']);
                        return true;
                    }
                }
            }
        },

        formatDate : function(date, format){
            return date ? moment(date).format(format) : '';
        }

    };

    return {
        initService : BeneficiaryService.initService.bind(BeneficiaryService),
        validate : BeneficiaryService.validate.bind(BeneficiaryService),
        isNew : BeneficiaryService.isNew.bind(BeneficiaryService),
        getData : BeneficiaryService.getData.bind(BeneficiaryService),
        getUniqueId : BeneficiaryService.getUniqueId.bind(BeneficiaryService),
        add : BeneficiaryService.add.bind(BeneficiaryService),
        update : BeneficiaryService.update.bind(BeneficiaryService),
        filterById : BeneficiaryService.filterById.bind(BeneficiaryService),
        delete : BeneficiaryService.delete.bind(BeneficiaryService),
        process : BeneficiaryService.process.bind(BeneficiaryService),
        formatDate : BeneficiaryService.formatDate,
        isOmni : BeneficiaryService.isOmni.bind(BeneficiaryService)
    }

});
