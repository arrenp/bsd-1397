/**
 * @module contributionService
 * @name spe:service:contributionService
 *
 * @description
 * this module help to build playlist from
 *
 * @requires jquery, underscore
 * */
define([
    'jquery',
    'jquery.ui',
    'spe/service/common',
    'spe/config',
    'spe/model/Section',
    './data'
], function($, ui, common, config, Section, data){
    
    var savedSalaryTotalPct = 0;
    var savedRothTotalPct = 0;
    var savedPostTotalPct = 0;
    var savedACAoptOut = 0;
    var hasSavedData = false;
    

    var contributionService = {

        initData : function(){
            var commonData = Section.getData('common'),
                retirementNeedsData = Section.getData('retirement_needs'),
                contributionsData = Section.getData('contributions');

            return {
                'commonData' : commonData,
                'retirementNeedsData' : retirementNeedsData,
                'contributionsData' : contributionsData,
                'totalAmount' : this.getTotalAmount(),
                'contributionTotal' : 0
            }
        },

        initSlider : function(ele,current,min,max,pctPre,type){
            var self = this;
            return $('#'+ele).slider({
                value: parseFloat(current),
                min: parseFloat(min),
                max: parseFloat(max),
                step: parseFloat(pctPre),
                slide: function(event, ui){
                    self.cslide(ui.value, type);
                },
                change: function(event, ui){
                    self.cchange(event,ui,type);
                },
                stop: function(event, ui){
                    self.cstop(event,ui,type);
                }
            })
        },

        cstop : function(event, ui,type){
            _gaq.push(['_trackEvent', 'Contributions', 'Value Changed', (type == 'salary') ? 'Pre-tax per paycheck' : 'After-tax (Roth) per paycheck' , ui.value]);
        },

        getTotalAmount : function(){
            var retirementNeedsData = Section.getData('retirement_needs');
            return parseInt(Math.round((parseInt(0)/common.isSet(retirementNeedsData.RN_RecommendedMonthlyPlanContribution) ? parseInt(retirementNeedsData.RN_RecommendedMonthlyPlanContribution) : 0))) * parseInt(common.isSet(retirementNeedsData.RN_EstimatedSavingsatRetirement) ? parseInt(retirementNeedsData.RN_EstimatedSavingsatRetirement) : 0) || 0
        },

        cchange : function(event, ui, type){
            if(event.eventPhase){
                $('#'+ type +'_total_pct').trigger('change');
            }
        },

        cslide : function(value, type){
            $('#'+ type +'_slider_current').val(value);
            var pct =  Math.round(value * 100)/100;
            pct = isNaN(pct) ? 0 : pct;
            var min = $('#'+type+"_slider_min").val(),
                max = $('#'+type+"_slider_max").val();

            if(pct < min || pct > max) {
                $('#'+type+"_total_pct").val(0);
                return false;
            }
            $('#'+type+"_total_pct").val(pct);
            this.totalContribution();
        },

        totalContribution : function(){
            var translation = Section.getData('translation');
            if($('#yearly_pay').val() == '' || parseInt($('#yearly_pay').val()) == 0){
                $(".contribution_amt, .actions").hide();
                $(".alert").show().html(translation.please_fill_out_current_yearly_income_field);
                return false;
            }
            if($('#get_paid').val()){
                $(".alert").hide().html('');
                $(".contribution_amt, .actions").show();
                this.calculateContribution();
            }else{
                $(".contribution_amt, .actions").hide();
                $(".alert").show().html(translation.above_please_tell_us_how_often_you_paid);
                return false;
            }
        },

        roundToNearestStep : function(num, step) {
            return Math.round(num *(1/step))/(1/step);
        },

        getMax: function () {
            var salary_total_value = parseFloat($('#salary_total_value').val());
            var roth_total_value = parseFloat($('#roth_total_value').val());
            salary_total_value = isNaN(salary_total_value) ? 0 : salary_total_value;
            roth_total_value = isNaN(roth_total_value) ? 0 : roth_total_value;
            return salary_total_value + roth_total_value;
        },
        getMaxRound: function () {
            return Math.round(this.getMax());
        },

        getPaycheckTotal: function () {

            var post_total_value = parseFloat($('#post_total_value').val());
            post_total_value = isNaN(post_total_value) ? 0 : post_total_value;

            return Math.round(this.getMax() + post_total_value);
        },

        getPayroll: function () {
            var payroll = [];

            payroll['Annually'] = 1,
                payroll['Quarterly'] = 4,
                payroll['Monthly'] = 12,
                payroll['Semi-monthly'] = 24,
                payroll['Bi-weekly'] = 26,
                payroll['Weekly'] = 52;

            return payroll;
        },

        calculateContribution : function(radioClicked){
            var model = this.initData(),
                get_paid = $('#get_paid').val(),
                payroll = [],
                yearly_pay = common.dollar2float($('#yearly_pay').val()),
                mode = "dlr";

            //radioClicked is 1 if event was fired from clicking the radio button instead of updating the contributions box
            if(radioClicked != 1) {
                radioClicked = 0;
            }
            payroll['Annually'] = 1,
                payroll['Quarterly'] = 4,
                payroll['Monthly'] = 12,
                payroll['Semi-monthly'] = 24,
                payroll['Bi-weekly'] = 26,
                payroll['Weekly'] = 52;

            $('#yearly_pay').val(common.formatCurrency(yearly_pay));

            if (($('#contribution_type_pct').is(':checked') && !radioClicked) || ($('#contribution_type_dlr').is(':checked') && radioClicked)) {
                mode = "pct";
                // Populate dollar value boxes based on PCT entries
                payroll['Annually'] = 1,
                    payroll['Quarterly'] = 1,
                    payroll['Monthly'] = 1,
                    payroll['Semi-monthly'] = 1,
                    payroll['Bi-weekly'] = 1,
                    payroll['Weekly'] = 1;
                this.calculateValues(payroll,yearly_pay,model,get_paid);
            } else {

                // Do the opposite
                // That is, populate the percent values based on the dollar values
                // But don't actually pull values into the inputs since we can't send RKDs fractional percentages

                // $("#salary_total_value").val(common.dollar2float($("#salary_total_value").val()));
                // $("#roth_total_value").val(common.dollar2float($("#roth_total_value").val()));
                // $("#post_total_value").val(common.dollar2float($("#post_total_value").val()));

                pre_tax_per_paycheck = $("#salary_total_value").val();
                roth_tax_per_paycheck = $("#roth_total_value").val();
                post_tax_per_paycheck = $("#post_total_value").val();

                pre_tax_pct = payroll[get_paid] * pre_tax_per_paycheck / yearly_pay * 100;
                roth_tax_pct = payroll[get_paid] * roth_tax_per_paycheck / yearly_pay * 100;
                post_tax_pct = payroll[get_paid] * post_tax_per_paycheck / yearly_pay * 100;


                $("#salary_total_pct").val(this.roundToNearestStep(pre_tax_pct, model.commonData.prePctPre));
                $("#roth_total_pct").val(this.roundToNearestStep(roth_tax_pct, model.commonData.rothPctPre));
                $("#post_total_pct").val(this.roundToNearestStep(post_tax_pct, model.commonData.postPctPre));

                if(radioClicked){
                    $('#salary_total_pct').trigger("change");
                    $('#roth_total_pct').trigger("change");
                    $('#post_total_pct').trigger("change");
                }
            }

            var total = this.getPaycheckTotal();

            //total monthly contribution
            var tmc = Math.round(total * (payroll[get_paid] / 12));
            tmc = isNaN(tmc) ? 0 : tmc;

            $('#total_value_display').html("$"+common.formatCurrency(tmc));
            $('#total_value').val(tmc);

            var total_assets = parseFloat($('#total_assets').val());
            var apr = 0.06;
            var ybr = parseFloat($('#years_before_retirement').val());

            var target_savings = $('#target_savings').val();
            var contribRate = tmc * 12 / yearly_pay;
            var monthlyPay = yearly_pay / 12;

            var matchCapApct = model.commonData.matchCapA/100,
                matchCapBpct = model.commonData.matchCapB/100,
                matchRateApct = model.commonData.matchRateA/100,
                matchRateBpct = model.commonData.matchRateB/100;

            var matchAmountA = 0;
            if (contribRate > matchCapApct) {
                matchAmountA = matchCapApct * monthlyPay * matchRateApct;
            } else {
                matchAmountA = contribRate * monthlyPay * matchRateApct;
            }

            var matchAmountB = 0;
            if (contribRate > matchCapBpct) {
                matchAmountB = (matchCapBpct - matchCapApct) * monthlyPay * matchRateBpct;
            } else if ((contribRate - matchCapApct) > 0) {
                matchAmountB = (contribRate - matchCapApct) * monthlyPay * matchRateBpct;
            }

            var totalMatchAmount = Math.round(matchAmountA + matchAmountB);
            var projected_match = ((totalMatchAmount * 12) * (Math.pow(1+apr,1+ybr) - (1+apr))) / apr; // this is the "Employer" section of the column

            if (model.commonData.matchingContributions == 'yes') {
                savingsMaximizer = Math.max(model.commonData.matchCapB, model.commonData.matchCapA);
            } else {
                savingsMaximizer = 0;
            }
            maxMatch = Math.round((matchCapBpct - matchCapApct) * monthlyPay * matchRateBpct + matchCapApct * monthlyPay * matchRateApct);

            $('#total_value_display_matching').html("$"+common.formatCurrency(totalMatchAmount));
            $('#total_value_matching').val(Math.round(totalMatchAmount));
            var projected_savings = (total_assets * Math.pow(1+apr,ybr)) + ((tmc * 12) * (Math.pow(1+apr,1+ybr) - (1+apr))) / apr; // this is the "You" section of the column
            var total_projected_savings = projected_savings;
            if (model.commonData.matchingContributions == 'yes') {
                total_projected_savings += projected_match;
            }

            var ratio = total_projected_savings / target_savings;
            var ratio_projected = projected_savings/total_projected_savings;

            var totHeight;
            if(ratio <= 1){
                totHeight = 0;
                totHeight += this.checkAndGetMinimumHeight(Math.ceil(160*ratio*ratio_projected), 17);
                totHeight += this.checkAndGetMinimumHeight(Math.ceil(160*ratio*(1-ratio_projected)), 17);

                $('#total_bar').stop().animate({height : this.checkAndGetMinimumHeight(Math.ceil(160*ratio), totHeight)+'px'}, 1000);
                $('#target_bar').stop().animate({height : '160px'}, 1000);

                $('#total_bar .savings').stop().animate({height : this.checkAndGetMinimumHeight(Math.ceil(160*ratio*ratio_projected), 17)+'px'}, 1000);
                $('#total_bar .match').stop().animate({height : this.checkAndGetMinimumHeight(Math.ceil(160*ratio*(1-ratio_projected)), 17)+'px'}, 1000);
            } else {
                totHeight = 0;
                totHeight += this.checkAndGetMinimumHeight(Math.ceil(160*ratio*ratio_projected), 17);
                totHeight += this.checkAndGetMinimumHeight(Math.ceil(160*ratio*(1-ratio_projected)), 17);

                $('#total_bar').stop().animate({height : '160px'}, 1000);
                $('#target_bar').stop().animate({height : Math.round(160/ratio)+'px'}, 1000);

                $('#total_bar .savings').stop().animate({height : this.checkAndGetMinimumHeight(Math.ceil(160*ratio_projected), 17)+'px'}, 1000);
                $('#total_bar .match').stop().animate({height : this.checkAndGetMinimumHeight(Math.ceil(160*(1-ratio_projected)), 17)+'px'}, 1000);
            }

            $(".match_amt").html("$" + common.formatCurrency(Math.round(projected_match)));
            $(".savings_amt").html("$" + common.formatCurrency(Math.round(projected_savings)));
            $(".total_amt").html("$" + common.formatCurrency(Math.round(total_projected_savings)));
            if (mode == "pct")
            {
                payroll = this.getPayroll();
                this.calculateValues(payroll,yearly_pay,model,get_paid);
            }
        },
        checkAndGetMinimumHeight : function(height, minimumHeight) { // cap this at 160 to match max height of graph
            if (height === 0) {
                return 0;
            }
            if (height > minimumHeight) {
                if (height > 160) {
                    return 160;
                }
                return height;
            }

            return minimumHeight;
        },
        calculateValues : function(payroll,yearly_pay,model,get_paid){
            pre_tax_pct = $("#salary_total_pct").val();
            roth_tax_pct = $("#roth_total_pct").val();
            post_tax_pct = $("#post_total_pct").val();

            pre_tax_per_paycheck   = (yearly_pay * (pre_tax_pct/100))/12/(payroll[get_paid]/12);
            roth_tax_per_paycheck  = (yearly_pay * (roth_tax_pct/100))/12/(payroll[get_paid]/12);
            post_tax_per_paycheck  = (yearly_pay * (post_tax_pct/100))/12/(payroll[get_paid]/12);

            $("#salary_total_value").val(this.roundToNearestStep(pre_tax_per_paycheck, model.commonData.preDlrPre));
            $("#roth_total_value").val(this.roundToNearestStep(roth_tax_per_paycheck, model.commonData.rothDlrPre));
            $("#post_total_value").val(this.roundToNearestStep(post_tax_per_paycheck, model.commonData.postDlrPre));
        },
        switchToPct : function(){
            var commonData = Section.getData('common');
            if (commonData.ACAon && hasSavedData) {
                $("#ACAoptOut").val(savedACAoptOut);
                Section.setData('contributions', {'C_ACAoptOut' : savedACAoptOut});
                $("#salary_total_pct").val(savedSalaryTotalPct).trigger("change");
                $("#roth_total_pct").val(savedRothTotalPct).trigger("change");
                $("#post_total_pct").val(savedPostTotalPct).trigger("change");
            }
            $('.cont_pct, .aggCon').show();
            $('.cont_dlr').hide();
            this.calculateContribution(1);
        },

        switchToDlr : function(){
            var commonData = Section.getData('common');
            if (commonData.ACAon) {
                hasSavedData = true;
                savedACAoptOut = $("#ACAoptOut").val();
                savedSalaryTotalPct = $("#salary_total_pct").val();
                savedRothTotalPct = $("#roth_total_pct").val();
                savedPostTotalPct = $("#post_total_pct").val();
                $("#ACAoptOut").val(1);
                Section.setData('contributions', {'C_ACAoptOut' : 1});
            }
            $('.cont_dlr').show();
            $('.cont_pct, .aggCon').hide();
            this.calculateContribution(1);
        },
        
        switchToDlrWithOptOutWarning: function(e) {
            var commonData = Section.getData('common');
            if (commonData.ACAon) {
                $("#dollarOptOutModal").modal('show');
                e.preventDefault();
                return false;
            }
            this.switchToDlr.call(this);
        },

        totalPctSalary : function(e){
            this.cslide($(e.target).val(), 'salary');
            $('#slider1').slider({ value: $(e.target).val() });
        },

        totalPctRoth : function(e){
            this.cslide($(e.target).val(), 'roth');
            $('#slider2').slider({ value: $(e.target).val() });
        },

        totalPctPost : function(e){
            this.cslide($(e.target).val(), 'post');
            $('#slider3').slider({ value: $(e.target).val() });
        },

        initWidget : function(){
            var model = this.initData(),
                translation = Section.getData('translation'),
                hint = (!(model.commonData.secondaryMatchPercent == 0 || model.commonData.secondaryMatchCap == 0)) ?
                    translation.employer_matching_your_employer_matches+" "+model.commonData.matchPercent+"% "+translation.of_your_contributions_up_to+' '+model.commonData.matchCap+'% '+translation.of_your_salary+' '+translation.and_then+' '+model.commonData.secondaryMatchPercent+'% '+translation.up_to+ ' ' +model.commonData.secondaryMatchCap+'% '+translation.of_your_salary+'.'
                    :
                    translation.employer_matching_your_employer_matches+" "+model.commonData.matchPercent+"% "+translation.of_your_contributions_up_to+' '+model.commonData.matchCap+'% '+translation.of_your_salary+'.';

            $('.hint').attr('title', hint);
            $('#total_bar').stop().animate({'height' : Math.round(160 * Math.round(parseInt(model.contributionTotal)/parseInt(model.retirementNeedsData.RN_RecommendedMonthlyPlanContribution))*160)+'px'}, 1000);
            $('#target_bar').stop().animate({'height' : '160px'}, 1000);
            this.totalContribution();
            if(model.contributionsData.C_PreTaxContributionValue) {
                if(model.contributionsData.contribution_mode == "DEFERDLR") {
                    $("#contribution_type_dlr").prop("checked", true).trigger("change");
                    $("#salary_total_value").val(model.contributionsData.C_PreTaxContributionValue).trigger("change");

                    if($('#roth').length)
                        $("#roth_total_value").val(model.contributionsData.C_RothContributionValue).trigger("change");
                    if($('#post').length)
                        $("#post_total_value").val(model.contributionsData.C_PostTaxContributionValue).trigger("change");
                    $('.cont_dlr').show();
                    $('.cont_pct, .aggCon').hide();
                } else {
                    $("#salary_total_pct").val(model.contributionsData.C_PreTaxContributionPct).trigger("change");

                    if($('#roth').length)
                        $("#roth_total_pct").val(model.contributionsData.C_RothContributionPct).trigger("change");
                    if($('#post').length)
                        $("#post_total_pct").val(model.contributionsData.C_PostTaxContributionPct).trigger("change");
                }


            }
            if(model.commonData.ACAon){
                $('#contributionNotice').modal('show');
            }else if(!model.commonData.planAllowsDeferrals){
                $('#contributionWarning').modal('show');
                _gaq.push(['_trackEvent', 'Contributions', 'Popup - Plan does not allow contribution']);
            }
            $('.allowed').keyup();
        },

        showACAMessage : function(e){
            var self = $(e.target);
            if($("#ACAoptOut").val() == 0 && parseFloat($(self).val()) != parseFloat($(self).attr('dval'))){
                if($("#ACAoptOutNotice").val() != 1){
                    $('#acaMessage').modal('show');
                }
            }else{
                $(".ACAoptOutNotice").hide();
                $("#ACAoptOutNotice").val(0);
            }
        },

        saveCatchup : function(){
            var popup = 1,
                maxWarning2 = 0,
                model = Section.getData('common');

            if($.inArray(model.data_source,['relius','educate','expertplan','retrev']) > -1 || model.data_source.match('omni')) {
                popup = 0;
                maxWarning2 = parseInt(model.showCatchup) ? 1 : 0;
            }
            if(popup) {
                if(!this.cvalidate() && !this.maxWarning(false)){
                    var catchupValue = 0,
                        cmode = $('#catchup_mode').val(),
                        mode_str = "<b>(%)</b> below.";

                    if(cmode == 0){
                        mode_str = "<b>(%)</b> below.";
                    }else if(cmode == 1){
                        mode_str = "<b>($)</b> below.";
                    }else if(cmode == 2){
                        mode_str = "below.<div class='catchup_option'>$<input type='radio' name='catchup_type' id='catchup_type_dlr'/> %<input type='radio' name='catchup_type' id='catchup_type_pct' checked/></div>";
                    }
                    $('#paycheckAmountModal').find('.mode_str').html(mode_str);
                    $('#paycheckAmountModal').modal('show');
                }
            }else if(maxWarning2){
                if(!this.cvalidate() && !this.maxWarning(true)){
                    this.showACAModalOrProcess(0,0);
                }
            }else{
                if(!this.cvalidate() && !this.maxWarning(false)){
                    this.showACAModalOrProcess(0,0);
                }
            }
        },

        paycheckAmount : function(e){
            var self = $(e.currentTarget),
                attr = self.attr('rel'),
                catchup = $.trim(self.parents('.modal').find('.catchupValue').val());

            if(attr == 'save' && catchup){
                this.showACAModalOrProcess(1,catchup);
                _gaq.push(['_trackEvent', 'Contributions', 'Popup', 'Catchup  - Proceed']);
            }else if(attr == 'cancel' || !catchup){
                this.showACAModalOrProcess(0,0);
                _gaq.push(['_trackEvent', 'Contributions', 'Popup', 'Catchup  - Cancel']);
            }
            $('#paycheckAmountModal').modal('hide');
        },
        csave : function(){
            if(!this.cvalidate() && !this.maxWarning(false)){
                this.showACAModalOrProcess(0,0);
            }
        },

        contributionArrangement : function(e){
            var self = $(e.currentTarget),
                attr = self.attr('rel');

            if(attr == 'Opt Out'){
                $("#ACAoptOut").val(1);
                Section.setData('contributions', {'C_ACAoptOut' : 1});
                this.cprocess(0,0);
                _gaq.push(['_trackEvent', 'Contributions', 'Popup', 'ACA - Opt Out']);
            }else if(attr == 'Go Back'){
                $('body').trigger('spe:router:redirect','contributions');
                _gaq.push(['_trackEvent', 'Contributions', 'Popup', 'ACA - Go Back']);
            }
            $('#cArrangementModal').modal('hide');
        },

        cvalidate : function(){
            var commonData = Section.getData("common");
            var warn = 0,
                yearly_pay = common.dollar2float($('#yearly_pay').val()),
                get_paid = $('#get_paid').val(),
                payroll = [];
            payroll['Annually'] = 1;
            payroll['Quarterly'] = 4;
            payroll['Monthly'] = 12;
            payroll['Semi-monthly'] = 24;
            payroll['Bi-weekly'] = 26;
            payroll['Weekly'] = 52;

            var stp = parseFloat($('#salary_total_pct').val()),
                stv = parseFloat($('#salary_total_value').val()),
                smin = parseInt($('#salary_slider_min').val()),
                smax = parseInt($('#salary_slider_max').val()),
                svmin = (yearly_pay * (smin/100))/12/(payroll[get_paid]/12),
                svmax = (yearly_pay * (smax/100))/12/(payroll[get_paid]/12),

                rtp = parseFloat($('#roth_total_pct').val()),
                rtv = parseFloat($('#roth_total_value').val()),
                rmin = parseInt($('#roth_slider_min').val()),
                rmax = parseInt($('#roth_slider_max').val()),
                rvmin = (yearly_pay * (rmin/100))/12/(payroll[get_paid]/12),
                rvmax = (yearly_pay * (rmax/100))/12/(payroll[get_paid]/12),

                ptp = parseFloat($('#post_total_pct').val()),
                ptv = parseFloat($('#post_total_value').val()),
                pmin = parseInt($('#post_slider_min').val()),
                pmax = parseInt($('#post_slider_max').val()),
                pvmin = (yearly_pay * (pmin/100))/12/(payroll[get_paid]/12),
                pvmax = (yearly_pay * (pmax/100))/12/(payroll[get_paid]/12);

            var totalPercent = stp + rtp + ptp;
            var totalDollar  = stv + ptv + rtv;



            var translation = Section.getData('translation');

            var aMin = 0;
            var aMax = 100;
            if($('#deferralMinRateAgg').length){
                aMin = $('#deferralMinRateAgg').val();
                aMax = $('#deferralMaxRateAgg').val();
            } else {
                if (Section.getData("module").combinedContributionAmount != 0)
                {
                    aMax = Section.getData("module").combinedContributionAmount;
                }
                else
                {
                    aMax = Math.max(smax, rmax, pmax);
                }
            }

            $('.contribution').find('.error').removeClass('error');
            if($('#yearly_pay').val() == '' || parseInt($('#yearly_pay').val()) == 0){
                $('#yearly_pay').addClass('error');
                common.showErrMsg(translation.please_provide_your_current_yearly_income);
                warn = 1;
            }else if($('#get_paid').val() == ''){
                $('#get_paid').addClass('error');
                common.showErrMsg(translation.please_select_how_ofter_you_get_paid);
                warn = 1;
            }

            if ($('#contribution_type_pct').is(':checked')) {
                if(totalPercent  < aMin || totalPercent > aMax) {
                    $('#salary_total_pct, #roth_total_pct, #post_total_pct').addClass('error');
                    common.showErrMsg(translation.please_enter_valid_percentage+'\n\n\t '+translation.aggregate_min+': ' + aMin + '%\n\t'+translation.aggregate_max+': ' + aMax + '%');
                    warn = 1;
                }
                else if ($('#pre').length && !common.isDivisible($('#salary_total_pct').val(),commonData.prePctPre)){
                    $('#salary_total_pct').addClass('error');
                    warn = 1;
                    this.divisibleByAlert("pct",commonData.prePctPre);
                }
                else if($('#pre').length && (stp < smin || stp > smax)){
                    $('#salary_total_pct').addClass('error');
                    common.showErrMsg(translation.please_enter_valid_percentage+'\n\n\t'+translation.min+': ' + smin + '%\n\t'+translation.max+': ' + smax + '%');
                    warn = 1;
                }
                else if ($('#roth').length && !common.isDivisible($('#roth_total_pct').val(),commonData.rothPctPre)){
                    $('#roth_total_pct').addClass('error');
                    warn = 1;
                    this.divisibleByAlert("pct",commonData.rothPctPre);
                }
                else if($('#roth').length && (rtp < rmin || rtp > rmax)){

                    $('#roth_total_pct').addClass('error');
                    common.showErrMsg(translation.please_enter_valid_percentage+'\n\n\t'+translation.min+': ' + rmin + '%\n\t'+translation.max+': ' + rmax + '%');
                    warn = 1;
                }
                else if ($('#post').length && !common.isDivisible($('#post_total_pct').val(),commonData.postPctPre)){
                    $('#post_total_pct').addClass('error');
                    warn = 1;
                    this.divisibleByAlert("pct",commonData.postPctPre);
                }
                else if($('#post').length && (ptp < pmin || ptp > pmax)){

                    $('#post_total_pct').addClass('error');
                    common.showErrMsg(translation.please_enter_valid_percentage+'\n\n\t'+translation.min+': ' + pmin + '%\n\t'+translation.max+': ' + pmax + '%');
                    warn = 1;
                }else if(totalPercent > 100){

                    $('#post_total_pct').addClass('error');
                    common.showErrMsg(translation.please_enter_valid_tota_deferral);
                    warn = 1;
                }
                $( "#slider1" ).slider({ value: stp });
                $( "#slider2" ).slider({ value: rtp });
                $( "#slider3" ).slider({ value: ptp });

            } else {
                var theMax = (yearly_pay/(100/aMax))/payroll[get_paid];

                if(totalDollar < 0 || totalDollar > theMax) {
                    $('#salary_total_value, #roth_total_value, #post_total_value').addClass('error');
                    common.showErrMsg(translation.please_enter_valid_dollar_amount + '\n\n\t '+ translation.aggregate_min +': $' + 0 + '\n\t'+ translation.aggregate_max +': $' + theMax.toFixed(2));
                    warn = 1;
                }
                else if ($('#pre').length && !common.isDivisible($('#salary_total_value').val(),commonData.preDlrPre)){
                    $('#salary_total_value').addClass('error');
                    warn = 1;
                    this.divisibleByAlert("dlr",commonData.preDlrPre);
                }
                else if ($('#pre').length && (stv < svmin || stv > svmax)){
                    $('#salary_total_value').addClass('error');
                    common.showErrMsg(translation.please_enter_valid_dollar_amount + '\n\n\t'+ translation.min +': $' + Math.ceil(svmin) + '\n\t'+ translation.max +': $' + Math.floor(svmax));
                    warn = 1;
                }
                else if($('#roth').length && !common.isDivisible($('#roth_total_value').val(),commonData.rothDlrPre)){
                    $('#roth_total_value').addClass('error');
                    warn = 1;
                    this.divisibleByAlert("dlr",commonData.rothDlrPre);
                }
                else if($('#roth').length && (rtv < rvmin || rtv > rvmax)){
                    $('#roth_total_value').addClass('error');
                    common.showErrMsg(translation.please_enter_valid_dollar_amount + '\n\n\t'+ translation.min +': $' + Math.ceil(rvmin) + '\n\t'+ translation.max +': $' + Math.floor(rvmax));
                    warn = 1;
                }
                else if ($('#post').length && !common.isDivisible($('#post_total_value').val(),commonData.postDlrPre)){
                    $('#post_total_value').addClass('error');
                    warn = 1;
                    this.divisibleByAlert("dlr",commonData.postDlrPre);
                }
                else if($('#post').length && (ptv < pvmin || ptv > pvmax)){
                    $('#post_total_value').addClass('error');
                    common.showErrMsg(translation.please_enter_valid_dollar_amount + '\n\n\t'+ translation.min +': $' + Math.ceil(pvmin) + '\n\t'+ translation.max +': $' + Math.floor(pvmax));
                    warn = 1;
                }else if(totalDollar > yearly_pay){
                    $('#post_total_value').addClass('error');
                    common.showErrMsg(translation.please_enter_valid_tota_deferral);
                    warn = 1;
                }
            }
            return warn;
        },
        divisibleByAlert : function(type,number)
        {
            common.showErrMsg(common.divisibleByMessage(number,type),"Oops");
        },

        maxWarning : function(catchup){
            var model = Section.getData('common'),
                total = $('#total_value').val(),
                translation = Section.getData('translation'),
                getPaid = $('#get_paid').val(),
                payroll = this.getPayroll(),
                limit_per_month = catchup ? model.limit_402_50_per_month : model.limit_402_per_month;

            var warn = 0;
            if((this.getMaxRound() * payroll[getPaid]) > (limit_per_month * 12)){
                common.showErrMsg(translation.max_warning);
            }
            return warn;
        },

        cprocess : function(catchup, catchupValue){
            var getPaid = $('#get_paid').val(),
                yearly_pay = common.dollar2float($('#yearly_pay').val()),
                salaryPct = $('#salary_total_pct').val(),
                salaryVal = parseFloat($('#salary_total_value').val()),
                rothPct = $('#roth_total_pct').val(),
                rothVal = parseFloat($('#roth_total_value').val()),
                postPct = $('#post_total_pct').val(),
                postVal = parseFloat($('#post_total_value').val()),
                totalVal = parseFloat($('#total_value').val()),
                totalValueMatching = Math.round($('#total_value_matching').val()),
                cmode = '';
            var translation = Section.getData('translation');
            var model = Section.getData('common')
            var mode = 'DEFERPCT';
            if($('#contribution_type_dlr').is(':checked')){
                mode = 'DEFERDLR';
            }else{
                if($('#contribution_type_pct').is(':checked')){
                    mode = 'DEFERPCT';
                }
            }

            if(catchup){
                cmode = 'DEFERPCT';
                if($('#catchup_type_dlr').is(':checked')){
                    cmode = 'DEFERDLR';
                }else if($('#catchup_type_pct').is(':checked')){
                    cmode = 'DEFERPCT';
                }else{
                    if($('#catchup_mode').val() == 1)
                        cmode = 'DEFERDLR';
                    else
                        cmode = 'DEFERPCT';
                }
            }
            var checkBoxCounter = 0;
            var checkBoxCounterUnchecked = 0;
            $( "#pretaxslidercheckbox,#rothslidercheckbox" ).each(function() {
                if ($(this).length)
                {
                    checkBoxCounter++;
                    if (!$(this).is(":checked") && $(this).attr('id') == "pretaxslidercheckbox")
                    {
                        salaryPct = null;
                        salaryVal = null;
                        checkBoxCounterUnchecked++;
                    }
                    if (!$(this).is(":checked") && $(this).attr('id') == "rothslidercheckbox")
                    {
                        rothPct = null;
                        rothVal = null;
                        checkBoxCounterUnchecked++;
                    }
                    Section.setItem($(this).attr('id'),$(this).is(":checked"));
                }
            });
            if (checkBoxCounter  && checkBoxCounter  == checkBoxCounterUnchecked)
            {
                common.showErrMsg(translation.check_one_contribution);
                return;
            }
            var sectionData = {
                contribution_mode : mode,
                catchup_mode      : cmode,
                C_CurrentYearlyIncome : yearly_pay,
                C_GetPaid : getPaid,
                C_PreTaxContributionPct : salaryPct,
                C_PreTaxContributionValue : salaryVal,
                C_RothContributionPct : rothPct,
                C_RothContributionValue : rothVal,
                C_PostTaxContributionPct : postPct,
                C_PostTaxContributionValue : postVal,
                C_MonthlyContributionTotal : totalVal,
                C_MonthlyContributionTotalEmployer : totalValueMatching,
                C_CatchUpContributionValue : catchupValue,
                contributionsStatus : 2,
                updateContributions : 1,
                updateCatchUp : catchup
            };

            Section.setData('contributions', sectionData);
            Section.setData('retirement_needs', {'RN_CurrentYearlyIncome' : yearly_pay});

            var pData = {
                'contributions' : Section.getData('contributions'),
                'common' : Section.getData('common')
            };

            data.saveContributions(pData).then(function(data){
                if ($("#ACAoptOut").val() == 0 && model.autoIncrease.enabled)
                {
                    $( "#contributions-main" ).hide();
                    $('.modal').modal('hide');
                    $.post( "/contributions/AutoIncrease", function( data )
                    {
                        $( "#contributions-ajax" ).html( data );
                    });
                }
                else if(parseInt(Section.getData('common').flexPlan)) {
                    $('body').trigger('spe:router:redirect', { to: 'investments', complete : 'contributions' });
                }else{
                    $('body').trigger('spe:router:redirect', { to: 'myProfile', complete : 'contributions' });
                }
                _gaq.push(['_trackEvent', 'Contributions', 'Save' ]);
            });
        },
        
        restoreDefaults: function() {
            var commonData = Section.getData('common');
            $('#salary_total_pct').val(commonData.preMax === commonData.preMin ? commonData.preMin : commonData.preCurrent).trigger('change');
            $('#roth_total_pct').val(commonData.rothMax === commonData.rothMin ? commonData.rothMin : commonData.rothCurrent).trigger('change');
            $('#post_total_pct').val(commonData.postMax === commonData.postMin ? commonData.postMin : commonData.postCurrent).trigger('change');
        },
        
        showACAModalOrProcess: function(catchup, catchupValue) {
            if($('#ACAoptOutNotice').length > 0 && $('#ACAoptOutNotice').val() == 1){
                $('#cArrangementModal').modal('show');
            }else{
                this.cprocess(catchup,catchupValue);
            }
        }
        
    };

    return {
        initData : contributionService.initData.bind(contributionService),
        calculateContribution : contributionService.calculateContribution.bind(contributionService),
        totalContribution : contributionService.totalContribution.bind(contributionService),
        initSlider : contributionService.initSlider.bind(contributionService),
        switchToPct : contributionService.switchToPct.bind(contributionService),
        switchToDlrWithOptOutWarning : contributionService.switchToDlrWithOptOutWarning.bind(contributionService),
        switchToDlr : contributionService.switchToDlr.bind(contributionService),
        totalPctSalary : contributionService.totalPctSalary.bind(contributionService),
        totalPctRoth : contributionService.totalPctRoth.bind(contributionService),
        initWidget : contributionService.initWidget.bind(contributionService),
        showACAMessage : contributionService.showACAMessage,
        saveCatchup : contributionService.saveCatchup.bind(contributionService),
        paycheckAmount : contributionService.paycheckAmount.bind(contributionService),
        csave : contributionService.csave.bind(contributionService),
        contributionArrangement : contributionService.contributionArrangement.bind(contributionService),
        totalPctPost: contributionService.totalPctPost.bind(contributionService),
        roundToNearestStep: contributionService.roundToNearestStep.bind(contributionService),
        cvalidate: contributionService.cvalidate,
        restoreDefaults: contributionService.restoreDefaults.bind(contributionService)
    }
});
