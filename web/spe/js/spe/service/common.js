define(['jquery', 'moment', 'spe/model/Section'],function($, moment, Section){

    function _setSpanishLocale() {
        $.datepicker.setDefaults({
            closeText: "Cerrar",
            prevText: "&#x3C;Ant",
            nextText: "Sig&#x3E;",
            currentText: "Hoy",
            monthNames: ["enero", "febrero", "marzo", "abril", "mayo", "junio",
                "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"],
            monthNamesShort: ["ene", "feb", "mar", "abr", "may", "jun",
                "jul", "ago", "sep", "oct", "nov", "dic"],
            dayNames: ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"],
            dayNamesShort: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],
            dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
            weekHeader: "Sm",
            dateFormat: "dd/mm/yy",
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ""
        });
    }

    $( document ).ready(function()
    {
        if (Section.getData('common').locale === "es") {
            _setSpanishLocale();
        }
    });
    var showConfirmMsg = function(msg, callback) {
        var translation = Section.getData('translation');
        var title = translation.confirm;
        bootbox.confirm({
            title: title,
            message: msg,
            callback: function(result){
                callback(result);
            }
		});
	};

    var showErrMsg = function(msg, customTitle) {
        var translation = Section.getData('translation');
        var title = translation.error;
        if(customTitle)
            title = customTitle;
        bootbox.dialog({
            title: title,
            message: msg,
            closeButton: false,
            size: 'medium',
            buttons : {
                ok: {
                    label: 'OK',
                    callback: function() {
                        this.modal('hide');
                    }
                }
            }
        });
    };

    var formatCurrency = function(n){
            if(!n){
                return 0;
            }
            n = n.toString().replace(/\$|\,/g, '');
            if (isNaN(n)) n = "0";
            sign = (n == (n = Math.abs(n)));
            n = Math.round(n).toString();
            for (var i = 0; i < Math.floor((n.length - (1 + i)) / 3); i++)
                n = n.substring(0, n.length - (4 * i + 3)) + ',' + n.substring(n.length - (4 * i + 3));
            return (((sign) ? '' : '-') + n);
        },
        dollar2float = function(str){
            //remove whitespace
            str = str.replace(/^\s*|\s*$/g, '');
            //remove all chars except '0-9' and '.'
            str = str.replace(/[^\d.]/g, '');
            return Math.round(parseFloat(str).toFixed(2));
        },
        validator = function(n){
            if(!n || isNaN(n)){
                return false;
            }
            return true;
        },
        hasError = function(msg){
            if(msg != ''){
                // alert(msg);
                showErrMsg(msg);
                return true;
            }
            return false;
        },
        isSet = function(data){
            if(typeof data !== typeof undefined && data !== ''){
                return true;
            }
            return false;
        },
        isEmail = function(email){
            var email_reg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if(!email_reg.test(email)){
                return false;
            }
            return true;
        },
        isPhone = function(phone){
            var phone_reg =  /^\d{3}-\d{3}-\d{4}/;
            if(!phone_reg.test(phone)){
                return false;
            }
            return true;
        },
        isZip = function(zip){
            var zip_reg = /(^\d{5}$)|(^\d{5}-\d{4}$)|(^\d{9}$)/;
            if(!zip_reg.test(zip)){
                return false;
            }
            return true;
        },
        isSsn = function(ssn){
            // 1 - 9
            if(/98765432\d{1}/g.test(ssn) || /123456789/g.test(ssn)){

                return false;
            }

            //consecutive 00's
            if(/000\d{2}\d{4}$/g.test(ssn) || /^\d{3}\d{2}0000/g.test(ssn) || /^\d{3}00\d{4}$/.test(ssn)){

                return false;
            }

            // All same numbers
            if (/^(.)\1+$/g.test(ssn)) {

                return false;
            }

            //illegal first three
            if(/666\d{2}\d{4}$/.test(ssn) || /899\d{2}\d{4}$/.test(ssn)){

                return false;
            }

            if (ssn.length < 9 || ssn.length === 0) {

                return false;
            }

            return true;
        },
        nl2br = function(str){
            var breakTag = '<br>';
            return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
        },
        isUrl = function(s) {
            var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
            return regexp.test(s);
        },
        htmlEntities = function(str) {
            return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        },
        isValidDob = function (dob) {
            if(moment(dob,"MM/DD/YYYY").year() < 1900 || moment(dob,"MM/DD/YYYY").isAfter(moment().format('MM/DD/YYYY')) || !moment(dob,"MM/DD/YYYY").isValid()){
                return false;
            }
            return true;
        },
        isDivisible = function (number,check) {
            var value = number/check;
            if (value== parseInt(value))
                return true;
            return false;
        },
        divisibleByMessage = function (number,type) {
            var translation = Section.getData('translation');
            if (type == "pct")
            return translation.divisible_by_percent.replace("%number%", number);
            if (type == "dlr")
            return translation.divisible_by_amount.replace("%number%", number);
        },
        iagreecheckbox = function(template){
            var checkbox = $(template).find(".check_proceed");
            if (checkbox.length)
            {
                 var identifier = $(template).find(".check_buttonidentifier").val();
                 var button = $(identifier);
                 var buttonid = null;
                 var isClass = false;
                 if (identifier.charAt(0) == ".")
                 {
                    isClass = true;
                    button.removeClass(identifier.substr(1,identifier.length));
                 }
                 else
                 {
                    buttonid = button.attr("id");
                    button.attr("id",'');
                 }
                 button.click(function()
                 {
                    if ($(template).find(".check_proceed").is(":checked"))
                    {
                        if (isClass)
                        {
                            button.addClass(identifier.substr(1,identifier.length));
                        }
                        else
                        {
                            button.attr("id",buttonid);
                        }
                    }
                 });
            }
        }

    return{
        formatCurrency : formatCurrency,
        dollar2float : dollar2float,
        validator : validator,
        hasError : hasError,
        isSet : isSet,
        isEmail : isEmail,
        isPhone : isPhone,
        isZip : isZip,
        isSsn : isSsn,
        nl2br : nl2br,
        isUrl : isUrl,
        htmlEntities: htmlEntities,
        isValidDob : isValidDob,
        showConfirmMsg: showConfirmMsg,
        showErrMsg: showErrMsg,
        isDivisible: isDivisible,
        divisibleByMessage: divisibleByMessage,
        iagreecheckbox: iagreecheckbox
    }
});


