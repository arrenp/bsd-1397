define(['jquery','jquery.ui','spe/service/common','spe/service/data','spe/model/Section','spe/view/PlayerView'], function($, ui, common, service, Section,playerView) {
    "use strict";
    var atBlueprintService = {
        init : function(){
        },
        playAudio : function() {
            var audios = $.trim($('.audio-area').html());
            if (audios) {
                playerView.render('.audio-area', _.extend(JSON.parse(localStorage.getItem('mediaPath')), {'playlist': audios.split('|')}), 'audio');
            }
        },
    }

    return {
        playAudio : atBlueprintService.playAudio

    }

});
