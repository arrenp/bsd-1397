define(['jquery', 'underscore', 'spe/config', 'spe/model/Section'], function ($, _, config, Section) {
    "use strict";

    var playList = function (data) {
        var extension = [{'path': 'm4v', 'ext': 'mp4'}, {'path': 'webmv', 'ext': 'webm'}, {'path': 'ogv', 'ext': 'ogv'}, {'path': '', 'ext': 'flv'}],
                list = _.map(data.playlist, function (item) {
                    var path = data.dir ? data.library_path + data.dir + "/" : (data.popup ? data.popup_path : data.video_path);
                    return {
                        'image': config.playerPoster,
                        'title': item,
                        'sources': buildSource(item, path, extension),
                        'tracks': Section.getData('common').locale.split("_").pop() == 'en' ? buildTracks(item, path) : []
                    };
                });
        return list;
    },
            buildSource = function (fileName, basePath, extension) {
                return  _.map(extension, function (info, index) {
                    return {
                        'default': index == 0 ? true : false,
                        'file': basePath + info.path + "/" + fileName + "." + info.ext
                    }
                });
            },
            buildTracks = function (item, path) {
                var Languages = {'en': 'English'};
                var tracks = [];
                $.each(Languages, function (index, label) {
                    tracks.push({'file': path + "vtt/" + item + "_" + index + ".vtt", 'label': label, 'kind': 'captions', 'default': false})
                });
                return tracks
            },
            getTitle = function () {
                return $(document).find("title").text() + ' - Video';
            },
            getVideoConfig = function (data) {
                var info = {type: data.type, controller: data.controller};
                var markCompleted = ['welcome', 'planBasics'];
                return  _.extend(config.player, {
                    'aspectratio': "16:9",
                    playlist: playList(data),
                    events: {
                        onPlay: function () {
                            if (data.type == 'warning') {
                                //$.publish("spe:section:update", info); // update all section in localStorage
                                $('body').trigger("spe:section:update", info);
                            }
                        },
                        onPlaylistItem: function () {
                            var $event = $('body');
                            $event.trigger('spe:disclosure:hide');
                            $event.trigger('spe:disclosure:show', {video: true, title: this.getPlaylistItem().title});
                        },
                        onPlaylistComplete: function () {
                            // if we have callback then call it return silently
                            if (data.callback) {
                                data.callback();
                                return '';
                            }

                            if (info.type == "normal")
                            {
                                
                                _gaq.push(['_setCustomVar',
                                    3,
                                    'completed_sections',
                                    info.controller,
                                    3
                                ]);
                            }

                            if (_.contains(markCompleted, data.controller)) {
                                $('body').trigger("spe:router:change", data.controller)
                            } else {
                                $.publish("spe:" + data.controller + ":complete", info)
                            }
                        }
                    }
                });
            },
            updateState = function (obj, state) {
                $("a.audioPanel[rel='" + obj.getPlaylistIndex() + "']").find('.audio-state').removeClass().addClass('audio-state audio-' + state);

            },
            getAudioConfig = function (data) {
                var extension = [{'path': '', 'ext': 'mp3'}],
                        playlist = _.map(data.playlist, function (item) {
                            return {
                                'sources': buildSource(item, data.audio_path, extension),
                                'tracks': []
                            };
                        });

                return  _.extend(config.player, {
                    playlist: playlist,
                    events: {
                        onPlay: function () {
                            $("a.audioPanel .audio-state").removeClass().addClass('audio-state audio-play');
                            updateState(this, 'pause');
                        },
                        onPause: function () {
                            updateState(this, 'play');
                        },
                        onBuffer: function () {
                            updateState(this, 'loading');
                        },
                        onComplete: function () {
                            updateState(this, 'play');
                            this.stop();
                        }

                    }
                });
            },
            buildConfig = function (data, type) {
                return type == 'audio' ? getAudioConfig(data) : getVideoConfig(data);
            };

    return {
        buildConfig: buildConfig
    }
});