<?php

require_once('pdfBase.php');

class pdfClass 
{
    public $pdf;
    public function __construct()
    {
       $noImage = "http://" . $_SERVER["HTTP_HOST"] . "/" . "images/noImage.png";

       $this->pdf = new pdfBase(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, false, $noImage, $noImage, '', '', '');


       $this->pdf->SetCreator(PDF_CREATOR);

       $this->pdf->SetAuthor('vWise');

       $this->pdf->SetTitle('Campagin Stats');

       $this->pdf->SetSubject('Stats Summary');

       $this->pdf->SetKeywords('Email, UI/UX, Google Stats');

       $this->pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));

       $this->pdf->setFooterData(array(0,64,0), array(0,64,128));

       $this->pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

       $this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

       // set default monospaced font

       $this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

       // set margins
       $this->pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

       $this->pdf->SetHeaderMargin(PDF_MARGIN_HEADER);

       $this->pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

       // set auto page breaks
       $this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

       // set image scale factor
       $this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

       // ---------------------------------------------------------

       // set default font subsetting mode
       $this->pdf->setFontSubsetting(true);

       // Set font
       // dejavusans is a UTF-8 Unicode font, if you only need to
       // print standard ASCII chars, you can use core fonts like
       // helvetica or times to reduce file size.

       $this->pdf->SetFont('dejavusans', '', 14, '', true);

       // Add a page
       // This method has several options, check the source code documentation for more information.

       $this->pdf->AddPage();

       // set text shadow effect
       $this->pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));



    }
    public function write($html,$filename)
    {
       $this->pdf->writeHTMLCell(0, 0, '', 0, $html, 0, 1, 0, true, '', true);
       $this->pdf->Output($filename.'pdf', 'D');
    }

}