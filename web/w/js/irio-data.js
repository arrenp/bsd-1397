function UpdateQueryString(key, value, url) {
    if (!url) url = window.location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
        hash;

    if (re.test(url)) {
        if (typeof value !== 'undefined' && value !== null)
            return url.replace(re, '$1' + key + "=" + value + '$2$3');
        else {
            hash = url.split('#');
            url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
            if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                url += '#' + hash[1];
            return url;
        }
    }
    else {
        if (typeof value !== 'undefined' && value !== null) {
            var separator = url.indexOf('?') !== -1 ? '&' : '?';
            hash = url.split('#');
            url = hash[0] + separator + key + '=' + value;
            if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                url += '#' + hash[1];
            return url;
        }
        else
            return url;
    }
}

$(document).ready(function () {
    $("#salaryForm").submit(function (event) {
        event.preventDefault();
        $('p.input-error-message').hide();
        $('input[name=salary]').removeClass('input-error');
        $('p.input-error-message').html('');

        var querystring = $("#salaryForm").serialize();
        var page = "/widgets/ajax/saveSalary";

        $.post(page, querystring).done(function (data) {
            var object = JSON.parse(data);

            if (object.success) {
                $('#salary-confirmation').hide();
                sendGoogleAnalyticsRequest();
                var object = JSON.parse(data);
                $("#irioOptions").css("paddingBottom","0px");
                var optionsTotal = 0;
                for (var j = 1; j <= 3; j++ )
                {
                    $("#irioOption" + j).toggle(object['showIncrease' + j]);
                    optionsTotal = optionsTotal + object['showIncrease' + j];
                }
                if (optionsTotal <= 1)
                {
                    $("#irioOptions").css("paddingBottom","100px");
                }
                $.each(object, function (key, value) {
                    var obj = $('.data_' + key);

                    obj.html(value);
                });
                if (object.maxOut || object.overLimit402g) {
                    $('#max-reached').fadeIn(transitionSpeed);
                }
                else {
                    $('#analysis').fadeIn(transitionSpeed);
                }
            }
            else {
                $('input[name=salary]').addClass('input-error');
                $('p.input-error-message').show();
                $('p.input-error-message').html(object.message);
            }
        });
    });

    var delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    $('input.details-input').keyup(function () {
        delay(function () {
            var querystring = $('#details-form').serialize();
            var page = "/widgets/ajax/validateContribution";

            $.post(page, querystring).done(function (data) {
                var object = JSON.parse(data);

                if (object.success) {
                    var object = JSON.parse(data);

                    $.each(object, function (key, value) {
                        var obj = $('.data_' + key);
                        obj.html(value);
                    });
                }
                else {
                    alert(object.message);
                }
            });

        }, 1000);
    });

    $("#details-form").submit(function (event) {
        event.preventDefault();
    });

    $('.increase-monthly').click(function () {
        var value = $(this).val();
        var params = $('#details-form').serialize() + '&contribution=' + value;
        var page = "/widgets/ajax/validateContribution";

        $.post(page, params).done(function (data) {
            var object = JSON.parse(data);

            if (object.success) {
                $('#analysis').hide();
                $('#congrats').fadeIn(transitionSpeed);
                //$('#cta').fadeIn(transitionSpeed);
                var object = JSON.parse(data);

                $.each(object, function (key, value) {
                    var obj = $('.data_' + key);
                    obj.html(value);
                });

            }
            else {
                alert(object.message);
            }
        });
    });

    $(".emailTemplateForm").submit(function (event) {
        event.preventDefault();
        //emailTemplate($(this));
        var querystring = $(this).serialize();
        var page = "/widgets/ajax/emailTemplate/";

        $.post(page, querystring).done(function (data) {
            var obj = $.parseJSON(data);

            if (obj.success) {
                $('#cta').hide();
                $('#success').hide();
                $('#thank-you').fadeIn(transitionSpeed);
                setTimeout(function () {
                    closeWidget()
                }, 2000);
            }
            else {
                alert(obj.message);
            }
        });
    });

    $('input.currency').autoNumeric('init', {lZero: 'deny', mDec: 0, aPad: false, vMin: 0});
    $('input.age').autoNumeric('init', {lZero: 'deny', mDec: 0, aPad: false, aSep: '', vMin: 0, vMax: 120});
});
