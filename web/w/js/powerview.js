function initVideoControls(){

	var controls = $('.video-controls');
	var player = $('video');
	var video = player[0];
	var playButton = controls.find('.play');
	var pauseButton = controls.find('.pause');
	var seekBar = controls.find('input[name=seek]');
	var progressBar = controls.find('.progress-bar > span');
	var indicator = controls.find('.indicator');

	if (player.prop('autoplay')) {
		playButton.hide();
		pauseButton.show();
	} else {
		playButton.show();
		pauseButton.hide();
	}

	playButton.on('click',function(){
    if (!video.ended) {
    	video.play();
	    playButton.hide();
	    pauseButton.show();
	   }
	});

	pauseButton.on('click',function(){
    video.pause();
    playButton.show();
    pauseButton.hide();
	});

	seekBar.on('mousedown',function(){
		video.pause();
		playButton.show();
		pauseButton.hide();
	});

	seekBar.on('change',function(){
		var time = video.duration * (seekBar.val() / 100);
		video.currentTime = time;
		if (video.paused) {
			$('section, article').each(function(){
				resetSlide($(this));
			});
			$('.fill').each(function(){
				resetFill($(this));
			});
			$('.odometer').each(function(){
				resetOdo($(this));
			});
		}
	});

	/* Update the time indicator values */
	function setIndicator(current, duration){
		var durationMinute 		= Math.floor(duration / 60);
		var durationSecond 		= Math.floor(duration - durationMinute * 60);
		var durationLabel 		= durationMinute + ":" + durationSecond;
		currentSecond 			= Math.floor(current);
		currentMinute 			= Math.floor(currentSecond / 60);
		currentSecond 			= currentSecond - ( currentMinute * 60 );
		currentSecond 			= ( String(currentSecond).length > 1 ) ? currentSecond : ( String("0") + currentSecond );
		var currentLabel 		= currentMinute + ":" + currentSecond;
		var indicatorLabel 		= currentLabel + " / " + durationLabel;
		indicator.text( indicatorLabel );
	}

	video.addEventListener('timeupdate',function(){
		var value = (100 / video.duration) * video.currentTime;
		seekBar.val(value);
		progressBar.width(value+'%');
		setIndicator(video.currentTime, video.duration);
	});

	video.addEventListener('ended', function(){
		pauseButton.fadeOut();
		playButton.fadeOut();
		$('.container').addClass('hold');
	});



}


function resetSlide(elem){

	var player = $('video');
	var video = player[0];

	var timeIn = elem.data('time-in');
	var timeOut = elem.data('time-out');
	var incoming = elem.data('incoming-remove');
	var outgoing = elem.data('outgoing-add');

	elem.addClass('no-transition');

	if (video.currentTime <= timeIn || video.currentTime > timeOut) { // slide is out
		elem.removeClass(outgoing);
		elem.addClass(incoming);
	} else if (video.currentTime >= timeIn && video.currentTime < timeOut) { // slide is in
		elem.removeClass(incoming);
	}

}



function resetFill(elem){

	var player = $('video');
	var video = player[0];

	var timeIn = elem.data('time-fill');
	var startWidth = elem.data('start-width');
	var finishWidth = elem.data('finish-width');

	elem.addClass('no-transition');

	if (video.currentTime < timeIn) { // bar is empty
		elem.css('width',startWidth);
	} else if (video.currentTime >= timeIn) { // bar is full
		elem.css('width',finishWidth);
	}

}



function resetOdo(elem){

	var player = $('video');
	var video = player[0];

	var timeIn = elem.data('odo-time');
	var start = String(elem.data('odo-start'));
	var finish = String(elem.data('odo-finish'));
	var startNum = parseInt(start.replace(/,/g,''));
	var finishNum = parseInt(finish.replace(/,/g,''));

	elem.removeProp('number');

	if (video.currentTime < timeIn) {
		elem.text(start);
	} else {
		elem.text(finish);
	}

}




$(document).ready(function(){
    var timerId = setInterval(function() {
            var page = "/widgets/ajax/ping";
            $.ajax({
                method: "GET",
                url: page,
                data: "json"
                })
            .done(function( data ) {
                var obj = $.parseJSON(data);
                if(obj.success !== true) {
                    //alert('Please refresh your browser');
                    clearInterval(timerId);
                } else {
                    //console.log('Still good');
                }
            });
        }, 300000);


	initVideoControls();

	var controls = $('.video-controls');
	var player = $('video');
	var video = player[0];
	var playButton = controls.find('.play');
	var pauseButton = controls.find('.pause');
	var seekBar = controls.find('input[name=seek]');
	var progressBar = controls.find('progress');
	var indicator = controls.find('.indicator');


	/* Hide Controls & Cursor if no movement */

	var idleMouseTimer = null;
	var forceHide = false;

	$('.container').mousemove(function() {

		if(!forceHide && $('#salary-confirmation').css('display') == 'none') {

			$(".container").css('cursor', '');
			clearTimeout(idleMouseTimer);
			$(".video-controls").addClass('show');

			idleMouseTimer = setTimeout(function() {
				$(".container").css('cursor', 'none');
				$(".video-controls").removeClass('show');
				forceHide = true;
				setTimeout(function() {
					forceHide = false;
				}, 1000);
			}, 3000); // set delay time here

		}

	});


	video.addEventListener('timeupdate',function(){

		if (!video.paused) {

			/* Transition Framework */

			$('.animate').each(function(){
				$(this).removeClass('no-transition');
				var timeIn = $(this).data('time-in');
				var timeOut = $(this).data('time-out');
				var incoming = $(this).data('incoming-remove');
				var outgoing = $(this).data('outgoing-add');
				if (video.currentTime >= timeIn && video.currentTime < timeOut) {
					$(this).removeClass(incoming);
				} else if (video.currentTime > timeOut) {
					$(this).addClass(outgoing);
				}
			});

			$('.fill').each(function(){
				$(this).removeClass('no-transition');
				var timeIn = $(this).data('time-fill');
				var finishWidth = $(this).data('finish-width');
				if (video.currentTime > timeIn) {
					$(this).css('width',finishWidth);
				}
			});

			$('.odometer').each(function(){
				var timeIn = $(this).data('odo-time');
				var start = String($(this).data('odo-start'));
				var finish = String($(this).data('odo-finish'));
				var startNum = parseInt(start.replace(/,/g,''));
				var finishNum = parseInt(finish.replace(/,/g,''));
				var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',');
				if (video.currentTime > timeIn && video.currentTime < timeIn + 1) {
					$(this).animateNumber({
	    			number: finishNum,
	    			numberStep: comma_separator_number_step
	    		},1000);
	    	}
			});

		}

	});




	/* 
$(document).on('click', '#confirm-salary', function(){
		$('.container').removeClass('hold');
		$('#salary-confirmation').fadeOut();
		//$('.video-controls').hide();
		video.play();
		pauseButton.show();
		playButton.hide();
	});
*/

/* Language Selector */

  var $LS = $('#languageSelect');
  var $LSopt = $LS.find('.options li');


  var openLS = function() {
    $LS.addClass('open');
    $LS.find('.menu .options').slideDown();
  };

  var closeLS = function() {
    $LS.removeClass('open');
    $LS.find('.menu .options').slideUp();
  };

	$LSopt.click(function(){
    $LS.find('.selected').removeClass('selected');
    $(this).addClass('selected');
    var currentLanguage = $(this).text();
    $('.currentLanguage').html( currentLanguage + ' <span></span>');
  });

  $LS.click(function(){
    if ( $LS.hasClass('open') ) {
		var lang = $('.selected').data('language');
		window.location = UpdateQueryString('lang', lang);
		console.log(lang);
      closeLS();
    } else {
      openLS();
      setTimeout(closeLS, 10000); // auto close after 10 seconds
    }
  });

  $('.container, small, .header').click(function(){
    if ( $LS.hasClass('open') ) {
      closeLS(); // close if user clicks elsewhere in the form\
    }
  });



	/* END Language Selector */








	$(document).on('click', '.return', function(){
		$('#vwise_powerview').fadeOut();
		$('video')[0].pause();
                redirect(false);
	});

	$(document).on('click', '.refresh', function(){
		location.reload();
	});

	$(window).keyup(function(e){
		if (e.keyCode == 27) { // esc key
			$('#vwise_powerview').fadeOut();
			$('video')[0].pause();
		}
	});

	$(window).keydown(function(e){
		if (e.keyCode == 32) { // spacebar
			e.preventDefault();
			if (video.paused && !video.ended) {
				video.play();
				pauseButton.show();
				playButton.hide();
			} else {
				video.pause();
				playButton.show();
				pauseButton.hide();
			}
		}
	});

	seekBar.keydown(function(e){
		if (e.keyCode == 37 || e.keyCode == 39) { // right & left arrow keys
			e.preventDefault();
		}
	});


	$(document).on('focus','input[type=text],input[type=email]',function(){
		$(this).select();
	});

	});
