function emailTemplate(form)
{
    var querystring  =  $(form).serialize();
    var page = "/widgets/ajax/emailTemplate/";
    $.post(page, querystring).done(function(data)
    {
        
    });
}

function redirect(success)
{
    if (success) {
		var page = "/widgets/ajax/saveRedirect";
		$.post(page).done(function(data)
		{
				$("#redirectForm").submit();
		});
	}
	else {
		$("#loginForm").submit();
	}
}

function startWidget() {
	var parent = window.parent;
	if (parent && parent.postMessage) {
		parent.postMessage("widget-started", '*');
	}
}

function closeWidget() {
	var page = "/widgets/ajax/closeSession";
	$.post(page).done(function(data) {
		var parent = window.parent;
		if (parent && parent.postMessage) {
                    parent.postMessage("widget-closed", '*');
		}
	});

	return false;
}

function irioFigure(figure){
    //alert('triggered');
    
    var parent = window.parent;
    if (parent && parent.postMessage) {
        parent.postMessage(figure, '*');
    } 
    
}
function UpdateQueryString(key, value, url) {
        if (!url) url = window.location.href;
        var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
            hash;

        if (re.test(url)) {
            if (typeof value !== 'undefined' && value !== null)
                return url.replace(re, '$1' + key + "=" + value + '$2$3');
            else {
                hash = url.split('#');
                url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
                if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                    url += '#' + hash[1];
                return url;
            }
        }
        else {
            if (typeof value !== 'undefined' && value !== null) {
                var separator = url.indexOf('?') !== -1 ? '&' : '?';
                hash = url.split('#');
                url = hash[0] + separator + key + '=' + value;
                if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                    url += '#' + hash[1];
                return url;
            }
            else
                return url;
        }
}