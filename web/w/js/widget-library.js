(function () {
	var scriptName    = 'widget-library.js';
	var jqueryPath    = 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.1/jquery.min.js';
	var jqueryVersion = '1.12.1';
	var jQuery;
	var scriptTag;
	var domainName;
	var counter = 0;
        var widgetids = new Object();     
        widgetids.sections = ["vwise_irio","vwise_nationalsurvey","vwise_survey","vwise_videolibrary"];
        if (document.getElementById('widgetProcessed') === null)
        init();       
	function init() {
                document.body.innerHTML += '<div id ="widgetProcessed" style ="display:none"></div>';

		var allScripts = document.getElementsByTagName('script');
		var targetScripts = [];
		for (var i in allScripts) {
		var name = allScripts[i].src;
		if(name && name.indexOf(scriptName) > 0)
			targetScripts.push(allScripts[i]);
		}
		scriptTag = targetScripts[targetScripts.length - 1];
		domainName = getHostname(scriptTag.src);

		if (window.jQuery === undefined || window.jQuery.fn.jquery !== jqueryVersion) {
                    loadScript(jqueryPath, initjQuery);
		} else {
                    initjQuery();
		}
                //console.log(widgetids);
                //alert('loaded');
	}

	function initjQuery() {
		jQuery = window.jQuery.noConflict(true);
		main();
	}

	function main() {
		loadCss(domainName + '/w/css/widget-library.css', runWidgets);
	}
        
        function runWidgets()
        {
            var counter = 0;
            for (var i = 0;i <  widgetids.sections.length;i++)
            {
                if (document.getElementById(widgetids.sections[i]) != null )
                {
                    scriptTag = document.getElementById(widgetids.sections[i]);
                    //alert(scriptTag.src);
                    counter++;
                    widgetAction();
                }
            }
            if (counter == 0)
            widgetAction();
        }
	function widgetAction() {
		var transitionSpeed = 300;
		/* TODO: Check if need to validate below variables and do proper error handling */
		var session_id = getParameterByName('sid');
		var planid = getParameterByName('planid');
		var partner_id = getParameterByName('partner_id');
                var testing = getParameterByName('testing');
                if (planid === null || planid === "")
                planid = getParameterByName('plan_id');
                if (planid === null || planid === "")
                planid = getParameterByName('id');
                if (partner_id === null || partner_id === "")
                partner_id = getParameterByName('partnerid');
                if (testing === null || !testing)
                testing = "";            
                

                
		var type = getParameterByName('type');
                var compareid = "widget-library-container-" + type;
                var containerid = "widget-library-container";
                if (document.getElementById(compareid) !== null)
                containerid = compareid;

		var str = jQuery.param({sid: session_id, planid: planid, partner_id: partner_id, type: type, testing: testing});
               
		if (type == 'powerview') {
			//var html = '<div class="wl-veil"><div class="wl-outer"><div class="wl-inner"><div class="wl-modal"></div></div></div></div>';
			var html = '<div class="wl-veil" style="z-index:-1"><div class="wl-outer"><div class="wl-inner"><div class="wl-iframe-container"></div></div></div></div>';
			jQuery('#widget-library-container').html(html);

			jQuery('.wl-veil').fadeIn(transitionSpeed);
			//jQuery('body').addClass('no-scroll');

			var iframe = '<iframe allowfullscreen frameBorder="0" id="wl-frame" src="' + domainName + '/widgets/?' + str + '" width="100%" height="100%"></iframe>';
			//jQuery('.wl-modal').html(iframe);
			jQuery('.wl-iframe-container').html(iframe);
		}
		else if (type == 'irio') {
			var zindex = 5000;
			var html = '<div class="widgets"><div class="vwise data"><div class="overlay"><div class="copy"><div id="spinner" style="position:relative;"><div class="wl-spinner"><img src="' + domainName + '/w/img/loading.svg" alt="One moment please ..." style="position: relative; top: 50%; transform: translateY(-50%);"/></div></div></div></div></div></div>';
			html += '<div class="wl-veil" id ="irio-viel" style="z-index:' + zindex + '"><div class="wl-outer"><div class="wl-inner"><div class="wl-iframe-container" id="' + containerid + '-iframe" style="display:none;"></div></div></div></div>';
			jQuery('#' + containerid).html(html);
                        var iframe = '<iframe allowfullscreen frameBorder="0" allowtransparency="true" id="wl-frame" src="' + domainName + '/widgets/?' + str + '" width="100%" height="100%"></iframe>';
                        jQuery('#' + containerid + '-iframe').html(iframe);                        

			jQuery(document).on('click', '#wl-button-irio', function(e) {
                           
				e.preventDefault();

				jQuery('#irio-viel').fadeIn(transitionSpeed);
				jQuery('#' + containerid + '-iframe').show();

				if (!jQuery('#wl-frame').length) {
					jQuery('#' + containerid + '-iframe').html(iframe);
				}
                                
                                
			});

			doPollIRIO();
		}
        else if (type == 'survey') {
			var html = '<div class="widgets"><a href="javascript:void(0);" id="wl-button-survey"><img src="' + domainName + '/w/img/SP_launch_HM_180x75.png"/></a></div>';
			html += '<div class="wl-veil" id ="wl-veil-survey" style="z-index:10000;"><div class="wl-outer"><div class="wl-inner"><div class="wl-iframe-container" id ="wl-iframe-container-survey"></div></div></div></div>';

			jQuery('#' + containerid).html(html);

			jQuery('#wl-button-survey').click(function(e){
				e.preventDefault();

				jQuery('#wl-veil-survey').fadeIn(transitionSpeed);
				//jQuery('body').addClass('no-scroll');

				var iframe = '<iframe allowfullscreen frameBorder="0" allowtransparency="true" id="wl-frame" src="' + domainName + '/widgets/?' + str + '" width="100%" height="100%"></iframe>';
				jQuery('#wl-iframe-container-survey').html(iframe);
			});
		} 
                else if (type == 'nationalsurvey') {
			var html = '<div class="widgets"><a href="javascript:void(0);" id="wl-button-nationalsurvey"><img src="' + domainName + '/w/img/SP_launch_HM_180x75.png"/></a></div>';
			html += '<div class="wl-veil" id ="wl-veil-nationalsurvey" style="z-index:10000;"><div class="wl-outer"><div class="wl-inner"><div class="wl-iframe-container" id ="wl-iframe-container-nationalsurvey"></div></div></div></div>';

			jQuery('#' + containerid).html(html);

			jQuery('#wl-button-nationalsurvey').click(function(e){
				e.preventDefault();

				jQuery('#wl-veil-nationalsurvey').fadeIn(transitionSpeed);
				//jQuery('body').addClass('no-scroll');

				var iframe = '<iframe allowfullscreen frameBorder="0" allowtransparency="true" id="wl-frame" src="' + domainName + '/widgets/?' + str + '" width="100%" height="100%"></iframe>';
				jQuery('#wl-iframe-container-nationalsurvey').html(iframe);
			});

                        }                 
                else if (type == 'videolibrary') {

                    var html  = '<div class="vwise" style="background: #fff; max-width:500px; min-height:300px; border: 2px solid #ccc; position: relative; display:block; border-radius: 3px">';
		        html += '<header style="text-align: left; height: auto; line-height: 24px; background-color: #ccc; font-size: 12px; font-weight: bold; font-family: Arial; padding-left: 5px; width: 100%;">Retirement Help</header>';
		        html += '<div class="gradient" style="background-image: radial-gradient(55% 60%, #108ACA -10%, #FFFFFF 80%); position: absolute; bottom: 0px; right: 20px; width: 55%; height: 90%;"></div>';
		        html += '<div class="actors"><img src="/w/img/actors.png" style=" position: absolute; bottom: 0; right: 10px; width: 50%; height: 85%;"></div>';
		        html += '<div class="cta" style="position: absolute; top: 0; left: 0; height: 100%; width: 100%; display: table;"><div class="inner" style="padding-left: 25px; text-align: left; display: table-cell; vertical-align: middle; height: 100%; width: 100%;">';
			html +=	'<div class="copy" style="width: 200px; margin-top: 20%;">';
			html +=	'<h2 style="font-family: Lato, sans-serif; font-weight: 300; margin-bottom: 0; font-size: 24px;">Helpful Videos</h2>';
			html +=	'<p style="font-weight: 300; font-size: 12px; margin: 5px auto;">Learn more about your benefits through our informative video library.</p>';
			html +=	'<button id="wl-button-videolibrary" style="background: #019EE3; border: 1px solid rgba(0,0,0,0.40); border-radius: 4px; color: #fff; line-height: 40px; padding: 0 25px; text-transform: uppercase; font-weight: bold; letter-spacing: 1.1px; font-size: 12px; margin-top: 10px; cursor: pointer;">Watch Now</button>';
			html +=	'</div></div></div></div>';
                        html +=	'<div class="wl-library-container" id ="videolibraryContainer"></div>';
                        jQuery('#' + containerid).html(html);
                        jQuery('#wl-button-videolibrary').click(function(e){

                            e.preventDefault();
                            //jQuery('.wl-veil').fadeIn(transitionSpeed);
                            //jQuery('body').addClass('no-scroll');

                            var iframe = '<iframe allowfullscreen frameBorder="0" allowtransparency="true" id="wl-frame" src="' + domainName + '/widgets/?' + str + '" width="100%" height="100%"></iframe>';
                            jQuery('#videolibraryContainer').html(iframe);

                        });

                }
	}


	function getHostname(url) {
		var m = url.match(/^http.?:\/\/[^/]+/);
		return m ? m[0] : null;
	}

	function getParameterByName(name) {
		var url = scriptTag.src;
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}

	function loadCss(href, onLoad) {
		var link_tag = document.createElement('link');
		link_tag.setAttribute("type", "text/css");
		link_tag.setAttribute("rel", "stylesheet");
		link_tag.setAttribute("href", href);

		if (link_tag.readyState) {
			link_tag.onreadystatechange = function () {
				if (this.readyState == 'complete' || this.readyState == 'loaded') {
					onLoad();
				}
			};
		} else {
			link_tag.onload = onLoad;
		}
		(document.getElementsByTagName("head")[0] || document.documentElement).appendChild(link_tag);
	}

	function loadScript(src, onLoad) {
		/* TODO: Check if we need to detect if script already exist and only load if necessary? */
		var script_tag = document.createElement('script');
		script_tag.setAttribute("type", "text/javascript");
		script_tag.setAttribute("src", src);

		if (script_tag.readyState) {
			script_tag.onreadystatechange = function () {
                            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                                    onLoad();
                            }
			};
		} else {
                    script_tag.onload = onLoad;
		}
		(document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
	}

	window.addEventListener('message', receiveMessage, false);

    var figure;
    var age;
    var retireAge;
    function receiveMessage (event) {
        if (typeof event.data === "object") {
            age = event.data.age;
            retireAge = event.data.retireAge;
            figure = event.data.figure;
        } else if (event.data === "widget-started") {
            if(window.location !== window.parent.location) {
                jQuery('body').addClass('wl-no-scroll');  
            }
        } else if (event.data === "widget-closed") {
            jQuery('body').removeClass('wl-no-scroll');
            jQuery('.wl-veil').hide();
            jQuery('#wl-frame').remove();
        }
    }       
            
    function doPollIRIO() {
        var compareid = "widget-library-container-irio";
        var containerid = "widget-library-container";
        if (document.getElementById(compareid) !== null)
        containerid = compareid;            
        counter++;
        if(typeof figure === 'undefined' && counter < 20){
            setTimeout(function(){
                doPollIRIO();
            }, 1000);
        }else if((counter >= 20 && typeof figure === 'undefined') || (typeof age !== 'undefined' && typeof retireAge !== 'undefined' && age >= retireAge) ){
            html = '<div class="vwise"><div class="overlay"><div class="copy"><h2>Will it be enough?</h2><h5>How much will you have in retirement?</h5><button id ="wl-button-irio" class="vwise-button bg-primary">Find Out</button></div></div></div>';
            jQuery('#' + containerid + ' .widgets').html(html);                    
        }else{
            html = '<div class="vwise data"><div class="overlay"><div class="copy"><h5>Looks like your total projected balance at retirement is</h5><div class="figure"><sup>$ </sup>' + figure + '</div><button id ="wl-button-irio" class="vwise-button bg-primary">Is that Enough?</button></div></div></div>';
            jQuery('#' + containerid + ' .widgets').html(html);               
        }
            
	}        
})();
