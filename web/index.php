<?php
use Symfony\Component\HttpFoundation\Request;
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
$loader = require_once __DIR__.'/../app/bootstrap.php.cache';
require_once __DIR__.'/../app/AppKernel.php';
umask(0000);
$mode = 'prod';

if($mode === 'prod') {
    $key = '2011';
    if ((!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') || strpos($_SERVER['REQUEST_URI'],'_profiler') !== false ) {
        session_start();
        if (isset($_SESSION['_sf2_attributes']['REQUEST']['debug']) && $_SESSION['_sf2_attributes']['REQUEST']['debug'] == $key) {
            $mode = 'dev';
        }
    } elseif (isset($_GET['debug']) && ($_GET['debug'] == $key)) {
        $mode = 'dev';
    }
}
$errorReporting = true;
if ($mode == "prod")
{
    // $errorReporting = false;
    error_reporting(0);
}
$kernel = new AppKernel($mode, $errorReporting);
#$kernel = new AppKernel('dev',true);
$kernel->loadClassCache();

$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
?>
