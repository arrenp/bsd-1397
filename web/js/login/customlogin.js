/*!
 * Form Login JS Animations & Toggle
 */

$(document).ready(function(){

    /*! Notification Animation Reset */
    $('.form-control').keypress(function(){
        $('.log-status').removeClass('wrong-entry');
        $('.log-status').removeClass('right-entry');
    });

    /*! Toggle Function to Reset Password */
    $('.message a').click(function(){
        $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
    });

});