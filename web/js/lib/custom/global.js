function getAjaxVariable()//placeholder for manage until manage finished, replace with jquery post
{
    var ajaxRequest;
    try
    {
         // Opera 8.0+, Firefox, Safari
        ajaxRequest = new XMLHttpRequest();
    } 
    catch (e)
    {
        // Internet Explorer Browsers
        try
        {
         ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
        } 
        catch (e)
        {
            try
            {
                ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } 
            catch (e)
            {
                // Something went wrong
                //alert("Your browser broke!");
                return false;
            }
        }
    }

    if (ajaxRequest == false)
    {
        alert('Your browser does not support ajax')
        return false;
    }

    return ajaxRequest;
}
function postAjax(ajaxRequest,page,querystring)//placeholder for manage until manage finished, replace with jquery post
{
    ajaxRequest.open("POST", page , true);
    ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajaxRequest.setRequestHeader("Content-length", querystring.length);
    ajaxRequest.setRequestHeader("Connection", "close");
    ajaxRequest.send(querystring);
}

function customParseFloat(id,signed)
{
  if(typeof(signed)==='undefined')
  signed = "signed";
  $("#" +id).val(parseFloat($("#" +id).val()));
  if ($("#" +id).val() == "NaN" ||  ($("#" +id).val() < 0 && signed == "unsigned"))
  $("#" +id).val(0);
}

function customParseInt(id,signed)
{
  if(typeof(signed)==='undefined')
  signed = "signed";
  $("#" +id).val(parseInt($("#" +id).val()));
  if ($("#" +id).val() == "NaN" ||  ($("#" +id).val() < 0 && signed == "unsigned"))
  $("#" +id).val(0);
}

function assignCheckboxValue(object)
{
    if (object.checked)
    object.value = 1;
    else
    object.value = 0;
}

function togglePills(elements,selectedElement)
{
    for (var i = 0; i < elements.length; i++)
    {
        document.getElementById(elements[i]).style.display = 'none';
        document.getElementById(elements[i] + "Pill").className = '';
    }
    document.getElementById(selectedElement).style.display = 'block';
    document.getElementById(selectedElement + "Pill").className = 'active';
    $.placeholder.shim();
}

function loadLightBoxVariables()
{
  $(".videopreview").colorbox({iframe:true, width:"900px", height:"530px"});
  $(".ajaxcontent").colorbox({width:"auto", height:"auto"});
  $(".ajaxcontentnoscroll").colorbox({width:"auto", height:"auto",scrolling:false});
  $(".iframeLightbox").colorbox({iframe:true,width:"600px",height:"370px",scrolling: false});
  $(".ajaxbox").colorbox({width:"auto", height:"auto"});
}

function closeLightBox()
{
  $.colorbox.close();
  loadLightBoxVariables();

  $("#ajaxModule").modal("hide");
}

function simpleSearch(searchVariable,id)
{
 var searchedValue = $("#" + id + "Search").val();
 for (var i = 0; i < searchVariable.length;i++)
 {
  var searchFields =  searchVariable[i];
  var valid = false;
  for (var key in searchFields)
  {
    if (key != "id")
    if (searchFields[key].toLowerCase().lastIndexOf(searchedValue.toLowerCase()) != -1 || searchedValue == "")
    valid = true;       
  }
  if (valid)
  $("#" + id + searchFields['id']).show();
  else
  $("#" + id + searchFields['id']).hide();
 }
 if (window.resizeColorBox)
 resizeColorBox();
}



